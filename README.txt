This is CED, an old university project which I hope one day I will have the time to bring back to life.

It is an extensible command driven text editor primarily for programmers.

Feature highlights:
* Configurable syntax highlighting, allowing you to easily define a language.
* Background swap file to prevent loss of work and avoid memory constraints.
* Fast large file editing due to B-Tree internal storage, mapped to the swap file.
* Command driven, with keybinding to any sequence of commands.
* Command macros.
* Rectangular cut and paste.
* Multiple edit sessions of the same file.
* Named copy/paste buffers.
* Transformation of named copy/paste buffers via shell calls.
* Coordinated mouse and text cursor control (great once you get used to it)
* Unlimited UNDO and REDO
* Customised keyboard mappings

I have not built it for nearly 20 years so I cannot guarantee it will build in a more modern environment.

Originally it was divided into 2 projects: the library (engine), and the user interface.

This allowed other user interfaces (including command line) could utilise the library, so for instance command sequences could be run against input files without having to open an editor window, similar to sed and awk.


