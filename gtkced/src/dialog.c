/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Last Updated
 *    17th August 2001
 *
 */

#include <stdio.h>
#include <gtk/gtk.h>
#include <ced/buffer.h>
#include <ced/sys.h>
#include <ced/utils.h>
#include "dialog.h"
#include "main.h"
#include "gtkced.h"

static void dialog_open_rdonly_callback(GtkWidget *widget, buffer *buff);
static void dialog_open_noswap_callback(GtkWidget *widget, buffer *buff);
static void dialog_open_recover_callback(GtkWidget *widget, buffer *buff);
static void dialog_open_discard_callback(GtkWidget *widget, buffer *buff);
static void dialog_open_ignore_callback(GtkWidget *widget, buffer *buff);
static void dialog_open_cancel_callback(GtkWidget *widget, buffer *buff);
static void dialog_close(GtkWidget *widget);
static GtkWidget *dialog_get_swapinfo_widget(buffer *buff);

////////////////////////////////////////////////////////////////////////////

static GtkWidget *dialog_get_swapinfo_widget(buffer *buff)
{
  GtkWidget *label_1;
  GtkWidget *label_2;
  GtkWidget *align;
  GtkWidget *hbox;
  char      buf[256];
  mblock    *mb;

  mb = (mblock*)buff->mstblk->data;

  sprintf(buf, "%s\n%s\n%s\n",
                  mb->fname,
                  mb->uname,
                  mb->hname);

  label_1 = gtk_label_new("Filename:\nModified by user:\nOn host:\n");
  label_2 = gtk_label_new(buf);

  gtk_label_set_justify(GTK_LABEL(label_1), GTK_JUSTIFY_RIGHT);
  gtk_label_set_justify(GTK_LABEL(label_2), GTK_JUSTIFY_LEFT);

  hbox = gtk_hbox_new(FALSE, 20);

  gtk_box_pack_start(GTK_BOX(hbox), label_1, FALSE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(hbox), label_2, FALSE, TRUE, 0);

  align = gtk_alignment_new(0.5, 0.5, 0, 0);

  gtk_container_add(GTK_CONTAINER(align), hbox);

  return(align);
}

void dialog_recover(buffer *buff)
{
  GtkWidget *dialog; 
  GtkWidget *button_recover;
  GtkWidget *button_discard;
  GtkWidget *button_ignore;
  GtkWidget *button_cancel;
  GtkWidget *swapinfo;
  GtkWidget *label;
  const char info[] =
    "\nA swap file already exists for this file but is not\n"
    "locked.  This usually means that ced was killed\n"
    "while editing this filein a previous session.\n";

  dialog = gtk_dialog_new();

  label    = gtk_label_new(info);
  swapinfo = dialog_get_swapinfo_widget(buff);

  gtk_widget_set_usize(dialog, 340, 200);
  gtk_window_set_title(GTK_WINDOW(dialog), "ced: swap file exists");

  button_recover = gtk_button_new_with_label("Recover");
  button_discard = gtk_button_new_with_label("Discard");
  button_ignore  = gtk_button_new_with_label("Next Swap");
  button_cancel  = gtk_button_new_with_label("Cancel");

  gtk_signal_connect(GTK_OBJECT(button_recover), "clicked",
                    (GtkSignalFunc)dialog_open_recover_callback, buff);

  gtk_signal_connect(GTK_OBJECT(button_discard), "clicked",
                    (GtkSignalFunc)dialog_open_discard_callback, buff);

  gtk_signal_connect(GTK_OBJECT(button_ignore), "clicked",
                    (GtkSignalFunc)dialog_open_ignore_callback, buff);

  gtk_signal_connect(GTK_OBJECT(button_cancel), "clicked",
                    (GtkSignalFunc)dialog_open_cancel_callback, buff);

  gtk_signal_connect(GTK_OBJECT(dialog), "delete_event",
                    (GtkSignalFunc)dialog_open_cancel_callback, buff);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
                        button_recover, TRUE, TRUE, 0);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
                        button_discard, TRUE, TRUE, 0);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
                        button_ignore, TRUE, TRUE, 0);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
                        button_cancel, TRUE, TRUE, 0);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
                        label, TRUE, TRUE, 0);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
                        swapinfo, TRUE, TRUE, 0);

  gtk_widget_show_all(dialog);

  OPEN_DIALOGS++;
}

void dialog_swap_noaccess(buffer *buff)
{
  GtkWidget *dialog; 
  GtkWidget *button_no_swap;
  GtkWidget *button_read_only;
  GtkWidget *button_ignore;
  GtkWidget *button_cancel;
  GtkWidget *label;
  const char info[] =
    "\nA swap file already exists for this file and you do not\n"
    "have the appropriate permissions to access it.\n";

  dialog = gtk_dialog_new();
  label = gtk_label_new(info);

  gtk_window_set_title(GTK_WINDOW(dialog), "ced: cannot access swap file");

  button_no_swap   = gtk_button_new_with_label("Don't use swap");
  button_read_only = gtk_button_new_with_label("Open Read Only");
  button_ignore    = gtk_button_new_with_label("Next Swap");
  button_cancel    = gtk_button_new_with_label("Cancel");

  gtk_signal_connect(GTK_OBJECT(button_no_swap), "clicked",
                    (GtkSignalFunc)dialog_open_noswap_callback, buff);

  gtk_signal_connect(GTK_OBJECT(button_read_only), "clicked",
                    (GtkSignalFunc)dialog_open_rdonly_callback, buff);

  gtk_signal_connect(GTK_OBJECT(button_ignore), "clicked",
                    (GtkSignalFunc)dialog_open_ignore_callback, buff);

  gtk_signal_connect(GTK_OBJECT(button_cancel), "clicked",
                    (GtkSignalFunc)dialog_open_cancel_callback, buff);

  gtk_signal_connect(GTK_OBJECT(dialog), "delete_event",
                    (GtkSignalFunc)dialog_open_cancel_callback, buff);

  gtk_box_pack_start(GTK_BOX (GTK_DIALOG (dialog)->action_area),
                        button_no_swap, TRUE, TRUE, 0);

  gtk_box_pack_start(GTK_BOX (GTK_DIALOG (dialog)->action_area),
                        button_read_only, TRUE, TRUE, 0);

  gtk_box_pack_start(GTK_BOX (GTK_DIALOG (dialog)->action_area),
                        button_ignore, TRUE, TRUE, 0);

  gtk_box_pack_start(GTK_BOX (GTK_DIALOG (dialog)->action_area),
                        button_cancel, TRUE, TRUE, 0);

  gtk_box_pack_start(GTK_BOX (GTK_DIALOG (dialog)->vbox),
                        label, TRUE, TRUE, 0);

  gtk_widget_show_all(dialog);

  OPEN_DIALOGS++;
}


void dialog_locked(buffer *buff)
{
  GtkWidget *dialog;
  GtkWidget *button_rdonly;
  GtkWidget *button_cancel;
  GtkWidget *swapinfo;
  GtkWidget *label;
  const char info[] =
    "\nThe swap file for the specified file is already\n"
    "locked by another ced session.\n";

  dialog = gtk_dialog_new();

  gtk_widget_set_usize(dialog, 350, 170);
  gtk_window_set_title(GTK_WINDOW(dialog), "ced: swap file locked");

  label = gtk_label_new(info);

  swapinfo = dialog_get_swapinfo_widget(buff);

  button_rdonly = gtk_button_new_with_label("Open Read Only");
  button_cancel  = gtk_button_new_with_label("Cancel");

  gtk_signal_connect(GTK_OBJECT(button_rdonly), "clicked",
                    (GtkSignalFunc)dialog_open_rdonly_callback, buff);

  gtk_signal_connect(GTK_OBJECT(button_cancel), "clicked",
                    (GtkSignalFunc)dialog_open_cancel_callback, buff);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
                        button_rdonly, TRUE, TRUE, 0);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
                        button_cancel, TRUE, TRUE, 0);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
                        label, TRUE, TRUE, 0);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
                        swapinfo, TRUE, TRUE, 0);


  gtk_widget_show_all(dialog);

  OPEN_DIALOGS++;
}

void dialog_fileinfo(buffer *buff)
{
  GtkWidget *dialog;
  GtkWidget *button_ok;
  GtkWidget *text;
  GtkWidget *frame;
  char      buf[512];
  event     *ev;

  dialog = gtk_dialog_new();

  gtk_window_set_title(GTK_WINDOW(dialog), "ced: file information");

  button_ok = gtk_button_new_with_label("Ok !");

  text = gtk_text_new(NULL, NULL);

  frame = gtk_frame_new(NULL);

  gtk_container_add((GtkContainer*)frame, text);

  ev = buff->undo_list->current;

  sprintf(buf, "Current File Information:\n\n");
  gtk_text_insert((GtkText*)text, NULL, NULL, NULL, buf, strlen(buf));
  sprintf(buf, "Filename: %s\n", buff->ffname);
  gtk_text_insert((GtkText*)text, NULL, NULL, NULL, buf, strlen(buf));
  sprintf(buf, "Swapname: %s\n", buff->swname);
  gtk_text_insert((GtkText*)text, NULL, NULL, NULL, buf, strlen(buf));
  sprintf(buf, "Lines: %lu\n", buff->nlines);
  gtk_text_insert((GtkText*)text, NULL, NULL, NULL, buf, strlen(buf));

  gtk_widget_set_usize(dialog, 350, 150);

  gtk_signal_connect(GTK_OBJECT(button_ok), "clicked",
                    (GtkSignalFunc)dialog_close, NULL);

  gtk_box_pack_start(GTK_BOX (GTK_DIALOG (dialog)->action_area),
                     button_ok, TRUE, TRUE, 0);
 
  gtk_box_pack_start(GTK_BOX (GTK_DIALOG (dialog)->vbox),
                     frame, TRUE, TRUE, 0);

  gtk_widget_show_all(dialog);

  OPEN_DIALOGS++;
}

void dialog_about()
{
  GtkWidget *dialog;
  GtkWidget *button_ok;
  GtkWidget *text;
  GtkWidget *frame;
  uint      len;
  char      about[1024];

  sprintf(about,
    "CED - The Editor\n"
    "LibCED Library Version: %s\n"
    "GtkCED Frontend Version: %s\n"
    "\n"
    "By Mark Simonetti\n"
    "nutty@users.sourceforge.net\n\n"
    "Copyright (C) 2000 Mark Simonetti\n\n"
    "Originally developed at DeMontfort University\n"
    "in Leicester, England (UK) for the\n"
    "B.Sc. (Hons) Software Engineering course\n"
    "studied between 1997 and 2001.\n"
    "\n"
    "http://ced.sourceforge.net/\n",
    get_libced_version(),
    gtkced_get_version());

    len = strlen(about);
    dialog = gtk_dialog_new();

    gtk_window_set_title(GTK_WINDOW(dialog), "ced: about");

    button_ok = gtk_button_new_with_label("Ok !");
    text = gtk_text_new(NULL, NULL);
    frame = gtk_frame_new(NULL);

    gtk_container_add((GtkContainer*)frame, text);
    gtk_text_insert((GtkText*)text, NULL, NULL, NULL, about, len);
    gtk_widget_set_usize(dialog, 340, 250);

    gtk_signal_connect(GTK_OBJECT(button_ok), "clicked",
                      (GtkSignalFunc)dialog_close, NULL);

    gtk_box_pack_start(GTK_BOX (GTK_DIALOG (dialog)->action_area),
                       button_ok, TRUE, TRUE, 0);
 
    gtk_box_pack_start(GTK_BOX (GTK_DIALOG (dialog)->vbox),
                       frame, TRUE, TRUE, 0);

    gtk_widget_show_all(dialog);

    OPEN_DIALOGS++;
}

void dialog_not_readable(buffer *buff)
{
  GtkWidget *dialog;
  GtkWidget *button_cancel;
  GtkWidget *label_msg;

  dialog = gtk_dialog_new();

  gtk_widget_set_usize(dialog, 250, 100);
  gtk_window_set_title(GTK_WINDOW(dialog), "ced: invalid file");

  label_msg = gtk_label_new("  File is not readable.  ");
  button_cancel  = gtk_button_new_with_label("Okay !");

  gtk_signal_connect(GTK_OBJECT(button_cancel), "clicked",
                    (GtkSignalFunc)dialog_open_cancel_callback, buff);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
                        label_msg, TRUE, TRUE, 15);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
                        button_cancel, TRUE, TRUE, 0);

  gtk_widget_show_all(dialog);

  OPEN_DIALOGS++;
}

void dialog_not_regular(buffer *buff)
{
  GtkWidget *dialog;
  GtkWidget *button_cancel;
  GtkWidget *label_msg;

  dialog = gtk_dialog_new();

  gtk_widget_set_usize(dialog, 250, 100);
  gtk_window_set_title(GTK_WINDOW(dialog), "ced: invalid file");

  label_msg = gtk_label_new("  This is not a regular file !  ");
  button_cancel  = gtk_button_new_with_label("Okay !");

  gtk_signal_connect(GTK_OBJECT(button_cancel), "clicked",
                    (GtkSignalFunc)dialog_open_cancel_callback, buff);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
                        label_msg, TRUE, TRUE, 15);

  gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->action_area),
                        button_cancel, TRUE, TRUE, 0);

  gtk_widget_show_all(dialog);

  OPEN_DIALOGS++;
}

static void dialog_open_rdonly_callback(GtkWidget *widget, buffer *buff)
{
    dialog_close(widget);
    main_open(buff, OF_OPEN | OF_READ_ONLY);
}

static void dialog_open_noswap_callback(GtkWidget *widget, buffer *buff)
{
    dialog_close(widget);
    main_open(buff, OF_OPEN);
}

static void dialog_open_recover_callback(GtkWidget *widget, buffer *buff)
{
    dialog_close(widget);
    main_open(buff, OF_OPEN | OF_USE_SWAP | OF_RECOVER);
}

static void dialog_open_discard_callback(GtkWidget *widget, buffer *buff)
{
    dialog_close(widget);
    main_open(buff, OF_OPEN | OF_USE_SWAP | OF_DISCARD_SWAP);
}

static void dialog_open_ignore_callback(GtkWidget *widget, buffer *buff)
{
    dialog_close(widget);
    main_open(buff, OF_OPEN | OF_USE_SWAP | OF_USE_NAS);
}

static void dialog_open_cancel_callback(GtkWidget *widget, buffer *buff)
{
    dialog_close(widget);

    if(main_windows_count() > 0 || OPEN_DIALOGS > 0) {
        sys_del_buffer(buff);
        buff_destroy(buff);
    } else {
        main_quit();
    }
}

static void dialog_close(GtkWidget *widget)
{
    GtkWidget *wtop;
    wtop = gtk_widget_get_toplevel(widget);
    gtk_widget_destroy(wtop);

    OPEN_DIALOGS--;
}

