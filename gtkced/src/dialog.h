#ifndef _DIALOG_H
#define _DIALOG_H

#include <ced/buffer.h>

void dialog_swap_noaccess (buffer *buff);
void dialog_recover       (buffer *buff);
void dialog_not_readable  (buffer *buff);
void dialog_not_regular   (buffer *buff);
void dialog_locked        (buffer *buff);
void dialog_fileinfo      (buffer *buff);
void dialog_about         (void);

#endif
