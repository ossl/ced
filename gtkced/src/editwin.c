/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *
 *  Last Updated
 *    12th November 2001
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdk.h>

#include <ced/common.h>
#include <ced/canvas.h>
#include <ced/buffer.h>
#include <ced/session.h>
#include <ced/utils.h>
#include <ced/sys.h>

#include "dialog.h"
#include "lnums.h"
#include "editwin.h"
#include "viewport.h"
#include "main.h"
#include "gtkced.h"

#define WINDOW_MENU_OFFSET 2

static gint editwin_close(GtkWidget *widget, GdkEvent *event, edwin *ew);
static gint editwin_focus_in(GtkWidget *widget, GdkEventFocus *event);
static gint editwin_focus_out(GtkWidget *widget, GdkEventFocus *event);
static void editwin_switch_to(edwin *ew, SESSION_CONTEXT ct);
static void editwin_switch_to_active(edwin *ew);
static void editwin_update_file_attribs(edwin *ew);
static void editwin_update_coords(edwin *ew);
static void editwin_update_prompt(edwin *ew);
static void editwin_update_filename(edwin *ew);
static void editwin_open_callback(edwin *ew, char *fname, int *retval);
static void editwin_close_callback(edwin *ew);
static void editwin_carbon_callback(edwin *ew);
static void editwin_message_callback(edwin *ew, char *message, int beep);
static void editwin_exec_string(edwin *ew, char *str);
static void editwin_menu_misc(edwin *ew, guint action, GtkWidget *widget);
static void editwin_lnums_update_callback(edwin *ew);
static void editwin_toggle_line_numbers(edwin *ew);
static void editwin_toggle_highlighting(edwin *ew);
static void editwin_toggle_mouse_binding(edwin *ew);
static void editwin_file_sel(edwin *ew, guint action, GtkWidget *widget);
static void editwin_fsel_save_as(GtkWidget *widget, edwin *ew);
static void editwin_fsel_closed_callback(edwin *ew);
static GtkItemFactory *editwin_get_menu(edwin *ew, GtkWidget *window, GtkWidget **menubar);
static gint editwin_motion_notify_event(GtkWidget *widget, GdkEventMotion *event);

static void editwin_menu_style(GtkMenuItem *item, gpointer str);

enum _menu_action {
  MA_DUMMY,

  /* The file menu */

  MA_NEW,
  MA_OPEN,
  MA_CLOSE,
  MA_SAVE,
  MA_SAVE_AS,
  MA_INFO,
  MA_PRINT,

  /* The edit menu */

  MA_UNDO,
  MA_REDO,
  MA_LINEAR_DEF,
  MA_LINEAR_ECHO,
  MA_LINEAR_CUT,
  MA_LINEAR_COPY,
  MA_LINEAR_PASTE,
  MA_RECT_DEF,
  MA_RECT_ECHO,
  MA_RECT_CUT,
  MA_RECT_COPY,
  MA_RECT_PASTE,

  /* The preferences menu */

  MA_SYNTAX,
  MA_MOUSE,
  MA_LNUMS,
  MA_EDIT_KEYS,
  MA_EDIT_LANG,
  MA_EDIT_STYLE,
  MA_LOC_KEYS,
  MA_LOC_SYNTAX,
  MA_LOC_GTKCED,

  /* The window menu */

  MA_CCOPY
};

static GtkItemFactoryEntry menu_items[] = {

  /* File Menu */

  { "/_File",                                     NULL, NULL,              MA_DUMMY,      "<Branch>"      },
  { "/File/New",                                  NULL, editwin_menu_misc, MA_NEW,        NULL            },
  { "/File/Open..",                               NULL, editwin_file_sel,  MA_OPEN,       NULL            },
  { "/File/sep1",                                 NULL, NULL,              MA_DUMMY,      "<Separator>"   },
  { "/File/Save",                                 NULL, editwin_menu_misc, MA_SAVE,       NULL            },
  { "/File/Save as..",                            NULL, editwin_file_sel,  MA_SAVE_AS,    NULL            },
  { "/File/Close",                                NULL, editwin_menu_misc, MA_CLOSE,      NULL            },
  { "/File/sep1",                                 NULL, NULL,              MA_DUMMY,      "<Separator>"   },
  { "/File/Information",                          NULL, editwin_menu_misc, MA_INFO,       NULL            },

  /* Preferences Menu */

  { "/_Preferences",                              NULL, NULL,              MA_DUMMY,      "<Branch>"      },
  { "/Preferences/Syntax highlighting",           NULL, editwin_menu_misc, MA_SYNTAX,     "<ToggleItem>"  },
  { "/Preferences/Line numbers",                  NULL, editwin_menu_misc, MA_LNUMS,      "<ToggleItem>"  },
  { "/Preferences/Mouse binding",                 NULL, editwin_menu_misc, MA_MOUSE,      "<ToggleItem>"  },
  { "/Preferences/sep1",                          NULL, NULL,              MA_DUMMY,      "<Separator>"   },
  { "/Preferences/Localise default key bindings", NULL, editwin_menu_misc, MA_LOC_KEYS,   NULL            },
  { "/Preferences/Localise default syntax file",  NULL, editwin_menu_misc, MA_LOC_SYNTAX, NULL            },
  { "/Preferences/Localise default style file",   NULL, editwin_menu_misc, MA_LOC_GTKCED, NULL            },
  { "/Preferences/sep1",                          NULL, NULL,              MA_DUMMY,      "<Separator>"   },
  { "/Preferences/Edit local key bindings",       NULL, editwin_menu_misc, MA_EDIT_KEYS,  NULL            },
  { "/Preferences/Edit local syntax file",        NULL, editwin_menu_misc, MA_EDIT_LANG,  NULL            },
  { "/Preferences/Edit local gtkced style file",  NULL, editwin_menu_misc, MA_EDIT_STYLE, NULL            },

  /* Styles Menu */

  { "/_Style",                                    NULL, NULL,              MA_DUMMY,      "<Branch>"      },
  { "/Style/Reload current",                      NULL, editwin_menu_misc, MA_DUMMY,      NULL            },
  { "/Style/sep1",                                NULL, NULL,              MA_DUMMY,      "<Separator>"   },
  { "/Style/Defaults/",                           NULL, editwin_menu_misc, MA_DUMMY,      NULL            },
  { "/Style/Personal/",                           NULL, editwin_menu_misc, MA_DUMMY,      NULL            },
  { "/Style/sep1",                                NULL, NULL,              MA_DUMMY,      "<Separator>"   },
  { "/Style/Refresh available styles list",       NULL, editwin_menu_misc, MA_DUMMY,      NULL            },
  { "/Style/Set current style as default",        NULL, editwin_menu_misc, MA_DUMMY,      NULL            },
  { "/Style/Create a new style",                  NULL, editwin_menu_misc, MA_DUMMY,      NULL            },

  /* Window Menu */

  { "/_Window",                                   NULL, NULL,              MA_DUMMY,      "<Branch>"      },
  { "/Window/Carbon copy window",                 NULL, editwin_menu_misc, MA_CCOPY,      NULL            },
  { "/Window/sep1",                               NULL, NULL,              MA_DUMMY,      "<Separator>"   },

  /* Help Menu */

  { "/_Help",                                     NULL, NULL,              MA_DUMMY,      "<LastBranch>"  },
  { "/Help/About",                                NULL, dialog_about,      MA_DUMMY,      NULL            },
};

static void editwin_menu_style(GtkMenuItem *item, gpointer str)
{
    main_set_style((char*)str);
}

static void editwin_menu_misc(edwin *ew, guint action, GtkWidget *widget)
{
    GList      *toplevels;
    GtkWidget  *w;

//    printf("%p\n", ew);

    if(!ew->initialised || ew->manual_toggle) {
        return;
    }

    switch(action) {
        case MA_CLOSE        : editwin_exec_string(ew, "wc"); break;
        case MA_SAVE         : editwin_exec_string(ew, "pw"); break;
        case MA_RECT_DEF     : editwin_exec_string(ew, "dr; echo -r"); break;
        case MA_RECT_ECHO    : editwin_exec_string(ew, "echo -r"); break;
        case MA_RECT_CUT     : editwin_exec_string(ew, "xd -r"); break;
        case MA_RECT_COPY    : editwin_exec_string(ew, "xc -r"); break;
        case MA_RECT_PASTE   : editwin_exec_string(ew, "xp -r"); break;
        case MA_LINEAR_DEF   : editwin_exec_string(ew, "dr; echo"); break;
        case MA_LINEAR_ECHO  : editwin_exec_string(ew, "echo"); break;
        case MA_LINEAR_CUT   : editwin_exec_string(ew, "xd"); break;
        case MA_LINEAR_COPY  : editwin_exec_string(ew, "xc"); break;
        case MA_LINEAR_PASTE : editwin_exec_string(ew, "xp"); break;
        case MA_UNDO         : editwin_exec_string(ew, "undo"); break;
        case MA_REDO         : editwin_exec_string(ew, "redo"); break;
        case MA_CCOPY        : editwin_exec_string(ew, "cc"); break;
        case MA_LNUMS        : editwin_exec_string(ew, "lineno"); break;
        case MA_EDIT_KEYS    : editwin_exec_string(ew, "ced ~/.ced/key.def"); break;
        case MA_EDIT_LANG    : editwin_exec_string(ew, "ced ~/.ced/syntax.conf"); break;
        case MA_EDIT_STYLE   : editwin_exec_string(ew, "ced ~/.ced/gtkced.conf"); break;
        case MA_NEW          : editwin_exec_string(ew, "ced"); break;
        case MA_SYNTAX       : editwin_exec_string(ew, "syntax"); break;
        case MA_MOUSE        : editwin_exec_string(ew, "mouse"); break;
        case MA_INFO         : dialog_fileinfo(ew->canv->buff); break;

        /* frontend functions */

        case MA_LOC_KEYS     :  main_load_style("Styles/default.style"); 
                                main_realize_defaults();

                                toplevels = gdk_window_get_toplevels();

//                                gtk_widget_reset_rc_styles(ew->window);

/********************************************************/
                                while(toplevels) {
                                    gdk_window_get_user_data(toplevels->data, (gpointer*)&w);
                                 
                                    if(w) {
                                        printf("%s\n", gtk_type_name(GTK_OBJECT(w)->klass->type));
                                        gtk_widget_reset_rc_styles(w);
                                    }

                                    toplevels = toplevels->next;
                                }

                                g_list_free(toplevels);
/**********************************************************/

                                editwin_update_syntax_styles(ew);

                                break;

//        case MA_LOC_KEYS     : main_localise_default_keydef(); break;
        case MA_LOC_GTKCED   : main_localise_default_gtkced(); break;
        case MA_LOC_SYNTAX   : main_localise_default_syntax(); break;

        default:
    }
}

void editwin_bring_to_front(edwin *ew)
{
    gdk_window_show(ew->window->window);
}

static void editwin_fsel_closed_callback(edwin *ew)
{
    GtkWidget *filesel;

    if((filesel = ew->fsel_saveas) != NULL) {
        gtk_widget_destroy(filesel);
        ew->fsel_saveas = NULL;
    }
}

static void editwin_fsel_save_as(GtkWidget *widget, edwin *ew)
{
    GtkWidget *fsel;
    char      *ffname;
    char      buf[512];

    fsel = gtk_widget_get_toplevel(widget);
    ffname = gtk_file_selection_get_filename(GTK_FILE_SELECTION(fsel));
    sprintf(buf, "pn -c -r %s; pw", ffname);

    editwin_exec_string(ew, buf);
    editwin_fsel_closed_callback(ew);
}

static void editwin_file_sel(edwin *ew, guint action, GtkWidget *widget)
{
    GtkFileSelection *filesel;
    GtkWidget *filesel_w;
    gpointer  func;
    gpointer  data;
    char      buf[1024];
    char      *title;

    func = NULL;
    data = NULL;

    switch(action) {
        case MA_SAVE_AS:

            if(ew->fsel_saveas != NULL) {
                return;
            }

            title = "Save As..";
            func  = editwin_fsel_save_as;
            data  = ew;

            break;

        case MA_OPEN:

            title = "Open..";
            func  = main_open_fsel_callback;
            data  = NULL;

            break;

        default :

            fprintf(stderr, "editwin_file_sel: invalid mode\r\n");

            return;
    }

    filesel_w = gtk_file_selection_new(title);
    filesel   = GTK_FILE_SELECTION(filesel_w);

    if(action == MA_SAVE_AS) {

        gtk_signal_connect_object(
            GTK_OBJECT(filesel->cancel_button), "clicked",
            GTK_SIGNAL_FUNC(editwin_fsel_closed_callback), (gpointer)ew);

        gtk_signal_connect_object(
            GTK_OBJECT(filesel), "delete_event",
            GTK_SIGNAL_FUNC(editwin_fsel_closed_callback), (gpointer)ew);

        ew->fsel_saveas = filesel_w;

        gtk_file_selection_set_filename(filesel, ew->canv->buff->ffname);

    } else {

        /* The CANCEL button in OPEN mode */

        gtk_signal_connect_object(
            GTK_OBJECT(filesel->cancel_button), "clicked",
            GTK_SIGNAL_FUNC(gtk_widget_destroy), (gpointer)filesel);

        if(get_path(buf, ew->canv->buff->ffname, 1024)) {
            gtk_file_selection_set_filename(filesel, buf);
        }
    }

    /* Handle the OK button */

    gtk_signal_connect(
        GTK_OBJECT(filesel->ok_button), "clicked",
        GTK_SIGNAL_FUNC(func), data);

    gtk_widget_show(filesel_w);
}

static void editwin_set_toggles(edwin *ew)
{
    GtkWidget *widget;
    GtkVp     *vp;
    buffer    *buff;
    canvas    *canv;

    vp   = (GtkVp*)ew->vp_edit;
    canv = ew->canv;
    buff = canv->buff;

    ew->manual_toggle = 1;

    widget = gtk_item_factory_get_widget(ew->ifactory, "/Preferences/Syntax highlighting");
    gtk_check_menu_item_set_active((GtkCheckMenuItem*)widget, (canv->highlighting) ? TRUE : FALSE);

    widget = gtk_item_factory_get_widget(ew->ifactory, "/Preferences/Mouse binding");
    gtk_check_menu_item_set_active((GtkCheckMenuItem*)widget, (canv->mouse_binding) ? TRUE : FALSE);

    widget = gtk_item_factory_get_widget(ew->ifactory, "/Preferences/Line numbers");
    gtk_check_menu_item_set_active((GtkCheckMenuItem*)widget, (canv->show_lnums) ? TRUE : FALSE);

    ew->manual_toggle = 0;
}

void editwin_window_menu_del(edwin *ew, uint i)
{
    GtkWidget *menu;
    GtkWidget *item;
    GList     *list;
    uint      pos;

    if(!ew) return;

    pos  = i + WINDOW_MENU_OFFSET;
    menu = gtk_item_factory_get_widget(ew->ifactory, "/Window");
    list = gtk_container_children((GtkContainer*)menu);
    item = (GtkWidget*)g_list_nth_data(list, pos);

    gtk_container_remove((GtkContainer*)menu, item);
}

void editwin_window_menu_add(edwin *ew, edwin *ew_ptr)
{
    GtkWidget *menu;
    GtkWidget *item;

    if(!ew) return;

    menu = gtk_item_factory_get_widget(ew->ifactory, "/Window");
    item = gtk_menu_item_new_with_label(ew_ptr->canv->buff->sfname);

    gtk_signal_connect_object(GTK_OBJECT(item), "activate",
                              GTK_SIGNAL_FUNC(editwin_bring_to_front),
                              (gpointer)ew_ptr);

    gtk_menu_append((GtkMenu*)menu, item);
    gtk_widget_show(item);
}

static void editwin_update_styles_menu(edwin *ew)
{
    GtkWidget *menu;
    GtkWidget *item;
    struct _ptr_array *styles;
    int i;

    styles = main_get_styles();

    menu = gtk_item_factory_get_widget(ew->ifactory, "/Style/Defaults");

    for(i=0; i < styles->items; i++) {
        item = gtk_menu_item_new_with_label(styles->data[i]);

        gtk_signal_connect(GTK_OBJECT(item), "activate",
                           GTK_SIGNAL_FUNC(editwin_menu_style),
                           (gpointer)styles->data[i]);

        gtk_menu_append((GtkMenu*)menu, item);
        gtk_widget_show(item);
    }
}

static void editwin_update_menus_callback(edwin *ew)
{
    main_update_window_menus(ew);
}

edwin *editwin_new(buffer *buff)
{
    char      buf[1024];
    edwin     *ew;
    buffer    *buff_tmp;
    canvas    *canv_cmd;
    canvas    *canv_msg;
    canvas    *canv_edit;
    session   *se;
    GtkWidget *vbox;
    GtkWidget *hbox;
    GtkWidget *hbox2;
    GtkWidget *align;
    GtkWidget *fixed;
    GtkWidget *menubar;
    GdkWindow *w1;
    GdkColor  col_bg;
    GdkColor  col_fg;
    int       win_width;
    int       win_height;
    int       i;

    /* create the command viewport */

    buff_tmp = buff_new("___cmd___");
    buff_open(buff_tmp, OF_OPEN);
    canv_cmd = canv_new(buff_tmp);

    /* create the message viewport */

    buff_tmp  = buff_new("___msg___");
    buff_open(buff_tmp, OF_OPEN);
    canv_msg  = canv_new(buff_tmp);
    canv_set_readonly(canv_msg, 1);

    /* create the main edit window viewport */

    canv_edit = canv_new(buff);

    /* create the session structure and assign callbacks */

    se = session_new(canv_edit, canv_cmd, canv_msg);
    session_set_callback(se, "open_file"          , editwin_open_callback);
    session_set_callback(se, "prompt_changed"     , editwin_update_prompt);
    session_set_callback(se, "close_window"       , editwin_close_callback);
    session_set_callback(se, "context_changed"    , editwin_switch_to_active);
    session_set_callback(se, "create_carbon_copy" , editwin_carbon_callback);

    /* create the edit-window structure */

    ew = (edwin*)calloc(1, sizeof(edwin));
    ew->manual_toggle = 0;
    ew->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    ew->ifactory = editwin_get_menu(ew, ew->window, &menubar);
    ew->prompt = gtk_label_new("");
    ew->info_ffname = gtk_label_new("");
    ew->info_coords = gtk_label_new("");
    ew->info_attr = gtk_label_new("");
    ew->vp_edit = gtk_vp_new(canv_edit, se);
    ew->vp_msg = gtk_vp_new(canv_msg, se);
    ew->vp_cmd = gtk_vp_new(canv_cmd, se);
    ew->lnums = gtk_lnums_new(GTK_VP(ew->vp_edit), DEFAULTS.lnums_border);
    ew->canv = canv_edit;
    ew->se = se;

    gtk_widget_set_name(ew->vp_edit , "EditVp");
    gtk_widget_set_name(ew->vp_msg  , "MsgVp");
    gtk_widget_set_name(ew->vp_cmd  , "CmdVp");
    gtk_widget_set_name(ew->lnums   , "LineNumbers");
    gtk_widget_set_name(ew->window  , "EditWindow");
    gtk_widget_set_name(ew->prompt  , "Prompt");
    gtk_menu_bar_set_shadow_type((GtkMenuBar*)menubar, GTK_SHADOW_ETCHED_IN);

    /* set this object to be the parent of some other objects */

    se->struct_ptr = (void*)ew;

    gtk_vp_set_parent(ew->vp_edit, (void*)ew);
    gtk_vp_set_parent(ew->vp_cmd, (void*)ew);
    gtk_vp_set_parent(ew->vp_msg, (void*)ew);

    /* set the callbacks on all the viewports */

    gtk_vp_set_callback(ew->vp_msg  , "message"               , editwin_message_callback);
    gtk_vp_set_callback(ew->vp_cmd  , "message"               , editwin_message_callback);
    gtk_vp_set_callback(ew->vp_edit , "message"               , editwin_message_callback);
    gtk_vp_set_callback(ew->vp_edit , "update_line_numbers"   , editwin_lnums_update_callback);
    gtk_vp_set_callback(ew->vp_edit , "line_numbers_toggled"  , editwin_toggle_line_numbers);
    gtk_vp_set_callback(ew->vp_edit , "mouse_binding_toggled" , editwin_toggle_mouse_binding);
    gtk_vp_set_callback(ew->vp_edit , "highlighting_toggled"  , editwin_toggle_highlighting);
    gtk_vp_set_callback(ew->vp_edit , "link_deleted"          , editwin_update_filename);
    gtk_vp_set_callback(ew->vp_edit , "buffer_renamed"        , editwin_update_menus_callback);
    gtk_vp_set_callback(ew->vp_edit , "cursor_moved"          , editwin_update_coords);
    gtk_vp_set_callback(ew->vp_edit , "attribs_changed"       , editwin_update_file_attribs);
    gtk_vp_set_callback(ew->vp_edit , "bring_to_front"        , editwin_bring_to_front);

    gtk_widget_set_usize(GTK_WIDGET(ew->window), 200, 200);

    win_width  = (DEFAULTS.win_width  != -1) ? DEFAULTS.win_width  : 630;
    win_height = (DEFAULTS.win_height != -1) ? DEFAULTS.win_height : 450;

    gtk_window_set_default_size((GtkWindow*)ew->window, win_width, win_height);

    gtk_container_set_border_width(GTK_CONTAINER(ew->window), 0);

    /* pack the menubar into the main vbox */

    vbox = gtk_vbox_new(FALSE, 0);

    gtk_box_pack_start(GTK_BOX(vbox), menubar, FALSE, TRUE, 0);

    /* pack the line numbers and edit viewport */

    hbox = gtk_hbox_new(FALSE, 0);

    gtk_box_pack_end(GTK_BOX(hbox), ew->vp_edit, TRUE, TRUE, 0);
    gtk_box_pack_end(GTK_BOX(hbox), ew->lnums, FALSE, TRUE, 0);

    gtk_box_pack_start(GTK_BOX(vbox), hbox, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), gtk_hseparator_new(), FALSE, TRUE, 0);

    /* pack the status bar */

    hbox = gtk_hbox_new(FALSE, 0);
    align = gtk_alignment_new(0, 0.5, 0, 0);

    sprintf(buf, "CED v%s", get_libced_version());

    gtk_container_add(GTK_CONTAINER(align), ew->info_ffname);
    gtk_box_pack_start(GTK_BOX(hbox), align, TRUE, TRUE, 2);
    gtk_box_pack_start(GTK_BOX(hbox), ew->info_coords, FALSE, TRUE, 2);
    gtk_box_pack_start(GTK_BOX(hbox), ew->info_attr, FALSE, TRUE, 2);
    gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(buf), FALSE, TRUE, 2);

    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(vbox), gtk_hseparator_new(), FALSE, TRUE, 0);

    /* pack the prompt, command viewport and message viewport */

    hbox = gtk_hbox_new(TRUE, 0);
    hbox2 = gtk_hbox_new(FALSE, 0);
    fixed = gtk_fixed_new();

    gtk_fixed_put(GTK_FIXED(fixed), ew->prompt, 2, 2);
    gtk_box_pack_start(GTK_BOX(hbox2), fixed, FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox2), ew->vp_cmd, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(hbox2), gtk_vseparator_new(), FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(hbox), hbox2, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), ew->vp_msg, TRUE, TRUE, 0);

    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, TRUE, 0);

    /* add the vbox that contains everything into the window */

    gtk_container_add(GTK_CONTAINER(ew->window), vbox);

    gtk_label_set_justify(GTK_LABEL(ew->info_ffname), GTK_JUSTIFY_RIGHT);

    /* connect some signals */

    gtk_signal_connect(GTK_OBJECT(ew->window), "delete_event",
                      (GtkSignalFunc)editwin_close, ew);

    gtk_signal_connect(GTK_OBJECT(ew->window), "focus_in_event",
                      (GtkSignalFunc)editwin_focus_in, NULL);

    gtk_signal_connect(GTK_OBJECT(ew->window), "focus_out_event",
                      (GtkSignalFunc)editwin_focus_out, NULL);

    gtk_signal_connect(GTK_OBJECT(ew->window), "motion_notify_event",
                      (GtkSignalFunc)editwin_motion_notify_event, NULL);

    /* show the window */

    style_process(MAIN_STYLE);

    gtk_widget_show_all(ew->window);

    /***********************************************************************/

//    gtk_widget_set_style(ew->window  , style_get_vp_gtk_style(MAIN_STYLE));
//    gtk_widget_set_style(ew->lnums   , style_get_vp_gtk_style(MAIN_STYLE));
//    gtk_widget_set_style(menubar , style_get_vp_gtk_style(MAIN_STYLE));

//    printf("%s\n", GTK_OBJECT_TYPE(GTK_OBJECT(menubar)));

//    printf("%p\n", GTK_MENU_SHELL(menubar)->children);

    /***********************************************************************/


    if(CED_CONFIG.lnums == 0) {
        gtk_widget_hide(ew->lnums);
    }

    w1 = ew->window->window;

    gdk_window_set_group(w1, w1);

    editwin_set_toggles(ew);
    editwin_update_file_attribs(ew);
    editwin_update_prompt(ew);
    editwin_update_coords(ew);                       

    ew->initialised = 1;

    editwin_update_syntax_styles(ew);

    col_bg.red   = DEFAULTS.bm_bg[0] << 8;
    col_bg.green = DEFAULTS.bm_bg[1] << 8;
    col_bg.blue  = DEFAULTS.bm_bg[2] << 8;

    col_fg.red   = DEFAULTS.bm_fg[0] << 8;
    col_fg.green = DEFAULTS.bm_fg[1] << 8;
    col_fg.blue  = DEFAULTS.bm_fg[2] << 8;

    gtk_vp_set_block_match_style(ew->vp_edit, &col_bg, &col_fg);

    editwin_update_styles_menu(ew);


    /***********************************************************************/

//    gtk_widget_restore_default_style(GTK_WIDGET(ew->vp_edit));

    /***********************************************************************/


//    gtk_widget_show_all(ew->window);

    return(ew);
}

// FIXME

void editwin_update_syntax_styles(edwin *ew)
{
    int i;

    gtk_vp_set_syntax_styles(ew->vp_edit, DEFAULTS.vpfs);

//    for(i=0; i < 5; i++) {
//        gtk_vp_set_syntax_style(ew->vp_edit, i, DEFAULTS.vpfs[i]);
//    }
}

static void editwin_update_prompt(edwin *ew)
{
    char prompt[128];

    strcpy(prompt, ew->se->prompt);

    gtk_label_set_text(GTK_LABEL(ew->prompt), prompt);
    gtk_widget_queue_resize(ew->prompt->parent);
}

static void editwin_update_coords(edwin *ew)
{
    char tmp[32];

    sprintf(tmp, "  [%u, %lu]", ew->canv->col  + 1,
                                ew->canv->ypos + 1);

    gtk_label_set_text(GTK_LABEL(ew->info_coords), tmp);
}

static void editwin_update_filename(edwin *ew)
{
    buffer *buff;
    char   inbar[1024];
    char   title[1024];
    int    pos = 0;
    int    cc;
    int    n;

    buff = ew->canv->buff;
    cc = canv_is_copy(ew->canv);

    if(buff->dirty) {
        inbar[0] = '*';
        title[0] = '*';
        pos = 1;
    }

    if(!cc) {
        sprintf(&inbar[pos], "%s", buff->ffname);
        sprintf(&title[pos], "%s", buff->sfname);
    } else {
        n = buff_get_link_number(buff, (void*)ew->canv);
        sprintf(&inbar[pos], "%s (Carbon Copy %d)", buff->ffname, n);
        sprintf(&title[pos], "%s[%d]", buff->sfname, n);
    }

    //printf("Setting window title (%p) to '%s'\n", ew->window, title);

    gtk_label_set_text(GTK_LABEL(ew->info_ffname), inbar);
    gtk_window_set_title(GTK_WINDOW(ew->window), title);
}

static void editwin_update_file_attribs(edwin *ew)
{
    char   tmp[32];
    buffer *buff;

    buff = ew->canv->buff;

    switch(buff->ro) {
        case 0 : strcpy(tmp, "[RW] "); break;
        case 1 : strcpy(tmp, "[RO] "); break;
    }

    switch(buff->ftype) {
        case FT_DOS  : strcat(tmp, "[DOS] "); break;
        case FT_MAC  : strcat(tmp, "[MAC] "); break;
        default      : strcat(tmp, "[UNIX] ");
    }

    gtk_label_set_text(GTK_LABEL(ew->info_attr), tmp);

    editwin_update_filename(ew);
}

static void editwin_lnums_update_callback(edwin *ew)
{
    gtk_lnums_update(ew->lnums);
}

static void editwin_toggle_line_numbers(edwin *ew)
{
    (ew->canv->show_lnums) 
        ? gtk_widget_show(ew->lnums)
        : gtk_widget_hide(ew->lnums);

    editwin_set_toggles(ew);
}

static void editwin_toggle_highlighting(edwin *ew)
{
    editwin_set_toggles(ew);
}

static void editwin_toggle_mouse_binding(edwin *ew)
{
    editwin_set_toggles(ew);
}

static void editwin_switch_to(edwin *ew, SESSION_CONTEXT ct)
{
    GtkWidget *widget;

    switch(ct) {
        case SESSION_EDITWIN : widget = ew->vp_edit; break;
        case SESSION_CMDWIN  : widget = ew->vp_cmd;  break;
        case SESSION_MSGWIN  : widget = ew->vp_msg;  break;
        default : return;
    }

    gtk_widget_grab_focus(widget);
    gtk_vp_sync_mouse_to_cursor(widget);
}

static void editwin_switch_to_active(edwin *ew)
{
    editwin_switch_to(ew, ew->se->active_canv);
}

static void editwin_exec_string(edwin *ew, char *str)
{
    session_exec_string(ew->se, str);
}

static void editwin_message_callback(edwin *ew, char *message, int beep)
{
    GtkVp *vp;

    vp = GTK_VP(ew->vp_msg);

    session_message(ew->se, message);

    if(beep) {
        gdk_beep();
    }
}

static gint editwin_close(GtkWidget *widget, GdkEvent *event, edwin *ew)
{
    editwin_exec_string(ew, "wc");

    return(TRUE);
}

static void editwin_open_callback(edwin *ew, char *fname, int *retval)
{
    main_open_file(fname, retval);
}

static void editwin_close_callback(edwin *ew)
{
    main_del_window(ew);
}

static void editwin_carbon_callback(edwin *ew)
{
    main_add_window(ew->canv->buff);
}

static gint editwin_motion_notify_event(GtkWidget *widget, GdkEventMotion *event)
{
//    printf("edit win motion\n");

    return(FALSE);
}

static gint editwin_focus_in(GtkWidget *widget, GdkEventFocus *event)
{
    printf("edit win focus in\n");
    GTK_WIDGET_SET_FLAGS(widget, GTK_HAS_FOCUS);
    return(FALSE);
}

static gint editwin_focus_out(GtkWidget *widget, GdkEventFocus *event)
{
    printf("edit win focus out\n");
    GTK_WIDGET_UNSET_FLAGS(widget, GTK_HAS_FOCUS);
    return(FALSE);
}

static GtkItemFactory *editwin_get_menu(edwin *ew, GtkWidget *window, GtkWidget **menubar)
{
    GtkItemFactory *item_factory;
    GtkAccelGroup *accel_group;
    gint nmenu_items = sizeof(menu_items) / sizeof(menu_items[0]);

    accel_group = gtk_accel_group_new ();
    item_factory = gtk_item_factory_new (GTK_TYPE_MENU_BAR, "<main>", accel_group);

    /*
     * This function generates the menu items. Pass the item factory,
     * the number of items in the array, the array itself, and any
     * callback data for the the menu items.
     */

    gtk_item_factory_create_items(item_factory, nmenu_items, menu_items, ew);

    /* 
     * Attach the new accelerator group to the window.
     */

    gtk_window_add_accel_group(GTK_WINDOW(window), accel_group);

    if (menubar) {
       *menubar = gtk_item_factory_get_widget(item_factory, "<main>");
    }

    gtk_widget_set_name(*menubar, "FMB");

    return(item_factory);
}

void editwin_free(edwin *ew)
{
    GtkWidget *filesel;
    canvas *canv_msg;
    canvas *canv_cmd;
    canvas *canv_tvp;
    buffer *buff_msg;
    buffer *buff_cmd;

    /* Get all the canvas's that we need to destroy */

    canv_tvp = GTK_VP(ew->vp_edit)->canv;
    canv_msg = GTK_VP(ew->vp_msg)->canv;
    canv_cmd = GTK_VP(ew->vp_cmd)->canv;

    /* Destroy the item factory */

    gtk_object_unref(GTK_OBJECT(ew->ifactory));

    /*
     * Destroy the main edit window widget.  This subsequently will
     * destroy all the viewports attached to it, hence why we
     * retreived all the canvas pointers first.
     */

    gtk_widget_destroy(ew->window);

    /* Get the message buffer and command buffer from there canvas */

    buff_msg = canv_msg->buff;
    buff_cmd = canv_cmd->buff;

    /* Destroy all the canvas's */

    canv_destroy(canv_tvp);
    canv_destroy(canv_msg);
    canv_destroy(canv_cmd);

    /*
     * Destroy the message and command buffer.  We don't want to destroy
     * the main buffer because although we've closed the edit window,
     * other edit windows could have a view open on the buffer.
     */

    buff_destroy(buff_msg);
    buff_destroy(buff_cmd);

    /* Free the session structure */

    session_free(ew->se);

    /* Close 'save as' file selecter if open */

    if((filesel = ew->fsel_saveas) != NULL) {
        gtk_signal_emit_by_name
            (GTK_OBJECT(GTK_FILE_SELECTION(filesel)->cancel_button), "clicked");
    }

    /* Free the edit window structure */

    free(ew);
}

