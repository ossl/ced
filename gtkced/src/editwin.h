#ifndef _EDITWIN_H
#define _EDITWIN_H

#include <gdk/gdk.h>
#include <gtk/gtk.h>

#include <ced/common.h>
#include <ced/canvas.h>
#include <ced/session.h>
#include <ced/structs.h>

typedef struct _editwin edwin;

struct _editwin
{
  GtkWidget      *window;        /* the main gtk window */
  GtkWidget      *vp_edit;       /* viewport with the current open file */
  GtkWidget      *vp_cmd;        /* command input viewport */
  GtkWidget      *vp_msg;        /* message output viewport */
  GtkWidget      *info_ffname;   /* infobar containing full filename */
  GtkWidget      *info_coords;   /* infobar containing current coordinates */
  GtkWidget      *info_attr;     /* infobar containing current file attributes */
  GtkWidget      *lnums;         /* line numbers widget */
  GtkWidget      *prompt;        /* prompt widget */
  GtkWidget      *fsel_saveas;   /* currently open saveas file selector */
  GtkWidget      *menubar;       /* pointer to menu bar widget */
  GtkItemFactory *ifactory;      /* item factory containing menu items */
  canvas         *canv;          /* current open file canvas */
  session        *se;            /* session controller */
  int            manual_toggle;  /* menubar toggle mode */
  int            initialised;    /* initialised flag */
};

edwin *editwin_new(buffer *buff);
void   editwin_free(edwin *ew);
void   editwin_bring_to_front(edwin *ew);
void   editwin_window_menu_add(edwin *ew, edwin *ew_ptr);
void   editwin_window_menu_del(edwin *ew, uint i);

#endif /* _EDITWIN_H */

