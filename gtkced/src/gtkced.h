#ifndef _GTKCED_H
#define _GTKCED_H

#define GTKCED_SYSDIR  "/usr/local/etc/ced/"
#define GTKCED_BINDIR  "/usr/local/bin/"
#define GTKCED_VERSION "1.2.2"
#define GTKCED_V_MAJOR 1
#define GTKCED_V_MINOR 2
#define GTKCED_V_MICRO 2

const char *gtkced_get_version(void);
const int   gtkced_get_version_major(void);
const int   gtkced_get_version_minor(void);
const int   gtkced_get_version_micro(void);

#endif /* _GTKCED_H */

