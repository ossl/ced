/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *
 *  Last Updated
 *    11th February 2000
 *
 */

#include <gtk/gtk.h>
#include <stdio.h>
#include <ced/common.h>
#include "lnums.h"
#include "viewport.h"

//#define DEBUG

static void gtk_lnums_class_init    (GtkLnumsClass *klass);
static void gtk_lnums_init          (GtkLnums      *lnums);
static void gtk_lnums_destroy       (GtkObject     *object);
static void gtk_lnums_realize       (GtkWidget     *widget);
static void gtk_lnums_draw          (GtkWidget     *widget, GdkRectangle       *rect);
static gint gtk_lnums_expose        (GtkWidget     *widget, GdkEventExpose     *event);
static void gtk_lnums_size_request  (GtkWidget     *widget, GtkRequisition     *req);
static void gtk_lnums_size_allocate (GtkWidget     *widget, GtkAllocation      *alloc);
static void gtk_lnums_style_set     (GtkWidget     *widget, GtkStyle  *prev_style);
static void gtk_lnums_draw_lines    (GtkWidget     *widget, int start, int end);
static void gtk_lnums_calc_area     (GtkWidget     *widget);

static GtkWidgetClass *parent_class = NULL;

static void gtk_lnums_style_set     (GtkWidget     *widget, GtkStyle  *prev_style)
{
    GtkLnums *lnums;

#ifdef DEBUG
    fprintf(stderr, "[lnums] style_set (prev: %p)\n", prev_style);
#endif

    if(prev_style) {
        lnums = GTK_LNUMS(widget);
        lnums->fg_gc = widget->style->fg_gc[GTK_STATE_NORMAL];
        lnums->bg_gc = gtk_lnums_create_bg_gc(widget);

        gtk_style_set_background(widget->style, widget->window, GTK_STATE_NORMAL);
        gdk_window_clear(widget->window);
        gtk_widget_queue_resize(widget);
    }
}

guint gtk_lnums_get_type()
{
    static guint lnums_type = 0;

    if(!lnums_type) {
        GtkTypeInfo lnums_info = {
            "GtkLnums",
            sizeof(GtkLnums),
            sizeof(GtkLnumsClass),
            (GtkClassInitFunc) gtk_lnums_class_init,
            (GtkObjectInitFunc) gtk_lnums_init,
            (GtkArgSetFunc) NULL,
            (GtkArgGetFunc) NULL
        };

        lnums_type = gtk_type_unique(gtk_widget_get_type(), &lnums_info);
    }

    return(lnums_type);
}

static void gtk_lnums_class_init(GtkLnumsClass *class)
{
    GtkObjectClass *object_class;
    GtkWidgetClass *widget_class;

    object_class = (GtkObjectClass*) class;
    widget_class = (GtkWidgetClass*) class;
    parent_class = gtk_type_class(gtk_widget_get_type());

    object_class->destroy = gtk_lnums_destroy;
    widget_class->style_set = gtk_lnums_style_set;
//    widget_class->draw = gtk_lnums_draw;
    widget_class->expose_event = gtk_lnums_expose;
    widget_class->realize = gtk_lnums_realize;
    widget_class->size_allocate = gtk_lnums_size_allocate;
    widget_class->size_request = gtk_lnums_size_request;
}

/*
 * Handles a resize request.  This function then calls a function to
 * recalculate various text display components.
 */

static void gtk_lnums_size_request(GtkWidget *widget, GtkRequisition *req)
{
    GtkLnums *lnums;
    GtkVp    *vp;

#ifdef DEBUG
    fprintf(stderr, "[lnums] size request..\n");
#endif

    lnums = GTK_LNUMS(widget);
    vp = lnums->vp;

    req->width = (8 * vp->font_width) + lnums->border - 2;
}
                 
static void gtk_lnums_size_allocate(GtkWidget *widget, GtkAllocation *alloc)
{
    GtkLnums *lnums;

#ifdef DEBUG
    fprintf(stderr, "[lnums] size allocate..\n");
#endif

    g_return_if_fail (widget != NULL);
    g_return_if_fail (GTK_IS_LNUMS(widget));
    g_return_if_fail (alloc != NULL);

    widget->allocation = *alloc;

    if(GTK_WIDGET_REALIZED(widget)) {
        lnums = GTK_LNUMS(widget);

        gdk_window_move_resize(widget->window,
                               alloc->x, alloc->y,
                               alloc->width, alloc->height);

        gtk_lnums_calc_area(widget);
    }
}

static void gtk_lnums_calc_area(GtkWidget *widget)
{
    GtkLnums      *lnums;
    GtkVp         *vp;
    int           h;
    int           w;

#ifdef DEBUG
    printf("[gtk_lnums_calc_area]\n");
#endif

    lnums = GTK_LNUMS(widget);
    vp = lnums->vp;

    gdk_window_get_size(widget->window, &w, &h);

    if(lnums->ddbuf) {
        gdk_pixmap_unref(lnums->ddbuf);
    }

    lnums->ddbuf = gdk_pixmap_new(widget->window, w, h, -1);
    lnums->width = w;
    lnums->height = h;
    lnums->maxlines = h / lnums->vp->font_height;

    gdk_draw_rectangle(lnums->ddbuf, lnums->bg_gc, TRUE, 0, 0, w, h);

    gdk_draw_rectangle(lnums->ddbuf, lnums->fg_gc, TRUE, w - lnums->border, 0,
                       lnums->border, h);

    gtk_lnums_draw_lines(widget, 0, lnums->maxlines);
}

static void gtk_lnums_init(GtkLnums *lnums)
{
    GTK_WIDGET_SET_FLAGS (lnums, GTK_CAN_FOCUS);
    GTK_WIDGET_UNSET_FLAGS (lnums, GTK_NO_WINDOW);

    lnums->policy = GTK_UPDATE_CONTINUOUS;
}

GtkWidget *gtk_lnums_new(GtkVp *vp, int border_width)
{
    GtkLnums  *lnums;
  
    lnums = gtk_type_new(gtk_lnums_get_type());
    lnums->vp = vp;
    lnums->last_yoff = 0;
    lnums->last_nlines = 0;
    lnums->border = border_width;
    lnums->ddbuf = NULL;
  
    return(GTK_WIDGET(lnums));
}

static void gtk_lnums_destroy(GtkObject *object)
{
    GtkLnums *lnums;

    g_return_if_fail(object != NULL);
    g_return_if_fail(GTK_IS_LNUMS(object));

    lnums = GTK_LNUMS(object);

    if(GTK_OBJECT_CLASS(parent_class)->destroy) {
        (*GTK_OBJECT_CLASS(parent_class)->destroy)(object);
    }
}

static void gtk_lnums_draw(GtkWidget *widget, GdkRectangle *rect)
{
    GtkLnums *lnums;

#ifdef DEBUG
    printf("[gtk_lnums_draw]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_LNUMS(widget));

    lnums = GTK_LNUMS(widget);

    /*
     * Redraw appropriate part of screen.  If rect is NULL, then the whole
     * back buffer will be redrawn to the screen.
     */

    if(rect) {
        gdk_draw_pixmap(widget->window,
                        lnums->fg_gc,
                        lnums->ddbuf,
                        rect->x,
                        rect->y,
                        rect->x,
                        rect->y,
                        rect->width,
                        rect->height);
    }
}

static void gtk_lnums_draw_lines(GtkWidget *widget, int start, int end)
{
    GtkLnums     *lnums;
    GtkVp        *vp;
    GdkRectangle r;
    char         buf[64];
    ulong        nlines;
    int          i;
    int          ypos;

#ifdef DEBUG
    printf("[gtk_lnums_draw_lines] start:%d end:%d\n", start, end);
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_LNUMS(widget));

    lnums = GTK_LNUMS(widget);
    vp = lnums->vp;
    nlines = max(vp->canv->buff->nlines, 1);

    gdk_draw_rectangle(lnums->ddbuf,
                       lnums->bg_gc, TRUE, 0, 0,
                       lnums->width - lnums->border,
                       lnums->height);

    for(i=start; i < end; i++) {
        if((i + vp->yoffset) < nlines) {
            sprintf(buf, "%7lu", vp->yoffset + i + 1);
        } else {
            *buf = '\0';
        }

        ypos = ((i + 1) * vp->font_height) - vp->font_descent;

        gdk_draw_string(lnums->ddbuf, vp->font, lnums->fg_gc, 0, ypos + 2, buf);
    }

    lnums->last_nlines = nlines;
    lnums->last_yoff = vp->yoffset;

    r.x = 0;
    r.y = 0;
    r.width = lnums->width;
    r.height = lnums->height;

    gtk_lnums_draw(widget, &r);
}

static gint gtk_lnums_expose(GtkWidget *widget, GdkEventExpose *event)
{
#ifdef DEBUG
  printf("[gtk_lnums_expose]\n");
#endif

    gtk_lnums_draw(widget, &event->area);

    return(TRUE);
}

static void gtk_lnums_realize(GtkWidget *widget)
{
    GtkLnums *lnums;
    GdkWindowAttr attributes;
    gint attributes_mask;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_LNUMS(widget));

    SELECTION_FLAG = 0;
    GM_FLAG = 0;

    lnums = GTK_LNUMS(widget);

    GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.x = widget->allocation.x;
    attributes.y = widget->allocation.y;
    attributes.width = widget->allocation.width;
    attributes.height = widget->allocation.height;
    attributes.wclass = GDK_INPUT_OUTPUT;
    attributes.event_mask = gtk_widget_get_events(widget);
    attributes.event_mask |= (GDK_ENTER_NOTIFY_MASK |
                              GDK_LEAVE_NOTIFY_MASK |
                              GDK_EXPOSURE_MASK |
                              GDK_BUTTON_PRESS_MASK |  
                              GDK_BUTTON_RELEASE_MASK |  
                              GDK_POINTER_MOTION_MASK |
                              GDK_KEY_RELEASE_MASK |
                              GDK_KEY_PRESS_MASK);

    attributes.visual = gtk_widget_get_visual(widget);
    attributes.colormap = gtk_widget_get_colormap(widget);

    attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

    widget->window = gdk_window_new(widget->parent->window, &attributes, attributes_mask);
    gdk_window_set_user_data(widget->window, widget);

    widget->style = gtk_style_attach(widget->style, widget->window);
    gtk_style_set_background(widget->style, widget->window, GTK_STATE_NORMAL);

    lnums->fg_gc = widget->style->fg_gc[GTK_STATE_NORMAL];
//    lnums->bg_gc = widget->style->bg_gc[GTK_STATE_NORMAL];
    lnums->bg_gc = gtk_lnums_create_bg_gc(widget);

    gtk_lnums_calc_area(widget);
}

static GdkGC *gtk_lnums_create_bg_gc(GtkWidget *widget)
{
    GdkGCValues values;

    if(widget->style->bg_pixmap[GTK_STATE_NORMAL]) {
        values.tile = widget->style->bg_pixmap[GTK_STATE_NORMAL];
        values.fill = GDK_TILED;

        return(gdk_gc_new_with_values(widget->window, &values, GDK_GC_FILL | GDK_GC_TILE));
    } else {
        return(widget->style->bg_gc[GTK_STATE_NORMAL]);
    }
}

void gtk_lnums_update(GtkWidget *widget)
{
    GtkLnums *lnums;
    GtkVp    *vp;

    lnums = GTK_LNUMS(widget);
    vp = lnums->vp;

    if( GTK_WIDGET_VISIBLE(widget) &&
        (vp->yoffset != lnums->last_yoff ||
        max(vp->canv->buff->nlines, 1) != lnums->last_nlines)) {

        gtk_lnums_draw_lines(widget, 0, lnums->maxlines);
    }
}

