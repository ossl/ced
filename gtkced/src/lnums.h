#ifndef _LNUMS_H
#define _LNUMS_H

#include <gtk/gtk.h>
#include <ced/common.h>
#include "viewport.h"

#define GTK_LNUMS(obj)          GTK_CHECK_CAST(obj, gtk_lnums_get_type(), GtkLnums)
#define GTK_LNUMS_CLASS(klass)  GTK_CHECK_CLASS_CAST(klass, gtk_lnums_get_type(), GtkLnumsClass)
#define GTK_IS_LNUMS(obj)       GTK_CHECK_TYPE(obj, gtk_lnums_get_type())

typedef struct _GtkLnums         GtkLnums;
typedef struct _GtkLnumsClass    GtkLnumsClass;

struct _GtkLnums
{
    GtkWidget  widget;       /* gtk internal */
    guint      policy : 2;   /* gtk internal */
    GdkPixmap  *ddbuf;       /* display double buffer */
    GdkGC      *bg_gc;       /* background graphics context */
    GdkGC      *fg_gc;       /* foreground graphics context */
    GdkFont    *font;        /* text font for text area and line nums */
    GtkVp      *vp;          /* viewport */
    char       *fontname;    /* name of font */
    int        fontwidth;    /* font width */
    int        fontheight;   /* font height */
    int        maxlines;     /* maximum lines */
    int        shown;        /* lnums shown */
    int        width;        /* width of window */
    int        height;       /* height of window */
    int        border;       /* the seperate width */
    ulong      last_yoff;    /* last known yoffset */
    ulong      last_nlines;  /* last known nlines */
};

struct _GtkLnumsClass
{
  GtkWidgetClass parent_class; 
};

GtkWidget*  gtk_lnums_new               (GtkVp *vp, int border_width);
guint       gtk_lnums_get_type          (void);
void        gtk_lnums_drawtext          (GtkWidget*, char*);
void        gtk_lnums_redraw            (GtkWidget *widget);
void        gtk_lnums_update            (GtkWidget *widget);

#endif /* _LNUMS_H */

