/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999, 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *
 *  Last Updated
 *    8th March 2001
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <gtk/gtk.h>
#include <dirent.h>

#include <ced/session.h>
#include <ced/common.h>
#include <ced/buffer.h>
#include <ced/utils.h>
#include <ced/sys.h>
#include <ced/structs.h>
#include <ced/error.h>

#include "dialog.h"
#include "editwin.h"
#include "main.h"
#include "style.h"

typedef struct _ptr_array WINDOW_LIST;

WINDOW_LIST MAIN_WINDOWS;
int MAIN_INITIALISED;

static int  main_timeout_callback(gpointer data);
static void main_init(int argc, char **argv);
static void main_get_rgb(char **str, int rgb[3]);
static void main_set_gtk_style(char *style_name, char *style_attrib, int cols[3]);
static void main_set_misc(char *style_name, char *style_attrib, char *misc);
static void main_set_vpfs(int r, int g, int b, char *fname, VP_FONT_STYLE *vpfs);
static void main_set_rgb(int r, int g, int b, int *rgb);
static void main_set_defaults();
static void main_load_config();
//static void main_realize_defaults();
static void main_localise(char *fname);
//static int  main_load_style(char *name);

int main(int argc, char *argv[])
{
    struct _ptr_array ARGS;
    char   *arg;
    uint   nodet = 0;
    uint   i;

    MAIN_INITIALISED = 0;

    ptr_array_init(&ARGS);

    ARGS.data = (void**)argv;
    ARGS.size = argc;
    ARGS.items = argc;

    for(i=1; i < ARGS.items; i++) {
        arg = ARGS.data[i];

        if(!strcmp(arg, "-nodetach")) {
            ptr_array_del(&ARGS, i);
            nodet = 1;
            i--;
        }
    }

    argc = ARGS.items;

    //if(!nodet) detach();

    sys_init(argc, argv, cmd_make_keylist());
    gtk_init(&argc, &argv);
    main_init(argc, argv);

//    main_quit();

    MAIN_INITIALISED = 1;

    gtk_main();

    return(0);
}

static void main_init(int argc, char **argv)
{
    int     retval;
    int     op = 0;
    uint    i;

    OPEN_DIALOGS = 0;
    DEFAULTS_CONFIG = NULL;

    ptr_array_init(&MAIN_WINDOWS);

    memset(&DEFAULTS, 0, sizeof(struct _gtkced_config));

    main_set_defaults();
    main_load_config();
    main_realize_defaults();

    if(argc == 1) {
        main_open_file(NULL, &retval);
        op = 1;
    } else {
        for(i=1; i < argc; i++) {
            main_open_file(argv[i], &retval);

            if(retval == -1) {
                fprintf(stderr, "[ced] %s: %s\n", argv[i], error_get_str(errno));
            } else {
                op++;
            }
        }
    }

    if(op == 0) {
        fprintf(stderr, "[ced] no valid files specified, aborting.\n\r");
        main_quit();
    }

    signal(SIGHUP, SIG_IGN);

    /* Sync the swaps every 20 seconds */

    gtk_timeout_add(20000, main_timeout_callback, NULL);
}

void main_set_style(char *style_name)
{
    GList      *toplevels;
    GtkWidget  *widget;
    char       sname[1024];
    uint  i;

//    printf("%s\n", GTK_LABEL(GTK_BIN(item)->child)->label);

    strcpy(sname, "Styles/");
    strcat(sname, style_name);
    strcat(sname, ".style");

    main_load_style(sname); 
    main_realize_defaults();

    toplevels = gdk_window_get_toplevels();

    while(toplevels) {
        gdk_window_get_user_data(toplevels->data, (gpointer*)&widget);
     
        if(widget) {
            gtk_widget_reset_rc_styles(widget);
            gtk_widget_queue_draw(widget);
        }

        toplevels = toplevels->next;
    }

    g_list_free(toplevels);

    for(i=0; i < MAIN_WINDOWS.items; i++) {
        editwin_update_syntax_styles(MAIN_WINDOWS.data[i]);
    }
}

struct _ptr_array *main_get_styles()
{
    DIR *dptr;
    struct dirent *de;
    static struct _ptr_array pa;
    char ffname[1024];
    char *sptr;
    int len;

    ptr_array_init(&pa);

    if(get_conf_file(ffname, NULL, NULL, "Styles", 1024)) {
        if(is_dir(ffname)) {
            dptr = opendir(ffname);

            while((de = readdir(dptr)) != NULL) {
                sptr = de->d_name;

//                printf("%d\n", de->d_reclen);

                if((sptr = strrchr(sptr, '.')) != NULL) {
                    if(!strncmp(sptr, ".style", 6)) {
                        len = strlen(de->d_name) - 6;
                        ptr_array_add(&pa, (void*)str_ndup(de->d_name, len));
//                        printf("%s\n", de->d_name);
                    }
                }
            }

            closedir(dptr);
        } else {
//            printf("??\n");
        }
    }

    return(&pa);
}

int main_timeout_callback(gpointer data)
{
    sys_sync_swaps(0);

    return(TRUE);
}

void main_window_to_front(edwin *ew)
{
    editwin_bring_to_front(ew);
}

void main_update_window_menus(edwin *ew)
{
    uint  i;
    ulong n;

    if(ptr_array_lookup(&MAIN_WINDOWS, (void*)ew, &n)) {
        for(i=0; i < MAIN_WINDOWS.items; i++) {
            editwin_window_menu_del(MAIN_WINDOWS.data[i], n);
            editwin_window_menu_add(MAIN_WINDOWS.data[i], ew);
        }

        ptr_array_del(&MAIN_WINDOWS, n);
        ptr_array_add(&MAIN_WINDOWS, (void*)ew);
    }
}

void main_del_window(edwin *ew)
{
    buffer *buff;
    ulong  n;
    uint   i;

    buff = ew->canv->buff;

    if(ptr_array_lookup(&MAIN_WINDOWS, (void*)ew, &n)) {

        for(i=0; i < MAIN_WINDOWS.items; i++) {
            editwin_window_menu_del(MAIN_WINDOWS.data[i], n);
        }

        ptr_array_del(&MAIN_WINDOWS, n);

        editwin_free(ew);

        if(buff_number_of_links(buff) == 0) {
            sys_del_buffer(buff);
            buff_destroy(buff);
        }

        if(MAIN_WINDOWS.items == 0 && OPEN_DIALOGS == 0) {
            main_quit();
        }
    } else {
        printf("[ced] couldn't find window (huh?)\n");
    }
}

void main_add_window(buffer *buff)
{
    edwin     *ew;
    uint      i;

    if((ew = editwin_new(buff))) {
        ptr_array_add(&MAIN_WINDOWS, (void*)ew);

        for(i=0; i < MAIN_WINDOWS.items; i++) {
            editwin_window_menu_add(MAIN_WINDOWS.data[i], ew);

            if(ew != MAIN_WINDOWS.data[i]) {
                editwin_window_menu_add(ew, MAIN_WINDOWS.data[i]);
            }
        }
    } else {
        fprintf(stderr, "[ced] failed to create new window\n");
    }
}

void main_open_fsel_callback(GtkWidget *widget, void *ptr)
{
    GtkWidget *fsel;
    char      *ffname;
    int        r;

    fsel = gtk_widget_get_toplevel(widget);
    ffname = gtk_file_selection_get_filename(GTK_FILE_SELECTION(fsel));
    main_open_file(ffname, &r);
    gtk_widget_destroy(fsel);
}

void main_open_file(char *fname, int *retval)
{
    char path[1024];
    buffer *buff;

    if(fname && (get_full_path(path, fname, 1024) == NULL)) {
        *retval = -1;
        return;
    }

    buff = buff_new(fname);
    main_open(buff, OF_OPEN | OF_USE_SWAP);
    *retval = 0;
    return;
}

void main_open(buffer *buff, opflags f)
{
    operror oe;
    oe = buff_open(buff, f);

//    dialog_not_regular(buff);
//    dialog_not_readable(buff);
//    dialog_recover(buff);
//    dialog_locked(buff);
//    dialog_swap_noaccess(buff);

    switch(oe) {
        case OE_NOT_FILE:       dialog_not_regular(buff); break;
        case OE_NOT_READABLE:   dialog_not_readable(buff); break;
        case OE_SWAP_EXISTS:    dialog_recover(buff); break;
        case OE_SWAP_LOCKED:    dialog_locked(buff); break;
        case OE_SWAP_NOACCESS:  dialog_swap_noaccess(buff); break;
        case OE_READ_ONLY:      break; /* unhandled */
        case OE_ERROR:          break; /* unhandled */
 
        case OE_SUCCESS:
            sys_add_buffer(buff);
            main_add_window(buff);
            break;
    }
}

int main_windows_count()
{
    return(MAIN_WINDOWS.items);
}

void main_quit()
{
    sys_quit();

    if(MAIN_INITIALISED) {
        gtk_main_quit();
    }

    exit(0);
}

/*
 * Functions to get all the configuration
 * for the gtkced frontend.
 */

static void main_get_rgb(char **str, int rgb[3])
{
    memset(rgb, 0, 3);

    if(str != NULL) {
        if(str[0] != NULL) rgb[0] = atoi(str[0]);
        if(str[1] != NULL) rgb[1] = atoi(str[1]);
        if(str[2] != NULL) rgb[2] = atoi(str[2]);
    }
}

static void main_set_gtk_style(char *style_name, char *style_attrib, int cols[3])
{
    char buf[256];
    float r;
    float g;
    float b;

    r = (float)cols[0] / 255;
    g = (float)cols[1] / 255;
    b = (float)cols[2] / 255;

    sprintf(buf, "style \"%s\" { %s = { %.6f, %.6f, %.6f } }", style_name, style_attrib, r, g, b);
//    sprintf(buf, "style \"%s\" { %s = { %.1f, %.1f, %.1f } }", style_name, style_attrib, r, g, b);

//    printf("%s\n", buf);

    gtk_rc_parse_string(buf);
}

static void main_set_misc(char *style_name, char *style_attrib, char *misc)
{
    char buf[256];

    sprintf(buf, "style \"%s\" { %s = \"%s\" }", style_name, style_attrib, misc);

    gtk_rc_parse_string(buf);
}

static void main_set_vpfs(int r, int g, int b, char *fname, VP_FONT_STYLE *vpfs)
{
    vpfs->red = r;
    vpfs->green = g;
    vpfs->blue = b;
    vpfs->fontname = fname;
}

static void main_set_rgb(int r, int g, int b, int *rgb)
{
    rgb[0] = r;
    rgb[1] = g;
    rgb[2] = b;
}

static void main_set_defaults()
{
    /* Window manager decides width and height */

    DEFAULTS.win_width = -1;
    DEFAULTS.win_height = -1;

    /* Some other sensible defaults */

    DEFAULTS.lnums_border = 2;
//    DEFAULTS.msgwin_font = "fixed";
//    DEFAULTS.editwin_font = "fixed";
//    DEFAULTS.cmdwin_font = "fixed";

    /* Block match background */

//    main_set_rgb(255 , 255 , 255 , DEFAULTS.bm_fg);
//    main_set_rgb(30  , 100 , 30  , DEFAULTS.bm_bg);

    /* Edit window defaults */

//    main_set_rgb(220 , 220 , 220 , DEFAULTS.editwin_bg);
//    main_set_rgb(0   , 0   , 0   , DEFAULTS.editwin_fg);
//    main_set_rgb(0   , 0   , 0   , DEFAULTS.editwin_sel_bg);
//    main_set_rgb(240 , 240 , 240 , DEFAULTS.editwin_sel_fg);

    /* Command window defaults */

//    main_set_rgb(220 , 220 , 220 , DEFAULTS.msgwin_bg);
//    main_set_rgb(0   , 0   , 0   , DEFAULTS.msgwin_fg);
//    main_set_rgb(0   , 0   , 0   , DEFAULTS.msgwin_sel_bg);
//    main_set_rgb(240 , 240 , 240 , DEFAULTS.msgwin_sel_fg);

    /* Message window defaults */

//    main_set_rgb(220 , 220 , 220 , DEFAULTS.cmdwin_bg);
//    main_set_rgb(0   , 0   , 0   , DEFAULTS.cmdwin_fg);
//    main_set_rgb(0   , 0   , 0   , DEFAULTS.cmdwin_sel_bg);
//    main_set_rgb(240 , 240 , 240 , DEFAULTS.cmdwin_sel_fg);

    /* Line numbers defaults */

//    main_set_rgb(220 , 220 , 220 , DEFAULTS.lnums_bg);
//    main_set_rgb(0   , 0   , 0   , DEFAULTS.lnums_fg);

    /* Menu defaults */

//    main_set_rgb(200 , 200 , 200 , DEFAULTS.menu_bar_bg);
//    main_set_rgb(0   , 0   , 0   , DEFAULTS.menu_bar_fg);
//    main_set_rgb(200 , 200 , 200 , DEFAULTS.menu_pop_bg);
//    main_set_rgb(0   , 0   , 0   , DEFAULTS.menu_pop_fg);

    /* Status bar defaults */

//    main_set_rgb(200, 200 , 200 , DEFAULTS.status_bar_bg);
//    main_set_rgb(0  , 0   , 0   , DEFAULTS.status_bar_fg);

    /* Set viewport font styles */

//    main_set_vpfs(150 , 0   , 0   , NULL , &DEFAULTS.vpfs[0]);
//    main_set_vpfs(0   , 0   , 150 , NULL , &DEFAULTS.vpfs[1]);
//    main_set_vpfs(0   , 100 , 0   , NULL , &DEFAULTS.vpfs[2]);
//    main_set_vpfs(0   , 150 , 150 , NULL , &DEFAULTS.vpfs[3]);
//    main_set_vpfs(120 , 0   , 120 , NULL , &DEFAULTS.vpfs[4]);
}

static void main_load_config()
{
    config *gtkced;
    char rcbuf[1024];
    char stbuf[1024];
    char *name;

    name = "";

    if(get_conf_file(rcbuf, NULL, NULL, "gtkced.conf", 1024)) {
        gtkced = conf_new(rcbuf);
        conf_parse(gtkced);

        if((name = conf_params(gtkced, NULL, "style")[0])) {
            sprintf(stbuf, "Styles/%s.style", name);
        }
    }
 
    if(!main_load_style(stbuf) && strcmp(name, "default")) {
        main_load_style("Styles/default.style");
    }

    //conf_free(gtkced);
}

//static int main_load_style(char *name)
int main_load_style(char *name)
{
    char rcbuf[1024];
    char **window_geom;
    char **syntax_cols;
    char **editwin;
    char **cmdwin;
    char **msgwin;
    char **menu;
    char **status;
    char **str;
    char param[10];
    int  i;
    int  j;

    if(get_conf_file(rcbuf, NULL, NULL, name, 1024)) {

        /*************************************************************************/

        printf("Initialising style object..\n");

        MAIN_STYLE = style_new(rcbuf);

        printf("Loading style..\n");

        style_load(MAIN_STYLE);

        printf("Done !\n");

        /*************************************************************************/

        printf("Continuing legacy style handling..\n");


        DEFAULTS_CONFIG = conf_new(rcbuf);
        conf_parse(DEFAULTS_CONFIG);

        /* Read the window geometry set up. */

        if((window_geom = conf_params(DEFAULTS_CONFIG, NULL, "WindowGeometry"))) {
            if((str = conf_params(DEFAULTS_CONFIG, window_geom[0], "width"))) {
                DEFAULTS.win_width = atoi(str[0]);
            }
            if((str = conf_params(DEFAULTS_CONFIG, window_geom[0], "height"))) {
                DEFAULTS.win_height = atoi(str[0]);
            }
        }

        /* Read the syntax highlighting colours from the default config file. */

        if((syntax_cols = conf_params(DEFAULTS_CONFIG, NULL, "SyntaxColours"))) {
            memset(DEFAULTS.vpfs, 0, sizeof(VP_FONT_STYLE) * 5);

            for(i=0; i < 5; i++) {
                sprintf(param, "set_%d", i+1);

                if((str = conf_params(DEFAULTS_CONFIG, syntax_cols[0], param))) {
                    for(j=0; str[j] != NULL; j++);
                    if(j >= 0) DEFAULTS.vpfs[i].red      = atoi(str[0]);
                    if(j >= 1) DEFAULTS.vpfs[i].green    = atoi(str[1]);
                    if(j >= 2) DEFAULTS.vpfs[i].blue     = atoi(str[2]);
                    if(j >= 3) DEFAULTS.vpfs[i].fontname = str[3];
                }
            }

            if((str = conf_params(DEFAULTS_CONFIG, syntax_cols[0], "BlockBG"))) {
                for(j=0; str[j] != NULL; j++);
                if(j >= 0) DEFAULTS.bm_bg[0] = atoi(str[0]);
                if(j >= 1) DEFAULTS.bm_bg[1] = atoi(str[1]);
                if(j >= 2) DEFAULTS.bm_bg[2] = atoi(str[2]);
            }

            if((str = conf_params(DEFAULTS_CONFIG, syntax_cols[0], "BlockFG"))) {
                for(j=0; str[j] != NULL; j++);
                if(j >= 0) DEFAULTS.bm_fg[0] = atoi(str[0]);
                if(j >= 1) DEFAULTS.bm_fg[1] = atoi(str[1]);
                if(j >= 2) DEFAULTS.bm_fg[2] = atoi(str[2]);
            }
        }

        if((editwin = conf_params(DEFAULTS_CONFIG, NULL, "EditWindow"))) {
            main_get_rgb(conf_params(DEFAULTS_CONFIG, editwin[0], "Background"), DEFAULTS.editwin_bg);
            main_get_rgb(conf_params(DEFAULTS_CONFIG, editwin[0], "Text"), DEFAULTS.editwin_fg);
            main_get_rgb(conf_params(DEFAULTS_CONFIG, editwin[0], "SelectBG"), DEFAULTS.editwin_sel_bg);
            main_get_rgb(conf_params(DEFAULTS_CONFIG, editwin[0], "SelectText"), DEFAULTS.editwin_sel_fg);

            if((str = conf_params(DEFAULTS_CONFIG, editwin[0], "Font"))) {
                DEFAULTS.editwin_font = strdup(str[0]);
            }
        }

        if((cmdwin = conf_params(DEFAULTS_CONFIG, NULL, "CommandWindow"))) {
            main_get_rgb(conf_params(DEFAULTS_CONFIG, cmdwin[0], "Background"), DEFAULTS.cmdwin_bg);
            main_get_rgb(conf_params(DEFAULTS_CONFIG, cmdwin[0], "Text"), DEFAULTS.cmdwin_fg);
            main_get_rgb(conf_params(DEFAULTS_CONFIG, cmdwin[0], "SelectBG"), DEFAULTS.cmdwin_sel_bg);
            main_get_rgb(conf_params(DEFAULTS_CONFIG, cmdwin[0], "SelectText"), DEFAULTS.cmdwin_sel_fg);

            if((str = conf_params(DEFAULTS_CONFIG, cmdwin[0], "Font"))) {
                DEFAULTS.cmdwin_font = strdup(str[0]);
            }
        }

        if((msgwin = conf_params(DEFAULTS_CONFIG, NULL, "MessageWindow"))) {
            main_get_rgb(conf_params(DEFAULTS_CONFIG, msgwin[0], "Background"), DEFAULTS.msgwin_bg);
            main_get_rgb(conf_params(DEFAULTS_CONFIG, msgwin[0], "Text"), DEFAULTS.msgwin_fg);
            main_get_rgb(conf_params(DEFAULTS_CONFIG, msgwin[0], "SelectBG"), DEFAULTS.msgwin_sel_bg);
            main_get_rgb(conf_params(DEFAULTS_CONFIG, msgwin[0], "SelectText"), DEFAULTS.msgwin_sel_fg);

            if((str = conf_params(DEFAULTS_CONFIG, msgwin[0], "Font"))) {
                DEFAULTS.msgwin_font = strdup(str[0]);
            }
        }

        if((menu = conf_params(DEFAULTS_CONFIG, NULL, "MenuColours"))) {
            main_get_rgb(conf_params(DEFAULTS_CONFIG, menu[0], "BarBG"), DEFAULTS.menu_bar_bg);
            main_get_rgb(conf_params(DEFAULTS_CONFIG, menu[0], "BarText"), DEFAULTS.menu_bar_fg);
            main_get_rgb(conf_params(DEFAULTS_CONFIG, menu[0], "MenuBG"), DEFAULTS.menu_pop_bg);
            main_get_rgb(conf_params(DEFAULTS_CONFIG, menu[0], "MenuText"), DEFAULTS.menu_pop_fg);
        }

        if((menu = conf_params(DEFAULTS_CONFIG, NULL, "LineNumbers"))) {
            main_get_rgb(conf_params(DEFAULTS_CONFIG, menu[0], "Background"), DEFAULTS.lnums_bg);
            main_get_rgb(conf_params(DEFAULTS_CONFIG, menu[0], "Text"), DEFAULTS.lnums_fg);

            if((str = conf_params(DEFAULTS_CONFIG, menu[0], "Border"))) {
                DEFAULTS.lnums_border = atoi(str[0]);
            }
        }

        if((status = conf_params(DEFAULTS_CONFIG, NULL, "StatusColours"))) {
            main_get_rgb(conf_params(DEFAULTS_CONFIG, status[0], "BarBG"), DEFAULTS.status_bar_bg);
            main_get_rgb(conf_params(DEFAULTS_CONFIG, status[0], "BarText"), DEFAULTS.status_bar_fg);

            if((str = conf_params(DEFAULTS_CONFIG, status[0], "Font"))) {
                DEFAULTS.status_bar_font = strdup(str[0]);
            }
        }

//        conf_free(DEFAULTS_CONFIG);

        return(1);
    } else {
        fprintf(stderr, "[ced] couldn't load %s\n", name);
    }

    return(0);
}

//static void main_realize_defaults()
void main_realize_defaults()
{
    // Set up the style for the main edit viewport

    style_process(MAIN_STYLE);

    return;

    main_set_gtk_style("CED_Edit_Vp"  , "bg[NORMAL]"   , DEFAULTS.editwin_bg);
    main_set_gtk_style("CED_Edit_Vp"  , "fg[NORMAL]"   , DEFAULTS.editwin_fg);
    main_set_gtk_style("CED_Edit_Vp"  , "fg[SELECTED]" , DEFAULTS.editwin_sel_fg);
    main_set_gtk_style("CED_Edit_Vp"  , "bg[SELECTED]" , DEFAULTS.editwin_sel_bg);

    // Set up the style for the message viewport

    main_set_gtk_style("CED_Msg_Vp"   , "bg[NORMAL]"   , DEFAULTS.msgwin_bg);
    main_set_gtk_style("CED_Msg_Vp"   , "fg[NORMAL]"   , DEFAULTS.msgwin_fg);
    main_set_gtk_style("CED_Msg_Vp"   , "fg[SELECTED]" , DEFAULTS.msgwin_sel_fg);
    main_set_gtk_style("CED_Msg_Vp"   , "bg[SELECTED]" , DEFAULTS.msgwin_sel_bg);

    // Set up the style for the command viewport

    main_set_gtk_style("CED_Cmd_Vp"   , "bg[NORMAL]"   , DEFAULTS.cmdwin_bg);
    main_set_gtk_style("CED_Cmd_Vp"   , "fg[NORMAL]"   , DEFAULTS.cmdwin_fg);
    main_set_gtk_style("CED_Cmd_Vp"   , "fg[SELECTED]" , DEFAULTS.cmdwin_sel_fg);
    main_set_gtk_style("CED_Cmd_Vp"   , "bg[SELECTED]" , DEFAULTS.cmdwin_sel_bg);

    // Set up the style for the status bar

    main_set_gtk_style("CED_Labels"   , "bg[NORMAL]"   , DEFAULTS.status_bar_bg);
    main_set_gtk_style("CED_Labels"   , "fg[NORMAL]"   , DEFAULTS.status_bar_fg);

    // Set up the style for the menubar and items

    main_set_gtk_style("CED_MenuBar"  , "bg[NORMAL]"   , DEFAULTS.menu_bar_bg);
    main_set_gtk_style("CED_MenuBar"  , "fg[NORMAL]"   , DEFAULTS.menu_bar_fg);
    main_set_gtk_style("CED_MenuBar"  , "fg[PRELIGHT]" , DEFAULTS.menu_bar_fg);
    main_set_gtk_style("CED_Menu"     , "bg[NORMAL]"   , DEFAULTS.menu_pop_bg);
    main_set_gtk_style("CED_Menu"     , "fg[NORMAL]"   , DEFAULTS.menu_pop_fg);
    main_set_gtk_style("CED_Menu"     , "fg[PRELIGHT]" , DEFAULTS.menu_bar_fg);

    // Set up the style for line numbers

    main_set_gtk_style("CED_LNums"    , "bg[NORMAL]"   , DEFAULTS.lnums_bg);
    main_set_gtk_style("CED_LNums"    , "fg[NORMAL]"   , DEFAULTS.lnums_fg);

    // Set up the fonts

    main_set_misc("CED_Edit_Vp" , "font", DEFAULTS.editwin_font);
    main_set_misc("CED_Msg_Vp"  , "font", DEFAULTS.msgwin_font);
    main_set_misc("CED_Cmd_Vp"  , "font", DEFAULTS.cmdwin_font);
    main_set_misc("CED_Labels"  , "font", DEFAULTS.status_bar_font);

    // Assign styles to widgets

    gtk_rc_parse_string("widget \"EditWindow\"               style \"CED_Labels\"");
    gtk_rc_parse_string("widget \"EditWindow.*.GtkLabel\"    style \"CED_Labels\"");

    // Viewport styles

    gtk_rc_parse_string("widget \"EditWindow.*.EditVp\"      style \"CED_Edit_Vp\"");
    gtk_rc_parse_string("widget \"EditWindow.*.MsgVp\"       style \"CED_Msg_Vp\"");
    gtk_rc_parse_string("widget \"EditWindow.*.CmdVp\"       style \"CED_Cmd_Vp\"");

    // Widgets attached to viewports should inherit the viewports style

    gtk_rc_parse_string("widget \"EditWindow.*.LineNumbers\" style \"CED_LNums\"");
    gtk_rc_parse_string("widget \"EditWindow.*.GtkFixed*\"   style \"CED_Cmd_Vp\"");

    // Menubar and menu pop-up

    gtk_rc_parse_string("widget_class \"*GtkMenu*\"          style \"CED_Menu\"");
    gtk_rc_parse_string("widget_class \"*GtkMenuBar*\"       style \"CED_MenuBar\"");
}

void main_localise_default_keydef()
{
    main_localise("key.def");
}

void main_localise_default_gtkced()
{
    main_localise("gtkced.conf");
}

void main_localise_default_syntax()
{
    main_localise("syntax.conf");
}

/*
 * Default files are files contained within $prefix/etc/ced/
 * Local files are files contained within ~user/.ced/
 * This function copies files from there default location to the local
 * location.  Not sure if this function belongs here or not, might be
 * more of a backend thing.
 */

static void main_localise(char *fname)
{
    char src[1024];
    char dest[1024];

    get_sys_dir(src, 1024);
    get_ced_dir(dest, 1024);

    strcat(src, fname);
    strcat(dest, fname);

    if(file_exists(src)) {
        if(file_copy(src, dest) == -1) {
            fprintf(stderr, "%s\n", error_get_str(errno));
        }
    }
}

