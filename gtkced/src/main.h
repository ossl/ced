#ifndef _MAIN_H
#define _MAIN_H

#include <ced/strhash.h>
#include <ced/session.h>

#include "editwin.h"
#include "viewport.h"
#include "style.h"

STYLE *MAIN_STYLE; // TEMP - FIXME
int OPEN_DIALOGS;
config *DEFAULTS_CONFIG;
struct _gtkced_config DEFAULTS;

struct _gtkced_config
{
    int win_top;
    int win_left;
    int win_width;
    int win_height;

    /* The edit window */

    int editwin_bg[3];
    int editwin_fg[3];
    int editwin_sel_bg[3];
    int editwin_sel_fg[3];

    char *editwin_font;

    /* The command window */

    int cmdwin_bg[3];
    int cmdwin_fg[3];
    int cmdwin_sel_bg[3];
    int cmdwin_sel_fg[3];

    char *cmdwin_font;

    /* The message window */

    int msgwin_bg[3];
    int msgwin_fg[3];
    int msgwin_sel_bg[3];
    int msgwin_sel_fg[3];

    char *msgwin_font;

    /* The menu */

    int menu_bar_bg[3];
    int menu_bar_fg[3];
    int menu_pop_bg[3];
    int menu_pop_fg[3];

    /* Line numbers */

    int lnums_bg[3];
    int lnums_fg[3];
    int lnums_border;

    /* Block Match */

    int bm_bg[3];
    int bm_fg[3];

    /* The status bar */

    int status_bar_bg[3];
    int status_bar_fg[3];

    char *status_bar_font;

    VP_FONT_STYLE vpfs[5];
};

int  main_windows_count(void);
void main_open_file(char *fname, int *retval);
void main_open(buffer *buff, opflags f);
void main_add_window(buffer *buff);
void main_del_window(edwin *ew);
void main_quit(void);
void main_window_to_front(edwin *ew);
void main_update_window_menus(edwin *ew);
void main_localise_default_keydef(void);
void main_localise_default_gtkced(void);
void main_localise_default_syntax(void);
void main_open_fsel_callback(GtkWidget *widget, void *ptr);

int main_load_style(char *name);
void main_realize_defaults();

#endif /* _MAIN_H */

