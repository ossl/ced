/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2001
 *
 *  This program is free software; you can redisectibute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is disectibuted in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *
 *  Last Updated
 *    11th November 2001
 *
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include <ced/conf.h>
#include <ced/utils.h>
#include "style.h"

static void style_copy_to_vp(STYLE_VP *svp, config *conf, char *sect_name);
static void style_copy_to_rgb(STYLE_RGB *rgb, char **str);
static int  style_ensure_int(char **str);

/********************************************************************************************/

// SHOULDNT BE HERE, TEST PURPOSES ONLY
// MODULE SHOULD NOT BE DEPENDENT ON GTK

static void style_copy_to_gdk_color(GdkColor *col, STYLE_RGB rgb)
{
    /* pad numbers to 16 bit */

    col->red   = rgb.r << 8;
    col->green = rgb.g << 8;
    col->blue  = rgb.b << 8;
}

static void style_process_attribs(STYLE_ATTRIBS *attribs, GtkRcStyle *rcs, int gstate)
{
    style_copy_to_gdk_color( &rcs->fg[gstate] , attribs->fg);
    style_copy_to_gdk_color( &rcs->bg[gstate] , attribs->bg);

    rcs->color_flags[gstate] = GTK_RC_BG | GTK_RC_FG;
    rcs->bg_pixmap_name[gstate] = attribs->pixmap;

    if(attribs->font) rcs->font_name = attribs->font;
}

void style_process(STYLE *st)
{
    GtkRcStyle *rcs_menu_bar;
    GtkRcStyle *rcs_menu_pop;
    GtkRcStyle *rcs_menu_item;
    GtkRcStyle *rcs_text;
    GtkRcStyle *rcs_msg;
    GtkRcStyle *rcs_cmd;
    GtkRcStyle *rcs_lnums;
    GtkRcStyle *rcs_status;

    /* Deal with all the menu bar stuff first */

    rcs_menu_bar  = gtk_rc_style_new();
    rcs_menu_pop  = gtk_rc_style_new();
    rcs_menu_item = gtk_rc_style_new();

    style_process_attribs(&(st->menu.bar) , rcs_menu_bar  , GTK_STATE_NORMAL);
    style_process_attribs(&(st->menu.list), rcs_menu_pop  , GTK_STATE_NORMAL);
    style_process_attribs(&(st->menu.item), rcs_menu_item , GTK_STATE_NORMAL);

    /* Now the viewports */

    rcs_text = gtk_rc_style_new();
    rcs_cmd  = gtk_rc_style_new();
    rcs_msg  = gtk_rc_style_new();

    style_process_attribs(&(st->text.sa), rcs_text , GTK_STATE_NORMAL);
    style_process_attribs(&(st->cmd.sa) , rcs_cmd  , GTK_STATE_NORMAL);
    style_process_attribs(&(st->msg.sa) , rcs_msg  , GTK_STATE_NORMAL);

    style_process_attribs(&(st->text.sel_sa), rcs_text , GTK_STATE_SELECTED);
    style_process_attribs(&(st->cmd.sel_sa) , rcs_cmd  , GTK_STATE_SELECTED);
    style_process_attribs(&(st->msg.sel_sa) , rcs_msg  , GTK_STATE_SELECTED);

    /* Status Bar */

    rcs_status  = gtk_rc_style_new();

    style_process_attribs(&(st->status.sa), rcs_status, GTK_STATE_NORMAL);

    /* Line numbers */

    rcs_lnums = gtk_rc_style_new();

    style_process_attribs(&(st->lnums.sa), rcs_lnums, GTK_STATE_NORMAL);

    /* Finally, apply all the rc styles to there associated widgets */


    /*
     * The status bar is transparent so we change the backing window
     * to the colour or pixmap set.  This is possibly inefficient and
     * flawed so this will prolly get changed.
     */

    gtk_rc_add_widget_name_style(rcs_status, "*EditWindow*");
    gtk_rc_add_widget_name_style(rcs_status, "*EditWindow*GtkLabel*");

    /* Set up the style for each named viewport */

    gtk_rc_add_widget_name_style(rcs_text, "*EditVp*");
    gtk_rc_add_widget_name_style(rcs_msg, "*MsgVp*");
    gtk_rc_add_widget_name_style(rcs_cmd, "*CmdVp*");

    /* Set up the style for the line numbers widget */

    gtk_rc_add_widget_name_style(rcs_lnums, "*LineNumbers");

    /* Set up the menu bar, menu pull down list and menu item styles */

    gtk_rc_add_widget_class_style(rcs_menu_pop, "*GtkMenu*");
    gtk_rc_add_widget_class_style(rcs_menu_bar, "*GtkMenuBar*");

    /* Change the prompt style to be the same as the command window */

    gtk_rc_add_widget_name_style(rcs_cmd, "*EditWindow*GtkFixed");
}

STYLE *style_new(char *filename)
{
    STYLE *s;
    char  ffname[1024];

    if(get_full_path(ffname, filename, 1024)) {
        s = (STYLE*)calloc(1, sizeof(STYLE));
        s->conf = conf_new(ffname);
        s->ffname = strdup(ffname);
        s->win_x = STYLE_DEFAULT_WIN_XPOS;
        s->win_y = STYLE_DEFAULT_WIN_YPOS;
        s->win_w = STYLE_DEFAULT_WIN_WIDTH;
        s->win_h = STYLE_DEFAULT_WIN_HEIGHT;

        return(s);
    }

    return(NULL);
}

void style_free(STYLE *st)
{
}

void style_load(STYLE *st)
{
    char param[10];
    char **sect;
    char **str;
    int  i;

    conf_parse(st->conf);

    /*************************************************************************************/

    if((sect = conf_params(st->conf, NULL, "WindowGeometry"))) {
        st->win_x = style_ensure_int(conf_params(st->conf, sect[0], "x"));
        st->win_y = style_ensure_int(conf_params(st->conf, sect[0], "y"));
        st->win_w = style_ensure_int(conf_params(st->conf, sect[0], "width"));
        st->win_h = style_ensure_int(conf_params(st->conf, sect[0], "height"));
    }

    /*************************************************************************************/

    if((sect = conf_params(st->conf, NULL, "SyntaxColours"))) {
        for(i=0; i < 5; i++) {
            sprintf(param, "set_%d", i + 1);

            if((str = conf_params(st->conf, sect[0], param))) {
                style_copy_to_rgb(&st->syn.cols[i], str);
            }
        }

        if((str = conf_params(st->conf, sect[0], "BlockBG"))) {
            style_copy_to_rgb(&st->syn.match_bg, str);
        }

        if((str = conf_params(st->conf, sect[0], "BlockFG"))) {
            style_copy_to_rgb(&st->syn.match_fg, str);
        }
    }

    /*************************************************************************************/

    style_copy_to_vp(&st->text , st->conf, "EditWindow");
    style_copy_to_vp(&st->cmd  , st->conf, "CommandWindow");
    style_copy_to_vp(&st->msg  , st->conf, "MessageWindow");

    /*************************************************************************************/

    if((sect = conf_params(st->conf, NULL, "MenuColours"))) {
        style_copy_to_rgb(&st->menu.bar.bg  , conf_params(st->conf, sect[0], "BarBG"));
        style_copy_to_rgb(&st->menu.bar.fg  , conf_params(st->conf, sect[0], "BarText"));
        style_copy_to_rgb(&st->menu.list.bg , conf_params(st->conf, sect[0], "MenuBG"));
        style_copy_to_rgb(&st->menu.list.fg , conf_params(st->conf, sect[0], "MenuText"));
        style_copy_to_rgb(&st->menu.item.bg , conf_params(st->conf, sect[0], "ItemBG"));
        style_copy_to_rgb(&st->menu.item.fg , conf_params(st->conf, sect[0], "ItemText"));

        if((str = conf_params(st->conf, sect[0], "BarPixmap"))) {
            st->menu.bar.pixmap = str[0];
        }

        if((str = conf_params(st->conf, sect[0], "MenuPixmap"))) {
            st->menu.list.pixmap = str[0];
        }
    }

    /*************************************************************************************/

    if((sect = conf_params(st->conf, NULL, "LineNumbers"))) {
        style_copy_to_rgb(&st->lnums.sa.bg, conf_params(st->conf, sect[0], "Background"));
        style_copy_to_rgb(&st->lnums.sa.fg, conf_params(st->conf, sect[0], "Text"));

        if((str = conf_params(st->conf, sect[0], "Border"))) {
            st->lnums.sep_width = atoi(str[0]);
        }

        if((str = conf_params(st->conf, sect[0], "Pixmap"))) {
            st->lnums.sa.pixmap = str[0];
        }
    }

    /*************************************************************************************/

    if((sect = conf_params(st->conf, NULL, "StatusColours"))) {
        style_copy_to_rgb(&st->status.sa.bg, conf_params(st->conf, sect[0], "BarBG"));
        style_copy_to_rgb(&st->status.sa.fg, conf_params(st->conf, sect[0], "BarText"));

        if((str = conf_params(st->conf, sect[0], "Font"))) {
            st->status.sa.font = str[0];
        }

        if((str = conf_params(st->conf, sect[0], "Pixmap"))) {
            st->status.sa.pixmap = str[0];
        }
    }

    /*************************************************************************************/
}


/************************************************************
 * Helper functions                                         *
 ************************************************************/

static void style_copy_to_vp(STYLE_VP *svp, config *conf, char *sect_name)
{
    char **sect;
    char **str;

    if((sect = conf_params(conf, NULL, sect_name))) {
        style_copy_to_rgb(&svp->sa.bg  , conf_params(conf, sect[0], "Background"));
        style_copy_to_rgb(&svp->sa.fg  , conf_params(conf, sect[0], "Text"));
        style_copy_to_rgb(&svp->sel_sa.bg , conf_params(conf, sect[0], "SelectBG"));
        style_copy_to_rgb(&svp->sel_sa.fg , conf_params(conf, sect[0], "SelectText"));

        if((str = conf_params(conf, sect[0], "Font"))) {
            svp->sa.font = str[0];
        }

        if((str = conf_params(conf, sect[0], "Pixmap"))) {
            svp->sa.pixmap = str[0];
        }

        if((str = conf_params(conf, sect[0], "SelPixmap"))) {
            svp->sel_sa.pixmap = str[0];
        }
    }
}

static void style_copy_to_rgb(STYLE_RGB *rgb, char **str)
{
    memset(rgb, 0, sizeof(STYLE_RGB));

    if(str != NULL) {
        if(str[0] != NULL) rgb->r = atoi(str[0]);
        if(str[1] != NULL) rgb->g = atoi(str[1]);
        if(str[2] != NULL) rgb->b = atoi(str[2]);
    }
}

static int style_ensure_int(char **str)
{
    if(str) {
        if(str[0]) {
            return(atoi(str[0]));
        }
    }

    return(0);
}
