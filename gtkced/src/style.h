#ifndef _STYLE_H
#define _STYLE_H

#define STYLE_DEFAULT_WIN_XPOS    -1
#define STYLE_DEFAULT_WIN_YPOS    -1
#define STYLE_DEFAULT_WIN_WIDTH   640
#define STYLE_DEFAULT_WIN_HEIGHT  480

typedef struct _style           STYLE;
typedef struct _style_attribs   STYLE_ATTRIBS;
typedef struct _style_rgb       STYLE_RGB;
typedef struct _style_vp        STYLE_VP;
typedef struct _style_lnums     STYLE_LNUMS;
typedef struct _style_menu      STYLE_MENU;
typedef struct _style_status    STYLE_STATUS;
typedef struct _style_syntax    STYLE_SYNTAX;

struct _style_rgb
{
    int ignore;

    int r;
    int g;
    int b;
};

struct _style_attribs
{
    int ignore;

    STYLE_RGB bg;
    STYLE_RGB fg;

    char *pixmap;
    char *font;
};

struct _style_syntax
{
    char     *font[5];

    STYLE_RGB cols[5];
    STYLE_RGB match_bg;
    STYLE_RGB match_fg;

//    STYLE_RGB set[3];
//    STYLE_RGB comments;
//    STYLE_RGB strings;
};

struct _style_vp
{
    STYLE_ATTRIBS sa;
    STYLE_ATTRIBS sel_sa;
    STYLE_RGB     syn[5];
};

struct _style_menu
{
    STYLE_ATTRIBS bar;
    STYLE_ATTRIBS list;
    STYLE_ATTRIBS item;
};

struct _style_status
{
    STYLE_ATTRIBS sa;
};

struct _style_lnums
{
    STYLE_ATTRIBS sa;

    int sep_width;
};

struct _style
{
    /* style data */

    char   *ffname;
    config *conf;

    /* main window size */

    int win_x;
    int win_y;
    int win_w;
    int win_h;

    /* viewports */

    STYLE_VP text;  /* main text editing viewport */
    STYLE_VP cmd;   /* command input viewport */
    STYLE_VP msg;   /* message output viewport */

    /* syntax highlighting */

    STYLE_SYNTAX syn;

    /* other widgets */

    STYLE_MENU    menu;
    STYLE_LNUMS   lnums;
    STYLE_STATUS  status;
};

GtkStyle  *style_get_vp_gtk_style(STYLE *st);
STYLE     *style_new(char *filename);
void       style_load(STYLE *st);

#endif /* _STYLE_H */

