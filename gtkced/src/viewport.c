/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2001
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Editor widget, a customized widget designed especially to
 *    interface CED's backend to a GTK frontend.
 *
 *  Last Updated
 *    11th November 2001
 *
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <time.h>

#include <ced/session.h>
#include <ced/region.h>
#include <ced/line.h>
#include <ced/canvas.h>
#include <ced/buffer.h>
#include <ced/utils.h>
#include <ced/callbacks.h>

#include "main.h"
#include "viewport.h"

//#define DEBUG
//#define VP_REDRAW_ALL_ON_CHANGE

#define X_PAD ((vp->font_width / 2) + 2)
#define Y_PAD 2

#define X_PIXELS(x) ((x) * vp->font_width  + X_PAD)
#define Y_PIXELS(y) ((y) * vp->font_height + Y_PAD)
#define W_PIXELS(w) ((w) * vp->font_width)
#define H_PIXELS(h) ((h) * vp->font_height)

#define X_CURSOR(x) ((x - X_PAD) / vp->font_width)
#define Y_CURSOR(y) ((y - Y_PAD) / vp->font_height)
#define W_CURSOR(w) ((w) / vp->font_width)
#define H_CURSOR(h) ((h) / vp->font_height)

enum update_mode
{
    SHOWCURSOR = 1 << 0,
    UPDATE     = 1 << 1,
    NOREGION   = 1 << 2,
    NOBUFF     = 1 << 3
};

enum draw_state
{
    VP_BACKGROUND,
    VP_FOREGROUND,
    VP_SELECTED
};

static void gtk_vp_class_init         (GtkVpClass  *klass);
static void gtk_vp_init               (GtkVp       *vp);
static void gtk_vp_destroy            (GtkObject   *object);
static void gtk_vp_realize            (GtkWidget   *widget);
static void gtk_vp_draw               (GtkWidget   *widget, GdkRectangle    *rect);
static void gtk_vp_size_request       (GtkWidget   *widget, GtkRequisition  *req);
static void gtk_vp_size_allocate      (GtkWidget   *widget, GtkAllocation   *alloc);
static gint gtk_vp_expose             (GtkWidget   *widget, GdkEventExpose  *event);

/* Drawing routines */

static void gtk_vp_draw_text          (GtkWidget *widget, GdkRectangle *rect, GdkGC *gc, uint use_ddbuf);
static void gtk_vp_draw_textraw       (GtkWidget *widget, int lnum, int x, int w, int state, uint use_ddbuf);
static void gtk_vp_draw_line          (GtkWidget *widget, int num, int x, int w, int flags);
static void gtk_vp_draw_solid_rect    (GtkWidget *widget, GdkRectangle *rect, GdkGC *gc, uint use_ddbuf);
static void gtk_vp_draw_hollow_rect   (GtkWidget *widget, GdkRectangle *rect, GdkGC *gc, uint use_ddbuf);
static void gtk_vp_draw_at_coords     (GtkWidget *widget, GdkRectangle *rect);

/* X-Selection handling routines */

static void gtk_vp_selection_handle   (GtkWidget        *widget,
                                       GtkSelectionData *selection_data,
                                       guint            info,
                                       guint            time_stamp);

static void gtk_vp_selection_received (GtkWidget *widget, GtkSelectionData *selection_data, guint time);
static void gtk_vp_get_x_selection    (GtkWidget *widget);
static void gtk_vp_claim_x_selection  (GtkWidget *widget);

/* X-Input events */

static gint gtk_vp_keypress           (GtkWidget   *widget, GdkEventKey     *event);
static gint gtk_vp_keyrelease         (GtkWidget   *widget, GdkEventKey     *event);
static gint gtk_vp_motion_notify      (GtkWidget   *widget, GdkEventMotion  *event);
static gint gtk_vp_button_press       (GtkWidget   *widget, GdkEventButton  *event);
static gint gtk_vp_button_release     (GtkWidget   *widget, GdkEventButton  *event);

/* Notify and callback handlers */

static void gtk_vp_link_added_notify(GtkWidget *widget);
static void gtk_vp_link_deleted_notify(GtkWidget *widget);
static void gtk_vp_file_attribs_changed_notify(GtkWidget *widget);
static void gtk_vp_buffer_renamed_notify(GtkWidget *widget);
static void gtk_vp_buffer_destroyed_notify(GtkWidget *widget);
static void gtk_vp_lines_changed_notify(GtkWidget *widget, ulong y, ulong n);
static void gtk_vp_lines_inserted_notify(GtkWidget *widget, ulong y, ulong n);
static void gtk_vp_lines_deleted_notify(GtkWidget *widget, ulong y, ulong n);
static void gtk_vp_bring_to_front_callback(GtkWidget *widget);
static void gtk_vp_offset_changed_notify (GtkWidget *widget);
static void gtk_vp_cursor_move_notify(GtkWidget *widget);
static void gtk_vp_region_notify(GtkWidget *widget);
static gint gtk_vp_leave_notify_event(GtkWidget *widget, GdkEventCrossing *event);
static gint gtk_vp_enter_notify_event(GtkWidget *widget, GdkEventCrossing *event);

/* Miscellaneous */

static void gtk_vp_button_event(GtkWidget *widget, GdkEventButton *event, uint upstroke);
static void gtk_vp_key_event(GtkWidget* widget, GdkEventKey *event, uint upstroke);
static void gtk_vp_message(GtkWidget *widget, char *message, int beep);
static void gtk_vp_return_event(GtkWidget *widget, ulong lnum);
static void gtk_vp_set_mouse_position(GtkWidget *widget, int x, int y);
static void gtk_vp_get_mouse_position(GtkWidget *widget, int *x, int *y);
static void gtk_vp_show_mouse(GtkWidget *widget);
static void gtk_vp_hide_mouse(GtkWidget *widget);
static void gtk_vp_invalidate_region(GtkWidget *widget);
static void gtk_vp_validate_region(GtkWidget *widget);
static void gtk_vp_toggle_line_numbers(GtkWidget *widget);
static void gtk_vp_toggle_highlighting(GtkWidget *widget);
static void gtk_vp_toggle_mouse_binding(GtkWidget *widget);
static void gtk_vp_region_echo(GtkWidget *widget);
static void gtk_vp_redraw_all(GtkWidget *widget, int use_buf);
static void gtk_vp_update_segment(GtkWidget *widget, uint y, uint n);
static void gtk_vp_setpad_abs(GtkWidget *widget, uint  x, ulong  y);
static void gtk_vp_cursor_move_abs(GtkWidget *widget, int x, int y);
static void gtk_vp_calc_area(GtkWidget *widget);
static int  gtk_vp_convert_states(int gdkstate);
static int  gtk_vp_font_is_monospaced(GdkFont *font);
static long gtk_vp_x_limits(GtkWidget *widget, long x);
static long gtk_vp_y_limits(GtkWidget *widget, long y);
static gint gtk_vp_focus_in(GtkWidget *widget, GdkEventFocus *event);
static gint gtk_vp_focus_out(GtkWidget *widget, GdkEventFocus *event);

/* NEW */

static void gtk_vp_set_syntax(GtkVp *vp, int setnum, GdkGC *gc, GdkFont *font);
static void gtk_vp_style_set (GtkWidget *widget, GtkStyle  *prev_style);
static void gtk_vp_set_syntax_style_internal(GtkWidget *widget, uint setnum, VP_FONT_STYLE vpfs, uint redraw);
static void gtk_vp_clip_coords(GtkWidget *widget, int *x, int *y);


static void gtk_vp_clear_area(GtkWidget *widget, GdkRectangle *rect, uint use_ddbuf);
static GdkGC *gtk_vp_create_bg_gc(GtkWidget *widget);

/* Module variables */

static GtkWidgetClass *parent_class = NULL;


/********** NEW OCT 20th 2001 *****************************************/

// FIXME - needs tidying up

static void gtk_vp_style_set(GtkWidget *widget, GtkStyle  *prev_style)
{
    GtkVp   *vp;

#ifdef DEBUG
    fprintf(stderr, "[gtkvp] style_set (prev: %p)\n", prev_style);
#endif

    if(prev_style) {
        vp = GTK_VP(widget);
        vp->fg_gc = widget->style->fg_gc[GTK_STATE_NORMAL];
//        vp->bg_gc = widget->style->bg_gc[GTK_STATE_NORMAL];
        vp->bg_gc = gtk_vp_create_bg_gc(widget);
        vp->font_loaded = 0;

        gtk_style_set_background(widget->style, widget->window, GTK_STATE_NORMAL);
        gdk_window_clear(widget->window);
        gtk_vp_calc_area(widget);
    }
}

/**********************************************************************/

guint gtk_vp_get_type()
{
    static guint vp_type = 0;

    if(!vp_type) {
        GtkTypeInfo vp_info =
        {
            "GtkVp",
            sizeof(GtkVp),
            sizeof(GtkVpClass),
            (GtkClassInitFunc) gtk_vp_class_init,
            (GtkObjectInitFunc) gtk_vp_init,
            (GtkArgSetFunc) NULL,
            (GtkArgGetFunc) NULL
        };

        vp_type = gtk_type_unique(gtk_widget_get_type(), &vp_info);
    }

    return(vp_type);
}

static gint gtk_vp_prox_in(GtkWidget *widget, GdkEventProximity *event)
{
    printf("prox in..\n");

    return(FALSE);
}

static gint gtk_vp_prox_out(GtkWidget *widget, GdkEventProximity *event)
{
    printf("prox in..\n");

    return(FALSE);
}

static void gtk_vp_class_init(GtkVpClass *class)
{
    GtkObjectClass *object_class;
    GtkWidgetClass *widget_class;

    object_class = (GtkObjectClass*) class;
    widget_class = (GtkWidgetClass*) class;
    parent_class = gtk_type_class(gtk_widget_get_type());

    object_class->destroy = gtk_vp_destroy;
    widget_class->style_set = gtk_vp_style_set;
    widget_class->expose_event = gtk_vp_expose;
    widget_class->realize = gtk_vp_realize;
    widget_class->key_press_event = gtk_vp_keypress;
    widget_class->key_release_event = gtk_vp_keyrelease;
    widget_class->button_press_event = gtk_vp_button_press;
    widget_class->button_release_event = gtk_vp_button_release;
    widget_class->size_allocate = gtk_vp_size_allocate;
    widget_class->motion_notify_event = gtk_vp_motion_notify;
    widget_class->leave_notify_event = gtk_vp_leave_notify_event;
    widget_class->enter_notify_event = gtk_vp_enter_notify_event;
    widget_class->selection_received = gtk_vp_selection_received;
    widget_class->selection_get = gtk_vp_selection_handle;
    widget_class->focus_in_event = gtk_vp_focus_in;
    widget_class->focus_out_event = gtk_vp_focus_out;
    widget_class->size_request = gtk_vp_size_request;

    widget_class->proximity_in_event = gtk_vp_prox_in;
    widget_class->proximity_out_event = gtk_vp_prox_out;
}

/*
 * Handles a resize request.  This function then calls a function to
 * recalculate various text display components.
 */

static void gtk_vp_size_request(GtkWidget *widget, GtkRequisition *req)
{
    GtkVp *vp;
    GdkFont *font;

    vp = GTK_VP(widget);

#ifdef DEBUG
    fprintf(stderr, "[gtkvp] size request\n");
#endif

    if(!vp->font_loaded) {
        font = GTK_WIDGET(vp)->style->font;

        if(!gtk_vp_font_is_monospaced(font)) {
            font = gdk_font_load("fixed");
            fprintf(stderr, "[viewport] gtk_vp_init: specified font is not monospaced.\n\r");
        }

        vp->font_descent = font->descent + 1;
        vp->font_height = font->ascent + vp->font_descent + 1;
        vp->font_width = gdk_char_width(font, ' ');
        vp->font_loaded = 1;
        vp->font = font;
    }

    req->height = vp->font_height;
}

static void gtk_vp_size_allocate(GtkWidget* widget, GtkAllocation* alloc)
{
    GtkVp *vp;
    uint  w;
    uint  h;
    uint  x;
    uint  y;
    uint  d;

#ifdef DEBUG
    printf("[gtkvp] size allocate\n");
#endif

    g_return_if_fail (widget != NULL);
    g_return_if_fail (GTK_IS_VP(widget));
    g_return_if_fail (alloc != NULL);

    widget->allocation = *alloc;

    if (GTK_WIDGET_REALIZED(widget)) {
        vp = GTK_VP(widget);

       /*
        * We check the current geometry because we don't want
        * to do anything if it matches.  Sometimes GTK sends
        * out resize events when it doesn't need to !
        */

        gdk_window_get_geometry(widget->window, &x, &y, &w, &h, &d);

        if(w != alloc->width || h != alloc->height ||
           x != alloc->x || y != alloc->y) {
            gdk_window_move_resize(widget->window,
                                   alloc->x, alloc->y,
                                   alloc->width, alloc->height);

            gtk_vp_calc_area(widget);
        }
    }
}

static int gtk_vp_font_is_monospaced(GdkFont *font)
{
    int width;
    int last_w;
    int i;

    last_w = gdk_char_width(font, ' ');

    for(i=1; i<127; i++) {
        width = gdk_char_width(font, i);

        if(width != 0 && width != last_w) {
            return(0);
        }
    }

    return(1);
}

static void gtk_vp_set_syntax(GtkVp *vp, int setnum, GdkGC *gc, GdkFont *font)
{
    vp->syn_rgbs[setnum] = gc;
    vp->syn_fonts[setnum] = font;
}

void gtk_vp_set_syntax_styles(GtkWidget *widget, VP_FONT_STYLE vpfs[5])
{
    int i;

    for(i=0; i < 5; i++) {
        gtk_vp_set_syntax_style_internal(widget, i, vpfs[i], 0);
    }

    gtk_vp_redraw_all(widget, 0);
}

void gtk_vp_set_syntax_style(GtkWidget *widget, uint setnum, VP_FONT_STYLE vpfs)
{
    gtk_vp_set_syntax_style_internal(widget, setnum, vpfs, 1);
}

static void gtk_vp_set_syntax_style_internal(GtkWidget *widget, uint setnum, VP_FONT_STYLE vpfs, uint redraw)
{
    GtkVp   *vp;
    GdkFont *font;
    GdkGC    *gc;
    GdkColor col;
    char     *err;

    vp = GTK_VP(widget);

    /*
     * First, set up the chosen colours for the
     * specified syntax set number
     */

    col.red   = vpfs.red   << 8;
    col.green = vpfs.green << 8;
    col.blue  = vpfs.blue  << 8;

    gc = gdk_gc_new(widget->window);
    gdk_color_alloc(gdk_colormap_get_system(), &col);
    gdk_gc_set_foreground(gc, &col);

    err = NULL;

    if(vpfs.fontname && strlen(vpfs.fontname) > 0) {
        if((font = gdk_font_load(vpfs.fontname)) == NULL) {
            err = "not found.";
        } else if(!gtk_vp_font_is_monospaced(font)) {
            err = "not monospaced.";
        } else if(gdk_char_width(font, ' ') != vp->font_width) {
            err = "size doesn't match main font.";
        }

        if(err) {
            font = gdk_font_ref(vp->font);
            fprintf(stderr, "[viewport] error loading set %d font: %s\n\r", setnum, err);
        }
    } else {
        font = gdk_font_ref(vp->font);
    }

    gtk_vp_set_syntax(vp, setnum, gc, font);

    if(redraw) {
        gtk_vp_redraw_all(widget, 0);
    }
}

void gtk_vp_set_block_match_style(GtkWidget *widget, GdkColor *bg, GdkColor *fg)
{
    GtkVp *vp;
    GdkGC *bg_gc;
    GdkGC *fg_gc;

    vp = GTK_VP(widget);

    /*
     * First, set up the chosen colours for the
     * specified syntax set number
     */

    fg_gc = gdk_gc_new(widget->window);
    bg_gc = gdk_gc_new(widget->window);

    gdk_color_alloc(gdk_colormap_get_system(), bg);
    gdk_color_alloc(gdk_colormap_get_system(), fg);

    gdk_gc_set_foreground(bg_gc, bg);
    gdk_gc_set_foreground(fg_gc, fg);

    vp->fg_bm_gc = fg_gc;
    vp->bg_bm_gc = bg_gc;

    gtk_vp_redraw_all(widget, 0);
}

static void gtk_vp_init(GtkVp *vp)
{
    GdkPixmap  *pix_p;
    GdkPixmap  *pix_m;
    GdkColor   bg;
    GdkColor   fg;
    char       cbits[] = { 0x00, 0x00, 0x00, 0x00 };

    GTK_WIDGET_SET_FLAGS (vp, GTK_CAN_FOCUS);
    GTK_WIDGET_UNSET_FLAGS (vp, GTK_NO_WINDOW);

    vp->policy = GTK_UPDATE_CONTINUOUS;
    vp->ddbuf = NULL;
    vp->fg_gc = NULL;
    vp->bg_gc = NULL;
    vp->xcursor = 0;
    vp->ycursor = 0;
    vp->xoffset = 0;
    vp->yoffset = 0;

    /* Ouch ! Messy */

    vp->reg.x = 0;
    vp->reg.y = 0;
    vp->reg.height = 0;
    vp->reg.width = 0;

    vp->r_state = 0;
    vp->r_mode = 'n';
    vp->rx_1 = 0;
    vp->ry_1 = 0;
    vp->rx_2 = 0;
    vp->ry_2 = 0;
    vp->reg_valid = 0;

    gdk_color_white(gdk_colormap_get_system(), &bg);
    gdk_color_black(gdk_colormap_get_system(), &fg);

    pix_p = gdk_bitmap_create_from_data(NULL, cbits, 2, 2);
    pix_m = gdk_bitmap_create_from_data(NULL, cbits, 2, 2);

    vp->inv_cursor = gdk_cursor_new_from_pixmap(pix_p, pix_m, &fg, &bg, 0, 0);

    gdk_pixmap_unref(pix_p);
    gdk_pixmap_unref(pix_m);

    gtk_selection_add_target (GTK_WIDGET(vp),
                              GDK_SELECTION_PRIMARY,
                              GDK_SELECTION_TYPE_STRING,
                              1);
}

GtkWidget *gtk_vp_new(canvas *canv, session *se)
{
    GtkVp     *vp;

    vp = gtk_type_new(gtk_vp_get_type());
    vp->struct_ptr = NULL;
    vp->canv = canv;
    vp->se   = se;
    vp->cbacks = callbacks_new("viewport");

    callbacks_add(vp->cbacks, "message"         , CALLBACK_REF(vp->func_message));
    callbacks_add(vp->cbacks, "return_event"    , CALLBACK_REF(vp->func_return_event));
    callbacks_add(vp->cbacks, "link_added"      , CALLBACK_REF(vp->func_link_added));
    callbacks_add(vp->cbacks, "link_deleted"    , CALLBACK_REF(vp->func_link_deleted));
    callbacks_add(vp->cbacks, "lines_changed"   , CALLBACK_REF(vp->func_lines_changed));
    callbacks_add(vp->cbacks, "lines_deleted"   , CALLBACK_REF(vp->func_lines_deleted));
    callbacks_add(vp->cbacks, "lines_inserted"  , CALLBACK_REF(vp->func_lines_inserted));
    callbacks_add(vp->cbacks, "attribs_changed" , CALLBACK_REF(vp->func_attribs_changed));
    callbacks_add(vp->cbacks, "buffer_renamed"  , CALLBACK_REF(vp->func_buffer_renamed));
    callbacks_add(vp->cbacks, "buffer_destroyed", CALLBACK_REF(vp->func_buffer_destroyed));
    callbacks_add(vp->cbacks, "offset_changed"  , CALLBACK_REF(vp->func_offset_changed));
    callbacks_add(vp->cbacks, "cursor_moved"    , CALLBACK_REF(vp->func_cursor_moved));
    callbacks_add(vp->cbacks, "region_changed"  , CALLBACK_REF(vp->func_region_changed));
    callbacks_add(vp->cbacks, "region_echo"     , CALLBACK_REF(vp->func_region_echo));
    callbacks_add(vp->cbacks, "redraw_all"      , CALLBACK_REF(vp->func_redraw_all));
    callbacks_add(vp->cbacks, "update_all"      , CALLBACK_REF(vp->func_update_all));
    callbacks_add(vp->cbacks, "get_selection"   , CALLBACK_REF(vp->func_get_selection));
    callbacks_add(vp->cbacks, "claim_selection" , CALLBACK_REF(vp->func_claim_selection));
    callbacks_add(vp->cbacks, "sync_cursor"     , CALLBACK_REF(vp->func_sync_cursor));
    callbacks_add(vp->cbacks, "update_line_numbers"   , CALLBACK_REF(vp->func_update_line_numbers));
    callbacks_add(vp->cbacks, "line_numbers_toggled"  , CALLBACK_REF(vp->func_line_numbers_toggled));
    callbacks_add(vp->cbacks, "mouse_binding_toggled" , CALLBACK_REF(vp->func_mouse_binding_toggled));
    callbacks_add(vp->cbacks, "highlighting_toggled"  , CALLBACK_REF(vp->func_highlighting_toggled));
    callbacks_add(vp->cbacks, "bring_to_front"        , CALLBACK_REF(vp->func_bring_to_front));

    canv->struct_ptr = vp;

    canv_set_callback(canv, "offset_changed"        , gtk_vp_offset_changed_notify);
    canv_set_callback(canv, "cursor_moved"          , gtk_vp_cursor_move_notify);
    canv_set_callback(canv, "region_changed"        , gtk_vp_region_notify);
    canv_set_callback(canv, "region_echo"           , gtk_vp_region_echo);
    canv_set_callback(canv, "redraw_all"            , gtk_vp_redraw_all);
    canv_set_callback(canv, "sync_cursor"           , gtk_vp_sync_output_cursor);
    canv_set_callback(canv, "link_added"            , gtk_vp_link_added_notify);
    canv_set_callback(canv, "link_deleted"          , gtk_vp_link_deleted_notify);
    canv_set_callback(canv, "buffer_renamed"        , gtk_vp_buffer_renamed_notify);
    canv_set_callback(canv, "buffer_destroyed"      , gtk_vp_buffer_destroyed_notify);
    canv_set_callback(canv, "lines_changed"         , gtk_vp_lines_changed_notify);
    canv_set_callback(canv, "lines_deleted"         , gtk_vp_lines_deleted_notify);
    canv_set_callback(canv, "lines_inserted"        , gtk_vp_lines_inserted_notify);
    canv_set_callback(canv, "attribs_changed"       , gtk_vp_file_attribs_changed_notify);
    canv_set_callback(canv, "message"               , gtk_vp_message);
    canv_set_callback(canv, "get_selection"         , gtk_vp_get_x_selection);
    canv_set_callback(canv, "claim_selection"       , gtk_vp_claim_x_selection);
    canv_set_callback(canv, "return_event"          , gtk_vp_return_event);
    canv_set_callback(canv, "line_numbers_toggled"  , gtk_vp_toggle_line_numbers);
    canv_set_callback(canv, "mouse_binding_toggled" , gtk_vp_toggle_mouse_binding);
    canv_set_callback(canv, "highlighting_toggled"  , gtk_vp_toggle_highlighting);
    canv_set_callback(canv, "bring_to_front"        , gtk_vp_bring_to_front_callback);

    return(GTK_WIDGET(vp));
}

static void gtk_vp_destroy(GtkObject *object)
{
    GtkVp *vp;
    int   i;

    g_return_if_fail(object != NULL);
    g_return_if_fail(GTK_IS_VP(object));

    vp = GTK_VP(object);

    for(i=0; i < 5; i++) {
        gdk_gc_unref(vp->syn_rgbs[i]);
    }

    if(vp->fg_bm_gc) gdk_gc_unref(vp->fg_bm_gc);
    if(vp->bg_bm_gc) gdk_gc_unref(vp->bg_bm_gc);

    gdk_cursor_destroy(vp->inv_cursor);

    callbacks_free(vp->cbacks);

    if(GTK_OBJECT_CLASS(parent_class)->destroy) {
        (*GTK_OBJECT_CLASS(parent_class)->destroy)(object);
    }
}

void vp_reinit(GtkWidget *widget)
{
//    gtk_vp_realize(widget);
}

static void gtk_vp_realize(GtkWidget *widget)
{
    GtkVp   *vp;
    GdkWindowAttr attributes;
    gint attributes_mask;
    gint i;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    SELECTION_FLAG = 0;
    GM_FLAG = 0;

    vp = GTK_VP(widget);
    GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);

    attributes.window_type = GDK_WINDOW_CHILD;
    attributes.x = widget->allocation.x;
    attributes.y = widget->allocation.y;
    attributes.width = widget->allocation.width;
    attributes.height = widget->allocation.height;
    attributes.wclass = GDK_INPUT_OUTPUT;
    attributes.event_mask = gtk_widget_get_events(widget);
    attributes.event_mask |= (GDK_EXPOSURE_MASK |
                              GDK_BUTTON_PRESS_MASK |
                              GDK_BUTTON_RELEASE_MASK |
                              GDK_POINTER_MOTION_MASK |
                              GDK_KEY_RELEASE_MASK |
                              GDK_KEY_PRESS_MASK |
                              GDK_FOCUS_CHANGE_MASK |
                              GDK_ENTER_NOTIFY_MASK |
                              GDK_LEAVE_NOTIFY_MASK);

    attributes.visual = gtk_widget_get_visual(widget);
    attributes.colormap = gtk_widget_get_colormap(widget);

    attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;

    widget->window = gdk_window_new(widget->parent->window, &attributes, attributes_mask);
    gdk_window_set_user_data(widget->window, widget);

    widget->style = gtk_style_attach(widget->style, widget->window);
    gtk_style_set_background(widget->style, widget->window, GTK_STATE_NORMAL);

    vp->fg_gc = widget->style->fg_gc[GTK_STATE_NORMAL];
//    vp->bg_gc = widget->style->bg_gc[GTK_STATE_NORMAL];
    vp->bg_gc = gtk_vp_create_bg_gc(widget);
    vp->sel_gc = gtk_vp_create_sel_gc(widget);

    /*
     * Check the specified font is monospaced, if not
     * fall back to system fixed font.
     */

    for(i=0; i < 5; i++) {
        gtk_vp_set_syntax(vp, i, gdk_gc_ref(vp->fg_gc), vp->font);
    }

    gtk_vp_calc_area(widget);
    gtk_vp_enable_highlighting(widget);
    gtk_vp_redraw_all(widget, 0);
}

GdkGC *gtk_vp_create_sel_gc(GtkWidget *widget)
{
    GdkGCValues values;

    if(widget->style->bg_pixmap[GTK_STATE_SELECTED]) {
        values.tile = widget->style->bg_pixmap[GTK_STATE_SELECTED];
        values.fill = GDK_TILED;

        return(gdk_gc_new_with_values(widget->window, &values, GDK_GC_FILL | GDK_GC_TILE));
    } else {
        return(widget->style->bg_gc[GTK_STATE_SELECTED]);
    }
}

static GdkGC *gtk_vp_create_bg_gc(GtkWidget *widget)
{
    GdkGCValues values;

    if(widget->style->bg_pixmap[GTK_STATE_NORMAL]) {
        values.tile = widget->style->bg_pixmap[GTK_STATE_NORMAL];
        values.fill = GDK_TILED;

        return(gdk_gc_new_with_values(widget->window, &values, GDK_GC_FILL | GDK_GC_TILE));
    } else {
        return(widget->style->bg_gc[GTK_STATE_NORMAL]);
    }
}


void gtk_vp_disable_highlighting(GtkWidget *widget)
{
    GtkVp    *vp;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    vp->highlight = 0;
    gtk_vp_redraw_all(widget, 0);
}

void gtk_vp_enable_highlighting(GtkWidget *widget)
{
    GtkVp    *vp;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    vp->highlight = 1;
    gtk_vp_redraw_all(widget, 0);
}

/*
 * This is called when part of the screen is exposed.  This function called
 * gtk_vp_draw with the rectangle that needs redrawing, far more efficient
 * than an entire screen redraw :)
 */

static gint gtk_vp_expose(GtkWidget *widget, GdkEventExpose *event)
{
    GtkVp       *vp;
    GdkRectangle r;
    GdkRectangle *rect;
    int x1,y1;
    int x2,y2;

#ifdef DEBUG
    printf("[gtk_vp_expose]\n");
#endif

    vp = GTK_VP(widget);
    rect = &event->area;

    x1 = rect->x;
    y1 = rect->y;
    x2 = rect->width + x1;
    y2 = rect->height + y1;

    x1 = X_CURSOR(x1);
    x2 = X_CURSOR(x2);
    y1 = Y_CURSOR(y1);
    y2 = Y_CURSOR(y2);

    /*
     * For some odd reason, expose events for parts of the pad which have just been
     * covered from a pad resize are being generated by x here.  This could be a
     * side effect from something though.. investigate further, but for now, ensure
     * pad boundaries are respected.
     */

    x1 = min(x1, vp->maxcols - 1);
    y1 = min(y1, vp->maxrows - 1);
    x2 = min(x2, vp->maxcols - 1);
    y2 = min(y2, vp->maxrows - 1);

    // printf("x1: %d x2: %d y1: %d y2: %d\n", x1, x2, y1, y2);

    r.x = x1;
    r.y = y1;
    r.width = x2 - x1 + 1;
    r.height = y2 - y1 + 1;

    gtk_vp_draw_text(widget, &r, vp->bg_gc, 0);

    return(TRUE);
}

/*
 * Draws the current display buffer to the screen.  Only redraws what is
 * neccessary to aid efficiency.  If NULL is passed as the rectangle, then
 * the whole backbuffer is draw to the window, but that should be avoided as
 * it slows down the xserver.
 */

static void gtk_vp_draw(GtkWidget *widget, GdkRectangle *rect)
{
    GtkVp *vp;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);

#ifdef DEBUG
    printf("[gtk_vp_draw]\n");
#endif

    /*
     * Redraw appropriate part of screen.  If rect is NULL, then the whole
     * back buffer will be redrawn to the screen.
     */

    if(rect) {
        gdk_draw_pixmap(widget->window,
                        vp->bg_gc,
                        vp->ddbuf,
                        rect->x,
                        rect->y,
                        rect->x,
                        rect->y,
                        rect->width,
                        rect->height);

    }

}

/*
 * When the pad is resized, various variables must be recalculated.  The maxrows
 * and maxcols need changing to reflect the new screen size and the backbuffer
 * pixmap needs resizing.
 */

static void gtk_vp_calc_area(GtkWidget *widget)
{
    GtkVp        *vp;
    canvas        *canv;
    int           h;
    int           w;

#ifdef DEBUG
    printf("[gtk_vp_calc_area]\n");
#endif

    vp = GTK_VP(widget);
    canv = vp->canv;

    gdk_window_get_size(widget->window, &w, &h);

    if(vp->ddbuf) {
        gdk_pixmap_unref(vp->ddbuf);
    }

    vp->ddbuf = gdk_pixmap_new(widget->window, w, h, -1);
    vp->maxcols = W_CURSOR(w);
    vp->maxrows = H_CURSOR(h);
    vp->xcursor = min(vp->xcursor, vp->maxcols - 1);
    vp->ycursor = min(vp->ycursor, vp->maxrows - 1);

    canv_set_cols(canv, vp->maxcols);
    canv_set_rows(canv, vp->maxrows);
}

static void gtk_vp_lines_changed_notify(GtkWidget *widget, ulong y, ulong n)
{
    canvas        *canv;
    GtkVp         *vp;
    ulong         ymin;
    ulong         ymax;
    GdkRectangle  r;

#ifdef DEBUG
    printf("[gtk_vp_lines_changed_notify] %lu %lu\n", y, n);
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;

    ymin = vp->yoffset;
    ymax = vp->yoffset + vp->maxrows;

    if(y < ymax && (y+n) > ymin) {
        if(y < ymin) {
            n -= (ymin - y);
            y = max(ymin, y);
        }

        n = min(ymax - y, n);

        if(n > 0) {
            r.x = 0;
            r.y = y - ymin;
            r.width = vp->maxcols;
            r.height = n;

            gtk_vp_draw_text(widget, &r, vp->fg_gc, 1);
        }

        vp->func_update_line_numbers(vp->struct_ptr);
    }

#ifdef VP_REDRAW_ALL_ON_CHANGE
    gtk_vp_redraw_all(widget, 1);
#endif

    vp->func_lines_changed(vp->struct_ptr, y, n);
}

static void gtk_vp_lines_deleted_notify(GtkWidget *widget, ulong y, ulong n)
{
    GtkVp  *vp;
    canvas *canv;
    ulong  ymin;
    ulong  ymax;

#ifdef DEBUG
    printf("[gtk_vp_lines_deleted_notify]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;

    ymin = vp->yoffset;
    ymax = vp->yoffset + vp->maxrows;

    if(y < ymax) {
        y = max(ymin, y);

        gtk_vp_update_segment(widget, y - ymin, ymax - y);
    }

    vp->func_update_line_numbers(vp->struct_ptr);
    vp->func_lines_deleted(vp->struct_ptr, y, n); /* fixme */
}

static void gtk_vp_lines_inserted_notify(GtkWidget *widget, ulong y, ulong n)
{
    GtkVp  *vp;
    canvas *canv;
    ulong  ymin;
    ulong  ymax;

#ifdef DEBUG
    printf("[gtk_vp_lines_inserted_notify] %lu %lu\n", y, n);
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;

    ymin = vp->yoffset;
    ymax = vp->yoffset + vp->maxrows;

    if(y < ymax) {
        y = max(ymin, y);

        gtk_vp_update_segment(widget, y - ymin, ymax - y);
    }

    vp->func_update_line_numbers(vp->struct_ptr);
    vp->func_lines_inserted(vp->struct_ptr, y, n);
}

static void gtk_vp_update_segment(GtkWidget *widget, uint y, uint n)
{
    GtkVp  *vp;
    canvas *canv;
    uint   ymax;
    uint   i;

#ifdef DEBUG
    printf("[gtk_vp_update_segment] y: %u, n: %u\n", y, n);
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;
    ymax = vp->maxrows;

    if(y > ymax) {
        //printf("update_segment: invalid y position\n");
        return;
    }

    n = min(ymax - y, n);

    if(n > 0) {
        for(i=y; i < (y+n); i++) {
            gtk_vp_draw_line(widget, i, 0, vp->maxcols, SHOWCURSOR|UPDATE|NOBUFF);
        }
    }
}

static void gtk_vp_redraw_all(GtkWidget *widget, int use_buf)
{
    GtkVp          *vp;
    canvas         *canv;
    GdkRectangle   r;

#ifdef DEBUG
    printf("[gtk_vp_redraw_all]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;

    //printf("maxrows set to %d\n", vp->maxrows);

    r.x = 0;
    r.y = 0;
    r.width = vp->maxcols;
    r.height = vp->maxrows;

    gtk_vp_draw_text(widget, &r, vp->fg_gc, use_buf);
    vp->func_redraw_all(vp->struct_ptr);
}

static void gtk_vp_invalidate_region(GtkWidget *widget)
{
    GtkVp          *vp;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);

    vp->rx_1 = 0;
    vp->rx_2 = 0;
    vp->ry_1 = 0;
    vp->ry_2 = 0;

    vp->reg_valid = 0;
}

static void gtk_vp_validate_region(GtkWidget *widget)
{
    GtkVp          *vp;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);

    vp->reg_valid = 1;
}

static void gtk_vp_region_echo(GtkWidget *widget)
{
    GtkVp          *vp;
    canvas         *canv;

#ifdef DEBUG
    //printf("[gtk_vp_region_echo]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;

    if(canv_region_is_defined(canv)) {
        if(vp->r_state == 0) {
            vp->r_state = 1;
            gtk_vp_invalidate_region(widget);
            gtk_vp_region_notify(widget);
        } else {
            gtk_vp_region_notify(widget);
        }
    }

    vp->func_region_echo(vp->struct_ptr);
}

static void gtk_vp_region_notify(GtkWidget *widget)
{
    GtkVp          *vp;
    canvas         *canv;
    region         *reg;
    regtype        rt_last;
    GdkRectangle   last;
    GdkRectangle   r;
    int            lx, ly;
    uint           nx, sx, ox;
    ulong          ny, sy, oy;
    int            y_start = 0;
    int            y_end   = 0;
    int            x_start = 0;
    int            x_end   = 0;

#ifdef DEBUG
    //printf("[gtk_vp_region_notify]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;
    reg = canv_get_region(canv);

    /* these are the last region coordinates */

    lx = gtk_vp_x_limits(widget, vp->rx_1);
    ly = gtk_vp_y_limits(widget, vp->ry_1);
    ox = gtk_vp_x_limits(widget, vp->rx_2);
    oy = gtk_vp_y_limits(widget, vp->ry_2);

    rt_last = vp->rt_last;

    /* these are the new coordinates */

    region_set_end_coords(reg, vp->xoffset + vp->xcursor, vp->yoffset + vp->ycursor);
    region_get_coords(reg, &sx, &sy, &nx, &ny);

    /* store them */

    vp->rx_1 = sx;
    vp->ry_1 = sy;
    vp->rx_2 = nx;
    vp->ry_2 = ny;
    vp->rt_last = region_get_type(reg);

    /* clip to current display */

    sx = gtk_vp_x_limits(widget, sx);
    sy = gtk_vp_y_limits(widget, sy);
    nx = gtk_vp_x_limits(widget, nx);
    ny = gtk_vp_y_limits(widget, ny);

    /* if the region is not to be echo'd, return */

    if(vp->r_state == 0) {
        return;
    }

    last.y = min(oy, ly);
    last.height = diff(oy, ly) + 1;

    if(rt_last == RT_RECTANGULAR) {
        last.x = min(ox, lx);
        last.width = diff(ox, lx);
    }

    if(rt_last == RT_NORMAL) {
        last.x = 0;
        last.width = vp->maxcols;
    }

    if(sx != lx || sy != ly || region_get_type(reg) != rt_last || !vp->reg_valid) {
        gtk_vp_draw_text(widget, &last, vp->bg_gc, 1);
        gtk_vp_validate_region(widget);
        ox = sx;
        oy = sy;
    }

    /*
     * if the region has been undefined, then reset r_state, also
     * remembering to blank the old region if r_state was previously set.
     */

    if(canv->region_defined == 0) {
        if(vp->r_state == 1) {
            vp->r_state = 0;
            gtk_vp_draw_text(widget, &last, vp->bg_gc, 1);
            vp->rx_1 = 0;
            vp->ry_1 = 0;
            vp->rx_2 = 0;
            vp->ry_2 = 0;
        }

        gtk_vp_show_cursor(widget);

        return;
    }

    if(oy != ny) {
        if(oy >= sy && ny <= sy) { y_start = ny; y_end = oy; }
        if(oy <= sy && ny >= sy) { y_start = oy; y_end = ny; }
        if(oy >= sy && ny >= sy) { y_start = min(oy, ny)+1; y_end = max(oy, ny);   }
        if(oy <= sy && ny <= sy) { y_start = min(oy, ny);   y_end = max(oy, ny)-1; }

        if(reg->rt == RT_RECTANGULAR) {
            x_start = smallest(sx, ox, nx);
            x_end   = largest(sx, ox, nx);
        }

        if(reg->rt == RT_NORMAL) {
            x_start = 0;
            x_end = vp->maxcols;

            if(oy >= sy && ny >= sy) y_start -= 1;
            if(oy <= sy && ny <= sy) y_end += 1;
        }

        r.x = x_start;
        r.y = y_start;
        r.width = x_end - x_start;
        r.height = (y_end - y_start) + 1;

        gtk_vp_draw_text(widget, &r, vp->fg_gc, 1);
    }

    if(ox != nx) {
        if(ox >= sx && nx <= sx) { x_start = nx; x_end = ox; }
        if(ox <= sx && nx >= sx) { x_start = ox; x_end = nx; }
        if(ox >= sx && nx >= sx) { x_start = min(ox, nx); x_end = max(ox, nx); }
        if(ox <= sx && nx <= sx) { x_start = min(ox, nx); x_end = max(ox, nx); }

        y_start = smallest(sy, oy, ny);
        y_end   = largest(sy, oy, ny);

        if(x_end > x_start) {
            r.x = x_start;
            r.y = y_start;
            r.width = x_end - x_start;
            r.height = (y_end - y_start) + 1;

            gtk_vp_draw_text(widget, &r, vp->fg_gc, 1);
        }
    }

    vp->func_region_changed(vp->struct_ptr);
}

/*
 * This is the same as gtk_vp_draw, except the coordinates in the rectangle
 * are text coordinates.  This makes it easier to specify areas of text to
 * be redrawn.
 */

static void gtk_vp_draw_at_coords(GtkWidget *widget, GdkRectangle *rect)
{
    GtkVp       *vp;
    GdkRectangle r;

#ifdef DEBUG
//  printf("[gtk_vp_draw_at_coords]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);

    r.x = X_PIXELS(rect->x);
    r.y = Y_PIXELS(rect->y);
    r.width = W_PIXELS(rect->width);
    r.height = H_PIXELS(rect->height);

    gtk_vp_draw(widget, &r);
}

/*
 * Clears an area of the window at the text coordinates specified,
 * maintaining the windows style.
 */

static void gtk_vp_clear_area(GtkWidget *widget, GdkRectangle *rect, uint use_ddbuf)
{
    GtkVp      *vp;
    GdkPixmap  *draw;

#ifdef DEBUG
    printf("[gtk_vp_clear_area]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);

    if(use_ddbuf) {
        draw = vp->ddbuf;
    } else {
        draw = widget->window;
    }

    gdk_draw_rectangle(draw,
                       vp->bg_gc, TRUE,
                       X_PIXELS(rect->x),
                       Y_PIXELS(rect->y),
                       W_PIXELS(rect->width),
                       H_PIXELS(rect->height));
}

/*
 * Draws a rectangle at the text coordinates specified.  Note the rectangle does
 * NOT contain pixel coordinated.  This is merely an interface to make it easier
 * to draw rectangles over a specified text area.
 */

static void gtk_vp_draw_solid_rect(GtkWidget *widget, GdkRectangle *rect, GdkGC *gc, uint use_ddbuf)
{
    GtkVp         *vp;
    GdkPixmap     *draw;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);

    if(use_ddbuf) {
        draw = vp->ddbuf;
    } else {
        draw = widget->window;
    }

    gdk_draw_rectangle(draw,
                       gc, TRUE,
                       X_PIXELS(rect->x),
                       Y_PIXELS(rect->y),
                       W_PIXELS(rect->width),
                       H_PIXELS(rect->height));
}

static void gtk_vp_draw_hollow_rect(GtkWidget *widget, GdkRectangle *rect, GdkGC *gc, uint use_ddbuf)
{
    GtkVp         *vp;
    GdkPixmap     *draw;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);

    if(use_ddbuf) {
        draw = vp->ddbuf;
    } else {
        draw = widget->window;
    }

    gdk_draw_rectangle(draw,
                       gc, FALSE,
                       X_PIXELS(rect->x),
                       Y_PIXELS(rect->y),
                       W_PIXELS(rect->width)-1,
                       H_PIXELS(rect->height)-1);
}

static long gtk_vp_x_limits(GtkWidget *widget, long x)
{
    GtkVp       *vp;
    long         xoff;
    long         xlim;

    g_return_val_if_fail (widget != NULL, FALSE);
    g_return_val_if_fail (GTK_IS_VP (widget), FALSE);

    vp = GTK_VP(widget);

    xoff = vp->xoffset;
    xlim = vp->maxcols;

    if(x > xoff) x -= xoff; else x = 0;
    if(x > xlim) x = xlim;

    return(x);
}

static long gtk_vp_y_limits(GtkWidget *widget, long y)
{
    GtkVp       *vp;
    long         yoff;
    long         ylim;

    g_return_val_if_fail (widget != NULL, FALSE);
    g_return_val_if_fail (GTK_IS_VP (widget), FALSE);

    vp = GTK_VP(widget);

    yoff = vp->yoffset;
    ylim = vp->maxrows-1;

    if(y > yoff) y -= yoff; else y = 0;
    if(y > ylim) y = ylim;

    return(y);
}

static void gtk_vp_clip_coords(GtkWidget *widget, int *x, int *y)
{
    GtkVp *vp;

    g_return_if_fail (widget != NULL);
    g_return_if_fail (GTK_IS_VP(widget));

    vp = GTK_VP(widget);

    if(x) *x = max(min(*x, vp->maxcols - 1), 0);
    if(y) *y = max(min(*y, vp->maxrows - 1), 0);
}

/* Textual display routines */

static void gtk_vp_draw_textraw(GtkWidget *widget, int lnum, int x, int w, int state, uint use_ddbuf)
{
    GtkVp        *vp;
    GdkPixmap    *draw;
    GdkGC        *gc;
    GdkGC        *use_gc;
    GdkFont      *use_font;
    canvas       *canv;
    int          ypos;
    int          pos;
    int          apos;
    int          xo;
    int          len;
    int          flen;
    int          epos;
    int          d;
    char         *start;
    char         *str;
    char         *ptr;
    static line  *ln = NULL;
    GdkRectangle rect;
    int draw_bg;

#ifdef DEBUG
    printf("[gtk_vp_draw_textraw] slnum: %d, x: %d, w: %d\n", lnum, x, w);
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;

    if(!ln) {
        ln = line_new();
    }

    if(use_ddbuf) {
        draw = vp->ddbuf;
    } else {
        draw = widget->window;
    }

    /* ERROR CHECK */

    if((x+w) > vp->maxcols) {
        printf("[viewport]: gtk_vp_draw_textraw: bad screen column: x:%d w:%d\n", x, w);
    }
    if(lnum > vp->maxrows-1) {
        fprintf(stderr, "[viewport] gtk_vp_draw_textraw: bad screen line number: %d\n", lnum);
        return;
    }

    canv_line_get(canv, lnum + vp->yoffset, ln);
    len = line_length(ln);
    flen = len;
    xo = vp->xoffset;
    pos = x + xo;
    epos = pos + w;

    if(len > pos)  {
        switch(state) {
            case VP_BACKGROUND:
                gc = vp->bg_gc;
                break;

            case VP_SELECTED:
                gc = widget->style->fg_gc[GTK_STATE_SELECTED];
//                gc = vp->sel_gc;
                break;

            default:
                gc = vp->fg_gc;

//                if(vp->highlight) {
//                    canv_line_get_formatted(canv, lnum + vp->yoffset, ln);
//                }
        }

        if(vp->highlight) {
            canv_line_get_formatted(canv, lnum + vp->yoffset, ln);
        }

        w = min(w, x + vp->maxcols);
        w = min(w, len - pos);

        ypos = Y_PIXELS(lnum + 1) - vp->font_descent;

        use_font = vp->font;
        use_gc = gc;
        apos = 0;
        draw_bg = 0;

        str = line_get_string(ln);

        start = str;

        while(apos < epos) {
            if((ptr = strchr(str, 27))) {
                *ptr = '\0';
                len = ptr - str;
                flen -= len;
            } else {
                len = flen;
            }

            if(apos < pos) {
                d = pos - apos;

                if(len > d) {
                    str += d;
                    len -= d;
                    apos += d;
                } else {
                    apos += len;
                    len = 0;
                }
            }

            if(apos > epos) {
                len -= (apos - epos);
            }

            len = min(len, epos - apos);

            if(draw_bg == 1) {
                rect.x = apos - xo;
                rect.y = lnum;
                rect.height = 1;
                rect.width = len;

                gtk_vp_draw_solid_rect(widget, &rect, vp->bg_bm_gc, use_ddbuf);

                draw_bg = 0;
            }

            if(state != VP_FOREGROUND) {
                use_gc = gc;
            }

            gdk_draw_text(draw, use_font, use_gc, X_PIXELS(apos - xo), ypos, str, len);

            if(!ptr) break;

            apos += len;

            switch(ptr[1]) {
                case 9  :
                    draw_bg = 1;
                    use_gc   = vp->fg_bm_gc;
                    use_font = vp->font;
                    break;

                case 8  :
                    use_gc   = gc;
                    use_font = vp->font;
                    break;

                default :
                    if(ptr[1] <= 5 && ptr[1] > 0) {
                        use_gc   = vp->syn_rgbs[ptr[1]-1];
                        use_font = vp->syn_fonts[ptr[1]-1];
                    }
            }

            str = ptr + 2;
        }

        free(start);
    }
}

static void gtk_vp_draw_line(GtkWidget *widget, int num, int x, int w, int flags)
{
    GtkVp         *vp;
    canvas        *canv;
    GdkRectangle  r_all;
    GdkRectangle  r_sel;
    GdkRectangle  r_cursor;
    GdkRectangle  r_left;
    GdkRectangle  r_right;
    GdkGC         *bg_selected;
    GdkGC         *fg_selected;
    int           rflag = 0;
    int           ypos, xpos;
    uint          sel_x;
    uint          sel_e;
    uint          sel_w;
    uint          xr1, xr2;
    uint          use_ddbuf;
    static line  *ln = NULL;
    static line  *lnraw = NULL;

    if(!ln) {
        ln = line_new();
        lnraw = line_new();
    }

#ifdef DEBUG
    printf("[gtk_vp_draw_line] vlnum: %d x: %d w: %d\n", num, x, w);
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;

    if(num < 0 || num > vp->maxrows-1) {
#ifdef DEBUG
        printf("[WARNING] BAD SCREENLINE NUMBER: %d\n", num);
#endif
        return;
    }

    if(flags & NOBUFF) {
        use_ddbuf = 0;
    } else {
        use_ddbuf = 1;
    }

    /* work out rectangle for entire line[w] for requested redraw */

    r_all.x = x;
    r_all.y = num;
    r_all.width = w;
    r_all.height = 1;

    /*
     * If there is currently a highlighted area, then see if any of it
     * falls within the redraw area, and draw it, overlaying white text.
     */

    if(canv->region_defined && vp->r_state && ((flags & NOREGION)==0)) {
        fg_selected = widget->style->fg_gc[GTK_STATE_SELECTED];
//        bg_selected = widget->style->bg_gc[GTK_STATE_SELECTED];
        bg_selected = vp->sel_gc;

        canv_line_get(canv, vp->yoffset + num, ln);
        canv_line_get_raw(canv, vp->yoffset + num, lnraw);

        region_set_nlines(canv_get_region(canv), canv_get_nlines(canv));
        region_get_line_geometry(canv_get_region(canv), vp->yoffset + num, &sel_x, &sel_w, line_length(ln));

        sel_e = sel_x + sel_w;      /* find selection end point */
        xr1 = vp->xoffset + x;      /* find real x start point  */
        xr2 = vp->xoffset + x + w;  /* find real x end point    */

        /* if we are between the redraw points */

        if((xr1 <= sel_e) && (xr2 >= sel_x)) {
            r_sel.x      = max(sel_x, xr1) - vp->xoffset;
            r_sel.y      = num;
            r_sel.width  = min(sel_e, xr2) - vp->xoffset - r_sel.x;
            r_sel.height = 1;

            rflag = 1;
            gtk_vp_draw_solid_rect(widget, &r_sel, bg_selected, use_ddbuf);
            gtk_vp_draw_textraw(widget, num, r_sel.x, r_sel.width, VP_SELECTED, use_ddbuf);
        }
    }

    if(rflag) {
        r_left.x = x;
        r_left.y = num;
        r_left.width = r_sel.x - r_left.x;
        r_left.height = 1;

        r_right.x = x + r_left.width + r_sel.width;
        r_right.y = num;
        r_right.width = w - r_left.width - r_sel.width;
        r_right.height = 1;

        gtk_vp_clear_area(widget, &r_left, use_ddbuf);
        gtk_vp_clear_area(widget, &r_right, use_ddbuf);
        gtk_vp_draw_textraw(widget, num, x, r_left.width, VP_FOREGROUND, use_ddbuf);
        gtk_vp_draw_textraw(widget, num, r_right.x, r_right.width, VP_FOREGROUND, use_ddbuf);
    }
    else {
        gtk_vp_clear_area(widget, &r_all, use_ddbuf);
        gtk_vp_draw_textraw(widget, num, x, w, VP_FOREGROUND, use_ddbuf);
    }

    /* Draw the cursor if its within the redraw area */

    if(GTK_WIDGET_HAS_FOCUS(widget) && (flags & SHOWCURSOR)) {
        ypos = vp->ycursor;
        xpos = vp->xcursor;

        if(num == ypos) {
            if((xpos <= (x + w)) && (xpos >= x)) {
                r_cursor.x = xpos;
                r_cursor.y = num;
                r_cursor.width = 1;
                r_cursor.height = 1;

                if(canv->region_defined) {
                    if(rflag && (r_cursor.x < r_sel.x + r_sel.width)) {
                        gtk_vp_draw_textraw(widget, num, r_cursor.x, 1, VP_BACKGROUND, use_ddbuf);
                    } else {
                        gtk_vp_draw_textraw(widget, num, r_cursor.x, 1, VP_FOREGROUND, use_ddbuf);
                    }

                    gtk_vp_draw_hollow_rect(widget, &r_cursor, vp->fg_gc, use_ddbuf);
                } else {
                    gtk_vp_draw_solid_rect(widget, &r_cursor, vp->fg_gc, use_ddbuf);
                    gtk_vp_draw_textraw(widget, num, r_cursor.x, 1, VP_BACKGROUND, use_ddbuf);
                }
            }
        }
    }

    /* Draw the area of text to the main window */

    if((flags & UPDATE) && use_ddbuf) {
        gtk_vp_draw_at_coords(widget, &r_all);
    }
}

/*
 * Draws text within the rectangle specified.  The rectangle contains text
 * coordinates, NOT pixel coordinates.
 */

static void gtk_vp_draw_text(GtkWidget *widget, GdkRectangle *rect, GdkGC *gc, uint use_ddbuf)
{
    GtkVp *vp;
    canvas *canv;
    int    i;
    int    flags = 0;

#ifdef DEBUG
    printf("[gtk_vp_draw_text] x:%d y:%d w:%d h:%d\n", rect->x, rect->y, rect->width, rect->height);
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;

    flags = SHOWCURSOR;

    if(!use_ddbuf) {
        flags |= NOBUFF;
    }

    for(i=rect->y;i<(rect->y+rect->height);i++) {
        gtk_vp_draw_line(widget, i, rect->x, rect->width, flags);
    }

    if(use_ddbuf) {
        gtk_vp_draw_at_coords(widget, rect);
    }
}

void gtk_vp_offset_changed_notify(GtkWidget *widget)
{
    canvas         *canv;
    GtkVp         *vp;

#ifdef DEBUG
    printf("[gtk_vp_offset_changed_notify]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;

    gtk_vp_setpad_abs(widget, canv->od_xoffset, canv->od_yoffset);

    vp->func_offset_changed(vp->struct_ptr);
}

/*
 * This procedure first undraws the last cursor position and then redraws the
 * new cursor position.  If the cursor position is outside the current area in
 * display, the pad position is adjusted to compensate.
 */

static void gtk_vp_cursor_move_abs(GtkWidget *widget, int x, int y)
{
    GtkVp   *vp;
    int     xdiff = 0;
    ulong   ydiff = 0;

#ifdef DEBUG
    printf("[gtk_vp_cursor_move_abs]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);

    gtk_vp_set_cursor_position(widget, x-xdiff, y-ydiff);
}

static void gtk_vp_cursor_move_notify(GtkWidget *widget)
{
    GtkVp  *vp;
    canvas *canv;

#ifdef DEBUG
    printf("[gtk_vp_cursor_move_notify]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;

    gtk_vp_cursor_move_abs(widget,
                           canv_get_col(canv) - canv->od_xoffset,
                           canv_get_ypos(canv) - canv->od_yoffset);

    vp->func_cursor_moved(vp->struct_ptr);
}

static void gtk_vp_setpad_abs(GtkWidget *widget, uint x, ulong y)
{
    GtkVp  *vp;
    canvas *canv;
    ulong   yoff;
    ulong   xoff;
    uint    sx, nx;
    ulong   sy, ny;

#ifdef DEBUG
    printf("[gtk_vp_setpad_abs]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;

    yoff = vp->yoffset;
    xoff = vp->xoffset;

    vp->xoffset = x;
    vp->yoffset = y;

    region_set_end_coords(canv_get_region(canv), x + vp->xcursor, y + vp->ycursor);
    region_get_coords(canv_get_region(canv), &sx, &sy, &nx, &ny);

    vp->rx_1 = sx;
    vp->ry_1 = sy;
    vp->rx_2 = nx;
    vp->ry_2 = ny;

    if(y != yoff) {
        gtk_vp_redraw_all(widget, 0);
        vp->func_update_line_numbers(vp->struct_ptr);
    } else if(x != xoff) {
        gtk_vp_redraw_all(widget, 0);
    }
}

/*
 * Input event handling routines for key and mouse events.  Press & release
 * events are captured, processed and sent to the canvas as a command.
 * The way an event behaves does not have any relevance here, that is for
 * the backend to deal with.  And yep, this lot could be tidied up a lot :-)
 */

static gint gtk_vp_keypress(GtkWidget* widget, GdkEventKey *event)
{
    g_return_val_if_fail (widget != NULL, FALSE);
    g_return_val_if_fail (GTK_IS_VP (widget), FALSE);
    g_return_val_if_fail (event != NULL, FALSE);

    gtk_vp_key_event(widget, event, 0);

    return(TRUE);
}

static gint gtk_vp_keyrelease(GtkWidget* widget, GdkEventKey *event)
{
    g_return_val_if_fail (widget != NULL, FALSE);
    g_return_val_if_fail (GTK_IS_VP (widget), FALSE);
    g_return_val_if_fail (event != NULL, FALSE);

    gtk_vp_key_event(widget, event, 1);

    return(TRUE);
}

static void gtk_vp_key_event(GtkWidget* widget, GdkEventKey *event, uint upstroke)
{
    GtkVp  *vp;
    canvas *canv;
    events ev;
    int    w, h;
    int    x, y;

#ifdef DEBUG
    printf("[gtk_vp_key]\n");
#endif

    g_return_if_fail (event != NULL);
    g_return_if_fail (widget != NULL);
    g_return_if_fail (GTK_IS_VP (widget));

    if(GTK_WIDGET_HAS_FOCUS(widget)) {
        // hmmmmm

    }

    vp = GTK_VP(widget);
    canv = vp->canv;

    gdk_window_get_size(widget->window, &w, &h);

    if(canv_get_mouse_binding(canv)) {
        gtk_vp_get_mouse_position(widget, &x, &y);

        x = X_PIXELS(x);
        y = Y_PIXELS(y);

        if(x < 0 || x > w || y < 0 || y > h) {
            return;
        }
    }

    /*
     * Set up an event for the backend session to interpret.  The event state
     * is things like the shift key, control key, mouse button, etc.  The
     * printable part is to ensure only "proper" characters get into the editor
     * buffer.  The motion part is a flag to tell the canvas if the mouse is
     * being moved.
     */

//    printf("state : %d\n", event->state);
//    printf("keyval: %d\n\n", event->keyval);

    ev.state = gtk_vp_convert_states(event->state);
    ev.button = 0;
    ev.upstroke = upstroke;
    ev.key = event->keyval;
    ev.motion = 0;

    if(event->string) {
        ev.printable = event->string[0];
    } else {
        ev.printable = '\0';
    }

    gtk_vp_sync_output_cursor(widget);

    if(!session_process_event(vp->se, vp->canv, &ev)) {
        return;
    }

    /*
     * Bind the mouse to the cursor.  Initially this was going to be put in
     * the cursor move notify, but obviously this would cause a feedback
     * loop, also we want the mouse cursor to dissapear on keypress really.
     */

    if(GTK_WIDGET_HAS_FOCUS(widget)) {
        x = canv_get_col(canv)  - vp->xoffset;
        y = canv_get_ypos(canv) - vp->yoffset;

        vp->cursor_state = 0;
        gtk_vp_hide_mouse(widget);
        gtk_vp_set_mouse_position(widget, x, y);
    }
}

static void gtk_vp_button_event(GtkWidget *widget, GdkEventButton *event, uint upstroke)
{
    GtkVp   *vp;
    canvas  *canv;
    events  ev;
    ulong   cposx;
    ulong   cposy;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(event != NULL);
    g_return_if_fail(GTK_IS_VP(widget));

    printf("button_event..\n");

    vp = GTK_VP(widget);
    canv = vp->canv;

    if(!GTK_WIDGET_HAS_FOCUS(widget)) {
        gtk_widget_grab_focus (widget);
    }

    cposx = vp->xoffset + X_CURSOR(event->x);
    cposy = vp->yoffset + Y_CURSOR(event->y);

    ev.state = gtk_vp_convert_states(event->state);
    ev.button = event->button;
    ev.upstroke = upstroke;
    ev.motion = 0;
    ev.xmpos = cposx;
    ev.ympos = cposy;
    ev.printable = 0;
    ev.key = 0;

    gtk_vp_sync_output_cursor(widget);

    session_process_event(vp->se, vp->canv, &ev);
}

static gint gtk_vp_button_release(GtkWidget *widget, GdkEventButton *event)
{
    g_return_val_if_fail(widget != NULL, FALSE);
    g_return_val_if_fail(GTK_IS_VP(widget), FALSE);
    g_return_val_if_fail(event != NULL, FALSE);

    gtk_vp_button_event(widget, event, 1);

    return(TRUE);
}

static gint gtk_vp_button_press(GtkWidget *widget, GdkEventButton *event)
{
    g_return_val_if_fail(widget != NULL, FALSE);
    g_return_val_if_fail(GTK_IS_VP(widget), FALSE);
    g_return_val_if_fail(event != NULL, FALSE);

    gtk_vp_button_event(widget, event, 0);

    return(TRUE);
}

static int gtk_vp_convert_states(int gdkstate)
{
    int state = 0;

    if(gdkstate & GDK_SHIFT_MASK)   state |= MOD_SHIFT;
    if(gdkstate & GDK_CONTROL_MASK) state |= MOD_CONTROL;
    if(gdkstate & GDK_MOD1_MASK)    state |= MOD_ALT;
    if(gdkstate & GDK_MOD3_MASK)    state |= MOD_ALT;
    if(gdkstate & GDK_MOD4_MASK)    state |= MOD_ALT;

    /* Don't do anything with modifiers 2 and 5 **********

    if(gdkstate & GDK_MOD2_MASK)    state |= MOD_ALT;
    if(gdkstate & GDK_MOD5_MASK)    state |= MOD_ALT;

    ******************************************************/

    return(state);
}

static gint gtk_vp_motion_notify(GtkWidget* widget, GdkEventMotion *event)
{
    GtkVp   *vp;     /* the viewport */
    canvas  *canv;   /* the canvas */
    int     xpos;    /* x mouse cursor position */
    int     ypos;    /* y mouse cursor position */

#ifdef DEBUG
    printf("[gtk_vp_motion_notify]\n");
#endif

    g_return_val_if_fail (widget != NULL, FALSE);
    g_return_val_if_fail (GTK_IS_VP(widget), FALSE);
    g_return_val_if_fail (event != NULL, FALSE);

    vp = GTK_VP(widget);

    canv = vp->canv;

    if( vp->cursor_state == 0) {
        vp->cursor_state = 1;

        return(TRUE);
    }

    if( vp->cursor_state == 1) {
        vp->cursor_state = 2;

        gdk_window_set_cursor(widget->window, NULL);
    }

    if(!vp->canv->mouse_binding) {
        return(FALSE);
    }

    gtk_vp_get_mouse_position(widget, &xpos, &ypos);
    gtk_vp_clip_coords(widget, &xpos, &ypos);

    if((xpos != vp->xcursor) || (ypos != vp->ycursor)) {
        gtk_vp_set_cursor_position(widget, xpos, ypos);

        if(canv->region_defined) {
            gtk_vp_region_notify(widget);
        }
    }

    return(TRUE);
}

static void gtk_vp_set_mouse_position(GtkWidget *widget, int x, int y)
{
    GtkVp   *vp;
    Display *xdisp;
    Window  xwin;
    int     xpos;
    int     ypos;

#ifdef DEBUG
    //printf("[gtk_vp_set_mouse_position] x: %d, y: %d\n", x, y);
#endif

    g_return_if_fail (widget != NULL);
    g_return_if_fail (GTK_IS_VP(widget));

    vp = GTK_VP(widget);

    if(!vp->canv->mouse_binding)
        return;

    gtk_vp_get_mouse_position(widget, &xpos, &ypos);

    if(x != xpos || y != ypos) {
        x = X_PIXELS(x);
        y = Y_PIXELS(y + 1) - (vp->font_height / 2);

        xdisp = GDK_WINDOW_XDISPLAY(widget->window);
        xwin  = GDK_WINDOW_XWINDOW(widget->window);

        XWarpPointer(xdisp, 0, xwin, 0, 0, 0, 0, x, y);
    }
}

void gtk_vp_sync_mouse_to_cursor(GtkWidget *widget)
{
    GtkVp  *vp;
    canvas *canv;

#ifdef DEBUG
    printf("[gtk_vp_sync_mouse_to_cursor]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP (widget));

    vp = GTK_VP(widget);

    if(!vp->canv->mouse_binding)
        return;

    canv = vp->canv;

    gtk_vp_set_mouse_position(widget, canv->col - canv->od_xoffset,
                                      canv->ypos - canv->od_yoffset);
}

void gtk_vp_sync_cursor_to_mouse(GtkWidget *widget)
{
    GtkVp  *vp;
    uint   x;
    uint   y;

#ifdef DEBUG
    printf("[gtk_vp_sync_mouse_to_cursor]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP (widget));

    vp = GTK_VP(widget);

    if(!vp->canv->mouse_binding)
        return;

    gtk_vp_get_mouse_position(widget, &x, &y);
    gtk_vp_set_cursor_position(widget, x, y);
}

void gtk_vp_sync_output_cursor(GtkWidget *widget)
{
    GtkVp  *vp;
    canvas *canv;
    uint   xpos;
    ulong  ypos;

#ifdef DEBUG
    printf("[gtk_vp_sync_output_cursor]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP (widget));

    vp = GTK_VP(widget);
    canv = vp->canv;

    xpos = vp->xcursor + vp->xoffset;
    ypos = vp->ycursor + vp->yoffset;

    if(canv->col != xpos || canv->ypos != ypos) {
        canv_set_col_row(canv, xpos, ypos);
    }

    vp->func_sync_cursor(vp->struct_ptr);
}

void gtk_vp_sync_input_cursor(GtkWidget *widget)
{
    GtkVp  *vp;
    canvas *canv;

#ifdef DEBUG
    printf("[gtk_vp_sync_input_cursor]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP (widget));

    vp = GTK_VP(widget);
    canv = vp->canv;
    vp->xcursor = canv->col - vp->xoffset;
    vp->ycursor = canv->ypos - vp->yoffset;
}

void gtk_vp_set_cursor_position(GtkWidget *widget, int x, int y)
{
    GtkVp *vp;
    line  *lbuf;

#ifdef DEBUG
    printf("[gtk_vp_set_cursor_position] x: %d, y: %d\n", x, y);
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP (widget));

    vp = GTK_VP(widget);

    if(x < 0) x = vp->xcursor;
    if(y < 0) y = vp->ycursor;

    lbuf = line_new();
    canv_line_get_raw(vp->canv, y + vp->yoffset, lbuf);
    x = get_actual_column_position(lbuf, vp->xoffset + x, vp->canv->buff->tabstops, 0) - vp->xoffset;
    line_free(lbuf);

    if(x != vp->xcursor || y != vp->ycursor) {
        gtk_vp_hide_cursor(widget);
        vp->xcursor = x;
        vp->ycursor = y;
        gtk_vp_show_cursor(widget);

        if(vp->canv->region_defined) {
            gtk_vp_region_notify(widget);
        }
    }
}

void gtk_vp_show_cursor(GtkWidget *widget)
{
    GtkVp *vp;

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP (widget));

    vp = GTK_VP(widget);

#ifdef DEBUG
    printf("[gtk_vp_show_cursor] xcursor: %d, ycursor: %d\n", vp->xcursor, vp->ycursor);
#endif

    if(vp->maxcols > 0) {
        gtk_vp_draw_line(widget, vp->ycursor, vp->xcursor, 1, SHOWCURSOR|UPDATE);
    }
}

void gtk_vp_hide_cursor(GtkWidget *widget)
{
    GtkVp *vp;

#ifdef DEBUG
    printf("[gtk_vp_hide_cursor]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP (widget));

    vp = GTK_VP(widget);

    if(vp->maxcols > 0) {
        gtk_vp_draw_line(widget, vp->ycursor, vp->xcursor, 1, UPDATE);
    }
}

void gtk_vp_hide_mouse(GtkWidget *widget)
{
    GtkVp *vp;

#ifdef DEBUG
    printf("[gtk_vp_hide_mouse]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP (widget));

    vp = GTK_VP(widget);
    vp->cursor_state = 0;
    gdk_window_set_cursor(widget->window, vp->inv_cursor);
}

void gtk_vp_show_mouse(GtkWidget *widget)
{
    GtkVp *vp;

#ifdef DEBUG
    printf("[gtk_vp_show_mouse]\n");
#endif

    g_return_if_fail(widget != NULL);
    g_return_if_fail(GTK_IS_VP (widget));

    vp = GTK_VP(widget);
    vp->cursor_state = 2;
    gdk_window_set_cursor(widget->window, NULL);
}

static void gtk_vp_get_mouse_position(GtkWidget *widget, int *x, int *y)
{
    GtkVp   *vp;
    gint    xpos;
    gint    ypos;

#ifdef DEBUG
    printf("[gtk_vp_get_mouse_position]\n");
#endif

    g_return_if_fail (widget != NULL);
    g_return_if_fail (GTK_IS_VP(widget));

    vp = GTK_VP(widget);

    gdk_window_get_pointer(widget->window, &xpos, &ypos, NULL);

    *x = X_CURSOR(xpos);
    *y = Y_CURSOR(ypos);
}

static void gtk_vp_selection_received(GtkWidget *widget, GtkSelectionData *selection_data, guint time)
{
    GtkVp   *vp;
    canvas  *canv;
    char    *buf1;
    char    *buf2;
    clip    *cl;
    line    *ln;
    uint    len;
    char    fname[1024];

#ifdef DEBUG
    printf("[gtk_vp_selection_received]\n");
#endif

    SELECTION_FLAG = 1;

    g_return_if_fail (widget != NULL);
    g_return_if_fail (GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;

    if(selection_data->type == GDK_SELECTION_TYPE_STRING) {
        if(selection_data->length >= 0) {
            cl = clip_new();
            ln = line_new();

            buf2 = selection_data->data;
            buf2[selection_data->length] = 0;

            while((buf1 = strchr(buf2, '\n'))) {
                len = buf1 - buf2;
                line_insert_raw(ln, buf2, len);
                clip_addline(cl, ln);
                line_reset(ln);
                buf2 = ++buf1;
            }

            line_insert_raw(ln, buf2, strlen(buf2));
            clip_addline(cl, ln);

            get_paste_buffer_ffname(fname, "PRIMARY", 1024);

            clip_save(cl, fname);
            clip_free(cl);
            line_free(ln);
        }
    }

    if(GM_FLAG == 1) {
        gtk_main_quit();
    }
}

static void gtk_vp_get_x_selection(GtkWidget *widget)
{
    GtkVp  *vp;
    canvas *canv;
    static GdkAtom targets_atom = GDK_NONE;

#ifdef DEBUG
    printf("[gtk_vp_get_x_selection]\n");
#endif

    g_return_if_fail (widget != NULL);
    g_return_if_fail (GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;

    if(targets_atom == GDK_NONE) {
      targets_atom = gdk_atom_intern("STRING", FALSE);
    }

    if(GM_FLAG == 0) {
        gtk_selection_convert(widget, GDK_SELECTION_PRIMARY, targets_atom, GDK_CURRENT_TIME);

        if(SELECTION_FLAG == 0) {
            GM_FLAG = 1;
            gtk_main();
            GM_FLAG = 0;
        }

        SELECTION_FLAG = 0;
    }
    else {
        canv_od_message(canv, "Still waiting for previous selection from client", 1);
    }

    vp->func_get_selection(vp->struct_ptr);
}

static void gtk_vp_claim_x_selection(GtkWidget *widget)
{
    GtkVp *vp;

    g_return_if_fail (widget != NULL);
    g_return_if_fail (GTK_IS_VP(widget));

#ifdef DEBUG
    printf("[gtk_vp_claim_x_selection]\n");
#endif

    vp = GTK_VP(widget);

    gtk_selection_owner_set(widget,
                            GDK_SELECTION_PRIMARY,
                            GDK_CURRENT_TIME);

    vp->func_claim_selection(vp->struct_ptr);
}

static void gtk_vp_selection_handle (GtkWidget        *widget,
                                     GtkSelectionData *selection_data,
                                     guint            info,
                                     guint            time_stamp)
{
    GtkVp  *vp;
    canvas *canv;
    clip   *cl;
    line   *lbuf;
    uint   i;
    char   *buf;
    char   fname[1024];

#ifdef DEBUG
    printf("[gtk_vp_selection_handle]\n");
#endif

    g_return_if_fail (widget != NULL);
    g_return_if_fail (GTK_IS_VP(widget));

    vp = GTK_VP(widget);
    canv = vp->canv;

    get_paste_buffer_ffname(fname, "PRIMARY", 1024);

    cl = clip_new();

    if(clip_load(cl, fname)) {
        if(cl->used > 0) {
            lbuf = line_new();
            line_insert(lbuf, cl->lines[0]);

            for(i=1;i<cl->used;i++) {
                line_insert_raw(lbuf, "\n", 1);
                line_insert(lbuf, cl->lines[i]);
            }

            buf = line_get_string(lbuf);

            gtk_selection_data_set(selection_data,
                                   GDK_SELECTION_TYPE_STRING,
                                   8 * sizeof(char),
                                   buf,
                                   strlen(buf));

            free(buf);

            line_free(lbuf);
        }
    }

    clip_free(cl);
}

static gint gtk_vp_focus_in(GtkWidget *widget, GdkEventFocus *event)
{
#ifdef DEBUG
    printf("[gtk_vp_focus_in]\n");
#endif

    GTK_WIDGET_SET_FLAGS (widget, GTK_HAS_FOCUS);
    gtk_vp_show_cursor(widget);
    gtk_vp_show_mouse(widget);

    return(FALSE);
}

static gint gtk_vp_focus_out(GtkWidget *widget, GdkEventFocus *event)
{
#ifdef DEBUG
    printf("[gtk_vp_focus_out]\n");
#endif

    GTK_WIDGET_UNSET_FLAGS (widget, GTK_HAS_FOCUS);
    gtk_vp_hide_cursor(widget);
    gtk_vp_show_mouse(widget);

    return(FALSE);
}

static gint gtk_vp_leave_notify_event(GtkWidget* widget, GdkEventCrossing *event)
{
#ifdef DEBUG
    printf("[gtk_vp_leave_notify_event]\n");
#endif

    g_return_val_if_fail (widget != NULL, FALSE);
    g_return_val_if_fail (GTK_IS_VP(widget), FALSE);
    g_return_val_if_fail (event != NULL, FALSE);

    gtk_vp_hide_cursor(widget);
    gtk_vp_show_mouse(widget);

    if(GTK_WIDGET_HAS_FOCUS(gtk_widget_get_toplevel(widget)))
    {
        gtk_widget_grab_focus(widget);
    }

    return(TRUE);
}

static gint gtk_vp_enter_notify_event(GtkWidget* widget, GdkEventCrossing *event)
{
    GtkVp   *vp;
    canvas  *canv;

#ifdef DEBUG
    printf("[gtk_vp_enter_notify_event]\n");
#endif

    g_return_val_if_fail (widget != NULL, FALSE);
    g_return_val_if_fail (GTK_IS_VP(widget), FALSE);
    g_return_val_if_fail (event != NULL, FALSE);

    vp = GTK_VP(widget);
    canv = vp->canv;

    if(GTK_WIDGET_HAS_FOCUS(gtk_widget_get_toplevel(widget))) {
        gtk_vp_sync_cursor_to_mouse(widget);
    }

    gtk_widget_grab_focus(widget);
    gtk_vp_show_cursor(widget);

    return(TRUE);
}

/* pass-through callbacks */

static void gtk_vp_message(GtkWidget *widget, char *message, int beep)
{
    GtkVp *vp;
    vp = GTK_VP(widget);
    vp->func_message(vp->struct_ptr, message, beep);
}

static void gtk_vp_return_event(GtkWidget *widget, ulong lnum)
{
    GtkVp *vp;
    vp = GTK_VP(widget);
    vp->func_return_event(vp->struct_ptr, lnum);
}

static void gtk_vp_toggle_line_numbers(GtkWidget *widget)
{
    GtkVp *vp;
    vp = GTK_VP(widget);
    vp->func_line_numbers_toggled(vp->struct_ptr);
}

static void gtk_vp_toggle_highlighting(GtkWidget *widget)
{
    GtkVp *vp;
    vp = GTK_VP(widget);

    (canv_get_highlighting(vp->canv))
        ? gtk_vp_enable_highlighting(widget)
        : gtk_vp_disable_highlighting(widget);

    vp->func_highlighting_toggled(vp->struct_ptr);
}

static void gtk_vp_toggle_mouse_binding(GtkWidget *widget)
{
    GtkVp *vp;
    vp = GTK_VP(widget);
    vp->func_mouse_binding_toggled(vp->struct_ptr);
}

static void gtk_vp_link_added_notify(GtkWidget *widget)
{
    GtkVp *vp;
    vp = GTK_VP(widget);
    vp->func_link_added(vp->struct_ptr);
}

static void gtk_vp_link_deleted_notify(GtkWidget *widget)
{
    GtkVp *vp;
    vp = GTK_VP(widget);
    vp->func_link_deleted(vp->struct_ptr);
}

static void gtk_vp_file_attribs_changed_notify(GtkWidget *widget)
{
    GtkVp *vp;
    vp = GTK_VP(widget);
    vp->func_attribs_changed(vp->struct_ptr);
}

static void gtk_vp_buffer_renamed_notify(GtkWidget *widget)
{
    GtkVp *vp;

//    gtk_vp_setup_highlighting(widget);

    vp = GTK_VP(widget);
    vp->func_buffer_renamed(vp->struct_ptr);
}

static void gtk_vp_buffer_destroyed_notify(GtkWidget *widget)
{
    GtkVp *vp;
    vp = GTK_VP(widget);
    vp->func_buffer_destroyed(vp->struct_ptr);
}

static void gtk_vp_bring_to_front_callback(GtkWidget *widget)
{
    GtkVp *vp;
    vp = GTK_VP(widget);
    vp->func_bring_to_front(vp->struct_ptr);
}

void gtk_vp_set_parent(GtkWidget *widget, void *struct_ptr)
{
    GtkVp *vp;
    vp = GTK_VP(widget);
    vp->struct_ptr = struct_ptr;
}

void gtk_vp_set_callback(GtkWidget *widget, char *name, void (*func)())
{
    GtkVp *vp;
    vp = GTK_VP(widget);
    callbacks_set(vp->cbacks, name, func);
}

