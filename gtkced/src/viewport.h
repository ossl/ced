#ifndef _VIEWPORT_H
#define _VIEWPORT_H

#include <gtk/gtk.h>

#include <ced/session.h>
#include <ced/common.h>
#include <ced/canvas.h>
#include <ced/callbacks.h>
           
#define GTK_VP(obj)          GTK_CHECK_CAST(obj, gtk_vp_get_type(), GtkVp)
#define GTK_VP_CLASS(klass)  GTK_CHECK_CLASS_CAST(klass, gtk_vp_get_type(), GtkVpClass)
#define GTK_IS_VP(obj)       GTK_CHECK_TYPE(obj, gtk_vp_get_type())

typedef struct _GtkVp         GtkVp;
typedef struct _GtkVpClass    GtkVpClass;
typedef struct _vp_font_style VP_FONT_STYLE;

/* Some global variables for X selection purposes */

uint SELECTION_FLAG;
uint GM_FLAG;

/* Main viewport structure */

struct _GtkVp
{
    GtkWidget  widget;        /* gtk internal */
    guint      policy : 2;    /* gtk internal */

    /* Display */

    GdkPixmap  *ddbuf;        /* display double buffer */
    gint       font_width;    /* font width */
    gint       font_height;   /* font height */
    gint       font_descent;  /* font height */
    gint       font_loaded;   /* main font has been established */
    gint       linespacing;   /* line spacing in pixels */
    canvas     *canv;         /* virtual canvas */
    session    *se;           /* session viewport belongs to */

    /* Viewport style */

    GdkGC      *sel_gc;       /* background graphics context */
    GdkGC      *bg_gc;        /* background graphics context */
    GdkGC      *fg_gc;        /* foreground graphics context */
    GdkGC      *bg_bm_gc;     /* block match graphics context */
    GdkGC      *fg_bm_gc;     /* block match graphics context */
    GdkFont    *font;         /* text font for text area */
    GdkFont    *syn_fonts[5]; /* fonts used for syntax highlighting */
    GdkGC      *syn_rgbs[5];  /* gcs used to draw syntax highlighted text */

    /* Cursor and screen positioning */

    gint       xcursor;       /* last x position of cursor */
    gint       ycursor;       /* last y position of cursor */
    ulong      yoffset;       /* row display offset */
    gint       xoffset;       /* column display offset */
    gint       maxcols;       /* max columns on screen, recalculated on resize */
    gint       maxrows;       /* max rows on screen, recalculated on resize */

    /* Region */

    GdkRectangle reg;

    /* Mouse pointer cursors */

    GdkCursor    *inv_cursor;
    GdkCursor    *main_cursor;

    /* Miscellaneous stuff */

    regtype      rt_last;
    int          r_mode;
    int          r_state;
    int          rx_1;
    int          ry_1;
    int          rx_2;
    int          ry_2;
    int          cursor_state;
    int          reg_valid;
    int          highlight;

    /* some callback stuff */

    callbacks *cbacks;

    void *struct_ptr;

    CALLBACK_DEF (func_message)          (void *, char *, int);
    CALLBACK_DEF (func_return_event)     (void *, ulong);
    CALLBACK_DEF (func_link_added)       (void *);
    CALLBACK_DEF (func_link_deleted)     (void *);
    CALLBACK_DEF (func_lines_changed)    (void *, ulong, ulong);
    CALLBACK_DEF (func_lines_deleted)    (void *, ulong, ulong);
    CALLBACK_DEF (func_lines_inserted)   (void *, ulong, ulong);
    CALLBACK_DEF (func_attribs_changed)  (void *);
    CALLBACK_DEF (func_buffer_renamed)   (void *);
    CALLBACK_DEF (func_buffer_destroyed) (void *);
    CALLBACK_DEF (func_offset_changed)   (void *);
    CALLBACK_DEF (func_cursor_moved)     (void *);
    CALLBACK_DEF (func_region_changed)   (void *);
    CALLBACK_DEF (func_region_echo)      (void *);
    CALLBACK_DEF (func_redraw_all)       (void *);
    CALLBACK_DEF (func_update_all)       (void *);
    CALLBACK_DEF (func_get_selection)    (void *);
    CALLBACK_DEF (func_claim_selection)  (void *);
    CALLBACK_DEF (func_sync_cursor)      (void *);
    CALLBACK_DEF (func_update_line_numbers)   (void *);
    CALLBACK_DEF (func_line_numbers_toggled)  (void *);
    CALLBACK_DEF (func_mouse_binding_toggled) (void *);
    CALLBACK_DEF (func_highlighting_toggled)  (void *);
    CALLBACK_DEF (func_bring_to_front)        (void *);
};

/* font and colours for syntax highlighting */

struct _vp_font_style
{
    char  *fontname;
    int    red;
    int    green;
    int    blue;
};

/* gtk class pointer */

struct _GtkVpClass
{
    GtkWidgetClass parent_class; 
};

/* interface */

GtkWidget* gtk_vp_new                   (canvas* canv, session *se);
guint      gtk_vp_get_type              (void);
void       gtk_vp_drawtext              (GtkWidget*, char*);
void       gtk_vp_redraw                (GtkWidget* widget);
void       gtk_vp_sync_output_cursor    (GtkWidget *widget);
void       gtk_vp_set_cursor_position   (GtkWidget *widget, int x, int y);
void       gtk_vp_show_cursor           (GtkWidget *widget);
void       gtk_vp_hide_cursor           (GtkWidget *widget);
void       gtk_vp_sync_input_cursor     (GtkWidget *widget);
void       gtk_vp_sync_mouse_to_cursor  (GtkWidget *widget);
void       gtk_vp_sync_cursor_to_mouse  (GtkWidget *widget);
void       gtk_vp_enable_highlighting   (GtkWidget *widget);
void       gtk_vp_disable_highlighting  (GtkWidget *widget);
void       gtk_vp_set_syntax_style      (GtkWidget *widget, uint setnum, VP_FONT_STYLE vpfs);
void       gtk_vp_set_callback          (GtkWidget *widget, char *name, void (*func)());
void       gtk_vp_set_parent            (GtkWidget *widget, void *struct_ptr);
void       gtk_vp_set_block_match_style (GtkWidget *widget, GdkColor *bg, GdkColor *fg);

#endif /* _VIEWPORT_H */

