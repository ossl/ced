/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *
 *  Last Updated
 *    16th December 2000
 *
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "buffer.h"
#include "utils.h"

//#define DEBUG

static change *change_new    (uint x, ulong y, optype op, edmode em, void *obj);
static int     change_apply  (buffer *buff, change *ch, int negate_change);
static void    change_free   (change *ch);

extern void difflist_free(difflist *dlist);

int changelist_apply(buffer *buff, chlist *chl, int negate_changes)
{
    change  *ch = NULL;
    uint    i;

    if(!negate_changes) {
        for(i=0;chl->changes[i] != NULL; i++) {
            ch = chl->changes[i];
            change_apply(buff, ch, 0);
        }
    } else {
        for(i=chl->size;i>0;) {
            i--;
            ch = chl->changes[i];
            change_apply(buff, ch, 1);
        }
    }

    return(1);
}

int change_apply(buffer *buff, change *ch, int negate_change)
{
    clip   *cl;
    dstring *ln;
    ulong  nlines;
    uint   len;
    uint   x, x2;
    ulong  y, y2;
    optype op = E_NONE;

    if(negate_change) {
        switch(ch->op) {
            case E_LIST_INSERT  : op = E_LIST_DELETE; break;
            case E_LIST_DELETE  : op = E_LIST_INSERT; break;
            case E_TEXT_INSERT  : op = E_TEXT_DELETE; break;
            case E_TEXT_DELETE  : op = E_TEXT_INSERT; break;
            case E_TEXT_SUBS    : op = E_TEXT_SUBS; break;
            case E_LINE_SPLIT   : op = E_LINE_DELNL; break;
            case E_LINE_DELNL   : op = E_LINE_SPLIT; break;
            case E_BLOCK_INSERT : op = E_BLOCK_DELETE; break;
            case E_BLOCK_DELETE : op = E_BLOCK_INSERT; break;
            case E_BLOCK_SUBS   : op = E_BLOCK_SUBS; break;
            case E_NONE         : printf("how odd\n");
        }
    } else {
        op = ch->op;
    }

    x = ch->x;
    y = ch->y;

    switch(op) {
        case E_LIST_INSERT:
        buff_apply_difflist(buff, ch->src.list, 0);
        break;

        case E_LIST_DELETE:
        buff_apply_difflist(buff, ch->src.list, 1);
        break;

        /*
         * Insert a previously deleted segment of text.  The segment
         * deleted is sent to the standard buffer procedure.
         */

        case E_TEXT_INSERT:
        buff_text_insert(buff, x, y, ch->src.ln, 0);
        break;

        /*
         * Delete a previously inserted segment of text.  The standard
         * buffer procedure is used, the amount of text to delete
         * is the length of the text that was inserted.
         */

        case E_TEXT_DELETE:
        len = dstring_get_length(ch->src.ln);
        buff_text_delete(buff, x, y, len, 0);
        break;

        /*
         * Substitutes a previously substituted segment of text from
         * a line with what it was substituted with.  We just use
         * the standard buffer procedure, but pass it a different
         * parameter depending on whether we are negating the change
         * or simply re-applying it.
         */

        case E_TEXT_SUBS:

        if(negate_change) {
          len = dstring_get_length(ch->dest.ln);
          buff_text_subs(buff, x, y, len, ch->src.ln, 0);
        } else {
          len = dstring_get_length(ch->src.ln);
          buff_text_subs(buff, x, y, len, ch->dest.ln, 0);
        }
        break;

        /*
         * Insert a newline in a line which previously had its newline
         * removed, ie was joined to the line proceding it.  We just
         * use the standard buffer procedure here.
         */

        case E_LINE_SPLIT:
        buff_line_split(buff, x, y, 0);
        break;

        /*
         * Delete a previously inserted newline.  Here we just pass the line
         * number to the buffer procedure.  The x parameter isn't needed,
         * because the procedure knows its on the end of the line !
         */

        case E_LINE_DELNL:
        buff_line_delete_newline(buff, y, 0);
        break;

        /*
         * Insert a previously deleted block.  We just call the standard
         * buffer block paste procedure here with the stored clip.
         */

        case E_BLOCK_INSERT:
        buff_block_insert(buff, x, y, ch->src.cl, 0);
        break;

        /*
         * Delete a previously insert block.  The buffer block delete
         * procedure will work fine, but we need to get 2 sets of coordinated.
         * The first set is just x and y, the second set is derived from the
         * stored clip.
         */

        case E_BLOCK_DELETE:

        cl = ch->src.cl;
        nlines = cl->used;
        ln = cl->lines[nlines-1];
        len = dstring_get_length(ln);
        y2 = y + nlines - 1;
        x2 = map_x_to_column(ln, len, buff->tabstops);

        if(nlines == 1) {
            x2 += x;
        }

        buff_block_delete(buff, x, y, x2, y2, 0);

        break;
    
        default: break;
    }

    return(1);
}

chlist *changelist_new()
{
    chlist *chl;
    chl = calloc(1, sizeof(chlist));
    chl->changes = calloc(1, sizeof(change));
    chl->size = 0;
    return(chl);
}

void changelist_free(chlist *chl)
{
    ulong i;

    for(i=0;chl->changes[i] != NULL; i++) {
        change_free(chl->changes[i]);
    }

    free(chl->changes);
    free(chl);
}

void changelist_get_start_coords(chlist *chl, uint *x, ulong *y)
{
    change *ch;
    ch = chl->changes[0];
    *x = ch->x;
    *y = ch->y;
}

void changelist_get_end_coords(chlist *chl, uint *x, ulong *y)
{
    change *ch;
    ch = chl->changes[chl->size];
    *x = ch->x;
    *y = ch->y;
}

void changelist_add(chlist *chl, uint x, ulong y, optype op, edmode em, void *obj)
{
    change *prev;  /* previous change */
    change *ch;    /* new change */
    uint   px, nx; /* previous and this change x positions */
    ulong  py, ny; /* previous and this change y positions */
    uint   len;    /* length of previous change line */
    uint   flag;   /* flag if this change was combined with previous */
    ulong  i;      /* count */

#ifdef DEBUG
    printf("undo: changelist add\n");
#endif

    /*
     * Create a new change structure containing the coordinates
     * of the change, the type of change and the change data.
     */

    ch = change_new(x, y, op, em, obj);

    /*
     * See if the new change can be amalgomated with the a previous
     * one.  This could also mean a change in the previous changes
     * x and y start points, ie if it is an erase.
     */

    flag = 0;
    i = chl->size;

    if(i>0) {
        prev = chl->changes[i-1];

        if(prev->op == ch->op) {
            px = prev->x;
            py = prev->y;
            nx = ch->x;
            ny = ch->y;

            switch(ch->op) {
                case E_TEXT_INSERT:
                    len = dstring_get_length(prev->src.ln);

                    if(ny == py && (nx >= px && nx <= px+len)) {
                        dstring_cursor_goto_x(prev->src.ln, (nx-px));
                        dstring_insert(prev->src.ln, ch->src.ln);
                        flag = 1;
                    }
                    break;

                case E_TEXT_DELETE:
                    len = dstring_get_length(prev->src.ln);

                    if(ny == py) {
                        if(nx >=px && nx <= px + len) {
                            dstring_cursor_goto_x(prev->src.ln, (len-(nx-px)));
                            dstring_insert(prev->src.ln, ch->src.ln);
                            flag = 1;
                        } else if(nx == px - 1) {
                            dstring_cursor_to_left(prev->src.ln);
                            dstring_insert(prev->src.ln, ch->src.ln);
                            prev->x = nx;
                            flag = 1;
                        }
                    }
                    break;
 
                default: break;
            }  /* end switch */
        }  /* end if */
    }  /* end if */

    /*
     * If the change could not be combined with the previous
     * then it is simply add to the list.
     */

    if(flag == 0) {
#ifdef DEBUG
        printf("undo: new change..\n");
#endif
        chl->changes = (change**)realloc(chl->changes, sizeof(change*) * (i+2));
        chl->changes[i] = ch;
        chl->changes[i+1] = NULL;
        chl->size++;
    } else {
#ifdef DEBUG
        printf("undo: appending change..\n");
#endif
        change_free(ch);
    }
}

/* private functions */

change *change_new(uint x, ulong y, optype op, edmode em, void *obj)
{
    change *ch;
    dstring **aln;
    clip   **acl;

    ch = calloc(1, sizeof(change));
    ch->op = op;
    ch->em = em;
    ch->x  = x;
    ch->y  = y;

    switch(op) {
        case E_LIST_INSERT:
        case E_LIST_DELETE:
            ch->src.list = difflist_dup((difflist*)obj);
            break;

        case E_TEXT_INSERT:
        case E_TEXT_DELETE:
            ch->src.ln = dstring_dup((dstring*)obj);
            break;

        case E_TEXT_SUBS:
            aln = (dstring**)obj;
            ch->src.ln  = dstring_dup(aln[0]);
            ch->dest.ln = dstring_dup(aln[1]);
            break;

        case E_BLOCK_INSERT:
        case E_BLOCK_DELETE:
            ch->src.cl = clip_duplicate((clip*)obj);
            break;

        case E_BLOCK_SUBS:
            acl = (clip**)obj;
            ch->src.cl  = clip_duplicate(acl[0]);
            ch->dest.cl = clip_duplicate(acl[1]);

        default: break;
    }

    return(ch);
}

void change_free(change *ch)
{
    switch(ch->op) {
        case E_TEXT_INSERT:
        case E_TEXT_DELETE:
            dstring_free(ch->src.ln);
            break;

        case E_TEXT_SUBS:
            dstring_free(ch->src.ln);
            dstring_free(ch->dest.ln);
            break;

        case E_BLOCK_INSERT:
        case E_BLOCK_DELETE:
            clip_free(ch->src.cl);
            break;

        case E_BLOCK_SUBS:
            clip_free(ch->src.cl);
            clip_free(ch->dest.cl);
            break;

        case E_LIST_INSERT:
        case E_LIST_DELETE:
            difflist_free(ch->src.list);
            break;

        default: break;
    }

    free(ch);
}

