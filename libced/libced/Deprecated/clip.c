/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    A clip is a portion of text that has typically been copied or cut from
 *    the main buffer.  A clip is simply a list of lines.  A clip can be
 *    appended to other clips, loaded and saved.
 *
 *  Last Updated
 *    16th December 2000
 *    07th April 2003
 *
 *  To Do
 *
 *  Functionality to provide :-
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>

#include <libced/tools/dstring.h>
#include <libced/tools/error.h>
#include <libced/utils/utils.h>

#include "clip.h"

clip *clip_new()
{
    clip *cl = malloc(sizeof(clip));

    cl->lines = (dstring**)calloc(4, sizeof(dstring*));
    cl->size = 4;
    cl->used = 0;

    return(cl);
}

void clip_free(clip *cl)
{
    ulong i;

    for(i=0; i < cl->used; i++)
    {
        dstring_free(cl->lines[i]);
    }

    free(cl->lines);
    free(cl);
}

void clip_reset(clip *cl)
{
    ulong i;

    for(i=0; i < cl->used; i++)
    {
        dstring_free(cl->lines[i]);
    }

    cl->lines[0] = NULL;
    cl->used = 0;
}

void clip_addline(clip *cl, dstring *ln)
{
    dstring *lbuf;

    if( cl->used >= cl->size ) {
        cl->lines = (dstring**)realloc(cl->lines, (cl->size+4) * sizeof(dstring*));
        cl->size += 4;
    }

    lbuf = dstring_new();

    dstring_insert(lbuf, ln);

    cl->lines[cl->used] = lbuf;
    cl->used++;
}

clip *clip_duplicate(clip *cl)
{
    ulong i;
    clip  *ccl;

    ccl = clip_new();

    for(i=0;i<cl->used;i++)
    {
        clip_addline(ccl, cl->lines[i]);
    }

    return(ccl);
}

int clip_save(clip *cl, char *fname)
{
    FILE *fptr;
    dstring *ln;
    ulong i;
    int err = 1;

    if((fptr = fopen(fname, "w")) != NULL)
    {
        err = 0;

        for(i=0; i < cl->used; i++)
        {
            ln = cl->lines[i];

            if(dstring_fputs(ln, fptr, FT_UNIX, (i != cl->used - 1)) == -1)
            {
                err = 1;

                break;
            }
        }

        fclose(fptr);
    }

    if(err == 1)
    {
        printf("[clip_save] error writing: %s\n", error_get_str(errno));

        return(0);
    }

    return(1);
}

int clip_load(clip *cl, char *fname)
{
    FILE_TYPE ft;
    FILE *fptr;
    dstring *lbuf;

    ft = file_get_type(fname, FT_UNIX);

    lbuf = dstring_new();

    if((fptr = fopen(fname, "r")) != NULL)
    {
        clip_reset(cl);

        while(!feof(fptr))
        {
            clip_addline(cl, dstring_fgets(lbuf, fptr, ft));
        }

        fclose(fptr);
    }

    dstring_free(lbuf);

    return(1);
}

ulong clip_nlines(clip *cl)
{
    return(cl->used);
}


