#ifndef _CLIP_H
#define _CLIP_H

#include <libced/common.h>
#include <libced/tools/dstring.h>

typedef struct _clip clip;

struct _clip
{
  ulong used;       /* number of lines used in the clip */
  ulong size;       /* max number of lines in the clip */
  dstring **lines;  /* actual lines in the clip */
};

clip  *clip_new(void);
clip  *clip_duplicate(clip *cl);
void   clip_free(clip *cl);
void   clip_reset(clip *cl);
void   clip_addline(clip *cl, dstring *ln);
ulong  clip_nlines(clip *cl);
int    clip_load(clip *cl, char *fname);
int    clip_save(clip *cl, char *fname);

#endif /* _CLIP_H */

