/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *
 *  Todo
 *
 *  Last Updated
 *
 */

#define DEBUG

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "diffs.h"

DIFF* diff_new()
{
    DIFF *d = (DIFF*)calloc(1, sizeof(DIFF));

    return(d);
}

void diff_set_insert_text(DIFF *d, long x, long y, line *text)
{
    d->cmd = DIFF_INS_TEXT;
    d->x = x;
    d->y = y;
    d->ln1 = text;
}

void diff_set_delete_text(DIFF *d, long x, long y, line *text)
{
}

void diff_set_subs_text(DIFF *d, long x, long y, line *src, line *dest)
{
}

void diff_set_insert_newlines(DIFF *d, long x, long y, long amount)
{
}

void diff_set_delete_newlines(DIFF *d, long y, long amount)
{
}

void diff_apply(DIFF *d, buffer *buff)
{
}

void diff_negate(DIFF *d, buffer *buff)
{
}

void diff_free(DIFF* d)
{
}
