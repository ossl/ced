/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Provides an undo mechanism.
 *
 *  Last Updated
 *    16th February 2000
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

//#define DEBUG

undo   *undo_new(void);
event  *undo_undo(undo *u);
event  *undo_redo(undo *u);
void    undo_add_event(undo *u);

/* private */

event  *undo_event_new(void);
void    undo_event_free(event *ev);

/* public functions */

undo *undo_new()
{
    undo *u;
    u = calloc(1, sizeof(undo));
    u->first   = NULL;
    u->last    = NULL;
    u->current = NULL;
    u->lastop  = U_NONE;

    return(u);
}

event *undo_undo(undo *u)
{
    event *ev = NULL;

    if(u->current == NULL) {
        if(u->lastop == U_REDO) {
            ev = u->last;
        }
    }
    else {
        ev = u->current;
    }

#ifdef DEBUG
    if(ev == NULL) printf("undo: end of undo chain !\n");
#endif

    if(ev != NULL) u->current = ev->prev;

    u->lastop = U_UNDO;

    return(ev);
}

event *undo_redo(undo *u)
{
    event *ev = NULL;

    if(u->current == NULL) {
        if(u->lastop == U_UNDO) {
            ev = u->first;
        }
    }
    else {
        ev = u->current->next;
    }

    u->current = ev;
    u->lastop = U_REDO;

#ifdef DEBUG
    if(ev == NULL) printf("undo: end of redo chain !\n");
#endif

    return(ev);
}

void undo_add_change(undo *u, uint x, ulong y, optype op, edmode em, void *obj)
{
    event  *ev;
    chlist *chl;

#ifdef DEBUG
    printf("undo: add change\n");
#endif

    if((ev = u->current)==NULL) {
        undo_add_event(u);
    }

    if((ev = u->current)) {
        if(ev->state == 0) {
            undo_add_event(u);
        }
    }
    
    if((ev = u->current)) {
        chl = ev->chl;
        changelist_add(chl, x, y, op, em, obj);
    }
    else {
        printf("undo subsystem: error, couldn't create new event\n");
    }
}

/*
 * This effectively closes the event and doesn't allow any more changes
 * to be added to it.  When a change is attempted to be added, a new
 * event is created instead and added to that.
 */

void undo_end_event(undo *u)
{
    event *ev;


    if((ev = u->current)) {
#ifdef DEBUG
        if(ev->state) {
            printf("undo: ended current event\n");
        }
#endif
        ev->state = 0;
    }
}

void undo_add_event(undo *u)
{
    event *e = undo_event_new();

#ifdef DEBUG
    printf("undo: add new event\n");
#endif

    if(u->current == NULL) {
        if(u->lastop == U_REDO) {
            u->current = u->last;
        }

        if(u->lastop == U_UNDO) {
            undo_event_free(u->first);
            u->last = NULL;
            u->first = NULL;
        }
    }

    if(u->current) {
        undo_event_free(u->current->next);
        u->current->next = e;
        e->prev = u->current;
    }

    u->current = e;
    u->last = e;
    u->lastop = U_NONE;

    if(!u->first) {
        u->first = e;
    }
}

void undo_flush(undo *u)
{
    undo_event_free(u->first);
    u->first = NULL;
    u->current = NULL;
    u->last = NULL;
    u->lastop = U_NONE;
}

void undo_free(undo *u)
{
    undo_event_free(u->first);
    free(u);
}

/* "undo event" functions */

event *undo_event_new()
{
    event *e;
    e = (event*)calloc(1, sizeof(event));
    e->state = 1;
    e->chl   = changelist_new();
    e->next  = NULL;
    e->prev  = NULL;
    return(e);
}

void undo_event_free(event *ev)
{
    event *temp;

    while(ev) {
        temp = ev;
        ev = ev->next;

        if(temp->chl) {
            changelist_free(temp->chl);
        }

        free(temp);
    }
}

