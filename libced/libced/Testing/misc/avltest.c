#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "avltree.h"

int SIZE;

int  avltree_maxdepth(const AVLTree *t);
void avltree_check_integrity(AVLTree *t);

void moo(void *item)
{
//    printf("%d ", (*(int*)item));
//    (*(int*)item)++;
}

void avltree_show_items(AVLTree *t)
{
    if(t) {
        printf("%d \n", *(int*)t->item);

        avltree_show_items(t->left);
        avltree_show_items(t->right);
    }
}

int main()
{
    AVLTree *t = NULL;
    AVLTree *t2 = NULL;
    int i;
    int r;
    int a;
    int *meep;
    int thing0;
    int thing1;
    int thing2;

    SIZE = 0;

    thing0 = 5; t = avltree_insert(t, 5, &thing0);
    thing1 = 6; t = avltree_insert(t, 6, &thing1);
    thing2 = 7; t = avltree_insert(t, 7, &thing2);

    avltree_map_preorder(t, moo);

    printf("\n");

    avltree_map_inorder(t, moo);

    printf("\n");

    avltree_map_postorder(t, moo);
//    avltree_show_items(t);

    if(avltree_remove(t, 6, (void*)&meep)) {
        printf("%d\n", *meep);
    } else {
        printf("N\n");
    }

    if(avltree_remove(t, 6, (void*)&meep)) {
        printf("%d\n", *meep);
    } else {
        printf("N\n");
    }

    if(meep) printf("%d\n", *meep);

    printf("\n");

    exit(0);

    if((t2 = avltree_get_lt_or_eq(t, 14, NULL)) != NULL) {
        printf("Yup: %d\n", t2->value);
    }

    exit(0);

    for(r=0; r < 1000; r++) {
        printf("[avltree] randomize: %d\n", r);

        srandom(r);

        printf("[avltree] doing inserts..\n");

    
        for(i=0; i < 100000; i++) {
//            while(avltree_find(t, a = (random() % 1000000)));
//            a = i;
            a = random();
            t = avltree_insert(t, a, NULL);
        }
/*       */
//exit(0);
        printf("[avltree] doing integrity check..\n");

        SIZE = 0;
        avltree_check_integrity(t);

        printf("[avltree] tree size: %ld\n", SIZE);
        printf("[avltree] biggest depth: %d\n", avltree_maxdepth(t));
        if(t && t->left)  printf("[avltree] left depth: %d\n", avltree_maxdepth(t->left));
        if(t && t->right) printf("[avltree] right depth: %d\n", avltree_maxdepth(t->right));        

/* */

        printf("[avltree] doing deletes..\n");

        srandom(r);

        for(i=0; i < 99993; i++) {
//            while(!avltree_find(t, a = (random() % 1000000)));
            a = random();
            t = avltree_remove(t, a, NULL);
        }
//exit(0);
        printf("[avltree] doing integrity check..\n");

        SIZE = 0;
        avltree_check_integrity(t);

        printf("[avltree] checking depth..\n");
 
        if(SIZE != 7) {
            printf("[avltree] tree size error: %ld\n", SIZE);
            abort();
        }

        printf("-------------------------------------------------------------\n");

        avltree_free(t);

        t = NULL;
    }

//    avltree_print(t);

    return(1);
}

int avltree_maxdepth(const AVLTree *t)
{
    int l;
    int r;

    if(!t) return(0);

    if((l = avltree_maxdepth(t->left)) >= (r = avltree_maxdepth(t->right))) {
        return(1 + l);
    }

    return(1 + r);
}

void avltree_check_integrity(AVLTree *t)
{
    int err = 0;
    int diff;

    if(t == NULL) return;

    SIZE++;

    diff = abs(avltree_maxdepth(t->right) - avltree_maxdepth(t->left));

    if(diff > 1) {
        printf("[avltree] tree out of balance: %ld\n", t->value);
        err = 1;
    }

    if(t->balance > 1 || t->balance < -1) {
        printf("[avltree] tree out of balance: %ld\n", t->value);
        err = 1;
    }

    if(t->balance == 1) {
        if(avltree_maxdepth(t->right) <= avltree_maxdepth(t->left)) {
            printf("[avltree] balance says right heavy, tree says not\n");
            err = 1;
        }
    }

    if(t->balance == -1) {
        if(avltree_maxdepth(t->right) >= avltree_maxdepth(t->left)) {
            printf("[avltree] balance says left heavy, tree says not\n");
            err = 1;
        }
    }

    if(t->balance == 0) {
        if(avltree_maxdepth(t->right) != avltree_maxdepth(t->left)) {
            printf("[avltree] balance says equal, tree says not\n");
            err = 1;
        }
    }

    if(err) abort();

    avltree_check_integrity(t->left);
    avltree_check_integrity(t->right);
}

/////////////////////////////////////////////////////////////////////////////////////////


