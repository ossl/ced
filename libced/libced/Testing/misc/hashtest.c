#include <stdio.h>
#include "strhash.h"

int main()
{
    strhash *s;
    char str[20];
    int i;

    s = sh_new(256, 0);

    for(i=0; i < 5000; i++) {
        sprintf(str, "abc%d", i);
        sh_set(s, str, "abcd");
    }

    printf("-----------------------------------------------------\n");
    
    sleep(2);

    for(i=0; i < 5000; i++) {
        sprintf(str, "abc%d", i);
        printf("%s\n", sh_get(s, str));
    }
}


