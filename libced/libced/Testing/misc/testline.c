#include <stdio.h>
#include "line.h"

int main()
{
//    short s;
    int i;
    char buf[1024];
    line *ln = line_new();

    s = 65537;

    printf("%d\n", s);

    while(1)
    {
        fgets(buf, 1024, stdin);

        buf[strlen(buf)-1] = '\0';

        line_reset(ln);
        line_insert_string(ln, buf);
        line_goto_col(ln, 16, LINE_TAB_RIGHT);

        for(i=0; i < 30; i++)
        {
            printf("%d", i % 10);
        }

        printf("\n");

        printf("%s <%d>\n", line_get_string(ln), ln->xpos);

        for(i=0; i < ln->xpos; i++)
        {
            if(i == line_length(ln))
            {
                printf(" ");
            }
            else
            {
                if(ln->data[i] == '\t')
                {
                    printf("\t");
                }
                else
                {
                    printf(" ");
                }
            }
        }

        printf("^\n");

        for(i=0; i < line_length(ln); i++)
        {
            if(ln->data[i] == '\t')
            {
                printf("*");
            }
            else
            {
                printf("%c", ln->data[i]);
            }
        }
    
        printf("\n");

        for(i=0; i < ln->xpos; i++)
        {
            printf(" ");
        }
    
        printf("^\n");
    }

    line_free(ln);
}

