/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999, 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Implements a variation of a B-TREE.  The tree consists of pointer
 *    blocks leading to data blocks at the leafs of the tree.
 *    Pointer blocks are split in half when they are full and a new node
 *    is created.  Two entries are created in the new node, the first
 *    pointing to the left side of the split, the second to the right
 *    side.  Each pointer contains the number of lines in a certain
 *    direction.
 *
 *  Last Updated
 *    17th November 2000
 *
 *  To Do
 *    o Improve space efficiency of insertion algorithm
 *    o Implement the buffer put routine, rather than use the ins/del
 *    o Break routines up a little into smaller tree handling routines.
 *      This would improve readability and which could lead to a more
 *      efficient implementation.
 *
 */

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "datablock.h"
#include "ptrblock.h"
#include "buffer.h"
#include "line.h"
#include "utils.h"                                                                                       
#include "vmem.h"
#include "structs.h"

#define DEBUG

#ifdef DEBUG
#define TRACE T
#else
#define TRACE(...)
#endif

extern route *route_new(void);
extern void   route_free(route *rt, buffer *buff);
extern void   route_trace(route *rt, buffer *buff, ulong lnum);
extern void   route_update(route *rt, buffer *buff);

static void   buff_update_mblock(buffer *buff);
static ulong  accumulate_block(block*);

ULONG accumulate_block(block *blk);
block * new_datablk(buffer *buff);
block * new_ptrblk(buffer *buff);

extern void buff_set_dirty(buffer *buff, int b);

#define LEFT   1
#define RIGHT  2

/*
 * Delete line at the specified line number.  The datablock with the
 * line is first located, with the route being stored in a stack.
 *
 * If the datablock contains more than 1 line, then the line is simply
 * deleted.  Update is set to the datablock for updating the ptr blocks
 * during the traversal.
 *
 * If the datablock contains only the line to be deleted, delete the line
 * and delete the block.
 *
 * LOOP
 *   Go through the stack.
 *   
 *   If the pointerblock with the pointer to be deleted is exactly half
 *   full, check siblings for pointers that it may borrow it keep the
 *   pointer block half full.  If both siblings are only half full too,
 *   then combine them and create one node.
 *
 *   hmm..
 *
 * END LOOP
 */

int buff_delete(buffer* buff, ULONG lnum)
{
    block      *update;
    block      *delete;
    block      *currblk;
    block      *leftsib_blk;
    block      *rightsib_blk;
    pbheader   *sib_left;
    pbheader   *sib_right;
    route      *rts;
    rtelem     *parent;
    rtelem     *rte;
    int        parent_entry;
    pbheader   *parent_ptrblk;
    pbheader   *pbh;
    dbheader   *dbh;
    ptrelem    pelem;
    int        offset;
    int        sl_nptrs = 0;
    int        sr_nptrs = 0;
    int        sibused = 0;
    vmem       *vm;

    TRACE("vmlbuf::delete line: %lu\n", lnum);

    if(lnum == 0 || lnum > buff->nlines) {
        return(0);
    }

    if(!buff->use_swap) {
        line_free(buff->lines2.data[lnum-1]);
        ptr_array_del(&buff->lines2, (lnum-1));
        buff->nlines--;
        buff_set_dirty(buff, 1);
        return(1);
    }

    /*
     * Before a line can be deleted, the route to the datablock containing
     * the line must be established.  A stack is filled with the route defined
     * by the pointer blocks.
     */

    vm = buff->vm;
    rts = buff->rt;
    route_trace(rts, buff, lnum);
    rte = rts->last;
    currblk = rte->blk;

    /*
     * Delete line from data block..
     * First find out offset in the datablock, ie the actual line
     * number in the datablock, then delete it.
     */

    offset = rte->entry;
    dbh = (dbheader*)currblk->data;
    dblk_delete(dbh, offset);

    /*
     * If there are still lines in the data block, then the pointer blocks
     * enroute simply need updating to reflect the change in the number of
     * lines.  If however the datablock is now empty, then the datablock
     * itself must be deleted from the tree.
     */

    if(dbh->nlines > 0) {
        TRACE("vmlbuf::delete: number of lines in datablock is more than 0\n");

        vm_set_dirty(vm, currblk);

        update = currblk;  /* want to update info about currblk */
        delete = NULL;     /* dont want to delete anything */

    } else {
        TRACE("vmlbuf::delete: datablock is empty\n");

        if(buff->nlines > 1) {
            update = NULL;     /* dont want to update anything about currblk */
            delete = currblk;  /* want to delete currblk */
        } else {
            TRACE("vmlbuf::delete: keeping empty datablock\n");
            update = currblk;
            delete = NULL;
        }
    }

    /*
     * Propergate back up the stack, making changes to the pointerblocks
     * enroute according to what is set (update/delete).  If the datablock
     * was not made empty, then no block or pointer deletion is necessary,
     * just updates.
     */

    rte = rte->prev;

    while(rte) {
        currblk = rte->blk;
        pbh = (pbheader*)currblk->data;

        if(update) {
            pelem.nlines = accumulate_block(update);
            pelem.blknum = update->num;
            pblk_putelem(pbh, rte->entry, pelem);

            vm_set_dirty(vm, currblk);

            update = currblk;

            TRACE("vmlbuf::delete: updated ptrblk (%lu) elem (%d) with blk (%lu) with (%lu) lines\n", currblk->num, rte->entry, pelem.blknum, pelem.nlines);

            rte = rte->prev;
        }

        if(delete) {
            TRACE("vmlbuf::delete: deleting blk (%lu) from ptrblk (%lu) at elem (%d) ", delete->num, currblk->num, rte->entry);

            pblk_delete(pbh, rte->entry);

            vm_set_dirty(vm, currblk);
            vm_free_block(vm, delete);

            /*
             * Calculate how many elements is a half full pointerblock.
             */

            offset = (pbh->maxptrs*0.5);

            /*
             * If the pointerblock is not the root, then it should never get
             * less than half full.  If it does, a pointer must be stolen from
             * its immediate siblings to make up the loss.
             */

            if(rte) {
                if(pbh->nptrs == (offset-1)) {
                    TRACE("vmlbuf::delete: pointer block is only half full\n");
                    TRACE("vmlbuf::delete: half entries is %d, block had %d\n", offset, (pbh->nptrs+1));

                    /*
                     * get left and right siblings..
                     * first get the parent pointer block and the entry from it
                     * which originally pointed to the original pointer block.
                     */

                    rte = rte->prev;

                    if(!rte) break;

                    parent = rte;
                    parent_ptrblk = (pbheader*)parent->blk->data;
                    parent_entry = parent->entry;

                    TRACE("vmlbuf::delete: not root block, continuing balance correction ..\n");
                    TRACE("vmlbuf::delete: parent number is (%lu)\n", parent->blk->num);
                    TRACE("vmlbuf::delete: parent contains (%d) pointers\n", parent_ptrblk->nptrs);
                    TRACE("vmlbuf::delete: looking at entry number (%d)\n", parent_entry);

                    leftsib_blk = NULL;
                    rightsib_blk = NULL;
                    sib_left = NULL;
                    sib_right = NULL;
                    sl_nptrs = 0;
                    sr_nptrs = 0;

                    /*
                     * If the entry is not the first, then grab the sibling left
                     * of the entry.  If the entry is not the last, also grab the
                     * right sibling of the entry.
                     */

                    if(parent_entry > 0) {
                        TRACE("vmlbuf::delete: fetching left sibling ..\n");

                        pblk_getelem(parent_ptrblk, parent_entry-1, &pelem);
                        leftsib_blk = vm_get_block(vm, pelem.blknum,1);
                        sib_left = (pbheader*)leftsib_blk->data;
                        sl_nptrs = sib_left->nptrs;
                    }

                    if(parent_entry < (parent_ptrblk->nptrs-1)) {
                        TRACE("vmlbuf::delete: fetching right sibling ..\n");

                        pblk_getelem(parent_ptrblk, parent_entry+1, &pelem);
                        rightsib_blk = vm_get_block(vm, pelem.blknum, 1);
                        sib_right = (pbheader*)rightsib_blk->data;
                        sr_nptrs = sib_right->nptrs;
                    }

                    /*
                     * If there are NO siblings, then the block becomes the
                     * root block and the process is done.
                     */

                    if(sl_nptrs == 0 && sr_nptrs == 0) {
                        TRACE("vmlbuf::delete: There are no siblings, block becomes root\n");

                        vm_free_block(vm, buff->root);
                        buff->root = currblk;
                        buff->rootnum = currblk->num;
                        buff_update_mblock(buff);

                        break;
                    }

                    /*
                     * If neither sibling is only exactly half full, borrow from
                     * the sibling ptrblock with most entries.  This helps keep
                     * things balanced.
                     */

                    TRACE("vmlbuf::delete: left sibling contains %d pointers\n", sl_nptrs);
                    TRACE("vmlbuf::delete: right sibling contains %d pointers\n", sr_nptrs);

                    if((sl_nptrs != offset) && (sr_nptrs != offset)) {
                        TRACE("vmlbuf::delete: neither sibling is half full, so steal from most full\n");

                        if(sl_nptrs > sr_nptrs) {
                            pblk_getelem(sib_left, sl_nptrs-1, &pelem);
                            pblk_insert(pbh, 0, pelem);
                            pblk_delete(sib_left, sl_nptrs-1);
                            sibused = LEFT;
                        } else {
                            pblk_getelem(sib_right, 0, &pelem);
                            pblk_insert(pbh, pbh->nptrs, pelem);
                            pblk_delete(sib_right, 0);
                            sibused = RIGHT;
                        }

                        update = currblk;
                        delete = NULL;
                    }

                    /*
                     * if it is only half full, then combine the pointerblocks.
                     */

                    else {
                        TRACE("vmlbuf::delete: combining pointer blocks ..\n");

                        /*
                         * If the left sibling is only half full, append the pointers
                         * onto the end of it, else if the right sibling is only half
                         * full, prepend the pointers onto the beginning of it.
                         */

                        if(sl_nptrs == offset) {
                            pblk_append(sib_left, pbh);
                            sibused = LEFT;
                        } else if(sr_nptrs == offset) {
                            pblk_prepend(sib_right, pbh);
                            sibused = RIGHT;
                        }

                        /*
                         * Don't want to update anything next time round, the siblings
                         * are updated below.  Do want to delete the empty pointer block.
                         */

                        update = NULL;
                        delete = currblk;
                    }

                    /*
                     * If there were changes made to one of the sibling blocks then
                     * reflect the change in the parent block.  There is never a need
                     * for both siblings to be updated.
                     */

                    if(sibused == LEFT) {
                        pelem.nlines = accumulate_block(leftsib_blk);
                        pelem.blknum = leftsib_blk->num;
                        pblk_putelem(parent_ptrblk, parent_entry-1, pelem);
                    } else if(sibused == RIGHT) {
                        pelem.nlines = accumulate_block(rightsib_blk);
                        pelem.blknum = rightsib_blk->num;
                        pblk_putelem(parent_ptrblk, parent_entry+1, pelem);
                    }

                    if(leftsib_blk) vm_put_block(vm, leftsib_blk);
                    if(rightsib_blk) vm_put_block(vm, rightsib_blk);
                }

                /*
                 * If the pointer block is more than half full, the pointer has
                 * already been deleted, now just set update to propergate the
                 * change in number of lines back up the tree.  Nothing needs to
                 * be deleted.
                 */

                else {
                    rte = rte->prev;
                    delete = NULL;
                    update = currblk;
                } /* endif pointer block half full */
            }   /* endif pointer block is root block */
        }     /* endif delete */
    }       /* end while not at top of route */

    /*
     * Decrement the number of lines in the buffer, done deletion.
     */

    buff->nlines--;
    buff_set_dirty(buff, 1);

    return(1);
}

/*
 * Insert line at the specified line number.  The datablock with the
 * line previous to the line being inserted at is first located.  The
 * route to the datablock is stored in a stack.
 *
 * If the datablock has space free, then insert into that block,
 * else a new one is created and all the lines from the old block that
 * proceeds lnum are moved to the new block.
 *
 * If there is now room in the original found datablock, then add
 * the new line to it, else put it in the new datablock.
 *
 * "Update" is set to the block that has changed so that changes can
 * be reflected in the pointer blocks on the way back up the tree.
 * "Passback" is set to the new block if it was created.
 *
 * LOOP
 *   Go through the stack.
 *   Change "Update" in the previous pointer block.
 *   Insert "Passback" in the previous pointer block.
 *
 *   If the pointer block is full, it is split in half, the latter half
 *   being put into a new block.  "Passback" is then set to the new
 *   block.
 * END LOOP
 */

int buff_insert(buffer *buff, ULONG lnum, line *ln)
{
    block      *update;
    block      *passback;
    block      *currblk;
    block      *newblk;
    pbheader   *newpbh;
    dbheader   *newdbh;
    pbheader   *pbh;
    dbheader   *dbh;
    ptrelem    pelem;
    ptrelem    pelem2;
    int        i;
    int        np;
    int        p;
    int        len;
    int        bfree;
    int        offset;
    int        nlines;
    route      *rts;
    rtelem     *rte;
    line       *lbuf;
    vmem       *vm;
    line       *l_ins;

    TRACE("vmlbuf::insert: line number: %lu\n", lnum);

    // FIXME - thinking that lines should start from 0..

    if(lnum == 0) return(0);

    if(!buff->use_swap) {
        l_ins = line_new();
        line_insert(l_ins, ln);
        ptr_array_ins(&buff->lines2, (lnum-1), (void*)l_ins);
        buff->nlines++;
        buff_set_dirty(buff, 1);
        return(1);
    }

    /* could wrap here instead */

    lbuf = line_new();
    len = line_length(ln);

    if(len > 980) {
        buff_call_message(buff, "Error, unhandled line length :-(  Truncating.", 1);
        line_goto_col(ln, 980);
        line_define_region(ln);
        line_to_right(ln);
        line_region_delete(ln);
    }

    /*
     * Before a line can be inserted, the route to the datablock containing
     * the line previous must be established.  A stack is filled with the
     * route defined by the pointer blocks.
     */

    vm = buff->vm;
    rts = buff->rt;
    route_trace(rts, buff, lnum-1);
    rte = rts->last;
    currblk = rte->blk;

    /*
     * The following algorithm inserts the actual line into the tree.
     *
     * If there is no room in the datablock, a new one is created.  The
     * pointer to this new block is passed back to be inserted into the
     * preceding pointer block.
     */

    dbh = (dbheader*)currblk->data;
    bfree = (DEFAULT_PAGE_SIZE-sizeof(dbheader)) - dbh->used - 2;
    offset = rte->entry + 1;

    /* could make a loop out of this.. */

    if(bfree < len) {
        TRACE("vmlbuf::insert: insufficient room in datablock\n");

        newblk = new_datablk(buff);
        newdbh = (dbheader*)newblk->data;

        /* 
         * need to move from lnum to end of full block to new block
         * if the line to be inserted is within the block.
         */

        nlines = dbh->nlines;

        for(i=offset;i<=nlines;i++) {
            dblk_getline(dbh, offset, lbuf);
            dblk_delete(dbh, offset);
            dblk_insert(newdbh, (i-offset)+1, lbuf);
        }

        bfree = (DEFAULT_PAGE_SIZE - sizeof(dbheader)) - dbh->used - 2;

        /*
         * ok, moves lots of lines out, is there enough room now ?
         * if not, insert at the beginning of the new block..
         */

        if(bfree < len) {
            dblk_insert(newdbh, 1, ln);
        } else {
            dblk_insert(dbh, offset, ln);
        }

        update = currblk;
        passback = newblk;
    }

    /*
     * If the datablock has room in it, then the line is inserted into
     * the correct position in that datablock and all pointer blocks
     * enroute updated to reflect the incremented number of lines in that
     * path.
     */

    else {
        dblk_insert(dbh, offset, ln);
        update = currblk;
        passback = NULL;
    }

    vm_set_dirty(vm, currblk);

    /*******************/

    while((rte = rte->prev)) {
        currblk = rte->blk;
        pbh = (pbheader*)currblk->data;

        if(update) {
            pelem.nlines = accumulate_block(update);
            pelem.blknum = update->num;
            pblk_putelem(pbh, rte->entry, pelem);
        }

        /*
         * If there is still a block to pass back :-
         * If the pointer block is full, split it in half.  Create a new
         * block and take the last half from the full block and put it
         * in the first half of the new block.  The new block is then
         * passed back to the previous pointer block, splitting it if
         * neccessary to make room, and so on.
         */

        if(passback) {
            pelem.nlines = accumulate_block(passback);
            pelem.blknum = passback->num;

            if(pbh->nptrs == pbh->maxptrs) {
                TRACE("vmlbuf::insert: pointer block is full\n");
                TRACE("vmlbuf::insert: pointer info: BLKNUM %lu\n", currblk->num);
                TRACE("vmlbuf::insert: nptrs: %d maxptrs: %d\n", pbh->nptrs, pbh->maxptrs);

                newblk = new_ptrblk(buff);
                newpbh = (pbheader*)newblk->data;
                offset = pbh->maxptrs/2;
                np = 0;

                for(p=offset;p<pbh->maxptrs;p++) {
                    pblk_getelem(pbh, offset, &pelem2);
                    pblk_delete(pbh, offset);
                    pblk_insert(newpbh, np, pelem2);
                    np++;
                }

                /*
                 * After the pointer block is split, the pointer must either
                 * be inserted into the new block or original block depending
                 * on its value.
                 */

                if(rte->entry < offset) {
                    pblk_insert(pbh, rte->entry+1, pelem);
                } else {
                    pblk_insert(newpbh, (rte->entry-(offset)+1), pelem);
                }

                vm_put_block(vm, passback);

                update = currblk;
                passback = newblk;
            }

            /*
             * If there is room in the pointer block then simply insert the
             * pointer into the correct position.  Nothing else needs to be
             * passed back.
             */

            else {
                pblk_insert(pbh, rte->entry+1, pelem);
                vm_put_block(vm, passback);
                passback = NULL;
            }
        }

        vm_set_dirty(vm, currblk);

        update = currblk;
    }

    /*
     * If the top of the stack is reached (the root node) and it is full,
     * then a new root node is needed.  Create a new root node, make the
     * first entry a pointer to the original tree, and the second point
     * to the tree passed back.
     */

    if(passback != NULL) {
        newblk = new_ptrblk(buff);
        pelem.nlines = accumulate_block(update);
        pelem.blknum = update->num;
        pblk_insert((pbheader*)newblk->data, 0, pelem);

        pelem.nlines = accumulate_block(passback);
        pelem.blknum = passback->num;
        pblk_insert((pbheader*)newblk->data, 1, pelem);
        buff->root = newblk;
        buff->rootnum = newblk->num;

        vm_put_block(vm, passback);

        buff_update_mblock(buff);
    }

    buff->nlines++;
    line_free(lbuf);
    buff_set_dirty(buff, 1);

    return(1); /* for now */
}

int buff_getline(buffer* buff, ULONG lnum, line *ln)
{
    block      *currblk;
    dbheader   *dbh;
    route      *rts;
    rtelem     *rte;

    TRACE("vmlbuf::getline: getting line (%lu)\n", lnum);

    if(lnum == 0 || lnum > buff->nlines) {
        line_reset(ln);
        return(0);
    }

    if(buff->use_swap == 0) {
        line_reset(ln);
        line_insert(ln, buff->lines2.data[lnum-1]);
    } else {
        rts = buff->rt;
        route_trace(rts, buff, lnum);
        rte = rts->last;
        currblk = rte->blk;

        dbh = (dbheader*)currblk->data;
        dblk_getline(dbh, rte->entry, ln);
    }

    return(1);
}

/*
 * Accumulates the total number of lines that a pointerblock points
 * to or if its a datablock, the number of actual lines in the block.
 */

ULONG accumulate_block(block *blk)
{
    dbheader*  dbh;
    pbheader*  pbh;
    int        i;
    ULONG      ltot = 0;

    pbh = (pbheader*)blk->data;
    dbh = (dbheader*)blk->data;

    if(pbh->blktype == 'p') {
        for(i=0;i<pbh->nptrs;i++) {
            ltot += pbh->bptr[i].nlines;
        }
    } else {
        ltot = dbh->nlines;
    }

    return(ltot);
}

//int buff_putline(buffer *buff, ULONG lnum, line *ln)
//{
//    buff_delete(buff, lnum);
//    buff_insert(buff, lnum, ln);
//
//    return(1);
//}

/*
 * Create a new data block.  A data block is tagged with the character
 * 'd' to indicate its type.  When reading and writing data from a
 * data block, the data area itself is cast to type dbheader.
 */

block * new_datablk(buffer *buff)
{
    vmem     *vm;
    block    *blk;

    vm = buff->vm;
    blk = vm_new_block(vm, 1);
    if(!blk) abort();
    dblk_new((dbheader*)blk->data);
    return(blk);
}

/*
 * Create a new pointer block.  A pointer block is tagged with the
 * character 'p' to indicate its type.  When reading and writing data
 * from a pointer block, the data area itself is cast to type pbheader.
 */

block * new_ptrblk(buffer *buff)
{
    vmem    *vm;
    block   *blk;

    vm = buff->vm;
    blk = vm_new_block(vm, 1);
    if(!blk) abort();
    pblk_new((pbheader*)blk->data);
    return(blk);
}

void buff_update_mblock(buffer *buff)
{
    mblock *mb;

    if(buff->mstblk != NULL) {
        mb = (mblock*)buff->mstblk->data;
        long_to_char(buff->rootnum, mb->rootnum);
        vm_set_dirty(buff->vm, buff->mstblk);
    }
}
     
int buff_putline(buffer *buff, ULONG lnum, line *ln)
{
//    block      *currblk;
//    dbheader   *dbh;
//    int        offset;
//    route      *rts;
//    rtelem     *rte;
//    line       *lbuf;
//    uint       old_len;
//    uint       new_len;

    if(lnum == 0)
        return(0);

    if(!buff->use_swap) {
        line_reset(buff->lines2.data[lnum-1]);
        line_insert(buff->lines2.data[lnum-1], ln);
        buff_set_dirty(buff, 1);
        return(1);
    }

/*************************
    if(line_length(ln) > 980) {
        buff_call_message(buff, "Error, unhandled line length :-(  Truncating.", 1);
        line_goto_col(ln, 980);
        line_define_region(ln);
        line_to_right(ln);
        line_region_delete(ln);
    }

    rts = buff->rt;
    route_trace(rts, buff, lnum);
    rte = rts->last;
    currblk = rte->blk;
    dbh = (dbheader*)currblk->data;
    offset = rte->entry;

    lbuf = line_new();

    dblk_getline(dbh, offset, lbuf);

    old_len = line_length(lbuf);
    new_len = line_length(ln);

    line_free(lbuf);
    
    if((new_len <= old_len) || ((dblk_avail(dbh)-2) >= new_len - old_len)) {
        dblk_delete(dbh, offset);
        dblk_insert(dbh, offset, ln);
        vm_set_dirty(buff->vm, currblk);
    } else {
*******/
        buff_delete(buff, lnum);
        buff_insert(buff, lnum, ln);
/********
    }
*********/

    return(1);
}
