void buff_z_pad(buffer *buff, long x, long y)
{
    if( y >= buff->nlines ) {
        y -= buff->nlines;

        prim_ins_v_space(buff, buff->nlines, y + 1);

        if( x > 0 )
        {
            prim_ins_h_space(buff, 0, buff->nlines - 1, x);
        }
    }
    else
    {
        
    }
}
