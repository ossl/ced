
static void diff_apply(DIFF *diff)
{
    line *lbuf;

    switch(d->cmd)
    {
        case DIFF_INS_HSPC:
        case DIFF_INS_TEXT:
        case DIFF_DEL_TEXT:
        case DIFF_SUB_TEXT:
            diff_apply_text(buff, diff);

            break;

        case DIFF_INS_ROW:
        case DIFF_DEL_ROW:
        case DIFF_SUB_ROW:
            diff_apply_row(buff, diff);

            break;
    }
}

static void diff_apply_text(buffer *buff, DIFF *diff)
{
    line *lbuf = line_new();

    buff_getline(buff, d->y + 1, lbuf);

    switch(d->cmd)
    {
        case DIFF_INS_HSPC: ln_ins_hspc(buff, lbuf, d->x, d->y, d->amount, DIFF_NONE); break;
        case DIFF_SUB_TEXT: ln_sub_text(buff, lbuf, d->x, d->y, d->amount, d->sub, DIFF_NONE); break;
        case DIFF_DEL_TEXT: ln_del_text(buff, lbuf, d->x, d->y, d->amount, DIFF_NONE); break;
        case DIFF_INS_TEXT: ln_ins_text(buff, lbuf, d->x, d->y, d->ins, DIFF_NONE); break;
    }

    line_free(lbuf);
}

static void diff_apply_row(buffer *buff, DIFF *d)
{
    switch(d->cmd)
    {
        case DIFF_INS_ROW: buff_insert(buff, d->y + 1, d->ins);
        case DIFF_DEL_ROW: buff_delete(buff, d->y + 1);
        case DIFF_SUB_ROW: buff_putline(buff, d->y + 1, d->ins);
    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// LOW LEVEL FUNCTIONS - PRIVATE
////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////
// MEDIUM LEVEL FUNCTIONS - PUBLIC
////////////////////////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////

/*
void buff_z_apply_diffs(buffer *buff, diffs *ds)
{
}

void buff_z_apply_diff(buffer *buff, diff *d)
{
    switch(diff_get_type(d))
    {
        case DIFF_INS_TEXT: text_insert (buff, NULL, diff->x, diff->y, diff->
        case DIFF_DEL_TEXT: text_delete (buff, NULL, diff->x, diff->y, diff->len); break;
        case DIFF_SUB_TEXT: text_subs   (buff, NULL, diff->x, diff->y, diff->len, diff->ln1); break;

        case DIFF_INS_NEWLINE: newline_insert (buff, NULL, diff->x, diff->y); break;
        case DIFF_DEL_NEWLINE: newline_delete (buff, NULL, diff->y); break;

        case DIFF_INS_ROW: rows_insert (buff, NULL, diff->y, diff->nls); break;
        case DIFF_DEL_ROW: rows_delete (buff, NULL, diff->y);
    }
}

void buff_z_negate_diffs(buffer *buff, diffs *d)
{
}

void buff_z_negate_diff(buffer *buff, diffs *d)
{
    switch(diff_get_type(d))
    {
        case DIFF_INS_TEXT: text_insert (buff, NULL, diff->x, diff->y, diff->
        case DIFF_DEL_TEXT: text_delete (buff, NULL, diff->x, diff->y, diff->len); break;
        case DIFF_SUB_TEXT: text_subs   (buff, NULL, diff->x, diff->y, diff->len, diff->ln1); break;

        case DIFF_INS_NEWLINE: newline_insert (buff, NULL, diff->x, diff->y); break;
        case DIFF_DEL_NEWLINE: newline_delete (buff, NULL, diff->y); break;

        case DIFF_INS_ROW: rows_insert (buff, NULL, diff->y, diff->nls); break;
        case DIFF_DEL_ROW: rows_delete (buff, NULL, diff->y);
    }
}

*/



///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void buff_set_dirty(buffer *buff, int b)
{
    if( buff->dirty != b) {
        buff->dirty = b;
        buff_call_file_attribs_changed_notify(buff);
    }
}

void buff_set_readonly(buffer *buff, int b)
{
    if( buff->ro != b) {
        buff->ro = b;
        buff_call_file_attribs_changed_notify(buff);
    }
}

lndiff *lndiff_new(uint x, ulong y, line *ln, line *sub)
{
    lndiff *ld;

    ld = calloc(1, sizeof(lndiff));

    ld->x = x;
    ld->y = y;
    ld->ldiff = line_dup(ln);
    ld->lsubs = line_dup(sub);

    return(ld);
}

void lndiff_free(lndiff *lnd)
{
    if(lnd->ldiff) line_free(lnd->ldiff);
    if(lnd->lsubs) line_free(lnd->lsubs);

    free(lnd);
}

difflist *difflist_new()
{
    difflist *dlist;
    dlist = calloc(1, sizeof(difflist));
    ptr_array_init(dlist);

    return(dlist);
}

void difflist_free(difflist *dlist)
{
    ulong i;
    lndiff *ld;

    for(i=0; i < dlist->items; i++) {
        ld = (lndiff*)dlist->data[i];
        lndiff_free(ld);
    }

    ptr_array_free_data(dlist);

    free(dlist);
}

void difflist_add(difflist *dlist, uint x, ulong y, line *ln, line *sub)
{
    lndiff *ld;
    ld = lndiff_new(x, y, ln, sub);
    ptr_array_add(dlist, (void*)ld);
}

difflist *difflist_dup(difflist *dlist)
{
    difflist *dest;
    lndiff   *ld;
    ulong    i;

    dest = difflist_new();

    for(i=0; i < dlist->items; i++) {
        ld = (lndiff*)dlist->data[i];

        difflist_add(dest, ld->x,
                           ld->y,
                           ld->ldiff,
                           ld->lsubs);
    }

    return(dest);
}

/*************************************************************************************
 * Buffer manipulation routines.
 *
 * It should be noted that the row and column positions passed to these
 * routines are actual screen positions, NOT positions in a raw line.  The
 * screen positions are different because tabs have been expanded.  The
 * routines must convert these values where neccessary by using the
 * map_column_to_x function.
 *
 * Functions:
 *   buff_text_insert         (buff, col, row, ln, flags);
 *   buff_text_delete         (buff, col, row, n, flags);
 *   buff_text_subs           (buff, col, row, n, ln, flags);
 *   buff_line_split          (buff, col, row, flags);
 *   buff_line_delete_newline (buff, row, flags);
 *   buff_block_insert        (buff, col, row, cl, flags);
 *   buff_block_delete        (buff, c1, r1, c2, r2, flags);
 *   buff_rect_insert         (buff, col, row, cl, flags);
 *   buff_rect_delete         (buff, col, row, w, h, flags);
 *   buff_pad                 (buff, col, row, flags);
 *
 * Alterations that occur during these routines must inform the syntax highlighting
 * sub-system and the front-end that lines have been: changed, inserted or deleted.
 *
 */

int buff_apply_difflist(buffer *buff, difflist *dl, int neg)
{
    line  *lbuf;
    line  *subs;
    line  *diff;
    ulong ymin = 0;
    ulong ymax = 0;
    ulong lnum;
    ulong i;
    ulong n = 0;
    uint  len;
    lndiff *ld;

    if(!dl->items)
        return(1);

    if(neg) n = dl->items - 1;

    lbuf = line_new();

    for(i=0; i < dl->items; i++) {
        ld = (lndiff*)dl->data[n];

        if(neg) {
            subs = ld->ldiff;
            diff = ld->lsubs;
            n--;
        } else {
            subs = ld->lsubs;
            diff = ld->ldiff;
            n++;
        }

        lnum = ld->y;
        ymin = min(ymin, lnum);
        ymax = max(ymax, lnum);

        if(i==0) {
            ymin = lnum;
            ymax = lnum;
        }

        buff_getline(buff, (lnum + 1), lbuf);
        line_goto_x(lbuf, ld->x);

        if(subs) {
            len = line_length(subs);
            line_define_region(lbuf);
            line_arrow_right(lbuf, len);
            line_region_delete(lbuf);
        }

        if(diff) {
            line_insert(lbuf, diff);
        }

        buff_putline(buff, (lnum + 1), lbuf);
        ymax += (buff_preprocess_line(buff, lbuf, lnum)-1);

    }

    buff_call_lines_changed_notify(buff, ymin, ((ymax - ymin) + 1));

    line_free(lbuf);

    return(1);
}

/*
 * Insert a single line segment of text into the buffer.  The buffer is first padded
 * out to 'row', 'col'.  The line 'row' is then fetched and the line segment inserted
 * at 'col'.  The change is recorded in the undo buffer, and the line is put back into
 * the line buffer.
 */

int buff_text_insert(buffer *buff, uint col, ulong row, line *ln, int flags)
{
    line *lbuf;
    ulong laff;
    uint  x;

    lbuf = line_new();

    buff_pad(buff, col, row, flags);
    buff_getline(buff, (row+1), lbuf);
    x = map_column_to_x(lbuf, col, buff->tabstops);

    line_goto_x(lbuf, x);

    if(line_length(ln) > 0) {
        line_insert(lbuf, ln);
        buff_putline(buff, (row+1), lbuf);
        buff_process_change(buff, col, row, E_TEXT_INSERT, (void*)ln, flags);
        laff = buff_preprocess_line(buff, lbuf, row);
        buff_call_lines_changed_notify(buff, row, max(laff, 1));
    }

    line_free(lbuf);

    return(1);
}

/*
 * Delete a segment of a line from the buffer.  The line at 'row' is first fetched from
 * the line buffer and the segment of length 'n' is cut from it and stored in the undo
 * buffer, and the altered line is then put back into the line buffer.
 */

int buff_text_delete(buffer *buff, uint col, ulong row, uint n, int flags)
{
    line *lbuf;
    line *ldel;
    ulong laff;
    uint  x;

    if(row < buff->nlines) {
        lbuf = line_new();
        ldel = line_new();

        buff_getline(buff, (row+1), lbuf);
        x = map_column_to_x(lbuf, col, buff->tabstops);

        if(x < line_length(lbuf)) {
            line_define_region_at(lbuf, x);
            line_goto_x(lbuf, x + n);
            line_region_cut(lbuf, ldel);

            buff_putline(buff, (row+1), lbuf);

            buff_process_change(buff, col, row, E_TEXT_DELETE, (void*)ldel, flags);
            laff = buff_preprocess_line(buff, lbuf, row);
            buff_call_lines_changed_notify(buff, row, max(laff, 1));
        }

        line_free(lbuf);
        line_free(ldel);
    }

    return(1);
}

/*
 * Substitute a segment of a line in the buffer with the line segment 'ln'.  First the
 * buffer is padded out if neccessary, and the line at 'row' is fetched.  The segment
 * of line to be replaced is cut, and the new segment is inserted.  The replaced text
 * and the new text is then put into the undo buffer.
 */

int buff_text_subs(buffer *buff, uint col, ulong row, uint n, line *ln, int flags)
{
    line *lbuf;
    line *ldel;
    difflist *dlist;
    ulong laff;
    uint xs;

    lbuf = line_new();
    ldel = line_new();

    buff_pad(buff, col, row, flags);
    buff_getline(buff, (row+1), lbuf);

    /* delete old segment of size n */

    line_define_region_at_col(lbuf, col, buff->tabstops, LINE_TAB_RIGHT);
    line_goto_col(lbuf, col + n, buff->tabstops, LINE_TAB_LEFT);
    line_region_cut(lbuf, ldel);

    /* insert replacement segment */

    xs = lbuf->xpos;

    line_insert(lbuf, ln);

    buff_putline(buff, row + 1, lbuf);

    dlist = difflist_new();
    difflist_add(dlist, xs, row, ldel, ln);

    buff_process_change(buff, col, row, E_LIST_DELETE, (void*)dlist, flags);

    laff = buff_preprocess_line(buff, lbuf, row);

    buff_call_lines_changed_notify(buff, row, max(laff, 1));
 
    line_free(lbuf);
    line_free(ldel);

    difflist_free(dlist);

    return(1);
}

/*
 * Split the line 'row' at 'col', inserting the second part of the split as a new
 * line after row.  The buffer is first padded to where the split is to occur, and
 * the line is fetched from the line buffer.  The line is cut from 'col' onwards
 * and inserted at line 'row' + 1.
 */

int buff_line_split(buffer *buff, uint col, ulong row, int flags)
{
    line *lbuf;
    line *lnew;
    ulong laff;
    uint x;

    lbuf = line_new();
    lnew = line_new();

    buff_pad(buff, col, row, flags);
    buff_getline(buff, (row+1), lbuf);
    x = map_column_to_x(lbuf, col, buff->tabstops);

    line_define_region_at(lbuf, x);
    line_to_right(lbuf);
    line_region_cut(lbuf, lnew);

    buff_putline(buff, (row+1), lbuf);
    buff_insert(buff, (row+2), lnew);

    /* update syntax sub-system, undo buffer and front-end */

    syntax_line_insert_compensate(buff->lsyn, (row + 1), 1);
    buff_call_lines_inserted_notify(buff, (row + 1), 1);
    buff_process_change(buff, col, row, E_LINE_SPLIT, NULL, flags);
    buff_preprocess_line(buff, lnew, (row + 1));
    laff = buff_preprocess_line(buff, lbuf, row);
    buff_call_lines_changed_notify(buff, row, max(laff, 1));

    line_free(lbuf);
    line_free(lnew);

    return(1);
}

/*
 * Delete the "newline" at the end of line 'row'.  In other words, append line 'row' + 1
 * onto line 'row'.  Firstly the buffer is padded out to 'row', and the line 'row'
 * fetched.  The line following is then fetched and appended onto line 'row'.
 */

int buff_line_delete_newline(buffer *buff, ulong row, int flags)
{
    line *lbuf;
    line *lnext;
    uint x;
    uint col;
    ulong laff = 1;

    buff_pad(buff, 0, row, flags);

    if((row + 1) < buff->nlines) {
        lbuf  = line_new();
        lnext = line_new();

        /* get the two lines that are to be joined */

        buff_getline(buff, (row + 1), lbuf);
        buff_getline(buff, (row + 2), lnext);

        /* goto the end of the first line and insert the second line */

        x = line_length(lbuf);
        line_to_right(lbuf);
        line_insert(lbuf, lnext);

        /* replace the new line and delete the second line */

        buff_putline(buff, (row + 1), lbuf);
        buff_delete(buff, (row + 2));

        /* update syntax sub-system, undo buffer and front-end */

        syntax_line_delete_compensate(buff->lsyn, (row + 1), 1);
        laff = buff_preprocess_line(buff, lbuf, row);
        buff_call_lines_deleted_notify(buff, (row + 1), 1);
        buff_call_lines_changed_notify(buff, row, max(laff, 1));
        col = map_x_to_column(lbuf, x, buff->tabstops);
        buff_process_change(buff, col, row, E_LINE_DELNL, NULL, flags);

        line_free(lbuf);
        line_free(lnext);
    }

    return(1);
}

int buff_line_replace(buffer *buff, ulong row, line *ln, int flags)
{
    line *lbuf;

    lbuf = line_new();

	buff_pad(buff, 0, row, flags);
	buff_getline(buff, (row + 1), lbuf);
	buff_text_subs(buff, 0, row, line_length(lbuf), ln, flags);

	line_free(lbuf);

    return(1);
}

int buff_line_delete(buffer *buff, ulong row, int flags)
{
    line *lbuf;

    lbuf = line_new();

	buff_pad(buff, 0, row, flags);
	buff_getline(buff, (row + 1), lbuf);
	buff_text_delete(buff, 0, row, line_length(lbuf), flags);
	buff_line_delete_newline(buff, row, flags);

	line_free(lbuf);

    return(1);
}

int buff_block_insert(buffer *buff, uint col, ulong row, clip *cl, int flags)
{
    line *lbuf;      /* line buffer */
    line *lnew;      /* buffer truncated part of line */
    line   *ln;      /* current line in clip */
    ulong  i;        /* current line */
    ulong  ins;      /* lines inserted */
    ulong  ey;       /* last line number */
    ulong  laff = 1;
    ulong  ltmp;
    uint   x;

    if(cl->used == 0) {
        return(1);
    }

    lbuf = line_new();
    lnew = line_new();

    ins = 0;
    ey = row + cl->used - 1;

    buff_pad(buff, col, row, flags);
    buff_process_change(buff, col, row, E_BLOCK_INSERT, (void*)cl, flags);
    buff_getline(buff, (row + 1), lbuf);
    x = map_column_to_x(lbuf, col, buff->tabstops);
    syntax_line_insert_compensate(buff->lsyn, row+1, cl->used - 1);

    for(i=row;i<=ey;i++) {
        ln = cl->lines[i - row];

        buff_getline(buff, (i + 1), lbuf);
        line_goto_x(lbuf, x);
        line_insert(lbuf, ln);

        /*
         * If we are not on the last line of the block, then we need to
         * put the remainder of the current line onto the next line.
         */

        if(i != ey) {
            line_define_region(lbuf);
            line_to_right(lbuf);
            line_region_cut(lbuf, lnew);

            buff_putline(buff, (i + 1), lbuf);
            buff_insert(buff, (i + 2), lnew);

            ltmp = buff_preprocess_line(buff, lbuf, i);
            laff = max(ltmp + i, laff);

            x = 0;
            ins++;
        }

        /*
         * If we are on the last line, we just put it back into the buffer
         * and process the line for the syntax highlighting.
         */

        else {
            buff_putline(buff, (i + 1), lbuf);
            ltmp = buff_preprocess_line(buff, lbuf, i);
            laff = max(ltmp + i, laff);
        }
    }

    buff_call_lines_inserted_notify(buff, row + 1, ins);
    buff_call_lines_changed_notify(buff, row, max(laff, ins));

    line_free(lbuf);
    line_free(lnew);

    return(1);
}

int buff_block_delete(buffer *buff, uint c1, ulong r1, uint c2, ulong r2, int flags)
{
    line   *lbuf;    /* line buffer */
    line   *lcut;    /* line buffer */
    line   *lnew;    /* buffer truncated part of line */
    ulong  i;        /* current line */
    ulong  del;      /* lines inserted */
    clip   *cl;      /* clip for undo buffer */
    uint   rx;
    uint   x;
    uint   x1;
    uint   x2 = 0;
    ulong  laff = 0;
    uint   delnl = 0;
    ulong  ltmp;
    uint   eof = 0;

    if(r1 >= buff->nlines) {
        return(1);
    }

    lbuf = line_new();

    x = 0;
    del = 0;

    /* If the start is beyond the end of the file, stop here */

    if(r2 == buff->nlines) {
        buff_getline(buff, buff->nlines, lbuf);

        if( c2 > line_length(lbuf)) {
            c2 = line_length(lbuf);
        }
    }

    if( r2 > buff->nlines - 1) {
        r2 = buff->nlines - 1;

        buff_getline(buff, buff->nlines, lbuf);

        c2 = line_length(lbuf);
    }

    if(r1 == r2 && c1 == c2) {
        line_free(lbuf);

        return(1);
    }

    lcut = line_new();
    lnew = line_new();

    buff_getline(buff, (r1 + 1), lbuf);

    rx = c1;

    x1 = map_column_to_x(lbuf, c1, buff->tabstops);

    if(r1 == r2) {
        x2 = map_column_to_x(lbuf, c2, buff->tabstops);
    }

    cl = clip_new();

    line_goto_x(lbuf, x1);
    line_define_region(lbuf);

    for(i=r1; i <= r2; i++) {
        if(i == r2) {
            delnl = ((x + x2) > line_length(lbuf));

            if(delnl && r1 < (buff->nlines - 1)) {
                c2 = 0;
                r2++;
            } else {
                //  eof = delnl;
                line_goto_x(lbuf, x + x2);
                line_region_cut(lbuf, lcut);
            }
        }

        if(i != r2) {
            buff_getline(buff, (r1 + 2), lnew);
            buff_delete(buff, (r1 + 2));

            line_to_right(lbuf);
            line_region_cut(lbuf, lcut);
            line_goto_x(lbuf, x1);
            line_insert(lbuf, lnew);

            if(i == r2 - 1) {
                x2 = map_column_to_x(lnew, c2, buff->tabstops);
            }

            line_reset(lnew);
            ltmp = buff_preprocess_line(buff, lnew, i + 1);
            laff = max(ltmp + i + 1, laff);

            x = x1;
            del++;
        }

        clip_addline(cl, lcut);
    }

    if(eof && x1 == 0) {
//        buff_delete(buff, (r1 + 1));
//        buff_line_delete_newline(buff, r1 - 1, flags);
    } else {
        buff_putline(buff, (r1 + 1), lbuf);
    }

    buff_process_change(buff, c1, r1, E_BLOCK_DELETE, (void*)cl, flags);

    if(del) {
        syntax_line_delete_compensate(buff->lsyn, r1 + 1, del);
        buff_call_lines_deleted_notify(buff, r1 + 1, del);
    }

    ltmp = buff_preprocess_line(buff, lbuf, r1);
    laff = max(ltmp + r1, laff);
    buff_call_lines_changed_notify(buff, r1, laff - r1);

    line_free(lbuf);
    line_free(lcut);
    line_free(lnew);

    clip_free(cl);
 
    return(1);
}

int buff_rect_insert(buffer *buff, uint col, ulong row, clip *cl, int flags)
{
    difflist *dlist;
    line *lbuf;
    line *ln;
    ulong i;
    ulong ey;
    uint  len;
    uint  diff_x;
    uint  col_x;
    uint  real_x;
    ulong laff = 1;
    ulong ltmp;

    if(cl->used == 0) {
        buff_pad(buff, col, row, flags);
        return(1);
    }

    ey = row + cl->used - 1;
    buff_pad(buff, 0, ey, flags);

    lbuf = line_new();
    dlist = difflist_new();

    for(i=row;i<=ey;i++) {
        ln = cl->lines[i-row];

        if(line_length(ln)) {
            buff_getline(buff, (i+1), lbuf);
            len = line_length(lbuf);
            col_x  = get_actual_column_position(lbuf, col, buff->tabstops, 0);
            real_x = map_column_to_x(lbuf, col_x, buff->tabstops);
            diff_x = col - col_x;

            if(real_x > len) {
                diff_x = real_x - len;
                real_x = len;
                col_x -= diff_x;
                line_to_left(ln);
                line_insert_padding(ln, diff_x);
            } else {
                if(lbuf->data[real_x] == '\t') {
                    line_to_left(ln);
                    line_insert_padding(ln, diff_x);
                }
            }

            line_goto_x(lbuf, real_x);
            line_insert(lbuf, ln);
            buff_putline(buff, (i+1), lbuf);

            // NEW DIFFLIST STUFF

            difflist_add(dlist, real_x, i, ln, NULL);

            ltmp = buff_preprocess_line(buff, lbuf, i);
            laff = max(ltmp + i, laff);
        }
    }

    buff_call_lines_changed_notify(buff, row, max(cl->used, laff));
    buff_process_change(buff, col, row, E_LIST_INSERT, (void*)dlist, flags);

    line_free(lbuf);

    difflist_free(dlist);

    return(1);
}

int buff_rect_delete(buffer *buff, uint col, ulong row, uint w, ulong h, int flags)
{
    difflist *dlist;
    line *lbuf;
    line *ldel;
    uint   col_x1;
    uint   col_x2;
    uint   real_x1;
    uint   real_x2;
    ulong  i;
    ulong  laff = 1; 
    ulong  ltmp;

    if(row >= buff->nlines) {
        return(1);
    }

    if((row+h-1) >= buff->nlines) {
        h = buff->nlines - row;
    }

    dlist = difflist_new();
    lbuf  = line_new();
    ldel  = line_new();

    for(i=row; i < (row+h); i++) {
        buff_getline(buff, (i+1), lbuf);
        col_x1 = get_actual_column_position(lbuf, col, buff->tabstops, 1);
        real_x1 = map_column_to_x(lbuf, col_x1, buff->tabstops);

        if(real_x1 < line_length(lbuf)) {
            col_x2 = get_actual_column_position(lbuf, (col + w), buff->tabstops, 1);
            real_x2 = map_column_to_x(lbuf, col_x2, buff->tabstops);

            line_define_region_at(lbuf, real_x1);
            line_goto_x(lbuf, real_x2);
            line_region_cut(lbuf, ldel);
            buff_putline(buff, (i+1), lbuf);

            // NEW DIFFLIST STUFF

            difflist_add(dlist, real_x1, i, ldel, NULL);

            if(buff->highlight) {
                ltmp = buff_preprocess_line(buff, lbuf, i);
                laff = max(ltmp + i, laff);
            }
        }
    }

    if(dlist->items) {
        buff_process_change(buff, col, row, E_LIST_DELETE, (void*)dlist, flags);
        buff_call_lines_changed_notify(buff, row, max(h, laff));
    }

    line_free(lbuf);
    line_free(ldel);

    difflist_free(dlist);

    return(1);
}

/*
 * Pads the buffer with newlines up to the y position and spaces at line y
 * to the specified x position.
 */

int buff_pad(buffer *buff, uint col, ulong row, int flags)
{
    line   *lbuf;
    clip   *cl;
    uint   npad, len;
    ulong  nlines;
    ulong  i;
    uint   x;

    lbuf = line_new();

    nlines = buff->nlines;

    if(nlines <= row) {
        cl = clip_new();
        buff_getline(buff, (nlines-1)+1, lbuf);
        len = line_length(lbuf);
        len = map_x_to_column(lbuf, len, buff->tabstops);

        line_reset(lbuf);
        buff_insert(buff, (nlines + 1), lbuf);
        clip_addline(cl, lbuf);
        clip_addline(cl, lbuf);
  
        for(i=nlines+1; i<=row; i++) {
            buff_insert(buff, (i+1), lbuf);
            clip_addline(cl, lbuf);
        }

        /* Lines have been inserted, notify attached objects of changed */

        buff_call_lines_inserted_notify(buff, nlines, (row - nlines) + 1);
        nlines = max(1, nlines); /*fixme*/
        buff_process_change(buff, len, nlines-1, E_BLOCK_INSERT, (void*)cl, flags);

        clip_free(cl);
    }

    buff_getline(buff, (row+1), lbuf);
    x = map_column_to_x(lbuf, col, buff->tabstops);
    len = line_length(lbuf);

    if(x > len) {
        npad = x - len;

        /* Pad out the line and reinsert it into the buffer */

        len = map_x_to_column(lbuf, len, buff->tabstops);
        line_goto_x(lbuf, x);
        line_pad(lbuf, ' ');
        buff_putline(buff, (row+1), lbuf);

        /*
         * Create a line that is just the required padding and put into
         * the undo buffer.  We can reuse the lbuf line because have
         * no further use of it.
         */

        line_reset(lbuf);
        line_goto_x(lbuf, npad);
        line_pad(lbuf, ' ');
        buff_process_change(buff, len, row, E_TEXT_INSERT, (void*)lbuf, flags);

        /* The line has changed, notify any attached objects */

        buff_call_lines_changed_notify(buff, row, 1);
    }

    line_free(lbuf);

    return(1);
}

