static DIFF *pr_ins_text(buffer *buff, long x, long y, line *ln)
{
    LCDATA lcd;
    long len = 0;
    long ins = 0;
    line *lbuf = line_new();
    line *lins = line_new();

    buff_getline(buff, (y+1), lbuf);

    line_get_column_data(lbuf, &lcd, x, buff->tabstops);

    if(lcd.col_diff > 0)
    {
        ins = lcd.col_diff;
    }
    else if(lcd.xpos > (len = line_length(lbuf)))
    {
        ins = lcd.xpos - len;
        lcd.xpos = len;
    }

    line_goto_x(lins, ins);
    line_pad(lins, ' ');
    line_insert(lins, ln);
    line_insert_at(lbuf, lcd.xpos, lins);

    buff_putline(buff, (y+1), lbuf);

    line_free(lbuf);
    line_free(lins);

    return(NULL);
}



static DIFF *pr_del_text(buffer *buff, long x, long y, long w)
{               
    LCDATA lcd_s;
    LCDATA lcd_e;
    line *lbuf = line_new();
    line *lcut = line_new();
    long len;

    buff_getline(buff, (y+1), lbuf);

    line_get_column_data(lbuf, &lcd_s, x, buff->tabstops);

    if(lcd_s.xpos < (len = line_length(lbuf)))
    {
        line_get_column_data(lbuf, &lcd_e, x + w, buff->tabstops);

        if( lcd_e.xpos > len ) {
            lcd_e.xpos = len;
        }

        if( lcd_e.xpos > lcd_s.xpos )
        {
            line_define_region_at(lbuf, lcd_s.xpos);
            line_goto_x(lbuf, lcd_e.xpos);
            line_region_cut(lbuf, lcut);

            buff_putline(buff, (y+1), lbuf);
        }
    }

    line_free(lbuf);
    line_free(lcut);

    return(NULL);
}
