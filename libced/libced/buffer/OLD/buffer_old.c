/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    The buffer contains functions for accessing and manipulating text
 *    contained within it.
 *
 *  Todo
 *    - (MAS) Need to rework and tidy the code up a little.
 *    - (MAS) Rethink "none swap" mode, don't like linear array method, slow.
 *    - (MAS) Add block and rect substitution methods.
 *
 *  Changes since 1.2.7
 *
 *  Last Updated
 *    21/Sep/2001
 *    30/Mar/2003
 *
 */

//#define DEBUG

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "datablock.h"
#include "ptrblock.h"
#include "buffer.h"
#include "line.h"
#include "utils.h"
#include "vmem.h"
#include "syntax.h"
#include "sys.h"
#include "structs.h"
#include "search.h"
#include "error.h"
#include "callbacks.h"
#include "debug.h"

#define DEFAULT_TABSTOPS 4

extern route *route_new(void);
extern void   route_free(route *rt, buffer *buff);
extern void   route_trace(route *rt, buffer *buff, ulong lnum);
extern void   route_update(route *rt, buffer *buff);

extern int buff_delete(buffer* buff, ULONG lnum);
extern int buff_insert(buffer *buff, ULONG lnum, line *ln);
extern int buff_putline(buffer *buff, ULONG lnum, line *ln);
extern int buff_getline(buffer* buff, ULONG lnum, line *ln);

// FOR NOW

extern block * new_datablk(buffer *buff);
extern block * new_ptrblk(buffer *buff);

inline void   buff_process_change(buffer *buff, uint col, ulong row, optype op, void *obj, int flags);
static void   buff_fill_mblock(buffer *buff);
static void   buff_preprocess(buffer *buff);
static int    buff_read_file(buffer *buff);
static long   buff_preprocess_line(buffer *buff, line *lbuf, long lnum);

/*
///////////////////////////////////////////////////////////////////////

void buff_z_apply_diffs(buffer *buff, diffs *ds)
{
}

void buff_z_apply_diff(buffer *buff, diff *d)
{
}

void buff_z_negate_diffs(buffer *buff, diffs *d)
{
}

void buff_z_negate_diff(buffer *buff, diffs *d)
{
}

////////////////////////////////////////////////////////////////////////////

void buff_z_exec_diffs(buffer *buff, diffs *d)
{

}

void buff_z_exec_diff(buffer *buff, diff *d)
{
    line lbuf;
    long len;

    buff_getline(buff, (d->y+1), &lbuf);

    len = line_length(&lbuf);

    switch(diff_get_type(d))
    {
        ////////////////////////////////////////////////////////////////////

        case DIFF_INS_TEXT:

            if( diff->x > len ) {
                diff->x -= (diff->x - len);

                line_insert_padding(diff->ln1);
            }

            text_insert(buff, diff->x, diff->y, diff->ln1);

            break;
 
        case DIFF_DEL_TEXT:  text_delete(buff, diff->x, diff->y, diff->len); break;
        case DIFF_SUBS_TEXT: text_subs(buff, diff->x, diff->y, diff->len, diff->ln1); break;

        ////////////////////////////////////////////////////////////////////

        case DIFF_INS_NEWLINE: newline_insert(buff, diff->x, diff->y); break;
        case DIFF_DEL_NEWLINE: newline_delete(buff, diff->y); break;

        ////////////////////////////////////////////////////////////////////

        case DIFF_INS_ROWS:    rows_insert(buff, diff->y, diff->nls);
        case DIFF_DEL_ROWS:    rows_delete(buff, diff->y, diff->nls);

        ////////////////////////////////////////////////////////////////////

    }
}

////////////////////////////////////////////////////////////////////////////////////////////
// Low level text editing functions
//
// These functions "trust" the functions calling them, i.e. it assumes
// the lines that are passed to be edited do exist.
////////////////////////////////////////////////////////////////////////////////////////////
 
static void text_insert(buffer *buff, long x, long y, line *ln)
{
    line lbuf;

    buff_getline(buff, (y+1), &lbuf);

    line_goto_x(&lbuf, x);
    line_insert(&lbuf, ln);

    buff_putline(buff, (y+1), &lbuf);
}

static void text_delete(buffer *buff, long x, long y, long len)
{
    line lbuf;

    buff_getline(buff, (y+1), &lbuf);

    line_define_region_at(&lbuf, x);
    line_goto_x(&lbuf, x + len);
    line_region_delete(&lbuf);

    buff_putline(buff, (y+1), &lbuf);
}

static void text_subs(buffer *buff, long x, long y, long len, line *ln)
{
    line lbuf;

    buff_getline(buff, (y+1), &lbuf);

    line_define_region_at(&lbuf, x);
    line_goto_x(&lbuf, x + len);
    line_region_subs(&lbuf);

    buff_putline(buff, (y+1), &lbuf);
}

static void rows_insert(buffer *buff, long y, long amount)
{
    line lbuf;
    line_reset(lbuf);

    for( ; amount >= 0; amount--)
    {
        buff_insert(buff, (y+1), lbuf);
    }
}

static void rows_delete(buffer *buff, long y, long amount)
{
    for( ; amount >= 0; amount++)
    {
        buff_delete(buff, (y+1));
    }
}

static void newline_insert(buffer *buff, long x, long y)
{
    line lbuf;
    line lnew;

    buff_getline(buff, (y+1), &lbuf);

    line_define_region_at(&lbuf, x);
    line_to_right(&lbuf);
    line_region_cut(&lbuf, lnew);

    buff_putline(buff, (y+1), &lbuf);
    buff_insert(buff, (y+2), &lnew);
}

static void newline_delete(buffer *buff, long y)
{
    line lbuf;
    line lnext;
    uint x;
    uint col;

    // get the two lines that are to be joined

    buff_getline(buff, (y + 1), &lbuf);
    buff_getline(buff, (y + 2), &lnext);

    // goto the end of the first line and insert the second line

    line_to_right(&lbuf);
    line_insert(&lbuf, &lnext);

    // replace the new line and delete the second line

    buff_putline(buff, (y + 1), &lbuf);
    buff_delete(buff, (y + 2));
}

///////////////////////////////////////////////////////////////////////
// Public buffer functions
///////////////////////////////////////////////////////////////////////

void buff_z_pad(buffer *buff, long x, long y)
{
    line lbuf;
    line lpad;
    long len;
    DIFFLIST *ds;

    if(y >= buff->nlines)
    {
        ds = difflist_new();

        difflist_add( ds, diff_new_ins_rows(y, y - buff->nlines) );
        difflist_add( ds, diff_new_ins_cols(0, y, x) );

        buff_exec_diffs(buff, ds);
    }
    else
    {
        len = buff_z_get_line_length(buff, y, &lbuf);

        if(len < x)
        {
            buff_exec_diffs( buff, difflist_new_with_diff(diff_new_ins_space(len, y, x - len)) )
        }
    }
}

///////////////////////////////////////////////////////////////////////

void buff_z_pad_to_row(buffer *buff, long y)
{
    if(y >= buff->nlines)
    {
        buff_exec_diffs(buff, difflist_add( ds, diff_new_ins_rows(y, y - buff->nlines)));
    }
}

///////////////////////////////////////////////////////////////////////

void buff_z_insert_text(buffer *buff, long x, long y, line *ln)
{
    buff_z_pad_to_row(buff, y);
    buff_z_exec_diffs(buff, difflist_new_with_diff(diff_new_ins_text(x, y, ln)));
}

void buff_z_delete_text(buffer *buff, long x, long y, long len)
{
    buff_z_pad(buff, x + len, y);
    buff_z_exec_diffs(buff, difflist_new_with_diff(diff_new_del_text(x, y, len)));
}

void buff_z_subs_text(buffer *buff, long x, long y, long len, line *subs)
{
    buff_z_pad(buff, x + len, y);
    buff_z_exec_diffs(buff, difflist_new_with_diff(diff_new_subs_text(x, y, subs)));
}

///////////////////////////////////////////////////////////////////////

void buff_z_insert_rectangle(buffer *buff, long x, long y, clip *cl)
{
    DIFFLIST *ds;
    long i;

    buff_z_pad_to_row(buff, y);

    ds = difflist_new();

    for(i=0; i < clip_nlines(cl); i++)
    {
        difflist_add(ds, diff_new_ins_text(x, y + i, clip_getline(cl, i, &lbuf)))
    }

    buff_z_exec_diffs(buff, ds);
}

void buff_z_insert_block(buffer *buff, long x, long y, clip *cl)
{
    DIFFLIST *ds;
    long i;

    buff_z_pad_to_row(buff, y);

    ds = difflist_new();

    if(clip_nlines(cl) > 1) difflist_add(ds, diff_new_ins_newline(x, y));
    if(clip_nlines(cl) > 2) difflist_add(ds, diff_new_ins_rows(x, y, clip_nlines(cl) - 2));

    for(i=0; i < clip_nlines(cl); i++)
    {
        difflist_add(ds, diff_new_ins_text(x, y + i, clip_getline(cl, i, &lbuf)));

        x = 0;
    }

    buff_z_exec_diffs(buff, ds);
}

void buff_z_delete_rectangle(buffer *buff, long x, long y, long w, long h)
{
    DIFFLIST *ds;
    long i;

    ds = difflist_new();

    for(i=0; i < h; i++)
    {
        difflist_add(ds, diff_new_del_text(x, y + i, w));
    }

    buff_z_exec_diffs(buff, ds);
}

void buff_z_delete_block()
{
    DIFFLIST *ds;

    ds = difflist_new();

    buff_z_exec_diffs(buff, ds);
}

void buff_z_subs_clip()
{
}

///////////////////////////////////////////////////////////////////////

void buff_z_split_line()
{
}

void buff_z_join_line()
{
}

///////////////////////////////////////////////////////////////////////

long buff_z_get_line_length(buffer *buff, long y)
{
    line lbuf;

    return(line_length(buff_getline(buff, (y+1), &lbuf)));
}
*/

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

void buff_set_dirty(buffer *buff, int b)
{
    if( buff->dirty != b) {
        buff->dirty = b;
        buff_call_file_attribs_changed_notify(buff);
    }
}

void buff_set_readonly(buffer *buff, int b)
{
    if( buff->ro != b) {
        buff->ro = b;
        buff_call_file_attribs_changed_notify(buff);
    }
}

lndiff *lndiff_new(uint x, ulong y, line *ln, line *sub)
{
    lndiff *ld;

    ld = calloc(1, sizeof(lndiff));

    ld->x = x;
    ld->y = y;
    ld->ldiff = line_dup(ln);
    ld->lsubs = line_dup(sub);

    return(ld);
}

void lndiff_free(lndiff *lnd)
{
    if(lnd->ldiff) line_free(lnd->ldiff);
    if(lnd->lsubs) line_free(lnd->lsubs);

    free(lnd);
}

difflist *difflist_new()
{
    difflist *dlist;
    dlist = calloc(1, sizeof(difflist));
    ptr_array_init(dlist);

    return(dlist);
}

void difflist_free(difflist *dlist)
{
    ulong i;
    lndiff *ld;

    for(i=0; i < dlist->items; i++) {
        ld = (lndiff*)dlist->data[i];
        lndiff_free(ld);
    }

    ptr_array_free_data(dlist);

    free(dlist);
}

void difflist_add(difflist *dlist, uint x, ulong y, line *ln, line *sub)
{
    lndiff *ld;
    ld = lndiff_new(x, y, ln, sub);
    ptr_array_add(dlist, (void*)ld);
}

difflist *difflist_dup(difflist *dlist)
{
    difflist *dest;
    lndiff   *ld;
    ulong    i;

    dest = difflist_new();

    for(i=0; i < dlist->items; i++) {
        ld = (lndiff*)dlist->data[i];

        difflist_add(dest, ld->x,
                           ld->y,
                           ld->ldiff,
                           ld->lsubs);
    }

    return(dest);
}

/*************************************************************************************
 * Buffer manipulation routines.
 *
 * It should be noted that the row and column positions passed to these
 * routines are actual screen positions, NOT positions in a raw line.  The
 * screen positions are different because tabs have been expanded.  The
 * routines must convert these values where neccessary by using the
 * map_column_to_x function.
 *
 * Functions:
 *   buff_text_insert         (buff, col, row, ln, flags);
 *   buff_text_delete         (buff, col, row, n, flags);
 *   buff_text_subs           (buff, col, row, n, ln, flags);
 *   buff_line_split          (buff, col, row, flags);
 *   buff_line_delete_newline (buff, row, flags);
 *   buff_block_insert        (buff, col, row, cl, flags);
 *   buff_block_delete        (buff, c1, r1, c2, r2, flags);
 *   buff_rect_insert         (buff, col, row, cl, flags);
 *   buff_rect_delete         (buff, col, row, w, h, flags);
 *   buff_pad                 (buff, col, row, flags);
 *
 * Alterations that occur during these routines must inform the syntax highlighting
 * sub-system and the front-end that lines have been: changed, inserted or deleted.
 *
 */

int buff_apply_difflist(buffer *buff, difflist *dl, int neg)
{
    line  *lbuf;
    line  *subs;
    line  *diff;
    ulong ymin = 0;
    ulong ymax = 0;
    ulong lnum;
    ulong i;
    ulong n = 0;
    uint  len;
    lndiff *ld;

    if(!dl->items)
        return(1);

    if(neg) n = dl->items - 1;

    lbuf = line_new();

    for(i=0; i < dl->items; i++) {
        ld = (lndiff*)dl->data[n];

        if(neg) {
            subs = ld->ldiff;
            diff = ld->lsubs;
            n--;
        } else {
            subs = ld->lsubs;
            diff = ld->ldiff;
            n++;
        }

        lnum = ld->y;
        ymin = min(ymin, lnum);
        ymax = max(ymax, lnum);

        if(i==0) {
            ymin = lnum;
            ymax = lnum;
        }

        buff_getline(buff, (lnum + 1), lbuf);
        line_goto_x(lbuf, ld->x);

        if(subs) {
            len = line_length(subs);
            line_define_region(lbuf);
            line_arrow_right(lbuf, len);
            line_region_delete(lbuf);
        }

        if(diff) {
            line_insert(lbuf, diff);
        }

        buff_putline(buff, (lnum + 1), lbuf);
        ymax += (buff_preprocess_line(buff, lbuf, lnum)-1);

    }

    buff_call_lines_changed_notify(buff, ymin, ((ymax - ymin) + 1));

    line_free(lbuf);

    return(1);
}

/*
 * Insert a single line segment of text into the buffer.  The buffer is first padded
 * out to 'row', 'col'.  The line 'row' is then fetched and the line segment inserted
 * at 'col'.  The change is recorded in the undo buffer, and the line is put back into
 * the line buffer.
 */

int buff_text_insert(buffer *buff, uint col, ulong row, line *ln, int flags)
{
    line *lbuf;
    ulong laff;
    uint  x;

    lbuf = line_new();

    buff_pad(buff, col, row, flags);
    buff_getline(buff, (row+1), lbuf);
    x = map_column_to_x(lbuf, col, buff->tabstops);

    line_goto_x(lbuf, x);

    if(line_length(ln) > 0) {
        line_insert(lbuf, ln);
        buff_putline(buff, (row+1), lbuf);
        buff_process_change(buff, col, row, E_TEXT_INSERT, (void*)ln, flags);
        laff = buff_preprocess_line(buff, lbuf, row);
        buff_call_lines_changed_notify(buff, row, max(laff, 1));
    }

    line_free(lbuf);

    return(1);
}

/*
 * Delete a segment of a line from the buffer.  The line at 'row' is first fetched from
 * the line buffer and the segment of length 'n' is cut from it and stored in the undo
 * buffer, and the altered line is then put back into the line buffer.
 */

int buff_text_delete(buffer *buff, uint col, ulong row, uint n, int flags)
{
    line *lbuf;
    line *ldel;
    ulong laff;
    uint  x;

    if(row < buff->nlines) {
        lbuf = line_new();
        ldel = line_new();

        buff_getline(buff, (row+1), lbuf);
        x = map_column_to_x(lbuf, col, buff->tabstops);

        if(x < line_length(lbuf)) {
            line_define_region_at(lbuf, x);
            line_goto_x(lbuf, x + n);
            line_region_cut(lbuf, ldel);

            buff_putline(buff, (row+1), lbuf);

            buff_process_change(buff, col, row, E_TEXT_DELETE, (void*)ldel, flags);
            laff = buff_preprocess_line(buff, lbuf, row);
            buff_call_lines_changed_notify(buff, row, max(laff, 1));
        }

        line_free(lbuf);
        line_free(ldel);
    }

    return(1);
}

/*
 * Substitute a segment of a line in the buffer with the line segment 'ln'.  First the
 * buffer is padded out if neccessary, and the line at 'row' is fetched.  The segment
 * of line to be replaced is cut, and the new segment is inserted.  The replaced text
 * and the new text is then put into the undo buffer.
 */

int buff_text_subs(buffer *buff, uint col, ulong row, uint n, line *ln, int flags)
{
    line *lbuf;
    line *ldel;
    difflist *dlist;
    ulong laff;
    uint xs;

    lbuf = line_new();
    ldel = line_new();

    buff_pad(buff, col, row, flags);
    buff_getline(buff, (row+1), lbuf);

    /* delete old segment of size n */

    line_define_region_at_col(lbuf, col, buff->tabstops, LINE_TAB_RIGHT);
    line_goto_col(lbuf, col + n, buff->tabstops, LINE_TAB_LEFT);
    line_region_cut(lbuf, ldel);

    /* insert replacement segment */

    xs = lbuf->xpos;

    line_insert(lbuf, ln);

    buff_putline(buff, row + 1, lbuf);

    dlist = difflist_new();
    difflist_add(dlist, xs, row, ldel, ln);

    buff_process_change(buff, col, row, E_LIST_DELETE, (void*)dlist, flags);

    laff = buff_preprocess_line(buff, lbuf, row);

    buff_call_lines_changed_notify(buff, row, max(laff, 1));
 
    line_free(lbuf);
    line_free(ldel);

    difflist_free(dlist);

    return(1);
}

/*
 * Split the line 'row' at 'col', inserting the second part of the split as a new
 * line after row.  The buffer is first padded to where the split is to occur, and
 * the line is fetched from the line buffer.  The line is cut from 'col' onwards
 * and inserted at line 'row' + 1.
 */

int buff_line_split(buffer *buff, uint col, ulong row, int flags)
{
    line *lbuf;
    line *lnew;
    ulong laff;
    uint x;

    lbuf = line_new();
    lnew = line_new();

    buff_pad(buff, col, row, flags);
    buff_getline(buff, (row+1), lbuf);
    x = map_column_to_x(lbuf, col, buff->tabstops);

    line_define_region_at(lbuf, x);
    line_to_right(lbuf);
    line_region_cut(lbuf, lnew);

    buff_putline(buff, (row+1), lbuf);
    buff_insert(buff, (row+2), lnew);

    /* update syntax sub-system, undo buffer and front-end */

    syntax_line_insert_compensate(buff->lsyn, (row + 1), 1);
    buff_call_lines_inserted_notify(buff, (row + 1), 1);
    buff_process_change(buff, col, row, E_LINE_SPLIT, NULL, flags);
    buff_preprocess_line(buff, lnew, (row + 1));
    laff = buff_preprocess_line(buff, lbuf, row);
    buff_call_lines_changed_notify(buff, row, max(laff, 1));

    line_free(lbuf);
    line_free(lnew);

    return(1);
}

/*
 * Delete the "newline" at the end of line 'row'.  In other words, append line 'row' + 1
 * onto line 'row'.  Firstly the buffer is padded out to 'row', and the line 'row'
 * fetched.  The line following is then fetched and appended onto line 'row'.
 */

int buff_line_delete_newline(buffer *buff, ulong row, int flags)
{
    line *lbuf;
    line *lnext;
    uint x;
    uint col;
    ulong laff = 1;

    buff_pad(buff, 0, row, flags);

    if((row + 1) < buff->nlines) {
        lbuf  = line_new();
        lnext = line_new();

        /* get the two lines that are to be joined */

        buff_getline(buff, (row + 1), lbuf);
        buff_getline(buff, (row + 2), lnext);

        /* goto the end of the first line and insert the second line */

        x = line_length(lbuf);
        line_to_right(lbuf);
        line_insert(lbuf, lnext);

        /* replace the new line and delete the second line */

        buff_putline(buff, (row + 1), lbuf);
        buff_delete(buff, (row + 2));

        /* update syntax sub-system, undo buffer and front-end */

        syntax_line_delete_compensate(buff->lsyn, (row + 1), 1);
        laff = buff_preprocess_line(buff, lbuf, row);
        buff_call_lines_deleted_notify(buff, (row + 1), 1);
        buff_call_lines_changed_notify(buff, row, max(laff, 1));
        col = map_x_to_column(lbuf, x, buff->tabstops);
        buff_process_change(buff, col, row, E_LINE_DELNL, NULL, flags);

        line_free(lbuf);
        line_free(lnext);
    }

    return(1);
}

int buff_line_replace(buffer *buff, ulong row, line *ln, int flags)
{
    line *lbuf;

    lbuf = line_new();

	buff_pad(buff, 0, row, flags);
	buff_getline(buff, (row + 1), lbuf);
	buff_text_subs(buff, 0, row, line_length(lbuf), ln, flags);

	line_free(lbuf);

    return(1);
}

int buff_line_delete(buffer *buff, ulong row, int flags)
{
    line *lbuf;

    lbuf = line_new();

	buff_pad(buff, 0, row, flags);
	buff_getline(buff, (row + 1), lbuf);
	buff_text_delete(buff, 0, row, line_length(lbuf), flags);
	buff_line_delete_newline(buff, row, flags);

	line_free(lbuf);

    return(1);
}

int buff_block_insert(buffer *buff, uint col, ulong row, clip *cl, int flags)
{
    line *lbuf;      /* line buffer */
    line *lnew;      /* buffer truncated part of line */
    line   *ln;      /* current line in clip */
    ulong  i;        /* current line */
    ulong  ins;      /* lines inserted */
    ulong  ey;       /* last line number */
    ulong  laff = 1;
    ulong  ltmp;
    uint   x;

    if(cl->used == 0) {
        return(1);
    }

    lbuf = line_new();
    lnew = line_new();

    ins = 0;
    ey = row + cl->used - 1;

    buff_pad(buff, col, row, flags);
    buff_process_change(buff, col, row, E_BLOCK_INSERT, (void*)cl, flags);
    buff_getline(buff, (row + 1), lbuf);
    x = map_column_to_x(lbuf, col, buff->tabstops);
    syntax_line_insert_compensate(buff->lsyn, row+1, cl->used - 1);

    for(i=row;i<=ey;i++) {
        ln = cl->lines[i - row];

        buff_getline(buff, (i + 1), lbuf);
        line_goto_x(lbuf, x);
        line_insert(lbuf, ln);

        /*
         * If we are not on the last line of the block, then we need to
         * put the remainder of the current line onto the next line.
         */

        if(i != ey) {
            line_define_region(lbuf);
            line_to_right(lbuf);
            line_region_cut(lbuf, lnew);

            buff_putline(buff, (i + 1), lbuf);
            buff_insert(buff, (i + 2), lnew);

            ltmp = buff_preprocess_line(buff, lbuf, i);
            laff = max(ltmp + i, laff);

            x = 0;
            ins++;
        }

        /*
         * If we are on the last line, we just put it back into the buffer
         * and process the line for the syntax highlighting.
         */

        else {
            buff_putline(buff, (i + 1), lbuf);
            ltmp = buff_preprocess_line(buff, lbuf, i);
            laff = max(ltmp + i, laff);
        }
    }

    buff_call_lines_inserted_notify(buff, row + 1, ins);
    buff_call_lines_changed_notify(buff, row, max(laff, ins));

    line_free(lbuf);
    line_free(lnew);

    return(1);
}

int buff_block_delete(buffer *buff, uint c1, ulong r1, uint c2, ulong r2, int flags)
{
    line   *lbuf;    /* line buffer */
    line   *lcut;    /* line buffer */
    line   *lnew;    /* buffer truncated part of line */
    ulong  i;        /* current line */
    ulong  del;      /* lines inserted */
    clip   *cl;      /* clip for undo buffer */
    uint   rx;
    uint   x;
    uint   x1;
    uint   x2 = 0;
    ulong  laff = 0;
    uint   delnl = 0;
    ulong  ltmp;
    uint   eof = 0;

    if(r1 >= buff->nlines) {
        return(1);
    }

    lbuf = line_new();

    x = 0;
    del = 0;

    /* If the start is beyond the end of the file, stop here */

    if(r2 == buff->nlines) {
        buff_getline(buff, buff->nlines, lbuf);

        if( c2 > line_length(lbuf)) {
            c2 = line_length(lbuf);
        }
    }

    if( r2 > buff->nlines - 1) {
        r2 = buff->nlines - 1;

        buff_getline(buff, buff->nlines, lbuf);

        c2 = line_length(lbuf);
    }

    if(r1 == r2 && c1 == c2) {
        line_free(lbuf);

        return(1);
    }

    lcut = line_new();
    lnew = line_new();

    buff_getline(buff, (r1 + 1), lbuf);

    rx = c1;

    x1 = map_column_to_x(lbuf, c1, buff->tabstops);

    if(r1 == r2) {
        x2 = map_column_to_x(lbuf, c2, buff->tabstops);
    }

    cl = clip_new();

    line_goto_x(lbuf, x1);
    line_define_region(lbuf);

    for(i=r1; i <= r2; i++) {
        if(i == r2) {
            delnl = ((x + x2) > line_length(lbuf));

            if(delnl && r1 < (buff->nlines - 1)) {
                c2 = 0;
                r2++;
            } else {
                //  eof = delnl;
                line_goto_x(lbuf, x + x2);
                line_region_cut(lbuf, lcut);
            }
        }

        if(i != r2) {
            buff_getline(buff, (r1 + 2), lnew);
            buff_delete(buff, (r1 + 2));

            line_to_right(lbuf);
            line_region_cut(lbuf, lcut);
            line_goto_x(lbuf, x1);
            line_insert(lbuf, lnew);

            if(i == r2 - 1) {
                x2 = map_column_to_x(lnew, c2, buff->tabstops);
            }

            line_reset(lnew);
            ltmp = buff_preprocess_line(buff, lnew, i + 1);
            laff = max(ltmp + i + 1, laff);

            x = x1;
            del++;
        }

        clip_addline(cl, lcut);
    }

    if(eof && x1 == 0) {
//        buff_delete(buff, (r1 + 1));
//        buff_line_delete_newline(buff, r1 - 1, flags);
    } else {
        buff_putline(buff, (r1 + 1), lbuf);
    }

    buff_process_change(buff, c1, r1, E_BLOCK_DELETE, (void*)cl, flags);

    if(del) {
        syntax_line_delete_compensate(buff->lsyn, r1 + 1, del);
        buff_call_lines_deleted_notify(buff, r1 + 1, del);
    }

    ltmp = buff_preprocess_line(buff, lbuf, r1);
    laff = max(ltmp + r1, laff);
    buff_call_lines_changed_notify(buff, r1, laff - r1);

    line_free(lbuf);
    line_free(lcut);
    line_free(lnew);

    clip_free(cl);
 
    return(1);
}

int buff_rect_insert(buffer *buff, uint col, ulong row, clip *cl, int flags)
{
    difflist *dlist;
    line *lbuf;
    line *ln;
    ulong i;
    ulong ey;
    uint  len;
    uint  diff_x;
    uint  col_x;
    uint  real_x;
    ulong laff = 1;
    ulong ltmp;

    if(cl->used == 0) {
        buff_pad(buff, col, row, flags);
        return(1);
    }

    ey = row + cl->used - 1;
    buff_pad(buff, 0, ey, flags);

    lbuf = line_new();
    dlist = difflist_new();

    for(i=row;i<=ey;i++) {
        ln = cl->lines[i-row];

        if(line_length(ln)) {
            buff_getline(buff, (i+1), lbuf);
            len = line_length(lbuf);
            col_x  = get_actual_column_position(lbuf, col, buff->tabstops, 0);
            real_x = map_column_to_x(lbuf, col_x, buff->tabstops);
            diff_x = col - col_x;

            if(real_x > len) {
                diff_x = real_x - len;
                real_x = len;
                col_x -= diff_x;
                line_to_left(ln);
                line_insert_padding(ln, diff_x);
            } else {
                if(lbuf->data[real_x] == '\t') {
                    line_to_left(ln);
                    line_insert_padding(ln, diff_x);
                }
            }

            line_goto_x(lbuf, real_x);
            line_insert(lbuf, ln);
            buff_putline(buff, (i+1), lbuf);

            // NEW DIFFLIST STUFF

            difflist_add(dlist, real_x, i, ln, NULL);

            ltmp = buff_preprocess_line(buff, lbuf, i);
            laff = max(ltmp + i, laff);
        }
    }

    buff_call_lines_changed_notify(buff, row, max(cl->used, laff));
    buff_process_change(buff, col, row, E_LIST_INSERT, (void*)dlist, flags);

    line_free(lbuf);

    difflist_free(dlist);

    return(1);
}

int buff_rect_delete(buffer *buff, uint col, ulong row, uint w, ulong h, int flags)
{
    difflist *dlist;
    line *lbuf;
    line *ldel;
    uint   col_x1;
    uint   col_x2;
    uint   real_x1;
    uint   real_x2;
    ulong  i;
    ulong  laff = 1; 
    ulong  ltmp;

    if(row >= buff->nlines) {
        return(1);
    }

    if((row+h-1) >= buff->nlines) {
        h = buff->nlines - row;
    }

    dlist = difflist_new();
    lbuf  = line_new();
    ldel  = line_new();

    for(i=row; i < (row+h); i++) {
        buff_getline(buff, (i+1), lbuf);
        col_x1 = get_actual_column_position(lbuf, col, buff->tabstops, 1);
        real_x1 = map_column_to_x(lbuf, col_x1, buff->tabstops);

        if(real_x1 < line_length(lbuf)) {
            col_x2 = get_actual_column_position(lbuf, (col + w), buff->tabstops, 1);
            real_x2 = map_column_to_x(lbuf, col_x2, buff->tabstops);

            line_define_region_at(lbuf, real_x1);
            line_goto_x(lbuf, real_x2);
            line_region_cut(lbuf, ldel);
            buff_putline(buff, (i+1), lbuf);

            // NEW DIFFLIST STUFF

            difflist_add(dlist, real_x1, i, ldel, NULL);

            if(buff->highlight) {
                ltmp = buff_preprocess_line(buff, lbuf, i);
                laff = max(ltmp + i, laff);
            }
        }
    }

    if(dlist->items) {
        buff_process_change(buff, col, row, E_LIST_DELETE, (void*)dlist, flags);
        buff_call_lines_changed_notify(buff, row, max(h, laff));
    }

    line_free(lbuf);
    line_free(ldel);

    difflist_free(dlist);

    return(1);
}

/*
 * Pads the buffer with newlines up to the y position and spaces at line y
 * to the specified x position.
 */

int buff_pad(buffer *buff, uint col, ulong row, int flags)
{
    line   *lbuf;
    clip   *cl;
    uint   npad, len;
    ulong  nlines;
    ulong  i;
    uint   x;

    lbuf = line_new();

    nlines = buff->nlines;

    if(nlines <= row) {
        cl = clip_new();
        buff_getline(buff, (nlines-1)+1, lbuf);
        len = line_length(lbuf);
        len = map_x_to_column(lbuf, len, buff->tabstops);

        line_reset(lbuf);
        buff_insert(buff, (nlines + 1), lbuf);
        clip_addline(cl, lbuf);
        clip_addline(cl, lbuf);
  
        for(i=nlines+1; i<=row; i++) {
            buff_insert(buff, (i+1), lbuf);
            clip_addline(cl, lbuf);
        }

        /* Lines have been inserted, notify attached objects of changed */

        buff_call_lines_inserted_notify(buff, nlines, (row - nlines) + 1);
        nlines = max(1, nlines); /*fixme*/
        buff_process_change(buff, len, nlines-1, E_BLOCK_INSERT, (void*)cl, flags);

        clip_free(cl);
    }

    buff_getline(buff, (row+1), lbuf);
    x = map_column_to_x(lbuf, col, buff->tabstops);
    len = line_length(lbuf);

    if(x > len) {
        npad = x - len;

        /* Pad out the line and reinsert it into the buffer */

        len = map_x_to_column(lbuf, len, buff->tabstops);
        line_goto_x(lbuf, x);
        line_pad(lbuf, ' ');
        buff_putline(buff, (row+1), lbuf);

        /*
         * Create a line that is just the required padding and put into
         * the undo buffer.  We can reuse the lbuf line because have
         * no further use of it.
         */

        line_reset(lbuf);
        line_goto_x(lbuf, npad);
        line_pad(lbuf, ' ');
        buff_process_change(buff, len, row, E_TEXT_INSERT, (void*)lbuf, flags);

        /* The line has changed, notify any attached objects */

        buff_call_lines_changed_notify(buff, row, 1);
    }

    line_free(lbuf);

    return(1);
}

/***************************************************************************************/

int buff_substitute(buffer *buff, uint x1, ulong y1, uint x2, ulong y2, search *srch, int sflags)
{
    difflist *dlist;
    line   *lbuf;
    line   *lrep;
    line   *lcut;
    int    status;
    uint   len;
    uint   xe;
    uint   xs;
    ulong  i;
    ulong  found;
    uint   fflag;
    uint   col;
    ulong  row;
    uint   c_x1;
    uint   c_x2;
    uint   r_x1;
    uint   r_x2;

    lcut = line_new();
    lbuf = line_new();
    lrep = line_new();

    col = x1;
    row = y1;

    if( sflags &  BUFFER_SUBS_ALL) {
        sflags |= BUFFER_SUBS_LINEAR;
        x1 = 0;
        y1 = 0;
        x2 = 0;
        y2 = buff->nlines;
    }

    /*
     * Should be >= but a small bug to do with the initial
     * new file creation stops this from working.. FIXME.
     */

    found = 0;
    dlist = difflist_new();
    search_set_line(srch, lbuf);

    for(i=y1; i <= y2; i++) {
        buff_getline(buff, (i+1), lbuf);

        c_x1 = get_actual_column_position(lbuf, x1, buff->tabstops, 1);
        r_x1 = map_column_to_x(lbuf, c_x1, buff->tabstops);

        if( r_x1 < line_length(lbuf)) {
            c_x2 = get_actual_column_position(lbuf, x2, buff->tabstops, 1);
            r_x2 = map_column_to_x(lbuf, c_x2, buff->tabstops);

            if(sflags & BUFFER_SUBS_LINEAR) {
                xs = (i == y1) ? r_x1 : 0;
                xe = (i == y2) ? min(r_x2, line_length(lbuf)) : line_length(lbuf);
            } else {
                xs = r_x1;
                xe = min(line_length(lbuf), r_x2);
            }

            search_set_start_pos(srch, xs);
            search_set_end_pos(srch, xe);

            fflag = 0;

            while((status = search_find(srch, 0)) == SEARCH_FOUND) {
                search_get_replace(srch, lrep);

                len = line_length(lrep);

                found++;

                if(len > 0 || srch->fnd_size > 0) {
                    line_goto_x(lbuf, srch->fnd_start);
                    line_define_region(lbuf);
                    line_arrow_right(lbuf, srch->fnd_size);
                    line_region_cut(lbuf, lcut);
                    line_goto_x(lbuf, srch->fnd_start);
                    line_insert(lbuf, lrep);

                    difflist_add(dlist, srch->fnd_start, i, lrep, lcut);

                    fflag = 1;

                    /*
                     * If what was found is the remainder of the area we want
                     * to search and replace in, then break the loop, otherwise
                     * update the start and end positions and carry on.
                     */

                    if((srch->fnd_start + srch->fnd_size) < srch->end_pos) {
                        srch->st_pos = srch->fnd_start + len;
                        srch->end_pos -= line_length(lcut);
                        srch->end_pos += len;
                    } else {
                        break;
                    }
                } else {
                    break;
                }

                if(sflags & BUFFER_SUBS_ONCE) {
                    break;
                }
            }

            if(fflag) {
                buff_putline(buff, (i+1), lbuf);
                buff_call_lines_changed_notify(buff, i, 1);

                if(sflags & BUFFER_SUBS_ONCE) {
                    break;
                }
			}
        } // if real_x1 > len
    }

    if(found) {
        buff_process_change(buff, col, row, E_LIST_INSERT, (void*)dlist, 1);
    }

    line_free(lcut);
    line_free(lbuf);
    line_free(lrep);

    difflist_free(dlist);

    return(found);
}

/***************************************************************************************/

inline void buff_process_change(buffer *buff, uint col, ulong row, optype op, void *obj, int flags)
{
    if(flags)
    {
        undo_add_change(buff->undo_list, col, row, op, buff->edit_mode, obj);
    }
}

/**********************************************************
 * Buffer linkage stuff.  This isn't that tidy at the
 * moment, but it will get an overhaul soonish.
 *
 * use_front obsolete ?
 */

void buff_add_link(buffer *buff, void *struct_ptr)
{
    if(struct_ptr) {
        ptr_array_add(&buff->bufflinks, struct_ptr);
        buff_call_link_added_notify(buff);
    }
}

void buff_del_link(buffer *buff, void *struct_ptr)
{
    ulong n;

    if(struct_ptr) {
        if(ptr_array_lookup(&buff->bufflinks, struct_ptr, &n)) {
            ptr_array_del(&buff->bufflinks, n);
            buff_call_link_deleted_notify(buff);
        }
    }
}

void *buff_get_link(buffer *buff, uint n)
{
    if(buff->bufflinks.items > n) {
        return(buff->bufflinks.data[n]);
    }

    return(NULL);
}

int buff_get_link_number(buffer *buff, void *struct_ptr)
{
    ulong n;

    if(struct_ptr) {
        if(ptr_array_lookup(&buff->bufflinks, struct_ptr, &n)) {
            return((int)n);
        }
    }

    return(-1);
}

void buff_call_link_added_notify(buffer *buff)
{
    uint i;
  
    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_link_added(buff->bufflinks.data[i]);
        }
    }
}

void buff_call_link_deleted_notify(buffer *buff)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_link_deleted(buff->bufflinks.data[i]);
        }
    }
}

void buff_call_file_attribs_changed_notify(buffer *buff)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++)
            buff->func_attribs_changed(buff->bufflinks.data[i]);
    }
}

void buff_call_buffer_renamed_notify(buffer *buff)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_buffer_renamed(buff->bufflinks.data[i]);
            buff->func_attribs_changed(buff->bufflinks.data[i]);
        }
    }
}

void buff_call_buffer_destroyed_notify(buffer *buff)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_buffer_destroyed(buff->bufflinks.data[i]);
        }
    }
}

void buff_call_lines_changed_notify(buffer *buff, ulong y, ulong n)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_lines_changed(buff->bufflinks.data[i], y, n);
        }
    }
}

void buff_call_lines_deleted_notify(buffer *buff, ulong y, ulong n)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_lines_deleted(buff->bufflinks.data[i], y, n);
        }
    }
}

void buff_call_lines_inserted_notify(buffer *buff, ulong y, ulong n)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_lines_inserted(buff->bufflinks.data[i], y, n);
        }
    }
}

void buff_call_message(buffer *buff, char *msg, uint beep)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_message(buff->bufflinks.data[i], msg, beep);
        }
    }
}

/*
 * Create a new buffer and return a pointer to it.  When a new
 * buffer is created, the root node is automatically created and
 * the first data block.  The first pointer in the root node is
 * set to point at the new data block.
 */

buffer *buff_new(char *fname)
{
    buffer     *buff;
    static int untitled = 0;

    TRACE(D_MSG, "buffer::buff_new: creating new buffer: %s", fname);

    buff = (buffer*)calloc(1, sizeof(buffer));

    if(fname && strlen(fname)) {
        if(get_full_path(buff->ffname, fname, 1024) != NULL) {
            get_file(buff->sfname, buff->ffname, 256);
            buff->is_named = 1;
        }
    }

    if(!buff->is_named) {
        if(untitled == 0) {
            strcpy(buff->sfname, "Untitled");
        } else {
            sprintf(buff->sfname, "Untitled_%d", untitled);
        }

        strcpy(buff->ffname, buff->sfname);
        untitled++;
    }

    buff->highlight = 1;
    buff->undo_list = undo_new();
    buff->rt = route_new();
    buff->lsyn = syntax_new(SYS_CONFIG_LANG);
    buff->ftype = CED_CONFIG.filemode;
    buff->tabstops = CED_CONFIG.tabstops;

    syntax_set_from_filename(buff->lsyn, fname);

    ptr_array_init(&buff->bufflinks);

    buff->cbacks = callbacks_new("buffer");

    callbacks_add(buff->cbacks, "link_added"       , CALLBACK_REF(buff->func_link_added));
    callbacks_add(buff->cbacks, "link_deleted"     , CALLBACK_REF(buff->func_link_deleted));
    callbacks_add(buff->cbacks, "lines_changed"    , CALLBACK_REF(buff->func_lines_changed));
    callbacks_add(buff->cbacks, "lines_inserted"   , CALLBACK_REF(buff->func_lines_inserted));
    callbacks_add(buff->cbacks, "lines_deleted"    , CALLBACK_REF(buff->func_lines_deleted));
    callbacks_add(buff->cbacks, "attribs_changed"  , CALLBACK_REF(buff->func_attribs_changed));
    callbacks_add(buff->cbacks, "message"          , CALLBACK_REF(buff->func_message));
    callbacks_add(buff->cbacks, "buffer_renamed"   , CALLBACK_REF(buff->func_buffer_renamed));
    callbacks_add(buff->cbacks, "buffer_destroyed" , CALLBACK_REF(buff->func_buffer_destroyed));

    return(buff);
}

/*
 * Free up the buffer.
 * This needs a lot more doing to it to gracefully close the buffer,
 * but for now this'll do for test purposes..
 */

int buff_destroy(buffer *buff)
{
    vmem  *vm;
    ulong i;

    TRACE(D_MSG, "buffer::buff_destroy: destroying buffer");

    syntax_free(buff->lsyn);
    route_free(buff->rt, buff);
    undo_free(buff->undo_list);
    buff_close(buff);

    if(buff->use_swap) {
        vm = buff->vm;
        vm_close(vm);
        vm_free(vm);
    } else {
        for(i=0; i < buff->lines2.items; i++) {
            line_free(buff->lines2.data[i]);
        }
    }

    callbacks_free(buff->cbacks);

    buff_call_buffer_destroyed_notify(buff);

    ptr_array_free_data(&buff->lines2);
    ptr_array_free_data(&buff->bufflinks);

    free(buff);

    return(0);
}

int buff_getline_all(buffer *buff, ULONG lnum, line *ln_raw, line *ln, line *fln, int hbn_1, int hbn_2)
{
    line *ln_tmp1;
    line *ln_tmp2;

    if(lnum > buff->nlines) {
        if(ln_raw) line_reset(ln_raw);
        if(fln)    line_reset(fln);
        if(ln)     line_reset(ln);
        return(0);
    }

    if(ln_raw) {
        ln_tmp1 = ln_raw;
    } else {
        ln_tmp1 = line_new();
    }

    buff_getline(buff, lnum, ln_tmp1);

    if(ln) {
        line_reset(ln);
        ln_tmp2 = ln;
    } else {
        ln_tmp2 = line_new();
    }

    line_insert(ln_tmp2, ln_tmp1);
    line_expand_tabs(ln_tmp2, buff->tabstops, (buff->show_tabs) ? '>' : ' ');

    if(fln) {
        line_reset(fln);
        line_insert(fln, ln_tmp2);

        if(buff->highlight) {
//            syntax_format_line(buff->lsyn, fln, NULL, (lnum - 1), hbn_1, hbn_2);
        }
    }

    if(!ln) line_free(ln_tmp2);
    if(!ln_raw) line_free(ln_tmp1);

    return(1);
}

operror buff_open(buffer *buff, opflags f)
{
    char      path[1024];
    vmem      *vm;
    block     *dtblk;
    mblock    *mblk;
    pbheader  *pblk;
    ptrelem   pelem;
    char      *ffname;
    char      *swname;
    char      *buf;
    char      ch;
    uint      recover;
    uint      i;

    recover = 0;
    ffname = buff->ffname;
    swname = buff->swname;

    /*
     * First check that we can write to the requested path.
     * If it can't, then we can't use a swap file.
     */

    get_path(path, ffname, 1024);

    if(buff->is_named && file_exists(ffname)) {
        if(!file_can_read(ffname)) {
            return(OE_NOT_READABLE);
        }
        if(!is_file(ffname)) {
            return(OE_NOT_FILE);
        }
        if(!file_can_write(ffname)) {
            buff->ro = 1;
        }

        buff->df_lastmod = file_get_last_modified(ffname);
    }

    if( f & OF_USE_SWAP && !file_can_write(path)) {
        f ^= OF_USE_SWAP;
    }

    /* Set the buffers read only flag */

    if(f & OF_READ_ONLY) {
        buff->ro = 1;
    }

    /*
     * If the use swap option is used, we first check that the
     * buffer is named.  If it isnt, we create random 8 character
     * name.
     */

    if(f & OF_USE_SWAP) {
        if(!buff->is_named) {
            ch = 'p';

            do {
                buf = str_random(8);
                get_full_path(swname, buf, 1024);
                get_swap_name(swname, swname, ch, 1024);
                ch++;
            } while(file_exists(swname));

            free(buf);
        } else {
            ch = 'p';
            get_swap_name(swname, ffname, ch, 1024);
        }

        /*
         * If the swapfile exists, we either want to either recover
         * the file from it, use another swapfile name, or return
         * an error.
         */

        if(file_exists(swname)) {
            if(!file_can_rw(swname)) {
                return(OE_SWAP_NOACCESS);
            } else {
                vm = vm_new(swname);
                vm_open(vm);
                buff->mstblk = vm_get_block(vm, 0, 1);
                vm_close(vm);
            }

            if(!(f & OF_READ_ONLY) && file_is_locked(swname)) {
                return(OE_SWAP_LOCKED);
            }
            else if(f & OF_DISCARD_SWAP) {
                unlink(swname);
            }
            else if(f & OF_RECOVER) {
                buff->vm = vm_new(swname);
                vm_open(buff->vm);
                buff->mstblk = vm_get_block(buff->vm, 0, 1);
                vm_set_locked(buff->vm, buff->mstblk);
                mblk = (mblock*)buff->mstblk->data;
                
                buff->rootnum = char_to_long(mblk->rootnum);
                buff->rootpages = char_to_long(mblk->rootpages);
                buff->root = vm_get_block(buff->vm, buff->rootnum, buff->rootpages);
				
                pblk = (pbheader*)buff->root->data;
                buff_set_dirty(buff, 1);
                recover = 1;

                for(i=0;i<pblk->nptrs;i++) {
                    buff->nlines += pblk->bptr[i].nlines;
                }
            }
            else if(f & OF_USE_NAS) {
                while(file_exists(swname)) {
                    ch++;
                    get_swap_name(swname, ffname, ch, 1024);
                }
            }
            else {
                return(OE_SWAP_EXISTS);
            }
        }

        /* if we are still using the swap.. */

        if(f & OF_USE_SWAP) {
            buff->use_swap = 1;

            if(recover == 0) {
                buff->vm = vm_new(swname);

                vm_open(buff->vm);

                buff->mstblk = vm_new_block(buff->vm, 1);
				
                buff->root = new_ptrblk(buff);
                buff->rootnum = buff->root->num;
				buff->rootpages = buff->root->pages;

                dtblk = new_datablk(buff);

                pelem.nlines = 0;
                pelem.blk_num = dtblk->num;
                pelem.blk_pages = 1;

                pblk_insert((pbheader*)buff->root->data, 0, pelem);

                vm_set_dirty(buff->vm, buff->root);
                vm_set_dirty(buff->vm, dtblk);
            }
        }
    }

    if(recover == 0) {
        buff_read_file(buff);
        buff_set_dirty(buff, 0);
    }
  
    if(buff->use_swap) {
        buff_fill_mblock(buff);
        vm_sync_all(buff->vm);
    }

    buff_preprocess(buff);

    return(OE_SUCCESS);
}

int buff_has_swap(buffer *buff)
{
    return(buff->use_swap);
}

int buff_sync_swap(buffer *buff)
{
    if(buff->use_swap)
    {
        vm_sync_all(buff->vm);
    }

    return(1);
}

int buff_disk_file_timestamp_changed(buffer *buff)
{
    if(buff->is_named) {
        if(file_get_last_modified(buff->ffname) > buff->df_lastmod) {
            return(1);
        }
    }

    return(0);
}

int buff_read_file(buffer *buff)
{
    FILE   *fptr;
    line   *lbuf;
    char   *ffname;
    ulong  lc = 1;

    if(buff->ffname == NULL)
    {
        return(0);
    }

    ffname = buff->ffname;

    if(file_exists(ffname))
    {
        buff_set_filetype(buff, file_get_type(ffname, FT_UNIX));

        lbuf = line_new();

        if((fptr = fopen(ffname, "rb")) != NULL)
        {
            while(!feof(fptr))
            {
                buff_insert(buff, lc++, line_fgets(lbuf, fptr, buff->ftype));
            }

            fclose(fptr);
        }

        line_free(lbuf);
    }

    if(buff->use_swap)
    {
        vm_sync_all(buff->vm);
    }

    if(buff->was_binary)
    {
        buff_set_dirty(buff, 1);
    }

    return(1);
}

void buff_fill_mblock(buffer *buff)
{
    mblock *mb;
    vmem   *vm;

    if(buff->mstblk != NULL) {
        mb = (mblock*)buff->mstblk->data;
        vm = buff->vm;

        strncpy(mb->prg_id, "ced", 3);
        strncpy(mb->version, "1.2-4", 10);
        strncpy(mb->fname, buff->sfname, 256);

        get_user_name(mb->uname, 128);
        get_host_name(mb->hname, 128);

        long_to_char(buff->rootnum, mb->rootnum);
		long_to_char(buff->rootpages, mb->rootpages);
        long_to_char(vm->page_size, mb->page_size);
		
        vm_set_dirty(vm, buff->mstblk);
    }
}

int buff_set_name(buffer *buff, char *fname)
{
    char  *buf;
    char  sfname[256];
    char  ffname[1024];
    char  swname[1024];

    if(fname != NULL && strlen(fname)) {
        get_full_path(ffname, fname, 1024);
        get_file(sfname, ffname, 256);
        get_swap_name(swname, ffname, 'p', 1024);

        if(!strcmp(ffname, buff->ffname)) {
            return(1);
        }

        if(buff->use_swap) {
            if(file_exists(swname)) {
                unlink(swname);
            }             
            if((vm_rename(buff->vm, swname)) == -1) {
                buf = error_get_str(errno);
                buff_call_message(buff, buf, 1);
                return(0);
            }
        }

        strcpy(buff->swname, swname);
        strcpy(buff->sfname, sfname);
        strcpy(buff->ffname, ffname);

        buff_set_dirty(buff, 1);
        buff->df_lastmod = file_get_last_modified(ffname);
        buff->is_named = 1;
        buff->highlight = syntax_set_from_filename(buff->lsyn, fname);
        buff_preprocess(buff);
        buff_call_lines_changed_notify(buff, 0, buff->nlines);
        buff_call_buffer_renamed_notify(buff);

        return(1);
    }

    return(0);
}

void buff_preprocess(buffer *buff)
{
    line *ln;
    ulong i;

    if(buff->highlight) {
        ln = line_new();

        for(i=0; i<buff->nlines; i++) {
            buff_getline(buff, (i+1), ln);
            buff_preprocess_line(buff, ln, i);
        }

        line_free(ln);
    }
}

static long buff_preprocess_line(buffer *buff, line *lbuf, long lnum)
{
    return(syntax_preprocess_line(buff->lsyn, lbuf, lnum));
}

int buff_save(buffer *buff)
{
    FILE   *fptr;
    line   *lbuf;
    ulong  i;
    int    err = 1;
    char   errstr[128];

    if(!buff->is_named) {
        buff_call_message(buff, "Cannot save untitled file.", 1);

        return(0);
    }

    if((fptr = fopen(buff->ffname, "w")) != NULL)
    {
        err = 0;
        lbuf = line_new();

        for(i=0; i < buff->nlines; i++)
        {
            buff_getline(buff, (i+1), lbuf);

            if(line_fputs(lbuf, fptr, buff->ftype, (i != buff->nlines - 1)) == -1)
            {
                err = 1;

                break;
            }
        }
    
        fclose(fptr);
        line_free(lbuf);
    }

    if(err == 0)
    {
        buff->df_lastmod = file_get_last_modified(buff->ffname);
        buff_set_dirty(buff, 0);
        buff_call_message(buff, "File Saved.", 0);
    }
    else
    {
        sprintf(errstr, "Error writing: %s", error_get_str(errno));

        buff_call_message(buff, errstr, 1);
    }

    return(1);
}

int buff_close(buffer *buff)
{
    TRACE(D_MSG, "buffer::buff_close: closing buffer");

    if(buff->use_swap) {
        unlink(buff->swname);
    }

    return(1);
}

/* extra interfaces */

ulong buff_get_nlines(buffer *buff)
{
    return(buff->nlines);
}

int buff_is_ro(buffer *buff)
{
    return(buff->ro);
}

int buff_is_rw(buffer *buff)
{
    return(!buff->ro);
}

int buff_is_named(buffer *buff)
{
    return(buff->is_named);
}

int buff_is_dirty(buffer *buff)
{
    return(buff->dirty);
}

FILE_TYPE buff_get_filetype(buffer *buff)
{
    return(buff->ftype);
}

const char *buff_get_filename(buffer *buff)
{
    return(buff->sfname);
}

const char *buff_get_pathname(buffer *buff)
{
    return(buff->ffname);
}

int buff_get_tabstops(buffer *buff)
{
    return(buff->tabstops);
}

void buff_set_filetype(buffer *buff, FILE_TYPE ftype)
{
    switch(ftype) {
        case FT_DOS : buff->ftype = FT_DOS; break;
        case FT_MAC : buff->ftype = FT_MAC; break;
        default     : buff->ftype = FT_UNIX;
    }
}

int buff_strip(buffer *buff)
{
    long i;

    line *lbuf = line_new();

    for(i=0; i < buff->nlines; i++)
    {
        buff_getline(buff, (i+1), lbuf);

        line_strip(lbuf);

        buff_putline(buff, (i+1), lbuf);
    }
   
    line_free(lbuf);

    buff_call_lines_changed_notify(buff, 0, buff->nlines);
    buff_call_message(buff, "Stripped non-printables.", 1);

    return(1);
}

void buff_undo(buffer *buff)
{
}

void buff_redo(buffer *buff)
{
}

void buff_toggle_tabs(buffer *buff)
{
    buff->show_tabs = !buff->show_tabs;
}

void buff_set_tabstops(buffer *buff, int tabstops)
{
    buff->tabstops = tabstops;
}

void buff_set_callback(buffer *buff, char *name, void (*func)())
{
    callbacks_set(buff->cbacks, name, func);
}

/* obsolete functions ? */

int buff_enable_frontend(buffer *buff)
{
    buff->use_front = 1;
    return(1);
}

int buff_disable_frontend(buffer *buff)
{
    buff->use_front = 0;
    return(1);
}

