static int ln_ins_text(buffer *buff, line *lbuf, long col, long row, line *ins)
{
    ln_ins_prep(buff, lbuf, col, row);
    line_insert(lbuf, ins);
}

static int ln_del_text(buffer *buff, line *lbuf, long col, long row, long amount)
{
    LCDATA lcd_s;
    LCDATA lcd_e;
    long len;
    int status = 0;

    line_get_column_data(lbuf, &lcd_s, x, buff->tabstops);

    if(lcd_s.xpos < (len = line_length(lbuf)))
    {
        line_get_column_data(lbuf, &lcd_e, x + w, buff->tabstops);

        if( lcd_e.xpos > len ) {
            lcd_e.xpos = len;
        }

        if( lcd_e.xpos > lcd_s.xpos )
        {
            line_define_region_at(lbuf, lcd_s.xpos);
            line_goto_x(lbuf, lcd_e.xpos);
            line_region_cut(lbuf, dbuf);

            status = 1;
        }
    }

    return(status);
}

static int ln_sub_text(buffer *buff, line *lbuf, long col, long row, long amount, line *ins)
{
}

static int ln_ins_space(buffer *buff, line *lbuf, long col, long row, long amount)
{
    ln_ins_prep(buff, lbuf, col, row);
    line_insert_padding(lbuf, amount);
}

static int ln_pad(buffer *buff, line *lbuf, long col, long row)
{
    ln_ins_prep(buff, lbuf, col, row);
}

static int ln_ins_prep(buffer *buff, line *lbuf, long col, long row)
{
    LCDATA lcd;
    long len = 0;
    long ins = 0;

    line_get_column_data(lbuf, &lcd, col, buff->tabstops);

    if(lcd.col_diff > 0)
    {
        ins = lcd.col_diff;
    }
    else if(lcd.xpos > (len = line_length(lbuf)))
    {
        ins = lcd.xpos - len;
    }      

    if(ins > 0)
    {
        line_insert_padding_at(lbuf, lcd.xpos, ins);
        line_goto_x(lbuf, lcd.xpos + ins);

        // store the diff as h_space
    }
    else
    {
        line_goto_x(lbuf, lcd.xpos)
    }
}

////////////////////////////////////////////////////////////////////////

static int ln_ins_text(buffer *buff, line *lbuf, line *ibuf, long x, long y, line *ln)
{
    LCDATA lcd;
    long len = 0;
    long ins = 0;

    line_get_column_data(lbuf, &lcd, x, buff->tabstops);

    if(lcd.col_diff > 0)
    {
        ins = lcd.col_diff;
    }
    else if(lcd.xpos > (len = line_length(lbuf)))
    {
        ins = lcd.xpos - len;
        lcd.xpos = len;
    }      

    line_goto_x(ibuf, ins);
    line_pad(ibuf, ' ');
    line_insert(ibuf, ln);
    line_insert_at(lbuf, lcd.xpos, ibuf);

    return(1);
}

/////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////

static DIFF *prim_ins_text(buffer *buff, long x, long y, line *ln)
{
    line *lbuf = line_new();
    line *lins = line_new();

    buff_getline(buff, (y+1), lbuf);

    if(ln_ins_text(buff, lbuf, lins, x, y, ln))
    {
        buff_putline(buff, (y+1), lbuf);
    }

    line_free(lbuf);
    line_free(lins);

    return(NULL);
}

/////////////////////////////////////////////////////////////////////////////////

static DIFF *prim_del_text(buffer *buff, long x, long y, long w)
{               
    line *lbuf = line_new();
    line *ldel = line_new();

    buff_getline(buff, (y+1), lbuf);

    if(ln_del_text(buff, lbuf, ldel, x, y, w))
    {
        buff_putline(buff, (y+1), lbuf);
    }

    line_free(lbuf);
    line_free(ldel);

    return(NULL);
}

/////////////////////////////////////////////////////////////////////////////////

static DIFF *prim_sub_text(buffer *buff, long x, long y, long w, line *ln)
{
    line *lbuf = line_new();
    line *ldel = line_new();
    line *lins = line_new();

    buff_getline(buff, (y+1), lbuf);

    ln_del_text(buff, lbuf, ldel, x, y, w);
    ln_ins_text(buff, lbuf, lins, x, y, ln);

    buff_putline(buff, (y+1), lbuf);

    line_free(lbuf);
    line_free(ldel);
    line_free(lins);

    return(NULL);
}

/////////////////////////////////////////////////////////////////////////////////

static DIFF *prim_del_remainder(buffer *buff, long x, long y)
{
    LCDATA lcd;
    line *lbuf = line_new();
    line *lcut = line_new();
    long len;

    buff_getline(buff, (y+1), lbuf);

    line_get_column_data(lbuf, &lcd, x, buff->tabstops);

    if(lcd.xpos < (len = line_length(lbuf)))
    {
        line_define_region_at(lbuf, lcd.xpos);
        line_to_right(lbuf);
        line_region_cut(lbuf, lcut);

        buff_putline(buff, (y+1), lbuf);
    }

    line_free(lbuf);
    line_free(lcut);

    return(NULL);
}

/////////////////////////////////////////////////////////////////////////////////

static DIFF *prim_sub_remainder(buffer *buff, long x, long y, line *ln)
{
    line *lbuf = line_new();
    line *ldel = line_new();
    line *lins = line_new();

    buff_getline(buff, (y+1), lbuf);

    ln_del_text(buff, lbuf, ldel, x, y, line_length(lbuf) - x);
    ln_ins_text(buff, lbuf, lins, x, y, ln);

    buff_putline(buff, (y+1), lbuf);

    line_free(lbuf);
    line_free(ldel);
    line_free(lins);

    return(NULL);
}

/////////////////////////////////////////////////////////////////////////////////

static DIFF *prim_ins_row(buffer *buff, long y, line *ln)
{
    buff_insert(buff, (y+1), ln);

    return(NULL);
}

/////////////////////////////////////////////////////////////////////////////////

static DIFF *prim_sub_row(buffer *buff, long y, line *ln)
{
    line *lbuf = line_new();

    buff_getline(buff, (y+1), lbuf);
    buff_putline(buff, (y+1), ln);

    line_free(lbuf);

    return(NULL);
}

/////////////////////////////////////////////////////////////////////////////////

static DIFF *prim_del_row(buffer *buff, long y)
{
//    line lcut;

//    if( diff != NULL ) {
//        diff_set_del_row(diff, y, buff_getline(buff, (y+1), &lcut));
//    }

    buff_delete(buff, (y+1));

    return(NULL);
}

/////////////////////////////////////////////////////////////////////////////////

static DIFF *prim_ins_newline(buffer *buff, long x, long y)
{
    LCDATA lcd;
    line *lbuf = line_new();
    line *lnew = line_new();
    long len;

    buff_getline(buff, (y+1), lbuf);

    line_get_column_data(lbuf, &lcd, x, buff->tabstops);

    // FIXME

    if( lcd.xpos > (len = line_length(lbuf)) ) {
        lcd.xpos = len;
    }

    line_define_region_at(lbuf, lcd.xpos);
    line_to_right(lbuf);
    line_region_cut(lbuf, lnew);

    buff_putline(buff, (y+1), lbuf);
    buff_insert(buff, (y+2), lnew);

    line_free(lbuf);
    line_free(lnew);

    return(NULL);
}

/////////////////////////////////////////////////////////////////////////////////

static DIFF *prim_del_newline(buffer *buff, long y)
{
    line *lbuf = line_new();
    line *lnext = line_new();

    buff_getline(buff, (y + 1), lbuf);
    buff_getline(buff, (y + 2), lnext);

    line_to_right(lbuf);
    line_insert(lbuf, lnext);

    buff_putline(buff, (y + 1), lbuf);
    buff_delete(buff, (y + 2));

    line_free(lbuf);
    line_free(lnext);

    return(NULL);
}

/////////////////////////////////////////////////////////////////////////////////

static DIFF *prim_ins_v_space(buffer *buff, long y, long amount)
{
    line *ldum = line_new();

    for( ; amount > 0; amount--)
    {
        buff_insert(buff, (y+1), ldum);
    }

    line_free(ldum);
}

/////////////////////////////////////////////////////////////////////////////////

static DIFF *prim_ins_h_space(buffer *buff, long x, long y, long amount)
{

}

/////////////////////////////////////////////////////////////////////////////////

static DIFF *prim_pad_to_col(buffer *buff, long col, long row)
{
    line *lbuf = line_new();

    buff_getline(buff, (row + 1), lbuf);

    buff_putline(buff, (row + 1), lbuf);
}

/////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////

