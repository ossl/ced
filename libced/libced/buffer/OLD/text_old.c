/**********************************************************************************************
 * Insert text at the specified coordinate.  If the line does not exist, the buffer is
 * padded out with blank rows, and then the line inserted at the end.
 **********************************************************************************************/

static void insert(buffer *buff, long col, long row, dstring *str, INSERT_FLAGS flag)
{
    short ins;
    dstring *lbuf = dstring_new();

    // If we do not have any lines in the buffer, or the row we are trying to
    // insert at doesn't exist yet, then we are going to be doing row inserts,
    // rather than a getline and putline.

    ins = (buff->nlines == 0 || row >= buff->nlines);

    if(ins)
    {
        buff_pad_to_row(buff, row - 1);

        if(col > 0)
        {
            prim_ins_row(buff, row, NULL);
            prim_ins_space(buff, lbuf, 0, col);

            if(flag == IFLAG_STRING && str != NULL)
            {
                prim_ins_text(buff, lbuf, col, str);
            }

            prim_write(buff, row, lbuf);
        }
        else
        {
            prim_ins_row(buff, lbuf, str);
        }

        // If we are inserting a newline, we can infact just insert a blank row
        // because since the insertion point did not originally exist, there'll
        // be no text to split and move onto the next line.

        if(flags == IFLAG_NEWLINE)
        {
            prim_ins_row(buff, row + 1, NULL);
        }
    }
    else
    {
        prim_read(buff, row, lbuf);

        if(col > 0)
        {
            // First we need to get the column data which will tell us the xpos
            // and the actual column position.

            dstring_get_column_data(lbuf, &lcd, col, buff->tabstops);

            // If the column position is beyond the line length, then we simply
            // insert spacing to pad out to that column, otherwise just insert
            // any spacing required for tab compensation.

            (lcd.xpos > (len = dstring_length(lbuf)))
                ? prim_ins_space(buff, lbuf, len, row, lcd.xpos - len)
                : prim_ins_space(buff, lbuf, lcd.xpos, row, lcd.col_diff);

            x = lcd.xpos + lcd.col_diff;
        }
        else
        {
            x = 0;
        }

        if(flag == IFLAG_STRING && str != NULL)
        {
            prim_ins_text(buff, lbuf, x, str);
        }

        prim_write(buff, row, lbuf);

        // If we are inserting a newline, if the insertion point is the
        // beginning of the line, then we can just insert a blank line,
        // otherwise we need to split the row.

        if(flag == IFLAG_NEWLINE)
        {
            (x == 0)
                ? prim_ins_row(buff, row, NULL)
                : prim_split_row(buff, x, row);
        }
    }

    dstring_free(lbuf);
}

