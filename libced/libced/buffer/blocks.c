/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Block level functions.  These are functions that can manipulate blocks
 *    of text at a time in the buffer.  The functions must only use the
 *    row and text level functions, not the primative functions.
 *
 *  Todo
 *    Finish off and tidy up.
 *
 *  Changes since 1.2.12
 *
 *  Created On
 *    29/MAY/2003
 *
 *  Last Updated
 *    29/MAY/2003
 *
 */

/**********************************************************************************************/

#define DEBUG
#define DEBUG_ASSERT

/**********************************************************************************************/

#include <libced/tools/debug.h>
#include <libced/tools/dstring.h>
#include "buffer.h"

/**********************************************************************************************
 * Internal function defintions.
 **********************************************************************************************/

static void normalize_coords(long *x1, long *y1, long *x2, long *y2);

/**********************************************************************************************
 * Insert a rectangular block of text.
 **********************************************************************************************/

void buff_insert_rectangle(buffer *buff, long col, long row, clip *cl)
{
    long i;

    for(i=0; i < clip_nlines(cl); i++)
    {
        buff_insert_text(buff, col, row + i, cl->lines[i]);
    }
}

/**********************************************************************************************
 * Insert a linear block of text.
 **********************************************************************************************/

void buff_insert_block(buffer *buff, long col, long row, clip *cl)
{
    long i;
    long nls;

    if((nls = clip_nlines(cl)) > 0)
    {
        buff_insert_text(buff, col, row, cl->lines[0]);

        if(nls > 1)
        {
            buff_insert_newline(buff, col + dstring_get_length(cl->lines[0]), row);
            buff_insert_text(buff, 0, row + 1, cl->lines[nls - 1]);

            for(i=1; i < nls - 1; i++)
            {
                buff_insert_row(buff, row + i, cl->lines[i]);
            }
        }
    }
}

/**********************************************************************************************
 * Delete a rectangular block of text.
 **********************************************************************************************/

void buff_delete_rectangle(buffer *buff, long col, long row, long w, long h)
{
    long i;

    if(h > 0 && w > 0)
    {
        for(i=0; i < h; i++)
        {
            buff_delete_text(buff, col, row + i, w);
        }
    }
}

/**********************************************************************************************
 * Delete a linear block of text.
 **********************************************************************************************/

void buff_delete_block(buffer *buff, long cs, long rs, long ce, long re)
{
    normalize_coords(&cs, &rs, &ce, &re);

    if(re == rs)
    {
        buff_delete_text(buff, cs, rs, ce - cs);
    }
    else
    {
        buff_truncate_line(buff, cs, rs);

        for(rs++; rs < re; re--)
        {
            buff_delete_row(buff, rs);
        }

        buff_delete_text(buff, 0, rs, ce);
        buff_delete_newline(buff, rs - 1);
    }
}

/**********************************************************************************************
 * Substitute a rectangular block of text.
 **********************************************************************************************/

void buff_put_rectangle(buffer *buff, long col, long row, long w, long h, clip *cl)
{
    long max_del;
    long max_ins;
    long max_sub;
    long i;

    max_del = h;
    max_ins = clip_nlines(cl);
    max_sub = min(max_ins, max_del);

    for(i=0; i < max_sub; i++) buff_subs_text  (buff, col, row + i, w, cl->lines[i]);
    for(   ; i < max_ins; i++) buff_insert_text(buff, col, row + i, cl->lines[i]);
    for(   ; i < max_del; i++) buff_delete_text(buff, col, row + i, w);
}

/**********************************************************************************************
 * Substitute a linear block of text.
 **********************************************************************************************/

void buff_put_block(buffer *buff, long cs, long rs, long ce, long re, clip *cl)
{
    long max_del;
    long max_ins;
    long max_sub;
    long nlines;
    long i;

    normalize_coords(&cs, &rs, &ce, &re);

    nlines = clip_nlines(cl);

    // Find out how many "full rows" should be deleted, inserted, and
    // substituted.

    max_del = re - rs;
    max_ins = nlines - 1;
    max_sub = min(max_ins, max_del);

    // Preparation.

    if(rs == re)
    {
        if(nlines > 1)
        {
            buff_insert_newline(buff, cs, rs);
            buff_insert_text(buff, cs, rs, cl->lines[0]);
            buff_insert_text(buff, 0, rs + 1, cl->lines[max_ins]);
        }
        else
        {
            buff_subs_text(buff, cs, rs, ce - cs, cl->lines[0]);
        }
    }
    else
    {
        buff_subs_remainder(buff, cs, rs, cl->lines[0]);

        (nlines > 1)
            ? buff_subs_text(buff, 0, re, ce, cl->lines[max_ins])
            : buff_delete_text(buff, 0, re, ce);
    }

    // Go through all of the "inbetween" lines, and either substitute them
    // for the replacement lines, insert the new lines, or delete the old
    // lines; in that order.

    for(i=1; i < max_sub; i++) buff_subs_row  (buff, rs + i, cl->lines[i]);
    for(   ; i < max_ins; i++) buff_insert_row(buff, rs + i, cl->lines[i]);
    for(   ; i < max_del; i++) buff_delete_row(buff, rs + max_ins);

    // If we are replacing multiple lines with just one line, then at this
    // point all of the rows inbetween will of been deleted, and we'll be
    // left with 2 lines that need to be joined together.

    if(rs != re && nlines == 1)
    {
        buff_delete_newline(buff, rs);
    }
}

/**********************************************************************************************
 * Normalize Coords - Make sure the coordinates always start with x1 and y1 being the top most
 * point.  If the y coordinates are the same, then we make sure the coordinates start with the
 * left most point.  That way we always have top to bottom, or left to right values.
 **********************************************************************************************/

static void normalize_coords(long *x1, long *y1, long *x2, long *y2)
{
    long ys;
    long ye;
    long xs;
    long xe;

    if(*y1 > *y2)
    {
        ys = *y2;
        ye = *y1;
        xs = *x2;
        xe = *x1;
    }
    else if(*y1 < *y2)
    {
        ys = *y1;
        ye = *y2;
        xs = *x1;
        xe = *x2;
    }
    else
    {
        ys = *y1;
        ye = *y2;
        xs = min(*x1, *x2);
        xe = max(*x1, *x2);
    }

    *x1 = xs;
    *x2 = xe;
    *y1 = ys;
    *y2 = ye;
}

