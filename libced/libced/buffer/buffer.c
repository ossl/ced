/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    The buffer contains functions for accessing and manipulating text
 *    contained within it.
 *
 *  Todo
 *    - (MAS) Need to rework and tidy the code up a little.
 *    - (MAS) Rethink "none swap" mode, don't like linear array method, slow.
 *    - (MAS) Add block and rect substitution methods.
 *
 *  Changes since 1.2.7
 *
 *  Last Updated
 *    21/Sep/2001
 *    30/Mar/2003
 *    31/May/2003
 *
 */

#define DEBUG
#define DEBUG_ASSERT

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <libced/vmlbuf/vmlbuf.h>
#include <libced/vmlbuf/datablock.h>
#include <libced/vmlbuf/ptrblock.h>

#include <libced/tools/structs.h>
#include <libced/tools/search.h>
#include <libced/tools/error.h>
#include <libced/tools/callbacks.h>
#include <libced/tools/debug.h>
#include <libced/tools/dstring.h>

#include <libced/utils/utils.h>
#include <libced/vmem/vmem.h>
#include <libced/tools/syntax.h>
#include <libced/sys/sys.h>

#include "buffer.h"

#define DEFAULT_TABSTOPS 4

#define DIFF_NONE 0
#define DIFF_ADD  1

#define AMOUNT_ALL -1

//extern route *route_new(void);
//extern void   route_free(route *rt, buffer *buff);
//extern void   route_trace(route *rt, buffer *buff, ulong lnum);
//extern void   route_update(route *rt, buffer *buff);

//extern int buff_delete(buffer* buff, ULONG lnum);
//extern int buff_insert(buffer *buff, ULONG lnum, line *ln);
//extern int buff_putline(buffer *buff, ULONG lnum, line *ln);
//extern int buff_getline(buffer* buff, ULONG lnum, line *ln);

// FOR NOW

extern block * new_datablk(buffer *buff);
extern block * new_ptrblk(buffer *buff);

inline void   buff_process_change(buffer *buff, uint col, ulong row, optype op, void *obj, int flags);
static void   buff_fill_mblock(buffer *buff);
static void   buff_preprocess(buffer *buff);
static int    buff_read_file(buffer *buff);
static long   buff_preprocess_line(buffer *buff, dstring *lbuf, long lnum);

dstring *buffer_ins_line(buffer *buff, long row, dstring *ln);
void  buffer_put_line(buffer *buff, long row, dstring *ln);
void  buffer_del_line(buffer *buff, long row);

///////////////////////////////////////////////////////////////////////////////////////////

inline void buff_process_change(buffer *buff, uint col, ulong row, optype op, void *obj, int flags)
{
    if(flags)
    {
        //undo_add_change(buff->undo_list, col, row, op, buff->edit_mode, obj);
    }
}

/**********************************************************
 * Buffer linkage stuff.  This isn't that tidy at the
 * moment, but it will get an overhaul soonish.
 *
 * use_front obsolete ?
 */

void buff_add_link(buffer *buff, void *struct_ptr)
{
    if(struct_ptr) {
        ptr_array_add(&buff->bufflinks, struct_ptr);
        buff_call_link_added_notify(buff);
    }
}

void buff_del_link(buffer *buff, void *struct_ptr)
{
    ulong n;

    if(struct_ptr) {
        if(ptr_array_lookup(&buff->bufflinks, struct_ptr, &n)) {
            ptr_array_del(&buff->bufflinks, n);
            buff_call_link_deleted_notify(buff);
        }
    }
}

void *buff_get_link(buffer *buff, uint n)
{
    if(buff->bufflinks.items > n) {
        return(buff->bufflinks.data[n]);
    }

    return(NULL);
}

int buff_get_link_number(buffer *buff, void *struct_ptr)
{
    ulong n;

    if(struct_ptr) {
        if(ptr_array_lookup(&buff->bufflinks, struct_ptr, &n)) {
            return((int)n);
        }
    }

    return(-1);
}

void buff_call_link_added_notify(buffer *buff)
{
    uint i;
  
    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_link_added(buff->bufflinks.data[i]);
        }
    }
}

void buff_call_link_deleted_notify(buffer *buff)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_link_deleted(buff->bufflinks.data[i]);
        }
    }
}

void buff_call_file_attribs_changed_notify(buffer *buff)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++)
            buff->func_attribs_changed(buff->bufflinks.data[i]);
    }
}

void buff_call_buffer_renamed_notify(buffer *buff)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_buffer_renamed(buff->bufflinks.data[i]);
            buff->func_attribs_changed(buff->bufflinks.data[i]);
        }
    }
}

void buff_call_buffer_destroyed_notify(buffer *buff)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_buffer_destroyed(buff->bufflinks.data[i]);
        }
    }
}

void buff_call_lines_changed_notify(buffer *buff, ulong y, ulong n)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_lines_changed(buff->bufflinks.data[i], y, n);
        }
    }
}

void buff_call_lines_deleted_notify(buffer *buff, ulong y, ulong n)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_lines_deleted(buff->bufflinks.data[i], y, n);
        }
    }
}

void buff_call_lines_inserted_notify(buffer *buff, ulong y, ulong n)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_lines_inserted(buff->bufflinks.data[i], y, n);
        }
    }
}

void buff_call_message(buffer *buff, char *msg, uint beep)
{
    uint i;

    if(buff->use_front) {
        for(i=0; i < buff->bufflinks.items; i++) {
            buff->func_message(buff->bufflinks.data[i], msg, beep);
        }
    }
}

/*
 * Create a new buffer and return a pointer to it.  When a new
 * buffer is created, the root node is automatically created and
 * the first data block.  The first pointer in the root node is
 * set to point at the new data block.
 */

buffer *buff_new(char *fname)
{
    buffer     *buff;
    static int untitled = 0;

    TRACE(D_MSG, "buffer::buff_new: creating new buffer: %s", fname);

    buff = (buffer*)calloc(1, sizeof(buffer));

    if(fname && strlen(fname)) {
        if(get_full_path(buff->ffname, fname, 1024) != NULL) {
            get_file(buff->sfname, buff->ffname, 256);
            buff->is_named = 1;
        }
    }

    if(!buff->is_named) {
        if(untitled == 0) {
            strcpy(buff->sfname, "Untitled");
        } else {
            sprintf(buff->sfname, "Untitled_%d", untitled);
        }

        strcpy(buff->ffname, buff->sfname);
        untitled++;
    }

    buff->highlight = 1;
//    buff->undo_list = undo_new();
//    buff->rt = route_new();
    buff->lsyn = syntax_new(SYS_CONFIG_LANG);
    buff->ftype = CED_CONFIG.filemode;
    buff->tabstops = CED_CONFIG.tabstops;

    syntax_set_from_filename(buff->lsyn, fname);

    ptr_array_init(&buff->bufflinks);

    buff->cbacks = callbacks_new("buffer");

    callbacks_add(buff->cbacks, "link_added"       , CALLBACK_REF(buff->func_link_added));
    callbacks_add(buff->cbacks, "link_deleted"     , CALLBACK_REF(buff->func_link_deleted));
    callbacks_add(buff->cbacks, "lines_changed"    , CALLBACK_REF(buff->func_lines_changed));
    callbacks_add(buff->cbacks, "lines_inserted"   , CALLBACK_REF(buff->func_lines_inserted));
    callbacks_add(buff->cbacks, "lines_deleted"    , CALLBACK_REF(buff->func_lines_deleted));
    callbacks_add(buff->cbacks, "attribs_changed"  , CALLBACK_REF(buff->func_attribs_changed));
    callbacks_add(buff->cbacks, "message"          , CALLBACK_REF(buff->func_message));
    callbacks_add(buff->cbacks, "buffer_renamed"   , CALLBACK_REF(buff->func_buffer_renamed));
    callbacks_add(buff->cbacks, "buffer_destroyed" , CALLBACK_REF(buff->func_buffer_destroyed));

    return(buff);
}

/*
 * Free up the buffer.
 * This needs a lot more doing to it to gracefully close the buffer,
 * but for now this'll do for test purposes..
 */

int buff_destroy(buffer *buff)
{
    vmem  *vm;
    ulong i;

    TRACE(D_MSG, "buffer::buff_destroy: destroying buffer");

    syntax_free(buff->lsyn);
//    route_free(buff->rt, buff);
    //undo_free(buff->undo_list);
    buff_close(buff);

    if(buff->use_swap) {
        vm = buff->vm;
        vm_close(vm);
        vm_free(vm);
    } else {
        for(i=0; i < buff->lines2.items; i++) {
            dstring_free(buff->lines2.data[i]);
        }
    }

    callbacks_free(buff->cbacks);

    buff_call_buffer_destroyed_notify(buff);

    ptr_array_free_data(&buff->lines2);
    ptr_array_free_data(&buff->bufflinks);

    free(buff);

    return(0);
}

int buff_getline_all(buffer *buff, ULONG lnum, dstring *ln_raw, dstring *ln, dstring *fln, int hbn_1, int hbn_2)
{
    dstring *ln_tmp1;
    dstring *ln_tmp2;

    if(lnum > buff->nlines) {
        if(ln_raw) dstring_reset(ln_raw);
        if(fln)    dstring_reset(fln);
        if(ln)     dstring_reset(ln);
        return(0);
    }

    if(ln_raw) {
        ln_tmp1 = ln_raw;
    } else {
        ln_tmp1 = dstring_new();
    }

//    buff_getline(buff, lnum, ln_tmp1);

    vmlbuf_get_line(buff->vbuf, lnum, ln_tmp1);

    if(ln) {
        dstring_reset(ln);
        ln_tmp2 = ln;
    } else {
        ln_tmp2 = dstring_new();
    }

    dstring_insert(ln_tmp2, ln_tmp1);
    dstring_expand_tabs(ln_tmp2, buff->tabstops, (buff->show_tabs) ? '>' : ' ');

    if(fln) {
        dstring_reset(fln);
        dstring_insert(fln, ln_tmp2);

        if(buff->highlight) {
//            syntax_format_line(buff->lsyn, fln, NULL, (lnum - 1), hbn_1, hbn_2);
        }
    }

    if(!ln) dstring_free(ln_tmp2);
    if(!ln_raw) dstring_free(ln_tmp1);

    return(1);
}

operror buff_open(buffer *buff, opflags f)
{
    char      path[1024];
    vmem      *vm;
    block     *dtblk;
    mblock    *mblk;
    pbheader  *pblk;
    ptrelem   pelem;
    char      *ffname;
    char      *swname;
    char      *buf;
    char      ch;
    uint      recover;
    uint      i;

    recover = 0;
    ffname = buff->ffname;
    swname = buff->swname;

    /*
     * First check that we can write to the requested path.
     * If it can't, then we can't use a swap file.
     */

    get_path(path, ffname, 1024);

    if(buff->is_named && file_exists(ffname)) {
        if(!file_can_read(ffname)) {
            return(OE_NOT_READABLE);
        }
        if(!is_file(ffname)) {
            return(OE_NOT_FILE);
        }
        if(!file_can_write(ffname)) {
            buff->ro = 1;
        }

        buff->df_lastmod = file_get_last_modified(ffname);
    }

    if( f  & OF_USE_SWAP && !file_can_write(path)) {
        f ^= OF_USE_SWAP;
    }

    /* Set the buffers read only flag */

    if(f & OF_READ_ONLY)
    {
        buff->ro = 1;
    }

    /*
     * If the use swap option is used, we first check that the
     * buffer is named.  If it isnt, we create random 8 character
     * name.
     */

    if(f & OF_USE_SWAP)
    {
        if(!buff->is_named)
        {
            ch = 'p';

            do
            {
                buf = str_random(8);
                get_full_path(swname, buf, 1024);
                get_swap_name(swname, swname, ch, 1024);
                ch++;
            } while(file_exists(swname));

            free(buf);
        }
        else
        {
            ch = 'p';
            get_swap_name(swname, ffname, ch, 1024);
        }

        /*
         * If the swapfile exists, we either want to either recover
         * the file from it, use another swapfile name, or return
         * an error.
         */

        if(file_exists(swname)) {
            if(!file_can_rw(swname)) {
                return(OE_SWAP_NOACCESS);
            } else {
                vm = vm_new(swname);
                vm_open(vm);
                buff->mstblk = vm_get_block(vm, 0, 1);
                vm_close(vm);
            }

            if(!(f & OF_READ_ONLY) && file_is_locked(swname)) {
                return(OE_SWAP_LOCKED);
            }
            else if(f & OF_DISCARD_SWAP) {
                unlink(swname);
            }
            else if(f & OF_RECOVER) {
                buff->vm = vm_new(swname);
                vm_open(buff->vm);
                buff->mstblk = vm_get_block(buff->vm, 0, 1);
                vm_set_locked(buff->vm, buff->mstblk);
                mblk = (mblock*)buff->mstblk->data;
                
                buff->rootnum = char_to_long(mblk->rootnum);
                buff->rootpages = char_to_long(mblk->rootpages);
                buff->root = vm_get_block(buff->vm, buff->rootnum, buff->rootpages);
				
                pblk = (pbheader*)buff->root->data;
                vmlbuf_set_dirty(buff->vbuf, 1);
                recover = 1;

                for(i=0;i<pblk->nptrs;i++) {
                    buff->nlines += pblk->bptr[i].nlines;
                }
            }
            else if(f & OF_USE_NAS) {
                while(file_exists(swname)) {
                    ch++;
                    get_swap_name(swname, ffname, ch, 1024);
                }
            }
            else {
                return(OE_SWAP_EXISTS);
            }
        }

        /* if we are still using the swap.. */

        if(f & OF_USE_SWAP) {
            buff->use_swap = 1;

            if(recover == 0) {
                buff->vm = vm_new(swname);

                vm_open(buff->vm);

                buff->mstblk = vm_new_block(buff->vm, 1);
				
                buff->root = new_ptrblk(buff);
                buff->rootnum = buff->root->num;
				buff->rootpages = buff->root->pages;

                dtblk = new_datablk(buff);

                pelem.nlines = 0;
                pelem.blk_num = dtblk->num;
                pelem.blk_pages = 1;

                pblk_insert((pbheader*)buff->root->data, 0, pelem);

                vm_set_dirty(buff->vm, buff->root);
                vm_set_dirty(buff->vm, dtblk);
            }
        }
    }

    if(recover == 0) {
        buff_read_file(buff);
        vmlbuf_set_dirty(vbuf->buff, 0);
    }
  
    if(buff->use_swap) {
        buff_fill_mblock(buff);
        vm_sync_all(buff->vm);
    }

    buff_preprocess(buff);

    return(OE_SUCCESS);
}

int buff_has_swap(buffer *buff)
{
    return(buff->use_swap);
}

int buff_sync_swap(buffer *buff)
{
    if(buff->use_swap)
    {
        vm_sync_all(buff->vm);
    }

    return(1);
}

int buff_disk_file_timestamp_changed(buffer *buff)
{
    if(buff->is_named) {
        if(file_get_last_modified(buff->ffname) > buff->df_lastmod) {
            return(1);
        }
    }

    return(0);
}

int buff_read_file(buffer *buff)
{
    FILE *fptr;
    dstring *lbuf;
    char *ffname;
    ulong lc = 1;

    if(buff->ffname == NULL)
    {
        return(0);
    }

    ffname = buff->ffname;

    if(file_exists(ffname))
    {
        buff_set_filetype(buff, file_get_type(ffname, FT_UNIX));

        lbuf = dstring_new();

        if((fptr = fopen(ffname, "rb")) != NULL)
        {
            while(!feof(fptr))
            {
                buff_insert(buff, lc++, dstring_fgets(lbuf, fptr, buff->ftype));
            }

            fclose(fptr);
        }

        dstring_free(lbuf);
    }

    if(buff->use_swap)
    {
        vm_sync_all(buff->vm);
    }

    if(buff->was_binary)
    {
        vmlbuf_set_dirty(buff->vbuf, 1);
    }

    return(1);
}

void buff_fill_mblock(buffer *buff)
{
    mblock *mb;
    vmem   *vm;

    if(buff->mstblk != NULL) {
        mb = (mblock*)buff->mstblk->data;
        vm = buff->vm;

        strncpy(mb->prg_id, "ced", 3);
        strncpy(mb->version, "1.2-4", 10);
        strncpy(mb->fname, buff->sfname, 256);

        get_user_name(mb->uname, 128);
        get_host_name(mb->hname, 128);

        long_to_char(buff->rootnum, mb->rootnum);
		long_to_char(buff->rootpages, mb->rootpages);
        long_to_char(vm->page_size, mb->page_size);
		
        vm_set_dirty(vm, buff->mstblk);
    }
}

int buff_set_name(buffer *buff, char *fname)
{
    char  *buf;
    char  sfname[256];
    char  ffname[1024];
    char  swname[1024];

    if(fname != NULL && strlen(fname)) {
        get_full_path(ffname, fname, 1024);
        get_file(sfname, ffname, 256);
        get_swap_name(swname, ffname, 'p', 1024);

        if(!strcmp(ffname, buff->ffname)) {
            return(1);
        }

        if(buff->use_swap) {
            if(file_exists(swname)) {
                unlink(swname);
            }             
            if((vm_rename(buff->vm, swname)) == -1) {
                buf = error_get_str(errno);
                buff_call_message(buff, buf, 1);
                return(0);
            }
        }

        strcpy(buff->swname, swname);
        strcpy(buff->sfname, sfname);
        strcpy(buff->ffname, ffname);

        vmlbuf_set_dirty(buff->vbuf, 1);
        buff->df_lastmod = file_get_last_modified(ffname);
        buff->is_named = 1;
        buff->highlight = syntax_set_from_filename(buff->lsyn, fname);
        buff_preprocess(buff);
        buff_call_lines_changed_notify(buff, 0, buff->nlines);
        buff_call_buffer_renamed_notify(buff);

        return(1);
    }

    return(0);
}

void buff_preprocess(buffer *buff)
{
    dstring *ln;
    ulong i;

    if(buff->highlight) {
        ln = dstring_new();

        for(i=0; i < buff->nlines; i++) {
            //buff_getline(buff, (i+1), ln);
            vmlbuf_get_line(buff->vbuf, i, ln);
            buff_preprocess_line(buff, ln, i);
        }

        dstring_free(ln);
    }
}

static long buff_preprocess_line(buffer *buff, dstring *lbuf, long lnum)
{
    return(syntax_preprocess_line(buff->lsyn, lbuf, lnum));
}

int buff_save(buffer *buff)
{
    FILE    *fptr;
    dstring *lbuf;
    ulong   i;
    int     err = 1;
    char    errstr[128];

    if(!buff->is_named) {
        buff_call_message(buff, "Cannot save untitled file.", 1);

        return(0);
    }

    if((fptr = fopen(buff->ffname, "w")) != NULL)
    {
        err = 0;
        lbuf = dstring_new();

        for(i=0; i < buff->nlines; i++)
        {
            //buff_getline(buff, (i+1), lbuf);
            vmlbuf_get_line(buff->vbuf, i, lbuf);

            if(dstring_fputs(lbuf, fptr, buff->ftype, (i != buff->nlines - 1)) == -1)
            {
                err = 1;

                break;
            }
        }
    
        fclose(fptr);
        dstring_free(lbuf);
    }

    if(err == 0)
    {
        buff->df_lastmod = file_get_last_modified(buff->ffname);
        vmlbuf_set_dirty(buff->vbuf, 0);
        buff_call_message(buff, "File Saved.", 0);
    }
    else
    {
        sprintf(errstr, "Error writing: %s", error_get_str(errno));

        buff_call_message(buff, errstr, 1);
    }

    return(1);
}

int buff_close(buffer *buff)
{
    TRACE(D_MSG, "buffer::buff_close: closing buffer");

    if(buff->use_swap) {
        unlink(buff->swname);
    }

    return(1);
}

/* extra interfaces */

ulong buff_get_nlines(buffer *buff)
{
    return(buff->nlines);
}

int buff_is_ro(buffer *buff)
{
    return(buff->ro);
}

int buff_is_rw(buffer *buff)
{
    return(!buff->ro);
}

int buff_is_named(buffer *buff)
{
    return(buff->is_named);
}

int buff_is_dirty(buffer *buff)
{
    return(buff->dirty);
}

FILE_TYPE buff_get_filetype(buffer *buff)
{
    return(buff->ftype);
}

const char *buff_get_filename(buffer *buff)
{
    return(buff->sfname);
}

const char *buff_get_pathname(buffer *buff)
{
    return(buff->ffname);
}

int buff_get_tabstops(buffer *buff)
{
    return(buff->tabstops);
}

void buff_set_filetype(buffer *buff, FILE_TYPE ftype)
{
    switch(ftype) {
        case FT_DOS : buff->ftype = FT_DOS; break;
        case FT_MAC : buff->ftype = FT_MAC; break;
        default     : buff->ftype = FT_UNIX;
    }
}

int buff_strip(buffer *buff)
{
    long i;
    dstring *lbuf = dstring_new();

    for(i=0; i < buff->nlines; i++)
    {
        vmlbuf_get_line(buff->vbuf, i, lbuf);
        dstring_strip(lbuf);
        vmlbuf_get_line(buff->vbuf, i, lbuf);
    }
   
    dstring_free(lbuf);

    buff_call_lines_changed_notify(buff, 0, buff->nlines);
    buff_call_message(buff, "Stripped non-printables.", 1);

    return(1);
}

void buff_undo(buffer *buff)
{
}

void buff_redo(buffer *buff)
{
}

void buff_toggle_tabs(buffer *buff)
{
    buff->show_tabs = !buff->show_tabs;
}

void buff_set_tabstops(buffer *buff, int tabstops)
{
    buff->tabstops = tabstops;
}

void buff_set_callback(buffer *buff, char *name, void (*func)())
{
    callbacks_set(buff->cbacks, name, func);
}

/* obsolete functions ? */

int buff_enable_frontend(buffer *buff)
{
    buff->use_front = 1;
    return(1);
}

int buff_disable_frontend(buffer *buff)
{
    buff->use_front = 0;
    return(1);
}





/////////////////////////////////////////////////////////////////////////////////
// TEMP FUNCTIONS
///////////////////////////////////////////////////////////////////////

/*
dstring *vmlbuf_ins_line(buffer *buff, long row, dstring *ln)
{
    buff_insert(buff, row + 1, ln);

    return(ln);
}

dstring *vmlbuf_put_line(buffer *buff, long row, dstring *ln)
{
    buff_putline(buff, row + 1, ln);

    return(ln);
}

void vmlbuf_del_line(buffer *buff, long row)
{
    buff_delete(buff, row + 1);
}
*/
