#ifndef _BUFFER_H
#define _BUFFER_H

#include <libced/Deprecated/clip.h>

#include <libced/tools/dstring.h>
#include <libced/tools/syntax.h>
#include <libced/tools/search.h>
#include <libced/tools/structs.h>
#include <libced/tools/callbacks.h>

#include <libced/vmem/vmem.h>

#include <libced/common.h>

#define buff_number_of_links(x) x->bufflinks.items

typedef struct _bline         bufline;  /* line in the buffer */
typedef struct _line_diff     lndiff;   /* line difference */
typedef struct _ptr_array     difflist; /* list of line differences to apply */
typedef struct _ptr_array     blinks;   /* buffer links array */
typedef struct _buffer        buffer;   /* main buffer structure */
typedef struct _change_event  change;   /* a change, undo redo */
typedef struct _change_list   chlist;   /* a list of changes */
typedef union  _change_item   chitem;   /* item representing a change */
typedef struct _event         event;    /* event ...... */
typedef struct _undo          undo;     /* undo ...... */
typedef enum   _optype        optype;   /* type of operation just done */
typedef enum   _undo_op       uop;      /* undo operation type */
typedef enum   _edit_mode     edmode;   /* insert or overwrite */
typedef enum   _open_error    operror;  /* open error */
typedef enum   _open_flags    opflags;  /* open flags */
typedef enum   _file_type     filetype; /* file type */

enum _open_flags
{
    OF_OPEN         = 1 << 0, /* normal open */
    OF_USE_SWAP     = 1 << 1, /* use a swap file */
    OF_USE_NAS      = 1 << 2, /* use next availble swap file */
    OF_RECOVER      = 1 << 3, /* recover from the swap file */
    OF_READ_ONLY    = 1 << 4, /* open file read-only */
    OF_DISCARD_SWAP = 1 << 5  /* if the swap already exists, just overwrite it */
};

enum _open_error
{
    OE_SUCCESS,
    OE_SWAP_EXISTS,
    OE_SWAP_LOCKED,
    OE_SWAP_NOACCESS,
    OE_NOT_READABLE,
    OE_NOT_FILE,
    OE_READ_ONLY,
    OE_ERROR
};

/***
enum _file_type
{
    FT_UNIX,
    FT_DOS,
    FT_MAC
};
***/

enum _edit_mode
{
    EM_INSERT,           /* edit mode is insert */
    EM_OVERWRITE         /* edit mode is overwrite */
};

enum _optype
{
    E_NONE,
    E_LIST_INSERT,
    E_LIST_DELETE,
    E_TEXT_INSERT,
    E_TEXT_DELETE,
    E_TEXT_SUBS,
    E_LINE_SPLIT,
    E_LINE_DELNL,
    E_BLOCK_INSERT,
    E_BLOCK_DELETE,
    E_BLOCK_SUBS
};

enum _undo_op
{
    U_NONE,
    U_UNDO,
    U_REDO
};

enum _search_params
{
    BUFFER_SUBS_ONCE      = 1 << 0,
    BUFFER_SUBS_LINEAR    = 1 << 1,
    BUFFER_SUBS_RECTANGLE = 1 << 2,
    BUFFER_SUBS_ALL       = 1 << 3
};

struct _event
{
    int    state;     /* open or closed */
    ulong  nchls;     /* number of changelists */
    chlist *chl;      /* list of changelists */
    event  *next;     /* next undo in list */
    event  *prev;     /* previous undo in list */
};

struct _bline
{
    dstring *ln;
    long row;
    int  dirty;
};

struct _undo
{
    event *first;     /* first event in undo list */
    event *current;   /* current event in undo list */
    event *last;      /* last event in undo list */
    uop   lastop;     /* whether last op was undo or redo or none */
};

struct _change_list // DEPRECATED
{
    ulong  size;
    change **changes;
};

union _change_item // DEPRECATED
{
    difflist *list;
    dstring  *ln;
    clip *cl;
};

struct _change_event // DEPRECATED
{
    optype  op;       /* type of event */
    edmode  em;       /* edit mode during change */
    uint    x;        /* x position of change */
    ulong   y;        /* y position of change */
    chitem  src;      /* source item representing change */
    chitem  dest;     /* destination item for substitutions */
};

struct _line_diff // DEPRECATED
{
    uint    x;        /* x position of diff */
    ulong   y;        /* y position of diff */
    dstring *ldiff;   /* the difference */
    dstring *lsubs;   /* optional substitution */
};

/* buffer tree routing stuff */

/* master block structure */

struct _master_block
{
  char  prg_id[7];    /* ced swap file id */
  char  version[10];  /* version of program */
  char  page_size[4]; /* block vm page size */
  char  rootnum[4];   /* root block number */
  char  rootpages[4]; /* NEW : root block number of pages */
  char  fname[256];   /* original file being edited */
  char  uname[128];   /* user that edited file */
  char  hname[128];   /* host-name file was edited on */
  char  inode[4];     /* inode of original file */
  char  mtime[4];     /* modification time */
  char  proc_id[4];   /* process id of ced when editing */
  long  byte_long;    /* byte order check of long */
  int   byte_int;     /* byte order check of int */
  short byte_short;   /* byte order check of short */
};

/* buffer structure */

struct _buffer
{
    int         buffnum;       /* buffer id number */
    int         in_undo;       /* should the buffer store diffs */
    int         fdesc;         /* file descriptor */
    FILE_TYPE   ftype;         /* the type of file */
    int         is_named;      /* bool if the buffer is named */
    char        sfname[256];   /* single filename of buffer */
    char        ffname[1024];  /* full filename of buffer */
    block       *mstblk;       /* master block for this buffer */
    block       *root;         /* root block (always locked) */
    block       *cblk;         /* current block (locked) */
    int         ro;            /* read only flag */
    int         dirty;         /* modified flag */
    undo        *undo_list;    /* undo, a list of events */
    uint        tabstops;      /* TEMP: tab stops */
    edmode      edit_mode;     /* current edit mode ... */
    int         use_swap;      /* use a swap file */
    int         use_front;     /* use a frontend */
    int         recover;       /* recover the swap */
    ulong       clnum;         /* line number of cached line */
    dstring     **lines;       /* lines in the buffer when not using vmem */
    syntax      *lsyn;         /* language syntax highlighter */
    uint        highlight;     /* use syntax highlight flag */
    blinks      bufflinks;     /* buffer links */
    long        df_lastmod;    /* disk file last modification time */
    int         show_tabs;     /* show tab characters when expanding tabs */
    int         was_binary;    /* was a binary file, now stripped */
    struct _ptr_array lines2;


    // OBSELETE ///////////////////////////////////////////////////////////

    char        swname[1024];  /* filename of swapfile */
    vmem        *vm;           /* virtual memory area */
    long        rootnum;       /* block number of root block */
    long        rootpages;     /* number of pages of root block */
    ulong       nlines;        /* number of lines in buffer */
//    route       *rt;           /* main route */

    ///////////////////////////////////////////////////////////////////////
  
    callbacks *cbacks;
  
    CALLBACK_DEF (func_link_added)       (void *);
    CALLBACK_DEF (func_link_deleted)     (void *);
    CALLBACK_DEF (func_buffer_renamed)   (void *);
    CALLBACK_DEF (func_buffer_destroyed) (void *);
    CALLBACK_DEF (func_attribs_changed)  (void *);
    CALLBACK_DEF (func_lines_changed)    (void *, ulong, ulong);
    CALLBACK_DEF (func_lines_deleted)    (void *, ulong, ulong);
    CALLBACK_DEF (func_lines_inserted)   (void *, ulong, ulong);
    CALLBACK_DEF (func_message)          (void *, char *, uint);
};

/*
int buff_apply_difflist      (buffer *buff, difflist *dl, int neg);
int buff_text_insert         (buffer *buff, uint x, ulong y, line *ln, int flags);
int buff_text_delete         (buffer *buff, uint x, ulong y, uint n, int flags);
int buff_text_subs           (buffer *buff, uint x, ulong y, uint n, line *ln, int flags);
int buff_line_split          (buffer *buff, uint x, ulong y, int flags);
int buff_line_delete_newline (buffer *buff, ulong y, int flags);
int buff_line_replace        (buffer *buff, ulong row, line *ln, int flags);
int buff_line_delete         (buffer *buff, ulong row, int flags);
int buff_block_insert        (buffer *buff, uint x, ulong y, clip *cl, int flags);
int buff_block_delete        (buffer *buff, uint x1,ulong y1,uint x2,ulong y2,int flags);
int buff_block_subs          (buffer *buff, uint x1,ulong y1,uint x2,ulong y2,clip *cl,int flags);
int buff_rect_insert         (buffer *buff, uint x, ulong y, clip *cl, int flags);
int buff_rect_delete         (buffer *buff, uint x, ulong y, uint w, ulong h, int flags);
int buff_pad                 (buffer *buff, uint x, ulong y, int flags);
*/



/* other functions */

buffer*  buff_new(char *fname);
operror  buff_open(buffer *buff, opflags f);
int      buff_save(buffer *buff);
int      buff_close(buffer *buff);
int      buff_destroy(buffer *buff);
int      buff_insert(buffer *buff, ULONG lnum, dstring *ln);  /* private */
int      buff_delete(buffer *buff, ULONG lnum);            /* private */
int      buff_getline_all(buffer *buff, ULONG lnum, dstring *ln_raw, dstring *ln, dstring *fln, int hbn_1, int hbn_2);
int      buff_getline(buffer *buff, ULONG lnum, dstring *ln);
int      buff_putline(buffer *buff, ULONG lnum, dstring *ln); /* private */
void     buff_push_canvas(buffer *buff, void *struct_ptr);
void     buff_pop_canvas(buffer *buff);
int      buff_enable_frontend(buffer *buff);
int      buff_disable_frontend(buffer *buff);
int      buff_set_name(buffer *buff, char *fname);
void     buff_set_readonly(buffer *buff, int b);
int      buff_substitute(buffer *buff, uint x1, ulong y1, uint x2, ulong y2, search *srch, int sflags);

chlist *changelist_new(void);
int     changelist_apply(buffer *buff, chlist *chl, int negate_changes);   
void    changelist_free(chlist *chl);
void    changelist_add(chlist *chl, uint x, ulong y, optype op, edmode em, void *obj);
void    changelist_get_end_coords(chlist *chl, uint *x, ulong *y);
void    changelist_get_start_coords(chlist *chl, uint *x, ulong *y);

/* line diff stuff */

difflist *difflist_new  (void);
difflist *difflist_dup  (difflist *dlist);
void      difflist_free (difflist *dlist);

/* undo interface */

undo   *undo_new(void);
event  *undo_undo(undo *u);
event  *undo_redo(undo *u);
void    undo_add_change(undo *u, uint x, ulong y, optype op, edmode em, void *obj);
void    undo_add_event(undo *u);
void    undo_end_event(undo *u);
void    undo_flush(undo *u);
void    undo_free(undo *u);

/* buff_call stuff */

void buff_call_link_added_notify(buffer *buff);
void buff_call_link_deleted_notify(buffer *buff);
void buff_call_file_attribs_changed_notify(buffer *buff);
void buff_call_buffer_renamed_notify(buffer *buff);
void buff_call_lines_changed_notify(buffer *buff, ulong y, ulong n);
void buff_call_lines_deleted_notify(buffer *buff, ulong y, ulong n);
void buff_call_lines_inserted_notify(buffer *buff, ulong y, ulong n);
void buff_call_message(buffer *buff, char *msg, uint beep);

/*
 * Functions for adding a front-end to the buffer,
 * which will then recieve notify events when
 * an event occurs.
 */

void  buff_add_link(buffer *buff, void *struct_ptr);
void  buff_del_link(buffer *buff, void *struct_ptr);
int   buff_get_link_number(buffer *buff, void *struct_ptr);
void *buff_get_link(buffer *buff, uint n);

ulong   buff_get_nlines(buffer *buff);
int     buff_is_ro(buffer *buff);
int     buff_is_rw(buffer *buff);
int     buff_is_named(buffer *buff);
int     buff_is_dirty(buffer *buff);

int     buff_strip(buffer *buff);

filetype  buff_get_filetype(buffer *buff);

const char *buff_get_filename(buffer *buff);
const char *buff_get_pathname(buffer *buff);

int buff_disk_file_timestamp_changed(buffer *buff);
int buff_sync_swap(buffer *buff);
int buff_has_swap(buffer *buff);
void   buff_set_tabstops(buffer *buff, int tabstops);
void   buff_set_filetype(buffer *buff, FILE_TYPE ftype);
int    buff_get_tabstops(buffer *buff);
void   buff_toggle_tabs(buffer *buff);

void buff_set_callback(buffer *buff, char *name, void (*func)());


//////////////////////////////////////////////////////////////////////////

void buff_insert_row(buffer *buff, long row, dstring *str);
void buff_delete_row(buffer *buff, long row);
void buff_subs_row(buffer *buff, long row, dstring *str);
void buff_pad_to_row(buffer *buff, long row);

void buff_insert_text(buffer *buff, long col, long row, dstring *str);
void buff_insert_text_nl(buffer *buff, long col, long row, dstring *str);
void buff_insert_newline(buffer *buff, long col, long row);

void buff_subs_text(buffer *buff, long col, long row, long amount, dstring *subs);
void buff_delete_text(buffer *buff, long col, long row, long amount);
void buff_truncate_line(buffer *buff, long col, long row);
void buff_subs_remainder(buffer *buff, long col, long row, dstring *subs);

void buff_insert_rectangle(buffer *buff, long col, long row, clip *cl);
void buff_insert_block(buffer *buff, long col, long row, clip *cl);
void buff_delete_rectangle(buffer *buff, long col, long row, long w, long h);
void buff_delete_block(buffer *buff, long cs, long rs, long ce, long re);
void buff_put_rectangle(buffer *buff, long col, long row, long w, long h, clip *cl);
void buff_put_block(buffer *buff, long cs, long rs, long ce, long re, clip *cl);

void buff_delete_newline(buffer *buff, long row);
long buff_get_line_length(buffer *buff, long row);
void buff_pad(buffer *buff, long col, long row);

//////////////////////////////////////////////////////////////////////////

#endif /* _BUFFER_H */

