/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2003
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    These are the lowest level primative operations that are carried out on the
 *    buffer.  All buffer higher level manipulations use these lower level primatives.
 *    The primatives store there changes in the buffer's undo buffer.
 *
 *  Todo
 *
 *  Created
 *    Saturday, 10th May 2003
 *
 *  Last Updated
 *    Saturday, 10th May 2003
 *
 */

/**********************************************************************************************/

#define DEBUG
#define DEBUG_ASSERT

/**********************************************************************************************/

#include <stdlib.h>
#include "diffs.h"
#include "buffer.h"

/**********************************************************************************************/

static void diff_print(DIFF *d);

/**********************************************************************************************/

DIFF *diff_new(DIFF_CMD cmd, long x, long y)
{
    DIFF *d = calloc(1, sizeof(DIFF));

    d->cmd = cmd;
    d->x = x;
    d->y = y;

    return(d);
}

/**********************************************************************************************/

void diff_free(DIFF *diff)
{
    free(diff);
}

/**********************************************************************************************/

void diff_add(buffer *buff, DIFF *d)
{
    diff_print(d);
}

/**********************************************************************************************/

static void diff_print(DIFF *d)
{
    char buf[4096];

    printf("[ DIFF ================================================================>\n");

    switch(d->cmd)
    {
        case DIFF_NONE     : printf("[ DIFF cmd: DIFF_NONE !!!\n"); break;

        case DIFF_INS_TEXT : printf("[ DIFF cmd: DIFF_INS_TEXT\n"); break;
        case DIFF_DEL_TEXT : printf("[ DIFF cmd: DIFF_INS_TEXT\n"); break;
        case DIFF_SUB_TEXT : printf("[ DIFF cmd: DIFF_INS_TEXT\n"); break;

        case DIFF_INS_ROW  : printf("[ DIFF cmd: DIFF_INS_ROW\n"); break;
        case DIFF_DEL_ROW  : printf("[ DIFF cmd: DIFF_INS_ROW\n"); break;
        case DIFF_SUB_ROW  : printf("[ DIFF cmd: DIFF_INS_ROW\n"); break;

        case DIFF_INS_H_SPACE : printf("[ DIFF cmd: DIFF_INS_H_SPACE\n"); break;

        case DIFF_SPLIT : printf("[ DIFF cmd: DIFF_SPLIT\n"); break;
        case DIFF_JOIN  : printf("[ DIFF cmd: DIFF_JOIN\n"); break;
    }

    printf("[ DIFF x pos  : %ld\n", d->x);
    printf("[ DIFF y pos  : %ld\n", d->y);
    printf("[ DIFF amount : %ld\n", d->amount);
    printf("[ DIFF ins    : %s\n", dstring_get_string2(d->ins, buf, 4096));
    printf("[ DIFF cut    : %s\n", dstring_get_string2(d->cut, buf, 4096));
    printf("[ DIFF ****************************************************************|\n");
    printf("\n");
}

/**********************************************************************************************/
