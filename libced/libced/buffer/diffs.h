#ifndef _DIFFS_H
#define _DIFFS_H

#include <libced/tools/dstring.h>
#include "buffer.h"

typedef enum _DIFF_CMD
{
    DIFF_NONE,

    // Primitives that we can insert

    DIFF_INS_TEXT,
    DIFF_INS_ROW,
    DIFF_INS_H_SPACE,

    // Primatives that we can delete

    DIFF_DEL_TEXT,
    DIFF_DEL_ROW,

    // Privatives that we can substitute

    DIFF_SUB_TEXT,
    DIFF_SUB_ROW,

    DIFF_SPLIT,
    DIFF_JOIN

} DIFF_CMD;

typedef struct _diff_
{
    DIFF_CMD cmd;
    long x;
    long y;
    long amount;
    dstring *ins;
    dstring *cut;
} DIFF;

DIFF *diff_new(DIFF_CMD cmd, long x, long y);
void  diff_free(DIFF *diff);
void  diff_add(buffer *buff, DIFF *d);

#endif /* _DIFFS_H */
