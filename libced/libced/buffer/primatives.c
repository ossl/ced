/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2003
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    These are the lowest level primative operations that are carried out on the
 *    buffer.  All buffer higher level manipulations use these lower level primatives.
 *    The primatives store there changes in the buffer's undo buffer.
 *
 *  Todo
 *
 *  Created
 *    Saturday, 10th May 2003
 *
 *  Last Updated
 *    Saturday, 10th May 2003
 *
 */

/**********************************************************************************************/

#define DEBUG
#define DEBUG_ASSERT

/**********************************************************************************************/

#include <libced/tools/debug.h>
#include <libced/tools/dstring.h>
#include "diffs.h"
#include "buffer.h"
#include "primatives.h"

/**********************************************************************************************
 * Some buffer primative operations have the actual line passed in containing the actual
 * line.  This is so the line doesn't have to keep being fetched from the buffer internal
 * structure when multiple primitave operations are carried out on it.
 **********************************************************************************************/

/**********************************************************************************************
 * Insert text at the given point in the buffer.
 **********************************************************************************************/

inline DIFF *prim_ins_text(buffer *buff, dstring *lbuf, long x, long row, dstring *ins)
{
    DIFF *diff = NULL;

    ASSERT(dstring_get_length(lbuf) >= x);

    if(ins)
    {
        dstring_insert_at(lbuf, x, ins);

        if(buff->in_undo == FALSE)
        {
            diff = diff_new(DIFF_INS_TEXT, x, row);
            diff->ins = dstring_dup(ins);
            diff_add(buff, diff);
        }
    }

    return(diff);
}

/**********************************************************************************************
 * Delete text from the given point in the buffer.
 **********************************************************************************************/

inline DIFF *prim_del_text(buffer *buff, dstring *lbuf, long x, long row, long amount)
{
    return(prim_sub_text(buff, lbuf, x, row, amount, NULL));
}

/**********************************************************************************************
 * Substitute text from the given point in the buffer with the passed in text.
 **********************************************************************************************/

inline DIFF *prim_sub_text(buffer *buff, dstring *lbuf, long x, long row, long amount, dstring *ins)
{
    DIFF *diff = NULL;

    ASSERT(dstring_get_length(lbuf) >= x);
    ASSERT(dstring_get_length(lbuf) >= x + amount);

    dstring_define_region_at(lbuf, x);

    (amount == -1)
        ? dstring_cursor_to_right(lbuf)
        : dstring_cursor_goto_x(lbuf, x + amount);

    // If we are storing the diff for this operation, then we need to cut the region we
    // are substituting and store it, otherwise we can just delete it.

    if(buff->in_undo == FALSE)
    {
        diff = diff_new((ins) ? DIFF_SUB_TEXT : DIFF_DEL_TEXT, x, row);
        diff->ins = (ins) ? dstring_dup(ins) : NULL;
        diff->cut = dstring_region_cut(lbuf, dstring_new());
        diff_add(buff, diff);
    }
    else
    {
        dstring_region_delete(lbuf);
    }

    if(ins)
    {
        dstring_insert(lbuf, ins);
    }

    return(diff);
}

/**********************************************************************************************
 * Insert the given number of spaces into the line.  This is primarily used for padding and
 * saves memory by storing the amount of space characters inserted, rather than the actual
 * space characters.
 **********************************************************************************************/

inline DIFF *prim_ins_space(buffer *buff, dstring *lbuf, long x, long row, long amount)
{
    DIFF *diff = NULL;

    ASSERT(dstring_get_length(lbuf) >= x);

    if(amount > 0)
    {
        dstring_cursor_goto_x(lbuf, x);
        dstring_insert_padding(lbuf, amount);

        if(buff->in_undo == FALSE)
        {
            diff = diff_new(DIFF_INS_H_SPACE, x, row);
            diff->amount = amount;
            diff_add(buff, diff);
        }
    }

    return(diff);
}

/**********************************************************************************************
 * Read a line from the buffer.
 **********************************************************************************************/

dstring *prim_read(buffer *buff, long row, dstring *lbuf)
{
    ASSERT(row < buff->nlines);

    vmlbuf_get_line(buff, row, lbuf);

    return(lbuf);
}

/**********************************************************************************************
 * Write back a line to the buffer
 **********************************************************************************************/

dstring *prim_write(buffer *buff, long row, dstring *lbuf)
{
    ASSERT(row < buff->nlines);

    vmlbuf_put_line(buff, row, lbuf);

    return(lbuf);
}

/**********************************************************************************************
 * Split the given row at the given point x.  The second part of the split is inserted at
 * the next line.
 **********************************************************************************************/

inline DIFF *prim_split_row(buffer *buff, long x, long row)
{
    DIFF *diff = NULL;
    dstring *lbuf = dstring_new();
    dstring *lcut = dstring_new();

    ASSERT(row <= buff->nlines);

    vmlbuf_get_line(buff, row, lbuf);

    ASSERT(dstring_get_length(lbuf) >= x);

    dstring_define_region_at(lbuf, x);
    dstring_cursor_to_right(lbuf);
    dstring_region_cut(lbuf, lcut);

    vmlbuf_put_line(buff, row, lbuf);
    vmlbuf_ins_line(buff, row + 1, lcut);

    dstring_free(lbuf);
    dstring_free(lcut);

    if(buff->in_undo == FALSE)
    {
        diff = diff_new(DIFF_SPLIT, x, row);
        diff_add(buff, diff);
    }

    return(diff);
}

/**********************************************************************************************
 * Join the given row with the row following it.  The second row is effectively cut, and
 * then pasted onto the end of the given row.
 **********************************************************************************************/

inline DIFF *prim_join_row(buffer *buff, long row)
{
    DIFF *diff = NULL;
    dstring *lbuf = dstring_new();
    dstring *lcut = dstring_new();

    ASSERT(row < buff->nlines - 1);

    vmlbuf_get_line(buff, row, lbuf);
    vmlbuf_get_line(buff, row + 1, lcut);

    dstring_cursor_to_right(lbuf);
    dstring_insert(lbuf, lcut);

    vmlbuf_put_line(buff, row, lbuf);
    vmlbuf_del_line(buff, row, lcut);

    dstring_free(lbuf);
    dstring_free(lcut);

    if(buff->in_undo == FALSE)
    {
        diff = diff_new(DIFF_JOIN, 0, row);
        diff_add(buff, diff);
    }

    return(diff);
}

/**********************************************************************************************
 * Insert the given line as a new row at the specified row.
 **********************************************************************************************/

inline DIFF *prim_ins_row(buffer *buff, long row, dstring *ln)
{
    DIFF *diff = NULL;

    ASSERT(row <= buff->nlines);

    if(buff->in_undo == FALSE)
    {
        diff = diff_new(DIFF_INS_ROW, 0, row);
        diff->ins = dstring_dup(ln);
        diff_add(buff, diff);
    }

    vmlbuf_ins_line(buff, row, ln);

    return(diff);
}

/**********************************************************************************************
 * Delete the row specified.
 **********************************************************************************************/

inline DIFF *prim_del_row(buffer *buff, long row)
{
    DIFF *diff = NULL;

    ASSERT(row < buff->nlines);

    if(buff->in_undo == FALSE)
    {
        diff = diff_new(DIFF_DEL_ROW, 0, row);
        diff->cut = vmlbuf_get_line(buff, row, dstring_new());
        diff_add(buff, diff);
    }

    vmlbuf_del_line(buff, row);

    return(diff);
}

/**********************************************************************************************
 * Substitute the row specified with the given row.
 **********************************************************************************************/

inline DIFF *prim_sub_row(buffer *buff, long row, dstring *ln)
{
    DIFF *diff = NULL;

    ASSERT(row < buff->nlines);

    if(buff->in_undo == FALSE)
    {
        diff = diff_new(DIFF_SUB_ROW, 0, row);
        diff->cut = vmlbuf_get_line(buff, row, dstring_new());
        diff->ins = ln;
        diff_add(buff, diff);
    }

    vmlbuf_put_line(buff, row, ln);

    return(diff);
}

/**********************************************************************************************/
