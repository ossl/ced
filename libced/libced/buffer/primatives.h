#ifndef _PRIMATIVES_H
#define _PRIMATIVES_H

#include <libced/tools/dstring.h>
#include "buffer.h"
#include "diffs.h"

dstring *prim_read(buffer *buff, long row, dstring *lbuf);
dstring *prim_write(buffer *buff, long row, dstring *lbuf);

inline DIFF *prim_ins_text  (buffer *buff, dstring *lbuf, long x, long row, dstring *ins);
inline DIFF *prim_del_text  (buffer *buff, dstring *lbuf, long x, long row, long amount);
inline DIFF *prim_sub_text  (buffer *buff, dstring *lbuf, long x, long row, long amount, dstring *ins);
inline DIFF *prim_ins_space (buffer *buff, dstring *lbuf, long x, long row, long amount);
inline DIFF *prim_split_row (buffer *buff, long x, long row);
inline DIFF *prim_join_row  (buffer *buff, long row);
inline DIFF *prim_ins_row   (buffer *buff, long row, dstring *ln);
inline DIFF *prim_del_row   (buffer *buff, long row);
inline DIFF *prim_sub_row   (buffer *buff, long row, dstring *ln);

#endif /* _PRIMATIVES_H */
