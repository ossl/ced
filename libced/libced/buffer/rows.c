/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Row level functions.  The are buffer operations that effect changes to
 *    the buffer one row at a time, such as inserting, deleting and substituting
 *    text.
 *
 *  Todo
 *    Finish off and tidy up.
 *
 *  Changes since 1.2.12
 *
 *  Created On
 *    29/MAY/2003
 *
 *  Last Updated
 *    29/MAY/2003
 *
 */

/**********************************************************************************************/

#define DEBUG
#define DEBUG_ASSERT

/**********************************************************************************************/

#include <libced/tools/debug.h>
#include <libced/tools/dstring.h>
#include "primatives.h"
#include "buffer.h"

/**********************************************************************************************
 * Replace the remainder of the line with the given text.
 **********************************************************************************************/

void buff_insert_row(buffer *buff, long row, dstring *str)
{
    buff_pad_to_row(buff, row - 1);
    prim_ins_row(buff, row, str);
}

/**********************************************************************************************
 * Delete the specified row if it exists.
 **********************************************************************************************/
 
void buff_delete_row(buffer *buff, long row)
{
    if(row < buff->nlines)
    {
        prim_del_row(buff, row);
    }
}

/**********************************************************************************************
 * Substitute the specified row with the text given, if the row exists.
 **********************************************************************************************/

void buff_subs_row(buffer *buff, long row, dstring *str)
{
    if(row < buff->nlines)
    {
        prim_sub_row(buff, row, str);
    }
}

/**********************************************************************************************
 * Pad the buffer out to the specified row.
 **********************************************************************************************/

void buff_pad_to_row(buffer *buff, long row)
{
    dstring *ldum;

    if(row < buff->nlines)
    {
        ldum = dstring_new();

        while(buff->nlines < row)
        {
            prim_ins_row(buff, row, ldum);
        }

        dstring_free(ldum);
    }
}

/**********************************************************************************************
 * Get the length of the line at the given row.
 **********************************************************************************************/

long buff_get_line_length(buffer *buff, long row)
{
    dstring lbuf;

    prim_read(buff, row, &lbuf);

    return(dstring_get_length(&lbuf));
}

/**********************************************************************************************
 * Delete the newline at the specified row.  This effectively joins the next row to the end
 * of the specified row.
 **********************************************************************************************/

void buff_delete_newline(buffer *buff, long row)
{
    if(row < buff->nlines - 1)
    {
        prim_join_row(buff, row);
    }
}

/**********************************************************************************************/
