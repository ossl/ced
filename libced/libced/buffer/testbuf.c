#include "buffer.h"

int main()
{
    int i;
    buffer *buff;
    dstring *ln = dstring_new();
    clip *cl;

    printf("balls..\n");

    cl = clip_new();
    clip_addline(cl, dstring_new_with_string("z1zz"));
    clip_addline(cl, dstring_new_with_string("z2zz"));
    clip_addline(cl, dstring_new_with_string("z3zz"));

    dstring_reset(ln); dstring_insert_string(ln, "0		123456789");

    buff = buff_new("fred.txt");
    buff_open(buff, OF_OPEN | OF_USE_SWAP);
    buff_z_insert_text(buff, 0, 0, ln);

    dstring_reset(ln); dstring_insert_string(ln, "0       123456789");

    for(i=0; i < 10; i++)
    {
        buff_z_insert_text(buff, 0, i + 1, ln);
    }

    buff_z_insert_rectangle(buff, 20, 0, cl);
//    buff_z_delete_rectangle(buff, 22, 0, 1, 3);
//    buff_z_delete_block(buff, 0, 0, 0, 1);
//    buff_z_sub_rectangle(buff, 10, 2, 2, 8, cl);

    buff_z_sub_block(buff, 8, 6, 8, 6, cl);


//    buff_z_delete_block(buff, 8, 6, 9, 6);

//    buff_block_delete(buff, 8, 6, 9, 100, 0);


    buff_save(buff);

    printf("===0<========1=========2=========3=========4=========5========>60\n");
    printf("===012345678901234567890123456789012345678901234567890123456789\n");

    for(i=0; i < buff->nlines; i++)
    {
        buff_getline(buff, i+1, ln);

        dstring_expand_tabs(ln, 4, '.');

        printf("%d:>%s$\n", i % 10, dstring_get_string(ln));
    }

    printf("================================================================\n");
    printf("nlines: %d\n", buff->nlines);
}
