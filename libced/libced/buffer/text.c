/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Text level functions.  These are buffer operations that effect text on
 *    a single row at a time only within the buffer.
 *
 *  Todo
 *    Finish off and tidy up.
 *
 *  Changes since 1.2.12
 *
 *  Created On
 *    29/MAY/2003
 *
 *  Last Updated
 *    29/MAY/2003
 *
 */

/**********************************************************************************************/

#define DEBUG
#define DEBUG_ASSERT

/**********************************************************************************************/

#include <libced/tools/debug.h>
#include <libced/tools/dstring.h>
#include "primatives.h"
#include "buffer.h"

/**********************************************************************************************/

static void insert(buffer *buff, long col, long row, dstring *str, uint ins_nl);

/**********************************************************************************************
 * Insert at the specified coordinate.  If the line does not exist, the buffer is
 * padded out with blank rows, and then the line inserted at the end.
 **********************************************************************************************/

static void insert(buffer *buff, long col, long row, dstring *str, uint ins_nl)
{
    long xpos;
    long len;
    short ins;
    LCDATA lcd;
    dstring *lbuf;

    // Firstly, create our line buffer, and then determine whether we are
    // padding out to and inserting a newline, or just modifying an
    // existing line.

    lbuf = dstring_new();

    if((ins = (buff->nlines == 0 || row >= buff->nlines)))
    {
        buff_pad_to_row(buff, row - 1);

        // If we are inserting at column zero, then we can just insert a
        // new line containing the string, otherwise, we just insert a
        // blank line for now.

        (col == 0)
            ? prim_ins_row(buff, row, str)
            : prim_ins_row(buff, row, NULL);
    }
    else
    {
        prim_read(buff, row, lbuf);
    }

    // If we are modifying the line, or inserting beyond the beginning of
    // the line, we need to first find the actual x position of the insertion
    // point, make sure we pad out to it, and then insert the string.

    if(!ins || col > 0)
    {
        dstring_get_column_data(lbuf, &lcd, col, buff->tabstops);

        xpos = lcd.col_diff + lcd.xpos;
        len  = lcd.col_diff + dstring_get_length(str);

        prim_ins_space(buff, lbuf, lcd.xpos, row, lcd.col_diff);
        prim_ins_space(buff, lbuf, len, row, xpos - len);
        prim_ins_text(buff, lbuf, xpos, row, str);
        prim_write(buff, row, lbuf);
    }
    else
    {
        xpos = col;
    }

    // Insert a newline.  This will insert a line after the inserted text,
    // if any.  We only bother using the row split primative if we are
    // inbetween text, otherwise we just use a more simple row insert.

    if(ins_nl)
    {
        xpos += dstring_get_length(str);
        len   = dstring_get_length(lbuf);

        (xpos == 0 || xpos == len)
            ? prim_ins_row(buff, row + (xpos == len) ? 1 : 0, NULL)
            : prim_split_row(buff, xpos, row);
    }

    dstring_free(lbuf);
}

/**********************************************************************************************
 * Insert text at the speficied coordinate.
 **********************************************************************************************/

void buff_insert_text(buffer *buff, long col, long row, dstring *str)
{
    insert(buff, col, row, str, 0);
}

/**********************************************************************************************
 * Insert text at the speficied coordinate, and then split the remainder of the line onto
 * the next line.
 **********************************************************************************************/

void buff_insert_text_nl(buffer *buff, long col, long row, dstring *str)
{
    insert(buff, col, row, str, 1);
}

/**********************************************************************************************
 * Split the line at the specified coordinates onto the following line.
 **********************************************************************************************/

void buff_insert_newline(buffer *buff, long col, long row)
{
    insert(buff, col, row, NULL, 1);
}

/**********************************************************************************************
 * Insert padding
 **********************************************************************************************/

void buff_pad(buffer *buff, long col, long row)
{
    insert(buff, col, row, NULL, 0);
}

/**********************************************************************************************
 * Substitute the amount of text at the specified coordinate with the string given.  If
 * NULL is passed as the string, then the text is replaced with nothing, i.e. the text
 * is just deleted.
 **********************************************************************************************/

void buff_subs_text(buffer *buff, long col, long row, long amount, dstring *subs)
{
    LCDATA lcd;
    dstring *lbuf;
    long len;
    long x1;
    long x2;

    // First we make sure a valid row has been passed in, and a valid amount.
    // An amount of -1 means the remainder of the line.

    if(row < buff->nlines && (amount > 0 || amount == -1))
    {
        lbuf = prim_read(buff, row, dstring_new());
        len = dstring_get_length(lbuf);

        // Find the x position of the column.  We only bother continuing if the
        // x position is within the boundries of the line.

        dstring_get_column_data(lbuf, &lcd, col, buff->tabstops);

        if(lcd.xpos < len)
        {
            x1 = lcd.xpos + lcd.col_diff;

            // If the amount is -1, then we delete all of the text from the given
            // x position, otherwise we delete the amount specified.

            if(amount == -1)
            {
                x2 = len;
            }
            else
            {
                dstring_get_column_data(lbuf, &lcd, col + amount, buff->tabstops);

                x2 = min(lcd.xpos + lcd.col_diff, len); 
            }

            prim_ins_space(buff, lbuf, lcd.xpos, row, lcd.col_diff);

            // If we are substituting the text for nothing in its place, then we
            // call the more simple delete primative rather than the substitution
            // primative.

            (subs == NULL)
                ? prim_del_text(buff, lbuf, x1, row, x2 - x1)
                : prim_sub_text(buff, lbuf, x1, row, x2 - x1, subs);
        }

        prim_write(buff, row, lbuf);
    }
}

/**********************************************************************************************
 * Delete an amount of text from the given coordinate.
 **********************************************************************************************/

void buff_delete_text(buffer *buff, long col, long row, long amount)
{
    buff_subs_text(buff, col, row, amount, NULL);
}

/**********************************************************************************************
 * Delete the remainder of the line from the given coordinate.
 **********************************************************************************************/

void buff_truncate_line(buffer *buff, long col, long row)
{
    buff_delete_text(buff, col, row, -1);
}

/**********************************************************************************************
 * Substitute the remainder of the specified line from the given column with the given text.
 **********************************************************************************************/

void buff_subs_remainder(buffer *buff, long col, long row, dstring *subs)
{
    buff_subs_text(buff, col, row, -1, subs);
}

