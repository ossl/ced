/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2001, 2002
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Virtual canvas, provides a view and a means of manipulating the buffer.
 *    The text in the canvas has an underlying structure called a buffer.
 *    The canvas structure contains function pointers to display routines
 *    which are set by the display itself.  This enables different frontends
 *    to be used without changing any of the backend.  When a particular
 *    canvas function is called, the appropriate canvas frontend functions
 *    via there pointers, unless the pointer is set to NULL.
 *
 *  Todo
 *    - Rethink "lncache" stuff
 *
 *  Changes since 1.2.7
 *
 *  02/11/2002
 *    (MAS) Put canvas substition output messages into one function.
 *    (MAS) Added method to get selection for a particular line.
 *    (MAS) Changed canv_get_selection_coords to be a public method.
 *    (MAS) Region object nlines property now updated properly.
 *
 *  Last Updated
 *    2nd January 2002
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>

#include <libced/cmdsubsys/cmdsubsys.h>
#include <libced/Deprecated/clip.h>
#include <libced/buffer/buffer.h>
#include <libced/utils/utils.h>
#include <libced/sys/sys.h>

#include <libced/tools/search.h>
#include <libced/tools/callbacks.h>
#include <libced/tools/dstring.h>

#include "canvas.h"

#define DEFAULT_BUFFER  "PRIMARY"
#define USE_LNCACHE

/* common calculations */

#define CANV_TABSTOPS(x) buff_get_tabstops(x->buff)
#define CANV_ROWS(x) (max(x->od_rows, 1) - 1)
#define CANV_COLS(x) (max(x->od_cols, 1) - 1)
#define LAST_LNUM(x) (max(buff_get_nlines(x->buff), 1) - 1)
#define LAST_YOFF(x) (LAST_LNUM(x) - min(CANV_ROWS(x), LAST_LNUM(x)))

#define CANV_MAX_COL 999999

static void canv_link_added_notify         (canvas *canv);
static void canv_link_deleted_notify       (canvas *canv);
static void canv_attribs_changed_notify    (canvas *canv);
static void canv_buffer_renamed_notify     (canvas *canv);
static void canv_buffer_destroyed_notify   (canvas *canv);
static void canv_cursor_moved_notify       (canvas *canv);
static void canv_lines_changed_notify      (canvas *canv, ulong y, ulong n);
static void canv_lines_deleted_notify      (canvas *canv, ulong y, ulong n);
static void canv_lines_inserted_notify     (canvas *canv, ulong y, ulong n);
static void canv_set_xpos_real             (canvas *canv, uint n);
static void canv_set_ypos_real             (canvas *canv, ulong n);
static void canv_set_col_real              (canvas *canv, uint n, int flag);
static void canv_ensure_region             (canvas *canv);
static void canv_reformat_lines            (canvas *canv, ulong y, ulong n);

/* canvas block matching routines */

static void canv_clear_matched_blocks      (canvas *canv);
static void canv_display_matched_blocks    (canvas *canv);
static void canv_match_current_block       (canvas *canv);

/* utility functions */

static void canv_subs_show_found           (canvas *canv, ulong found);

/* canvas line cache routines */

extern lncache *lncache_new                (canvas *canv, uint init_size);
extern void     lncache_free               (lncache *lnc);
extern void     lncache_lines_inserted     (lncache *lnc, ulong lnum, ulong n);
extern void     lncache_lines_deleted      (lncache *lnc, ulong lnum, ulong n);
extern void     lncache_lines_changed      (lncache *lnc, ulong lnum, ulong n);
extern void     lncache_reformat_lines     (lncache *lnc, ulong lnum, ulong n);
extern void     lncache_set_yoffset        (lncache *lnc, ulong ny);
extern void     lncache_set_size           (lncache *lnc, uint newsize);
extern void     lncache_free               (lncache *lnc);
extern void     lncache_force_update       (lncache *lnc);
extern uint     lncache_get_line           (lncache *lnc, ulong lnum, dstring *ln);
extern uint     lncache_get_line_raw       (lncache *lnc, ulong lnum, dstring *ln);
extern uint     lncache_get_line_formatted (lncache *lnc, ulong lnum, dstring *ln);

int canv_set_filetype(canvas *canv, FILE_TYPE ftype)
{
    buff_set_filetype(canv->buff, ftype);

    canv->func_attribs_changed(FRONTEND);

    return(1);
}

int canv_define_label(canvas *canv, char *name)
{
    ulong *coord;
    coord = calloc(2, sizeof(ulong));
    coord[0] = canv->col;
    coord[1] = canv->ypos;

    sh_set(canv->labels, name, (void*)coord);

    return(1);
}

int canv_goto_label(canvas *canv, char *name)
{
    ulong *coord;

    if((coord = sh_get(canv->labels, name))) {
       if(coord[1] > canv_max_ypos(canv)) {
           canv->func_message(FRONTEND, "Label out of range.", 0);
       } else {
           canv_set_col_row(canv, coord[0], coord[1]);
       }
    } else {
        canv->func_message(FRONTEND, "No such label.", 0);
    }

    return(1);
}

void canv_ensure_region(canvas *canv)
{
    dstring *lbuf;
    uint len;

    if(!canv->region_defined) {
        lbuf = dstring_new();
        canv_line_get(canv, canv->ypos, lbuf);
        len = dstring_length(lbuf);

        region_set_start_coords(canv->reg, canv->col, canv->ypos);
        region_set_end_coords(canv->reg, len+1, canv->ypos);
        region_set_type(canv->reg, RT_NORMAL);

        dstring_free(lbuf);
    } else {
        region_set_end_coords(canv->reg, canv->col, canv->ypos);
    }
}

void canv_get_selection_at_line(canvas *canv, ulong lnum, uint *x, uint *w)
{
    dstring *lbuf;

    lbuf = dstring_new();

    canv_ensure_region(canv);
    canv_line_get(canv, lnum, lbuf);

    dstring_free(lbuf);

    region_get_line_geometry(canv->reg, lnum, x, w, dstring_length(lbuf));
}

void canv_get_selection_coords(canvas *canv, uint *x1, ulong *y1, uint *x2, ulong *y2)
{
    ulong tmp_y2;
    ulong nlines;
    dstring *lbuf;

//region_set_nlines(canv->reg, canv_get_nlines(canv));
    region_get_top_coords(canv->reg, x1, y1);
    region_get_bottom_coords(canv->reg, x2, &tmp_y2);

	if(region_get_type(canv->reg) == RT_NORMAL) {
        nlines = canv_get_nlines(canv);
        lbuf = dstring_new();

        if( tmp_y2 > LAST_LNUM(canv)) {
            tmp_y2 = LAST_LNUM(canv);

            if(x2) {
                canv_line_get(canv, tmp_y2, lbuf);
                *x2 = dstring_length(lbuf);
            }
        } else {
            if(x2) {
                canv_line_get(canv, tmp_y2, lbuf);

                if( *x2 > dstring_length(lbuf)) {
                    *x2 = dstring_length(lbuf);

                    if(tmp_y2 < LAST_LNUM(canv)) {
                        (*x2)++;
                    }
                }
            }
        }

        dstring_free(lbuf);
    }

    if(y2) *y2 = tmp_y2;
}

int canv_set_tab_stops(canvas *canv, uint n)
{
    if(n < 1 || n > 99) {
        canv_od_message(canv, "Value out of range.", 0);
        return(0);
    }

    buff_set_tabstops(canv->buff, n);
    lncache_force_update(canv->lnc);
    canv->func_redraw_all(FRONTEND);
    canv_set_xpos(canv, canv->xpos);

    return(1);
}

int canv_append_buffers(canvas *canv, char *dest, char *src)
{
    char buf[1024];
    clip *cl_dest;
    clip *cl_src;
    dstring *ln;
    ulong i;

    if(!(src && strlen(src) > 0)) src = DEFAULT_BUFFER;
    if(!(dest && strlen(dest) > 0)) dest = DEFAULT_BUFFER;

    cl_dest = clip_new();
    cl_src  = clip_new();

    get_paste_buffer_ffname(buf, src, 1024);

    if(clip_load(cl_src, buf)) {
        get_paste_buffer_ffname(buf, dest, 1024);

        if(clip_load(cl_dest, buf)) {
            ln = cl_dest->lines[cl_dest->used-1];
            dstring_to_right(ln);
            dstring_insert(ln, cl_src->lines[0]);

            for(i=1; i < cl_src->used; i++) {
                clip_addline(cl_dest, cl_src->lines[i]);
            }

            clip_save(cl_dest, buf);
        } else {
            sprintf(buf, "Couldn't load buffer %s\n", dest);
            canv->func_message(FRONTEND, buf, 0); 
        }
    } else {
        sprintf(buf, "Couldn't load buffer %s\n", src);
        canv->func_message(FRONTEND, buf, 0);
    }

    clip_free(cl_dest);
    clip_free(cl_src);

    return(1);
}

int canv_show_keydef(canvas *canv, char *keyname)
{
    return(0);
}

int canv_is_read_only(canvas *canv)
{
    if(buff_is_ro(canv->buff)) {
        canv->func_message(FRONTEND, "Canvas is read only.", 1);
        return(1);
    }

    return(0);
}

int canv_find_init(canvas *canv, char *find, char *repl)
{
    search *srch = canv->srch;

    if(find) {
        search_reset(srch);
        search_set_find(srch, find);
    }

    if(repl) {
        search_set_replace(srch, repl);
    }

    if(!search_init(srch)) {
        canv->func_message(FRONTEND, srch->error, 1);
        return(0);
    }

    return(1);
}

int canv_find_next(canvas *canv, uint reverse, uint wrap)
{
    static int init = 0;
    static dstring *lbuf;
    search *srch;
    ulong  y;
    ulong  nlines;
    ulong  ystart;
    uint   xstart;
    uint   len;
    int    adder;
    int    loop_flag;
    int    wrap_flag;
    int    fail_flag;
    char   buf[64];

    if(!init) {
        lbuf = dstring_new();
        init = 1;
    }

    srch = canv->srch;

    if(!srch->init_ok) {
        canv->func_message(FRONTEND, "No search pattern.", 1);
        return(0);
    }

    canv->func_message(FRONTEND, "Searching..", 0);

    nlines = LAST_LNUM(canv);
    ystart = canv_get_ypos(canv);
    xstart = canv_get_col(canv);
    adder  = (reverse) ? -1 : 1;
    loop_flag = 0;
    wrap_flag = 0;
    fail_flag = 0;

    y = ystart;

    if(reverse && xstart == 0) {
        loop_flag = 1;

        if(y == 0) {
            if(wrap) {
                y = nlines;
                wrap_flag = 1;
            }
        } else {
            y--;
        }
    }

    xstart += adder;

    for( ; ; ) {
        canv_line_get_raw(canv, y, lbuf);

        len = dstring_length(lbuf);

        if(loop_flag) {
            (reverse) ? (xstart = len)
                      : (xstart = 0);
        }

        search_set_line(srch, lbuf);
        search_set_start_pos(srch, xstart);
        search_set_end_pos(srch, (reverse) ? 0 : len);

        switch(search_find(srch, reverse)) {
            case SEARCH_FOUND:
                canv_set_ypos(canv, y);
                canv_set_xpos(canv, srch->fnd_start);

                sprintf(buf, "Found at: (%u, %lu)", canv->col + 1, y + 1);

                if(wrap_flag) {
                    strcat(buf, " [Search Wrapped]");
                }

                canv->func_message(FRONTEND, buf, 0);

                return(1);

                break;

            case SEARCH_NOT_FOUND:
                if(y == ystart && wrap_flag) {
                    fail_flag = 1;
                }
                else if(reverse && y == 0) {
                    y = max(nlines, ystart);
                    (wrap) ? (wrap_flag = 1) : (fail_flag = 1);
                }
                else if(!reverse && y >= nlines && !wrap_flag) {
                    y = 0;
                    (wrap) ? (wrap_flag = 1) : (fail_flag = 1);
                }
                else {
                    y+=adder;
                }

                break;

                case SEARCH_NOT_READY:
                    canv->func_message(FRONTEND, "Search subsystem init error (oops)", 1);
                    return(0);
                    break;
        }

        if(fail_flag) {
            switch(reverse) {
                case 0  : sprintf(buf, "No match: /%s/", srch->srch); break;
                default : sprintf(buf, "No match: ?%s?", srch->srch);
            }

            canv->func_message(FRONTEND, buf, 0);

            return(0);
        }

        loop_flag = 1;
    }

    return(1);
}

static void canv_subs_show_found(canvas *canv, ulong found)
{
    char   buf[64];

    switch(found) {
        case 0  : sprintf(buf, "No match: /%s/", canv->srch->srch); break;
        default : sprintf(buf, "Replaced %lu occurances.", found);
    }

    canv->func_message(FRONTEND, buf, 0);
}

int canv_substitute_line(canvas *canv)
{
    uint   xstart;
    uint   xend;
    ulong  ystart;
    ulong  yend;
    int    type;

    if(!canv->srch->init_ok) {
        canv->func_message(FRONTEND, "No search pattern.", 1);
        return(0);
    }

    canv->func_message(FRONTEND, "Doing replace..", 0);
    canv_ensure_region(canv);
	canv_get_selection_coords(canv, &xstart, &ystart, &xend, &yend);

    switch(region_get_type(canv->reg)) {
        case RT_RECTANGULAR : type = BUFFER_SUBS_RECTANGLE; break;
        default             : type = BUFFER_SUBS_LINEAR;
    }

    canv_subs_show_found(canv, buff_substitute(canv->buff, xstart, ystart, xend, yend, canv->srch, type));
    canv_undefine_region(canv);

    return(0);
}

int canv_substitute_once(canvas *canv)
{
    uint   xstart;
    uint   xend;
    ulong  ystart;
    ulong  yend;
    int    type;

    if(!canv->srch->init_ok) {
        canv->func_message(FRONTEND, "No search pattern.", 1);
        return(0);
    }

    canv_ensure_region(canv);
	canv_get_selection_coords(canv, &xstart, &ystart, &xend, &yend);

    // BUG ?? MAS - CHECK - FIXME

    type = BUFFER_SUBS_ONCE;

    switch(region_get_type(canv->reg)) {
        case RT_RECTANGULAR : type |= BUFFER_SUBS_RECTANGLE; break;
        default             : type |= BUFFER_SUBS_LINEAR;
    }

    canv_subs_show_found(canv, buff_substitute(canv->buff, xstart, ystart, xend, yend, canv->srch, type));
    canv_undefine_region(canv);

    return(0);
}

int canv_substitute_global(canvas *canv)
{
    if(!canv->srch->init_ok) {
        canv->func_message(FRONTEND, "No search pattern.", 1);
        return(0);
    }

    canv->func_message(FRONTEND, "Doing global replace..", 0);

    canv_subs_show_found(canv,
        buff_substitute(canv->buff, canv->xpos, canv->ypos, 0, 0, canv->srch, BUFFER_SUBS_ALL)
    );

    return(0);
}

/*
 * Pastes a clip to the canvas at the specified position.  The region type
 * is used as the type of insertion.  If it is rectangular, then each line
 * of text is merely inserted into the x position of each line in the canvas.
 * If it is of normal type, the lines are inserted as is, complete with newlines.
 */

int canv_paste_region(canvas *canv, regtype rt, char *param)
{
    clip    *cl;         /* clip containing paste data */
    char    fname[1024]; /* filename of clip */

    if(canv_is_read_only(canv))
    {
        return(0);
    }

    if(!(param && strlen(param) > 0))
    {
        param = DEFAULT_BUFFER;
    }

    if(!strcmp(param, DEFAULT_BUFFER))
    {
        canv->func_get_selection(FRONTEND);
    }

    cl = clip_new();

    get_paste_buffer_ffname(fname, param, 1024);

    if(clip_load(cl, fname))
    {
        (rt == RT_NORMAL)
            ? buff_block_insert(canv->buff, canv->col, canv->ypos, cl, 1)
            : buff_rect_insert(canv->buff, canv->col, canv->ypos, cl, 1);
    }
    else
    {
        canv->func_message(FRONTEND, "No such paste buffer.", 1);
    }

    clip_free(cl);

    return(1);
}

/*
 * Copies the selected region and stores it in its named buffer.  The flag
 * and parameters are the flag which indicates a rectangular copy, and
 * the name of the buffer itself respectively.  If no flag is specified,
 * it is a normal flowing region.  If no parameter is specified, the
 * default buffer is used.
 */

int canv_copy_region(canvas *canv, regtype rt, char *param)
{
    ulong   sy;          /* start line of region */
    ulong   ey;          /* end line of region */
    ulong   i;           /* current line number */
    uint    col;         /* line column position */
    uint    col_x1;      /* screen x1 column start position */
    uint    col_x2;      /* screen x2 column end position */
    uint    real_x1;     /* real x1 position in string */
    uint    real_x2;     /* real x2 position in string */
    int     diff_l;      /* padding onto left of copied string */
    int     diff_r;      /* padding onto left of copied string */
    uint    w;           /* width of region for a particular dstring */
    uint    x1, x2;      /* x positions of region end and start points */
    dstring *ln;         /* working line during get */
    dstring *lbuf;       /* region of line, passed to clip and duplicated */
    clip    *cl;         /* clip to copy to */
    char    fname[1024]; /* file name of clip */

    canv_ensure_region(canv);

    /* get the clip to use */

    if(!(param && strlen(param) > 0)) param = DEFAULT_BUFFER;

    region_set_type(canv->reg, rt);

	canv_get_selection_coords(canv, &x1, &sy, &x2, &ey);

    real_x1 = 0;
    real_x2 = 0;

    if(!(sy == ey && x1 == x2)) {
        if(!strcmp(param, DEFAULT_BUFFER)) {
            canv->func_claim_selection(FRONTEND);
        }

        cl = clip_new();

        /* initialise line buffers, reset clip, get region info */

        ln = dstring_new();
        lbuf = dstring_new();
        rt = region_get_type(canv->reg);

        clip_reset(cl);

        /* perform copy */

        for(i=sy; i<=ey; i++)
        {
            canv_line_get_raw(canv, i, ln);

            region_get_line_geometry(canv->reg, i, &col, &w, dstring_length_cols(ln, CANV_TABSTOPS(canv)));

            col_x1 = get_actual_column_position(ln, col, CANV_TABSTOPS(canv), 1);
            col_x2 = get_actual_column_position(ln, (col + w), CANV_TABSTOPS(canv), 0);

            real_x1 = map_column_to_x(ln, col_x1, CANV_TABSTOPS(canv));
            real_x2 = map_column_to_x(ln, col_x2, CANV_TABSTOPS(canv));

            dstring_define_region_at(ln, real_x1);
            dstring_goto_x(ln, real_x2);
            dstring_region_copy(ln, lbuf);
 
            if(rt == RT_RECTANGULAR)
            {
                diff_l = col_x1 - col;
                diff_r = (col + w) - col_x2;

                dstring_goto_x(lbuf, (real_x2 - real_x1) + diff_r);
                dstring_pad(lbuf, ' ');
                dstring_to_left(lbuf);
                dstring_insert_padding(lbuf, diff_l);
            }

            clip_addline(cl, lbuf);    
        }

        /*
         * Add any neccessary newlines to linear selections.  This occurs when
         * the user has highlighted the implied newline character at the end
         * of a line.
         */

        if(rt == RT_NORMAL) {
            if(real_x2 > dstring_length(ln)) {
                dstring_reset(lbuf);
                clip_addline(cl, lbuf);
            }
        }

        /*
         * Free up working lines, get the paste buffer name and output the
         * clip data.  Free the clip, the copy is then complete.
         */

        dstring_free(ln);
        dstring_free(lbuf);

        get_paste_buffer_ffname(fname, param, 1024);

        clip_save(cl, fname);
        clip_free(cl);
    }

    /*
     * If a region is defined, undefine it and tell the front-end
     * about it.
     */

    canv_undefine_region(canv);

    return(1);
}

/*
 * Cuts the selected region and stores it in its named buffer.  The flag
 * and parameters are the flag which indicates a rectangular cut, and
 * the name of the buffer itself respectively.  If no flag is specified,
 * it is a normal flowing region.  If no parameter is specified, the
 * default buffer is used.
 */

int canv_cut_region(canvas *canv, regtype rt, char *param)
{
    ulong   w, h;
    ulong   y1, y2;
    uint    x1, x2;

    canv_ensure_region(canv);

    if(canv_is_read_only(canv)) {
        return(0);
    }

    if(!(param && strlen(param) > 0)) {
        param = DEFAULT_BUFFER;
    }

    canv_get_selection_coords(canv, &x1, &y1, &x2, &y2);
    canv_copy_region(canv, rt, param);

    w = x2 - x1;
    h = y2 - y1; 

    if(!(h == 0 && w == 0)) {
        if(rt == RT_NORMAL) {
            buff_block_delete(canv->buff, x1, y1, x2, y2, 1);
        } else {
            buff_rect_delete(canv->buff, x1, y1, w, h + 1, 1);
        }

        canv_set_col_real(canv, x1, 0);
        canv_set_ypos_real(canv, y1);
        canv_cursor_moved_notify(canv);
    }

    return(1);
}

int canv_case(canvas *canv, CASE_CHANGE flag)
{
    dstring *lbuf;
    dstring *lrep;
    region  *reg;
    uint    len;
    uint    col;
    ulong   ys;
    ulong   ye;
    ulong   y;

    canv_ensure_region(canv);
    canv_get_selection_coords(canv, NULL, &ys, NULL, &ye);

    lbuf = dstring_new();
    lrep = dstring_new();
    reg  = canv->reg;

    for(y = ys; y <= ye; y++)
    {
        canv_line_get_raw(canv, y, lbuf);

        region_get_line_geometry(reg, y, &col, &len, dstring_length_cols(lbuf, CANV_TABSTOPS(canv)));

        dstring_define_region_at_col(lbuf, col, CANV_TABSTOPS(canv), DSTRING_TAB_END);
        dstring_goto_col(lbuf, col + len, CANV_TABSTOPS(canv), DSTRING_TAB_START);

        switch(flag)
        {
            case CANV_CASE_FLIP  : dstring_region_invert_case(lbuf); break;
            case CANV_CASE_UPPER : dstring_region_to_upper(lbuf); break;
            case CANV_CASE_LOWER : dstring_region_to_lower(lbuf); break;
        }

        dstring_region_copy(lbuf, lrep);

        buff_text_subs(canv->buff, col, y, len, lrep, 1);
    }

    dstring_free(lbuf);
    dstring_free(lrep);

    canv_undefine_region(canv);

    return(1);
}

int canv_strip(canvas *canv)
{
    return(buff_strip(canv->buff));
}

int canv_untab(canvas *canv)
{
    dstring *lbuf;
    dstring *lrep;
    region  *reg;
    uint    len;
    uint    col;
    ulong   ys;
    ulong   ye;
    ulong   y;

    canv_ensure_region(canv);

    lbuf = dstring_new();
    lrep = dstring_new();
    reg  = canv->reg;

    canv_get_selection_coords(canv, NULL, &ys, NULL, &ye);

    for(y = ys; y <= ye; y++)
    {
        canv_line_get_raw(canv, y, lbuf);

        region_get_line_geometry(reg, y, &col, &len, dstring_length_cols(lbuf, CANV_TABSTOPS(canv)));

        dstring_define_region_at_col(lbuf, CANV_TABSTOPS(canv), col, DSTRING_TAB_END);
        dstring_goto_col(lbuf, col + len, CANV_TABSTOPS(canv), DSTRING_TAB_START);
        dstring_region_expand_tabs(lbuf, CANV_TABSTOPS(canv), ' ');
        dstring_region_copy(lbuf, lrep);

        buff_text_subs(canv->buff, col, y, len, lrep, 1);
    }

    dstring_free(lbuf);
    dstring_free(lrep);

    canv_undefine_region(canv);

    return(1);
}

int canv_exec_region(canvas *canv, char *prog, int no_cut)
{
    regtype rt;
    FILE    *app;
    clip    *src;
    region  *reg;
    char    *bufline;
    char    buf[1024];
    char    pbo[1024];
    char    pbe[1024];
    uint    i;
    uint    x;
    ulong   y;

    canv_ensure_region(canv);

    reg = canv->reg;
    rt = region_get_type(reg);

    get_paste_buffer_ffname(pbo, "BangOut", 1024);
    get_paste_buffer_ffname(pbe, "BangErr", 1024);

    sprintf(buf, "%s 1>%s 2>%s", prog, pbo, pbe);

    app = popen(buf, "w");

    get_paste_buffer_ffname(buf, "BangIn", 1024);

    if(app) {
        if(no_cut) {
            canv_copy_region(canv, rt, "BangIn");
        } else {
            canv_cut_region(canv, rt, "BangIn");
        }

        src = clip_new();

        if(clip_load(src, buf)) {
            for(i=0;i<src->used;i++) {         
                bufline = dstring_get_string(src->lines[i]);
                fprintf(app, "%s", bufline);

                if(i+1<src->used) {
                    fputc(10, app);
                }

                free(bufline);
            }

            pclose(app);

            region_get_top_coords(canv->reg, &x, &y);

            canv_set_col_real(canv, x, 0);
            canv_set_ypos_real(canv, y);
            canv_cursor_moved_notify(canv);
            canv_paste_region(canv, rt, "BangOut");

            get_paste_buffer_ffname(buf, "BangErr", 1024);

            clip_reset(src);

            if(clip_load(src, buf)) {
                for(i=0; i < src->used; i++) {         
                    canv_od_message_ln(canv, src->lines[i], 0);
                }
            }
        } else {
            canv->func_message(FRONTEND, "Couldn't get input paste buffer !", 1);
        }

        clip_free(src);
    } else {
        canv->func_message(FRONTEND, "Couldn't get app output !", 1);
    }

    return(1);
}

int canv_tab_right(canvas *canv)
{
    uint col;
    uint ts;

    ts  = CANV_TABSTOPS(canv);
    col = canv->col;
    col = col - (col % ts) + ts;

    canv_set_col(canv, col, 0);

    return(1);
}

int canv_tab_left(canvas *canv)
{
    uint col;
    uint ts;

    ts  = CANV_TABSTOPS(canv);
    col = canv->col;

    (col > ts)
        ? (col = col + (col % ts) - ts)
        : (col = 0);

    canv_set_col(canv, col, 0);

    return(1);
}

/*
 * Insert a newline at the current position.  If there is any text at the cursor
 * position and beyond, then that text is moved onto the newline created.
 * The y position is incremented and the x position set to the first column.
 */

int canv_insert_newline(canvas *canv)
{
    if(canv_is_read_only(canv)) {
        return(0);
    }

    buff_dstring_split(canv->buff, canv->col, canv->ypos, 1);

    canv_set_ypos(canv, canv->ypos + 1);
    canv_set_xpos(canv, 0);

    return(1);
}

/*
 * Move the current position down by one row.  If the current y position
 * is already at the maximum number of lines then nothing is done.  Note,
 * maximum lines does not mean the last line in the file.
 */

int canv_arrow_down(canvas *canv)
{
    ulong n;

    n = canv_max_ypos(canv);

    if(canv->ypos < n) {
        canv_set_ypos(canv, canv->ypos + 1);
        return(1);
    }

    return(0);
}

/*
 * Move the current position up by one row.  If the current
 * y position is already at zero, then nothing is done.
 */

int canv_arrow_up(canvas *canv)
{
    if(canv->ypos > 0) {
        canv_set_ypos(canv, canv->ypos-1);
        return(1);
    }

    return(0);
}

/*
 * Move the current position back by one column.  If the current
 * x position is already at zero, then nothing is done.
 */

int canv_arrow_left(canvas *canv, int wrap)
{
    dstring *lbuf;

    if(canv->xpos > 0) {
        canv_set_xpos(canv, canv->xpos-1);
    } else if(wrap) {
        if(canv_arrow_up(canv)) {
            lbuf = dstring_new();
            canv_line_get_raw(canv, canv->ypos, lbuf);
            canv_set_xpos(canv, dstring_length(lbuf));
            dstring_free(lbuf);
        } else {
            return(0);
        }
    } else {
        return(0);
    }

    return(1);
}

/*
 * Move the current position forward by one column.  If the current x position
 * is already at the maximum number of columns, then nothing is done.  Note,
 * maximum column does not mean the last character on the line. 
 */

int canv_arrow_right(canvas *canv, int wrap)
{
    dstring *lbuf;

    if(wrap) {
        lbuf = dstring_new();
        canv_line_get_raw(canv, canv->ypos, lbuf);

        if(canv->xpos < dstring_length(lbuf)) {
            canv_set_xpos(canv, canv->xpos + 1);
        } else {
            if(canv_arrow_down(canv)) {
                canv_set_xpos(canv, 0);
            } else {
                return(0);
            }
        }
        dstring_free(lbuf);
    } else if(canv->xpos < CANV_MAX_COL) {
        canv_set_xpos(canv, canv->xpos + 1);
    } else {
        return(0);
    }

    return(1);
}

int canv_cursor_to_eol(canvas *canv)
{
    dstring *lbuf = dstring_new();

    canv_line_get_raw(canv, canv->ypos, lbuf);
    canv_set_xpos(canv, dstring_length(lbuf));

    dstring_free(lbuf);

    return(1);
}

/*
 * Goto the line number specified.
 */

int canv_goto_line(canvas *canv, ULONG lnum)
{
    canv_set_ypos(canv, lnum - 1);

    return(1);
}

/*
 * Erase the character before the current position.  The remainder of
 * the line is moved back one, and the cursor position is moved back.
 */

int canv_erase_char(canvas *canv, int wrap)
{
    uint xold;

    if(canv_is_read_only(canv)) {
        return(0);
    }

    if(canv->xpos > 0 || canv->ypos > 0) {
        xold = canv->xpos;

        if(canv_arrow_left(canv, wrap)) {
            if(xold == 0) {
                canv_delete_char(canv);
            } else {
                buff_text_delete(canv->buff, canv->col, canv->ypos, 1, 1);
            }
        }
    }

    return(1);
}

/*
 * Delete the character at the current position.  If the current
 * position is ahead of the end of the current line, then the
 * proceding line is moved up and appended to the current
 * line at the current cursor position.
 */

int canv_delete_char(canvas *canv)
{
    dstring *lbuf;

    if(canv_is_read_only(canv)) {
        return(0);
    }

    lbuf = dstring_new();

    canv_line_get_raw(canv, canv->ypos, lbuf);

    if(canv->xpos >= dstring_length(lbuf)) {
        if(canv->ypos >= LAST_LNUM(canv)) {
            canv->func_message(FRONTEND, "End of file !", 1);
        } else {
            // FIXME
//            buff_pad(canv->buff, canv->col, canv->ypos, 1);
            buff_dstring_delete_newline(canv->buff, canv->ypos, 1);
        }
    } else {
        buff_text_delete(canv->buff, canv->col, canv->ypos, 1, 1);
    }

    dstring_free(lbuf);

    return(1);
}

/*
 * Insert a character at the current position.  The remainder of
 * the line is moved across to make space for the character.
 */

int canv_insert_char(canvas *canv, char ch)
{
    static dstring *ln = NULL;

    printf("canv_insert_char\n");

    if(!ln) {
        ln = dstring_new();
    }

    if(canv_is_read_only(canv)) {
        return(0);
    }

    if((ch & 96) || ch == 9) {
        dstring_reset(ln);
        dstring_insert_raw(ln, &ch, 1);
        buff_text_insert(canv->buff, canv->col, canv->ypos, ln, 1);
        canv_set_xpos(canv, canv->xpos + dstring_length(ln));
    }

    return(1);
}

/*
 * Insert a string at the current position.  The remainder of
 * the line is moved across to make space for the string. 
 */

int canv_insert_string(canvas *canv, char *st)
{
    dstring *ln;

    printf("canv_insert_string\n");

    if(canv_is_read_only(canv)) {
        return(0);
    }

    if(st) {
        ln = dstring_new();
        dstring_insert_string(ln, st);
        buff_text_insert(canv->buff, canv->col, canv->ypos, ln, 1);
        canv_set_xpos(canv, canv->xpos + dstring_length(ln));
        dstring_free(ln);
    }

    return(1);
}  

/*
 * Insert a line string at the current position.
 */

int canv_insert_ln(canvas *canv, dstring *ln)
{
    printf("canv_insert_ln\n");

    if(canv_is_read_only(canv)) {
        return(0);
    }

    if(ln) {
        buff_text_insert(canv->buff, canv->col, canv->ypos, ln, 1);
        canv_set_xpos(canv, canv->xpos + dstring_length(ln));
    }

    return(1);
}  

/*
 * Define a region start point at the current position.  The end
 * point is also specified as the current cursor position.
 */

int canv_define_region(canvas *canv)
{
    region_set_coords(canv->reg, canv->col, canv->ypos, canv->col, canv->ypos);
    region_set_type(canv->reg, RT_NORMAL);

    canv->region_defined = 1;
    canv_cursor_moved_notify(canv);

    return(1);
}           

/*
 * Saves the buffer.
 */

int canv_save(canvas *canv) {
    if(canv_is_read_only(canv)) {
        return(0);
    }

    if(buff_is_named(canv_get_buffer(canv))) {
        buff_save(canv->buff);
    } else {
        canv->func_message(FRONTEND, "File is untitled, please use 'pn' to name file.", 1);
    }

    return(1);
}

int canv_disk_file_timestamp_changed(canvas *canv)
{
    return(buff_disk_file_timestamp_changed(canv->buff));
}

/*
 * Changes the current region type to the type specified.  The
 * frontend should change the region type being displayed
 * dynamically.
 */

int canv_echo_region(canvas *canv, regtype rt)
{
    region_set_type(canv->reg, rt);
    canv->func_region_echo(FRONTEND);

    return(1);
}

/*
 * Aborts the current region.
 */

int canv_undefine_region(canvas *canv)
{
    if( canv->region_defined == 1) {
        canv->region_defined = 0;
        canv->func_region_changed(FRONTEND);
    }

    return(1);
}

/*
 * Undoes the last event.
 */

int canv_undo(canvas *canv)
{
    event  *ev;
    uint   x;
    ulong  y;

    if(canv_is_read_only(canv)) {
        return(0);
    }

    if((ev = undo_undo(canv->buff->undo_list))) {
        changelist_get_start_coords(ev->chl, &x, &y);
        canv_set_col(canv, x, CANV_TABSTOPS(canv));
        canv_set_ypos(canv, y);
        changelist_apply(canv->buff, ev->chl, 1);
    } else {
        canv->func_message(FRONTEND, "End of UNDO chain !", 1);
    }

    return(1);
}

/*
 * Redo what was undone.
 */

int canv_redo(canvas *canv)
{
    event  *ev;
    uint   x;
    ulong  y;

    if(canv_is_read_only(canv)) {
        return(0);
    }

    if((ev = undo_redo(canv->buff->undo_list))) {
        changelist_get_start_coords(ev->chl, &x, &y);
        canv_set_xpos(canv, x);
        canv_set_ypos(canv, y);
        changelist_apply(canv->buff, ev->chl, 0);
    } else {
        canv->func_message(FRONTEND, "End of REDO chain !", 1);
    }

    return(1);
}

int canv_set_readonly(canvas *canv, int state)
{
    buff_set_readonly(canv->buff, state);

    return(1);
}

int canv_toggle_readonly(canvas *canv)
{
    buff_set_readonly(canv->buff, !canv->buff->ro);

    return(1);
}

int canv_toggle_tabs(canvas *canv)
{
    buff_toggle_tabs(canv->buff);
    lncache_force_update(canv->lnc);

    canv->func_redraw_all(FRONTEND);

    return(1);
}

int canv_toggle_lnums(canvas *canv)
{
    canv->show_lnums = !canv->show_lnums;
    canv->func_line_numbers_toggled(FRONTEND);

    return(1);
}

int canv_toggle_highlighting(canvas *canv)
{
    canv->highlighting = !canv->highlighting;
    canv->func_highlighting_toggled(FRONTEND);

    return(1);
}

int canv_toggle_mouse_binding(canvas *canv)
{
    canv->mouse_binding = !canv->mouse_binding;
    canv->func_mouse_binding_toggled(FRONTEND);

    return(1);
}

void canv_set_yoffset(canvas *canv, ulong yoff)
{
    if(yoff != canv->od_yoffset) {
        canv->od_yoffset = yoff;
#ifdef USE_LNCACHE
        lncache_set_yoffset(canv->lnc, yoff);
#endif
        canv->func_offset_changed(FRONTEND);
    }
}

void canv_set_xoffset(canvas *canv, uint xoff)
{
    if( canv->od_xoffset != xoff) {
        canv->od_xoffset = xoff;
        canv->func_offset_changed(FRONTEND);
    }
}

/*
 * Sets the absoloute y position of the cursor.
 */

void canv_set_ypos_real(canvas *canv, ulong n)
{
    long ymin;
    long ymax;
    uint last;
    uint xadj;

    n = min(n, canv_max_ypos(canv) - 1);

    canv->ypos = n;

    last = canv->col;
    xadj = canv->xadj;

    canv_set_col_real(canv, canv->col - canv->xadj, 0);
    canv->xadj = canv->col - last + xadj;

    ymin = canv->od_yoffset;
    ymax = canv->od_yoffset + canv->od_rows - 1;

    if(n < ymin) ymin -= diff(ymin, n);
    if(n > ymax) ymin += diff(ymax, n);

    canv_set_yoffset(canv, ymin);

    if(canv->region_defined) {
        region_set_end_coords(canv->reg, canv->col, canv->ypos);
        canv->func_region_changed(FRONTEND);
    }
}

void canv_set_ypos(canvas *canv, ulong n)
{
    canv_set_ypos_real(canv, n);
    canv_cursor_moved_notify(canv);
}

/*
 * Sets the absoloute x position of the cursor.  The cursor can be set
 * beyond the number of characters in the line.  No padding is done.
 * A notify is made to the frontend informing it of the cursor movement.
 */

void canv_set_col_real(canvas *canv, uint n, int flag)
{
    int  xmin;
    int  xmax;
    dstring *ln;
    uint p;

    canv->xadj = 0;

    ln = dstring_new();
    canv_line_get_raw(canv, canv->ypos, ln);

    n = get_actual_column_position(ln, n, CANV_TABSTOPS(canv), flag);
    p = map_column_to_x(ln, n, CANV_TABSTOPS(canv));

    canv->col = n;
    canv->xpos = p;

    xmin = canv->od_xoffset;
    xmax = canv->od_xoffset + canv->od_cols - 1;
 
    if(n < xmin) xmin -= diff(xmin, n);
    if(n > xmax) xmin += diff(xmax, n);

    canv_set_xoffset(canv, xmin);

    if(canv->region_defined) {
        region_set_end_coords(canv->reg, canv->col, canv->ypos);
        canv->func_region_changed(FRONTEND);
    }

    dstring_free(ln);
}

void canv_set_col(canvas *canv, uint n, int flag){
     canv_set_col_real(canv, n, flag);
     canv_cursor_moved_notify(canv);
}

/*
 * Sets the actual x position at the current line.  The actual column
 * is then changed by being mapped appropriately, initially to take tabs
 * into account, but this could be adopted for other things..
 */

void canv_set_xpos_real(canvas *canv, uint n)
{
    int  xmin;
    int  xmax;
    char *buf;  /* copy of dstring */
    char *ptr;
    dstring *ln;   /* actual dstring */
    uint len;   /* real length of dstring */
    uint tab;   /* accumulation of tab spacing */
    uint ins;
    uint p;
//    uint i;

    tab = 0;
    ln = dstring_new();

    canv_line_get_raw(canv, canv->ypos, ln);

    buf = dstring_get_string(ln);
    len = dstring_length(ln);

    canv->xadj = 0;

    if(n < len)
    {
        buf[n] = '\0';
    }

    ptr = buf;

    while((ptr = strchr(ptr, '\t')) != NULL)
    {
        p = ptr - buf;

        if(p >= n)
        {
            break;
        }

        ins  = (CANV_TABSTOPS(canv) - ((tab + p) % CANV_TABSTOPS(canv))) - 1;
        tab += ins;

        ptr++;
    }

    p = n + tab;

    canv->xpos = n;
    canv->col = p;

    xmin = canv->od_xoffset;
    xmax = canv->od_xoffset + canv->od_cols - 1;
 
    if(p < xmin) xmin -= diff(xmin, p);
    if(p > xmax) xmin += diff(xmax, p);

    canv_set_xoffset(canv, xmin);

    if(canv->region_defined) {
        region_set_end_coords(canv->reg, canv->col, canv->ypos);
        canv->func_region_changed(FRONTEND);
    }

    free(buf);
    dstring_free(ln);
}

void canv_set_xpos(canvas *canv, uint n)
{
    canv_set_xpos_real(canv, n);
    canv_cursor_moved_notify(canv);
}

void canv_set_col_row(canvas *canv, uint col, uint row)
{
    canv_set_ypos_real(canv, row);
    canv_set_col_real(canv, col, 0);
    canv_cursor_moved_notify(canv);
}

uint canv_get_col(canvas *canv)
{
    return(canv->col);
}

uint canv_get_xpos(canvas *canv)
{
    return(canv->xpos);
}

uint canv_get_ypos(canvas *canv)
{
    return(canv->ypos);
}

int canv_line_get(canvas *canv, ulong lnum, dstring *ln)
{
#ifdef USE_LNCACHE
    if(!lncache_get_line(canv->lnc, lnum, ln)) {
//        buff_getdstring_all(canv->buff, (lnum+1), NULL, ln, NULL, -1);
        buff_getdstring_all(canv->buff, (lnum+1), NULL, ln, NULL, -1, -1);
    }
#else
//    buff_getdstring_all(canv->buff, (lnum+1), NULL, ln, NULL, -1);
    buff_getdstring_all(canv->buff, (lnum+1), NULL, ln, NULL, -1, -1);
#endif

    return(1);
}

ulong canv_max_ypos(canvas *canv)
{
    return(canv->od_rows + LAST_LNUM(canv));
}

int canv_line_get_raw(canvas *canv, ulong lnum, dstring *ln)
{
#ifdef USE_LNCACHE
    if(!lncache_get_line_raw(canv->lnc, lnum, ln)) {
        buff_getline(canv->buff, (lnum+1), ln);
    }
#else
    buff_getline(canv->buff, (lnum+1), ln);
#endif

    return(1);
}

int canv_line_get_formatted(canvas *canv, ulong lnum, dstring *ln)
{
#ifdef USE_LNCACHE
    if(!lncache_get_line_formatted(canv->lnc, lnum, ln)) {
//        buff_getdstring_all(canv->buff, (lnum+1), NULL, NULL, ln, -1);
        buff_getdstring_all(canv->buff, (lnum+1), NULL, NULL, ln, -1, -1);
    }
#else
//    buff_getdstring_all(canv->buff, (lnum+1), NULL, NULL, ln, -1);
    buff_getdstring_all(canv->buff, (lnum+1), NULL, NULL, ln, -1, -1);
#endif

    return(1);
}

void canv_set_rows(canvas *canv, uint n)
{
    canv->od_rows = n;

    if((canv->ypos - canv->od_yoffset) >= canv->od_rows) {
        canv_set_ypos(canv, canv->ypos);
    }

#ifdef USE_LNCACHE
    lncache_set_size(canv->lnc, n);
#endif
}

void canv_set_cols(canvas *canv, uint n)
{
    canv->od_cols = n;

    if((canv->col - canv->od_xoffset) >= canv->od_cols) {
        canv_set_xpos(canv, canv->xpos);
    }
}

/*
 * Create a new canvas on the buffer specified.
 */

canvas* canv_new(buffer *buff)
{
    canvas *canv;

    canv = (canvas*)calloc(1, sizeof(canvas));

    /* initialise region and clip stuff */

    canv->reg = region_new(RT_NORMAL);
    canv->region_defined = 0;
    canv->clips = sh_new(16, 1);
    canv->labels = sh_new(16, 1);
    canv->srch = search_new();

    canv->cbacks = callbacks_new("canvas");

    callbacks_add(canv->cbacks, "offset_changed"        , CALLBACK_REF(canv->func_offset_changed));
    callbacks_add(canv->cbacks, "cursor_moved"          , CALLBACK_REF(canv->func_cursor_moved));
    callbacks_add(canv->cbacks, "region_changed"        , CALLBACK_REF(canv->func_region_changed));
    callbacks_add(canv->cbacks, "region_echo"           , CALLBACK_REF(canv->func_region_echo));
    callbacks_add(canv->cbacks, "redraw_all"            , CALLBACK_REF(canv->func_redraw_all));
    callbacks_add(canv->cbacks, "get_selection"         , CALLBACK_REF(canv->func_get_selection));
    callbacks_add(canv->cbacks, "claim_selection"       , CALLBACK_REF(canv->func_claim_selection));
    callbacks_add(canv->cbacks, "sync_cursor"           , CALLBACK_REF(canv->func_sync_cursor));
    callbacks_add(canv->cbacks, "message"               , CALLBACK_REF(canv->func_message));
    callbacks_add(canv->cbacks, "link_added"            , CALLBACK_REF(canv->func_link_added));
    callbacks_add(canv->cbacks, "link_deleted"          , CALLBACK_REF(canv->func_link_deleted));
    callbacks_add(canv->cbacks, "lines_changed"         , CALLBACK_REF(canv->func_lines_changed));
    callbacks_add(canv->cbacks, "lines_inserted"        , CALLBACK_REF(canv->func_lines_inserted));
    callbacks_add(canv->cbacks, "lines_deleted"         , CALLBACK_REF(canv->func_lines_deleted));
    callbacks_add(canv->cbacks, "attribs_changed"       , CALLBACK_REF(canv->func_attribs_changed));
    callbacks_add(canv->cbacks, "buffer_renamed"        , CALLBACK_REF(canv->func_buffer_renamed));
    callbacks_add(canv->cbacks, "buffer_destroyed"      , CALLBACK_REF(canv->func_buffer_destroyed));
    callbacks_add(canv->cbacks, "return_event"          , CALLBACK_REF(canv->func_return_event));
    callbacks_add(canv->cbacks, "line_numbers_toggled"  , CALLBACK_REF(canv->func_line_numbers_toggled));
    callbacks_add(canv->cbacks, "mouse_binding_toggled" , CALLBACK_REF(canv->func_mouse_binding_toggled));
    callbacks_add(canv->cbacks, "highlighting_toggled"  , CALLBACK_REF(canv->func_highlighting_toggled));
    callbacks_add(canv->cbacks, "bring_to_front"        , CALLBACK_REF(canv->func_bring_to_front));

    /* initialise canvas cache */

    canv->buff = buff;

    /* set up callbacks */

    buff_set_callback(buff, "lines_changed"    , canv_lines_changed_notify);
    buff_set_callback(buff, "lines_deleted"    , canv_lines_deleted_notify);
    buff_set_callback(buff, "lines_inserted"   , canv_lines_inserted_notify);
    buff_set_callback(buff, "link_added"       , canv_link_added_notify);
    buff_set_callback(buff, "link_deleted"     , canv_link_deleted_notify);
    buff_set_callback(buff, "attribs_changed"  , canv_attribs_changed_notify);
    buff_set_callback(buff, "message"          , canv_od_message);
    buff_set_callback(buff, "buffer_renamed"   , canv_buffer_renamed_notify);
    buff_set_callback(buff, "buffer_destroyed" , canv_buffer_destroyed_notify);

    buff_add_link(buff, (void*)canv);

    buff_enable_frontend(buff);

    /* set some miscellaneous stuff */

    canv->show_lnums = CED_CONFIG.lnums;
    canv->mouse_binding = CED_CONFIG.mousebind;
    canv->highlighting = CED_CONFIG.highlight;

    region_set_nlines(canv->reg, buff_get_nlines(buff));

#ifdef USE_LNCACHE
    canv->lnc = lncache_new(canv, 0);
    lncache_force_update(canv->lnc);
#endif

    canv->func_redraw_all(FRONTEND);

    return(canv);
}

int canv_destroy(canvas *canv)
{
#ifdef USE_LNCACHE
    lncache_free(canv->lnc);
#endif

    region_free(canv->reg);
    search_free(canv->srch);
    sh_free(canv->labels);
    sh_free(canv->clips);
    buff_del_link(canv->buff, (void*)canv);
    callbacks_free(canv->cbacks);

    free(canv);

    return(1);
}

/*
 * Are we a carbon copy ?  To find out, we see where we are
 * in the buffers callback list.  If we are the first entry,
 * then we are not.
 */

uint canv_is_copy(canvas *canv)
{
    int n;

    n = buff_get_link_number(canv->buff, (void*)canv);

    return((n==0)?0:1);
}

/*
 * Some callback wrappers for the buffer to use as a passthrough
 * on the way to the frontend.
 */

void canv_cursor_moved_notify(canvas *canv)
{
    canv->func_cursor_moved(FRONTEND);
    canv_match_current_block(canv);
}

void canv_link_added_notify(canvas *canv)
{
    canv->func_link_added(FRONTEND);
}

void canv_link_deleted_notify(canvas *canv)
{
    canv->func_link_deleted(FRONTEND);
}

void canv_attribs_changed_notify(canvas *canv)
{
    canv->func_attribs_changed(FRONTEND);
}

void canv_buffer_renamed_notify(canvas *canv)
{
    canv->func_buffer_renamed(FRONTEND);
    canv_match_current_block(canv);
}

void canv_buffer_destroyed_notify(canvas *canv)
{
    canv->func_buffer_destroyed(FRONTEND);
}

void canv_lines_changed_notify(canvas *canv, ulong y, ulong n)
{
 #ifdef USE_LNCACHE
    lncache_lines_changed(canv->lnc, y, n);
#endif

    canv->func_lines_changed(FRONTEND, y, n);

    canv_match_current_block(canv);
}

void canv_lines_deleted_notify(canvas *canv, ulong y, ulong n)
{
    canv_clear_matched_blocks(canv);

#ifdef USE_LNCACHE
    lncache_lines_deleted(canv->lnc, y, n);
#endif

    region_set_nlines(canv->reg, buff_get_nlines(canv->buff));

    canv->func_lines_deleted(FRONTEND, y, n);
    canv_match_current_block(canv);
}

void canv_lines_inserted_notify(canvas *canv, ulong y, ulong n)
{
    canv_clear_matched_blocks(canv);

#ifdef USE_LNCACHE
    lncache_lines_inserted(canv->lnc, y, n);
#endif

    region_set_nlines(canv->reg, buff_get_nlines(canv->buff));

    canv->func_lines_inserted(FRONTEND, y, n);
    canv_match_current_block(canv);
}

void canv_od_message_ln(canvas *canv, dstring *ln, int beep)
{
    char *msg;

    msg = dstring_get_string(ln);

    if(msg) {
        canv->func_message(FRONTEND, msg, beep);
        free(msg);
    }
}

void canv_od_message(canvas *canv, char *text, int beep)
{
    canv->func_message(FRONTEND, text, beep);
}

int canv_replace_at_line(canvas *canv, ulong lnum, dstring *ln)
{
    buff_dstring_replace(canv->buff, lnum, ln, 1);

    return(1);
} 

int canv_replace_line(canvas *canv, dstring *ln)
{
    buff_dstring_replace(canv->buff, canv->ypos, ln, 1);

    return(1);
} 

int canv_delete_at_line(canvas *canv, ulong lnum)
{
    buff_dstring_delete(canv->buff, lnum, 1);

    return(1);
} 

int canv_delete_line(canvas *canv)
{
    buff_dstring_delete(canv->buff, canv->ypos, 1);

    return(1);
} 

int canv_pad_set_position(canvas *canv, uint x, ulong y)
{
    y = min(y, LAST_LNUM(canv));

    canv->od_xoffset = x;
    canv_set_yoffset(canv, y);
    canv->func_offset_changed(FRONTEND);
    canv->func_sync_cursor(FRONTEND);

    return(1);
}

ulong canv_get_yoffset(canvas *canv)
{
    return(canv->od_yoffset);
}

// PAD MOVEMENT

int canv_pad_move_vertical(canvas *canv, long n)
{
    ulong ypos;
    ulong yoff;

    if(n) {
        ypos = canv_get_ypos(canv) - canv->od_yoffset;

        if(n < 0 && abs(n) > canv->od_yoffset) {
            yoff = 0;
        } else {
            yoff = canv->od_yoffset + n;
            yoff = min(yoff, LAST_LNUM(canv));
        }

        canv_set_yoffset(canv, yoff);
        canv_set_ypos(canv, ypos + canv->od_yoffset);
    }

    return(1);
}

int canv_pad_move_horizontal(canvas *canv, long n)
{
    uint xpos;
    uint td;

    if(n) {
        xpos = canv_get_col(canv) - canv->od_xoffset;

        if(n < 0 && abs(n) > canv->od_xoffset) {
            canv->od_xoffset = 0;
        } else {
            canv->od_xoffset += n;
            canv->od_xoffset = min(canv->od_xoffset, CANV_MAX_COL - canv->od_cols);
        }

        td = (n>0) ? 1 : 0;
        canv->func_offset_changed(FRONTEND);
        canv_set_col(canv, xpos + canv->od_xoffset, td);
    }

    return(1);
}

int canv_pad_move_page(canvas *canv, double d)
{
    long amount = canv->od_rows * d;
    canv_pad_move_vertical(canv, amount);
    return(1);
}

int canv_pad_to_top(canvas *canv)
{
    ulong ycursor;

    ycursor = canv->ypos - canv->od_yoffset;

    canv_set_ypos(canv, ycursor);
    canv_set_yoffset(canv, 0);
  
    return(1);
}

int canv_pad_to_bottom(canvas *canv)
{
    ulong ycursor;

    ycursor = canv->ypos - canv->od_yoffset;

    canv_set_ypos(canv, LAST_YOFF(canv) + ycursor);
    canv_set_yoffset(canv, LAST_YOFF(canv));

    return(1);
}

// VIEWPORT MOVEMENT

int canv_window_move_vertical(canvas *canv, long n)
{
    ulong ypos;

    ypos = canv->ypos;
 
    canv_pad_move_vertical(canv, n);

    ypos = max(ypos, canv->od_yoffset);
    ypos = min(ypos, canv->od_yoffset + canv->od_rows - 1);

    canv_set_ypos(canv, ypos);

    return(1);
}

int canv_window_move_horizontal(canvas *canv, long n)
{
    uint xpos;
    uint td;

    xpos = canv->col;

    canv_pad_move_horizontal(canv, n);

    xpos = max(xpos, canv->od_xoffset);
    xpos = min(xpos, canv->od_xoffset + canv->od_cols - 1);

    td = (n>0) ? 1 : 0;
    canv_set_col(canv, xpos, td);

    return(1);
}

int canv_window_move_page(canvas *canv, double d)
{
    long amount = canv->od_rows * d;
    canv_window_move_vertical(canv, amount);
    return(1);
}

int canv_window_to_top(canvas *canv)
{
    ulong yoff_old;

    yoff_old = canv->od_yoffset;

    if(yoff_old > 0) {
        if(canv->ypos >= canv->od_rows) {
            canv_set_ypos(canv, canv->od_rows - 1);
        }

        canv_set_yoffset(canv, 0);
    }

    return(1);
}

int canv_window_to_bottom(canvas *canv)
{
    ulong yoff_old;
    ulong yoff_bottom;

    yoff_old = canv->od_yoffset;
    yoff_bottom = LAST_YOFF(canv);

    if(canv->ypos < yoff_bottom) {
        canv_set_ypos(canv, yoff_bottom);
    }

    canv_set_yoffset(canv, yoff_bottom);

    return(1);
}

// CURSOR MOVEMENT

int canv_cursor_move_horizontal(canvas *canv, int n)
{
    uint col;
    uint td;

    if(n) {
        col = canv_get_col(canv);

        if(n < 0 && abs(n) > col) {
            n = -col;
        }

        td = (n>0) ? 1 : 0;

        canv_set_col(canv, col + n, td);
    }

    return(1);
}

int canv_cursor_move_vertical(canvas *canv, long n)
{
    uint ypos;

    if(n) {
        ypos = canv_get_ypos(canv);

        if(n < 0 && abs(n) > ypos) {
            n = -ypos;
        }

        canv_set_ypos(canv, ypos + n);
    }

    return(1);
}

int canv_cursor_page(canvas *canv, double d)
{
    int amount = canv->od_rows * d;
    canv_cursor_move_vertical(canv, amount);
    return(1);
}

int canv_cursor_to_top(canvas *canv)
{
    canv_set_ypos(canv, 0);
    return(1);
}

int canv_cursor_to_bottom(canvas *canv)
{
    canv_set_ypos(canv, LAST_LNUM(canv));
    return(1);
}

int canv_cursor_to_left(canvas *canv)
{
    canv_set_xpos(canv, 0);
    return(1);
}

int canv_cursor_to_right(canvas *canv)
{
    dstring *lbuf = dstring_new();

    canv_line_get_raw(canv, canv->ypos, lbuf);
    canv_set_xpos(canv, dstring_length(lbuf));

    dstring_free(lbuf);

    return(1);
}

//int canv_cursor_to_window_top(canvas *canv)
//{
//    canv_set_ypos(canv, canv->od_yoffset);
//    return(1);
//}  
//
//int canv_cursor_to_window_bottom(canvas *canv)
//{
//    canv_set_ypos(canv, canv->od_yoffset + max(canv->od_rows, 1) - 1);
//    return(1);
//}
//
//int canv_cursor_to_window_left(canvas *canv)
//{
//    canv_set_col(canv, 0, 0);
//    return(1);
//}
//
//int canv_cursor_to_window_right(canvas *canv)
//{
//    canv_set_col(canv, canv->od_xoffset + max(canv->od_cols, 1) - 1, 0);
//    return(1);
//}

int canv_cursor_to_window_top(canvas *canv)
{
    canv_set_ypos(canv, canv->od_yoffset);
    return(1);
}  

int canv_cursor_to_window_bottom(canvas *canv)
{
    ulong ypos;

    ypos = max(canv->od_yoffset + canv->od_rows - 1, 0);
    ypos = min(ypos, max(canv->buff->nlines, 1) - 1);

    canv_set_ypos(canv, ypos);

    return(1);
}

int canv_cursor_to_window_border(canvas *canv, WINDOW_BORDER wb)
{
    switch(wb) {
        case CANV_WB_LEFT   : canv_set_col(canv, 0, 0); break;
        case CANV_WB_RIGHT  : canv_set_col(canv, canv->od_xoffset + canv->od_cols - 1, 0); break;
        case CANV_WB_TOP    : canv_set_ypos(canv, canv->od_yoffset); break;
        case CANV_WB_BOTTOM : canv_set_ypos(canv, canv->od_yoffset + canv->od_rows - 1); break;
    }

    return(1);
}

int canv_is_dirty(canvas *canv)
{
    return(buff_is_dirty(canv->buff));
}

int canv_is_rdonly(canvas *canv)
{
    return(buff_is_ro(canv->buff));
}

uint canv_get_xoffset(canvas *canv)
{
    return(canv->od_xoffset);
}

uint canv_get_cols(canvas *canv)
{
    return(canv->od_cols);
}

ulong canv_get_rows(canvas *canv)
{
    return(canv->od_rows);
}

int canv_get_show_dstring_numbers(canvas *canv)
{
    return(canv->show_lnums);
}

int canv_get_mouse_binding(canvas *canv)
{
    return(canv->mouse_binding);
}

int canv_get_highlighting(canvas *canv)
{
    return(canv->highlighting);
}

ulong canv_get_nlines(canvas *canv)
{
    return(LAST_LNUM(canv) + 1);
}

buffer *canv_get_buffer(canvas *canv)
{
    return(canv->buff);
}

region *canv_get_region(canvas *canv)
{
    return(canv->reg);
}

int canv_region_is_defined(canvas *canv)
{
    return(canv->region_defined);
}

void canv_set_callback(canvas *canv, char *name, void (*func)())
{
    callbacks_set(canv->cbacks, name, func);
}

void canv_reformat_lines(canvas *canv, ulong y, ulong n)
{
#ifdef USE_LNCACHE
    lncache_reformat_lines(canv->lnc, y, n);
#endif

    canv->func_lines_changed(FRONTEND, y, n);
}

/*************************************************************************************/

void canv_clear_matched_blocks(canvas *canv)
{
    if( canv->b_lnum >= 0) {
        canv->b_bnum = -1;
        canv_reformat_lines(canv, canv->b_lnum, 1);
        canv->b_lnum = -1;
    }

    if( canv->m_lnum >= 0) {
        canv->m_bnum = -1;
        canv_reformat_lines(canv, canv->m_lnum, 1);
        canv->m_lnum = -1;
    }
}

void canv_display_matched_blocks(canvas *canv)
{
    if(canv->m_lnum >= 0 && canv->b_lnum >= 0) {
        canv_reformat_lines(canv, canv->m_lnum, 1);
        canv_reformat_lines(canv, canv->b_lnum, 1);
    }
}

void canv_match_current_block(canvas *canv)
{
    LINE_ATTRIBS *la;
    lncache *lnc;
    int m;
    int i;
    int j;
    int col = 0;
    int bs = 0;
    int bp = 0;

    canv_clear_matched_blocks(canv);

    if((col = canv->col) == 0) {
        return;
    }

    lnc = canv->lnc;
    la = lnc->lne[canv->ypos - canv->od_yoffset].la;

    for(m=0; m < la->bpos.items; m++) {
        if(col == la->bpos.data[m]) {
            bs = la->blocks.data[m];
            bp = la->bpairs.data[m];
            break;
        }
    }

    if(bs == 1) {
        col++;

        for(i=canv->ypos - canv->od_yoffset; i < canv->od_rows; i++) {
            la = lnc->lne[i].la;

            for(j=0; j < la->blocks.items; j++) {
                if(la->bpairs.data[j] == bp && la->bpos.data[j] >= col) {
                    if((bs += la->blocks.data[j]) == 0) {
                        canv->b_lnum = canv->od_yoffset + i;
                        canv->b_bnum = j;
                        canv->m_lnum = canv->ypos;
                        canv->m_bnum = m;
                        canv_display_matched_blocks(canv);

                        return;
                    }
                }
            }
    
            col = 0;
        }
    } else if(bs == -1) {
        col--;

        for(i=canv->ypos - canv->od_yoffset; i >= 0; i--) {
            la = lnc->lne[i].la;

            for(j=la->blocks.items-1; j >= 0; j--) {
                if(la->bpairs.data[j] == bp && la->bpos.data[j] <= col) {
                    if((bs += la->blocks.data[j]) == 0) {
                        canv->b_lnum = canv->od_yoffset + i;
                        canv->b_bnum = j;
                        canv->m_lnum = canv->ypos;
                        canv->m_bnum = m;
                        canv_display_matched_blocks(canv);

                        return;
                    }
                }
            }
    
            col = 9999; // FIXME
        }
    }
}

