#ifndef _CANVAS_H
#define _CANVAS_H

#include <libced/common.h>
#include <libced/buffer/buffer.h>
#include <libced/hash/strhash.h>
#include <libced/Deprecated/clip.h>

#include <libced/tools/region.h>
#include <libced/tools/dstring.h>
#include <libced/tools/search.h>
#include <libced/tools/callbacks.h>

#define FRONTEND canv->struct_ptr

typedef enum   _case_change     CASE_CHANGE;    /* case change */
typedef enum   _window_border   WINDOW_BORDER;  /* window border */
typedef struct _canvas          canvas;         /* holds info for the canvas */
typedef struct _frontend_event  events;         /* event from viewport */
typedef struct _ln_entry        lnentry;        /* line cache entry */
typedef struct _ln_cache        lncache;        /* line cache structure */

enum _case_change
{
    CANV_CASE_FLIP,
    CANV_CASE_UPPER,
    CANV_CASE_LOWER
};

enum _window_border
{
    CANV_WB_TOP,
    CANV_WB_BOTTOM,
    CANV_WB_LEFT,
    CANV_WB_RIGHT
};

/*
 * line cache and line entry stuff.  basically stores all the lines
 * for the current viewable area but also stores data about them for
 * things like block matching.  Needs work because it is no longer just
 * a cache and the line entry attributes stuff is a little hacked in :-(
 * FIXME.
 */

struct _ln_cache
{
    uint    alloced;
    uint    size;
    ulong   yoffset;
    canvas  *canv;
    lnentry *lne;
};

struct _ln_entry
{
    LINE_ATTRIBS *la;
    dstring *ln;
    dstring *ln_f;
    dstring *ln_raw;
};

/* Events passed back from the front end */

struct _frontend_event
{
  int   motion;      /* NOT YET USED */
  int   xmpos;       /* NOT YET USED - x position of motion */
  int   ympos;       /* NOT YET USED - y position of motion */
  int   state;       /* keyboard modifier (shift/control/etc) */
  int   upstroke;    /* event occured on the upstroke of the key/button */
  int   button;      /* which mouse button, if any, was used */
  int   key;         /* the key being pressed (not modifier), ie a, b, C, D */
  char  printable;   /* a printable character, if any */
};

struct _canvas
{
  /* cursor stuff */

  uint     ic_col;           /* input cursor - current column */
  uint     ic_row;           /* input cursor - current row */
  uint     col;              /* actual column position with tabs expanded */
  uint     xadj;             /* x position adjustment, tab compensation */
  uint     xpos;             /* current x position */
  ulong    ypos;             /* current y position */

  /* offsets and display info */

  uint     od_xoffset;       /* output device current x offset */
  ulong    od_yoffset;       /* output device current y offset */
  uint     od_cols;          /* output device number of columns */
  ulong    od_rows;          /* output device number of rows */

  /* misc */

  edmode   edit_mode;        /* current edit mode, not implemented yet */
  optype   lastop;           /* type of last operation, for undo/redo */

  /* helper objects */

  buffer   *buff;            /* buffer related to this canvas */
  dstring  *last_srch;       /* previous search string */
  region   *reg;             /* main region of selection */
  strhash  *clips;           /* hash of cut/paste buffers */
  strhash  *labels;          /* positions in canvas */
  search   *srch;            /* search object */
  lncache  *lnc;             /* line cache object */

  /* flags */

  int      region_defined;   /* region defined flag */
  int      show_tabs;        /* show tabs */
  int      show_lnums;       /* show line numbers flag */
  int      mouse_binding;    /* mouse binding on or off */
  int      highlighting;     /* do syntax highlighting */

  /* blocks to match - bit messy at the moment */

  int      b_bnum;           /* start block number */
  long     b_lnum;           /* start block line number */
  int      m_bnum;           /* end block number */
  long     m_lnum;           /* end block line number */

  /* callback stuff */

  void     *struct_ptr;      /* pointer to the frontend struct */

  callbacks *cbacks;

  CALLBACK_DEF (func_offset_changed)        (void *);
  CALLBACK_DEF (func_cursor_moved)          (void *);
  CALLBACK_DEF (func_region_changed)        (void *);
  CALLBACK_DEF (func_region_echo)           (void *);
  CALLBACK_DEF (func_redraw_all)            (void *);
  CALLBACK_DEF (func_get_selection)         (void *);
  CALLBACK_DEF (func_claim_selection)       (void *);
  CALLBACK_DEF (func_sync_cursor)           (void *);
  CALLBACK_DEF (func_message)               (void *, char *, int);
  CALLBACK_DEF (func_link_added)            (void *);
  CALLBACK_DEF (func_link_deleted)          (void *);
  CALLBACK_DEF (func_attribs_changed)       (void *);
  CALLBACK_DEF (func_buffer_renamed)        (void *);
  CALLBACK_DEF (func_buffer_destroyed)      (void *);
  CALLBACK_DEF (func_lines_changed)         (void *, ulong, ulong);
  CALLBACK_DEF (func_lines_deleted)         (void *, ulong, ulong);
  CALLBACK_DEF (func_lines_inserted)        (void *, ulong, ulong);

  // Move to session ?

  CALLBACK_DEF (func_line_numbers_toggled)  (void *);
  CALLBACK_DEF (func_mouse_binding_toggled) (void *);
  CALLBACK_DEF (func_highlighting_toggled)  (void *);
  CALLBACK_DEF (func_bring_to_front)        (void *);

  // Obselete ?

  CALLBACK_DEF (func_return_event)          (void *, ulong);
};

/* creation and destruction */

canvas  *canv_new          (buffer* buff);
int      canv_destroy      (canvas* canv);

/* cursor move by arrow */

int canv_arrow_down              (canvas *canv);
int canv_arrow_up                (canvas *canv);
int canv_arrow_left              (canvas *canv, int wrap);
int canv_arrow_right             (canvas *canv, int wrap);

/* pad movement */

int canv_pad_move_vertical(canvas *canv, long n);
int canv_pad_move_horizontal(canvas *canv, long n);
int canv_pad_move_page(canvas *canv, double d);
int canv_pad_to_top(canvas *canv);
int canv_pad_to_bottom(canvas *canv);

/* window viewport movement */

int canv_window_move_vertical(canvas *canv, long n);
int canv_window_move_horizontal(canvas *canv, long n);
int canv_window_move_page(canvas *canv, double d);
int canv_window_to_top(canvas *canv);
int canv_window_to_bottom(canvas *canv);

/* cursor movement */

int canv_cursor_move_horizontal(canvas *canv, int n);
int canv_cursor_move_vertical(canvas *canv, long n);
int canv_cursor_page(canvas *canv, double d);
int canv_cursor_to_top(canvas *canv);
int canv_cursor_to_bottom(canvas *canv);
int canv_cursor_to_left(canvas *canv);
int canv_cursor_to_right(canvas *canv);
int canv_cursor_to_window_border(canvas *canv, WINDOW_BORDER wb);

/* miscellaneous */

int canv_tab_left              (canvas *canv);
int canv_tab_right             (canvas *canv);
int canv_set_tab_stops         (canvas *canv, uint n);
int canv_insert_newline        (canvas *canv);
int canv_goto_line             (canvas *canv, ULONG lnum);
int canv_erase_char            (canvas *canv, int wrap);
int canv_delete_char           (canvas *canv);
int canv_insert_char           (canvas *canv, char ch);
int canv_insert_string         (canvas *canv, char *st);
int canv_insert_ln             (canvas *canv, dstring *ln);
int canv_define_region         (canvas *canv);
int canv_save                  (canvas *canv);
int canv_undefine_region       (canvas *canv);
int canv_append_buffers        (canvas *canv, char *dest, char *src);
int canv_copy_region           (canvas *canv, regtype rt, char *param);
int canv_cut_region            (canvas *canv, regtype rt, char *param);
int canv_paste_region          (canvas *canv, regtype rt, char *param);
int canv_undo                  (canvas *canv);
int canv_redo                  (canvas *canv);
int canv_search                (canvas *canv, char *p1, char *p2, uint replace_all);
int canv_toggle_tabs           (canvas *canv);
int canv_toggle_lnums          (canvas *canv);
int canv_toggle_highlighting   (canvas *canv);
int canv_toggle_mouse_binding  (canvas *canv);
int canv_toggle_readonly       (canvas *canv);
int canv_case                  (canvas *canv, CASE_CHANGE flag);
int canv_pad_set_position      (canvas *canv, uint x, ulong y);
int canv_echo_region           (canvas *canv, regtype rt);
int canv_to_window_border      (canvas *canv, WINDOW_BORDER wb);
int canv_define_label          (canvas *canv, char *name);
int canv_goto_label            (canvas *canv, char *name);
int canv_replace_at_line       (canvas *canv, ulong lnum, dstring *ln);
int canv_replace_line          (canvas *canv, dstring *ln);
int canv_delete_at_line        (canvas *canv, ulong lnum);
int canv_delete_line           (canvas *canv);
int canv_set_readonly          (canvas *canv, int state);
int canv_untab                 (canvas *canv);
int canv_strip                 (canvas *canv);

/* canvas find replace functions */

int canv_find_init(canvas *canv, char *find, char *repl);
int canv_find_next(canvas *canv, uint reverse, uint wrap);

int canv_substitute_once(canvas *canv);
int canv_substitute_line(canvas *canv);
int canv_substitute_global(canvas *canv);

void canv_od_goto_cmdline(canvas *canv);
void canv_od_carbon_copy(canvas *canv);

void canv_set_buffer(canvas *canv, buffer *buff);

int canv_exec_region(canvas *canv, char *prog, int no_cut);

/* text access functions */

int  canv_text_get         (canvas *canv, uint x, ulong y, uint n, dstring *ln);
int  canv_text_insert      (canvas *canv, uint x, ulong y, dstring *ln, int flags);
int  canv_text_replace     (canvas *canv, uint x, ulong y, uint n, dstring *ln, int flags);
int  canv_text_delete      (canvas *canv, uint x, ulong y, uint n, int flags);
int  canv_text_erase       (canvas *canv, uint x, ulong y, uint n, int flags);
int  canv_text_newline     (canvas *canv, uint x, ulong y, int flags);

/* line access functions - want to try and eliminate these */

int  canv_line_get_raw       (canvas *canv, ulong lnum, dstring *ln);
int  canv_line_get_formatted (canvas *canv, ulong lnum, dstring *ln);
int  canv_line_get           (canvas *canv, ulong lnum, dstring *ln);
int  canv_line_insert        (canvas *canv, ulong lnum, dstring *ln);
int  canv_line_delete        (canvas *canv, ulong lnum);

/* output cursor position access functions */

void  canv_inc_ypos         (canvas *canv);
void  canv_dec_ypos         (canvas *canv);
void  canv_inc_xpos         (canvas *canv);
void  canv_dec_xpos         (canvas *canv);
void  canv_set_ypos         (canvas *canv, ulong n);
void  canv_set_xpos         (canvas *canv, uint n);
void  canv_set_col          (canvas *canv, uint n, int flag);
void  canv_set_col_row      (canvas *canv, uint col, uint row);
void  canv_set_yoffset      (canvas *canv, ulong yoff);
void  canv_set_rows         (canvas *canv, uint n);
void  canv_set_cols         (canvas *canv, uint n);
uint  canv_get_xpos         (canvas *canv);
uint  canv_get_ypos         (canvas *canv);
uint  canv_get_col          (canvas *canv);
ulong canv_max_ypos         (canvas *canv);

/* miscellaneous access functions */

int  canv_pad              (canvas *canv, uint x, ulong y, int flags);

void canv_od_message(canvas *canv, char *message, int beep);
void canv_od_message_ln(canvas *canv, dstring *ln, int beep);

int     canv_set_filetype(canvas *canv, FILE_TYPE ftype);
uint    canv_is_copy(canvas *canv);
int     canv_is_dirty(canvas *canv);

int       canv_disk_file_timestamp_changed(canvas *canv);
int       canv_is_rdonly(canvas *canv);
int       canv_region_is_defined(canvas *canv);
uint      canv_get_xoffset(canvas *canv);
uint      canv_get_cols(canvas *canv);
ulong     canv_get_rows(canvas *canv);
ulong     canv_get_nlines(canvas *canv);
int       canv_get_show_line_numbers(canvas *canv);
int       canv_get_mouse_binding(canvas *canv);
int       canv_get_highlighting(canvas *canv);
buffer   *canv_get_buffer(canvas *canv);
region   *canv_get_region(canvas *canv);

void canv_set_callback(canvas *canv, char *name, void (*func)());

void canv_get_selection_at_line(canvas *canv, ulong lnum, uint *x, uint *w);
void canv_get_selection_coords(canvas *canv, uint *x1, ulong *y1, uint *x2, ulong *y2);

#endif /* _CANVAS_H */

