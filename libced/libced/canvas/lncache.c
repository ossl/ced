/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Provides a line cache for the canvas.
 *
 *  Last Updated
 *    13th September 2000
 *
 */

#include <stdio.h>
#include <malloc.h>
#include <libced/buffer/buffer.h>
#include <libced/tools/dstring.h>
#include "canvas.h"

//#define DEBUG

void lncache_lines_move_up   (lncache *lnc, uint y, uint n);
void lncache_lines_move_down (lncache *lnc, uint y, uint n);
uint lncache_line_in_cache   (lncache *lnc, ulong lnum);

lncache *lncache_new(canvas *canv, uint init_size)
{
    lncache *lnc;
    uint    i;

    lnc = (lncache*)calloc(1, sizeof(lncache));
    lnc->yoffset = 0;
    lnc->alloced = init_size;
    lnc->size    = init_size;
    lnc->canv    = canv;
    lnc->lne     = NULL;

    if(init_size) {
        lnc->lne = calloc(init_size, sizeof(lnentry));
    }

    for(i=0; i < init_size; i++) {
        lnc->lne[i].ln = dstring_new();
        lnc->lne[i].ln_f = dstring_new();
        lnc->lne[i].ln_raw = dstring_new();
        lnc->lne[i].la = (LINE_ATTRIBS*)calloc(1, sizeof(LINE_ATTRIBS)); //FIXME
    }

    return(lnc);
}

void lncache_free(lncache *lnc)
{
    uint i;

    if(lnc->alloced > 0) {
        for(i=0; i < lnc->alloced; i++) {
            dstring_free(lnc->lne[i].ln);
            dstring_free(lnc->lne[i].ln_f);
            dstring_free(lnc->lne[i].ln_raw);

            free(lnc->lne[i].la);
        }

        free(lnc->lne);
    }

    free(lnc);
}

void lncache_force_update(lncache *lnc)
{
    uint     i;
    ulong    yoff;
    lnentry  *lne;
    canvas   *canv;

#ifdef DEBUG
    printf("[lncache] force update\n");
#endif

    yoff = lnc->yoffset;
    canv = lnc->canv;
    lne  = lnc->lne;

    for(i=0; i < lnc->size; i++) {
        buff_getline_all(canv->buff, yoff + i + 1,
                         lne[i].ln_raw,
                         lne[i].ln,
                         lne[i].ln_f, -1, -1);

        syntax_format_line(lnc->canv->buff->lsyn, lne[i].ln_f, lne[i].la, yoff + i, -1, -1);
    }
}

void lncache_set_size(lncache *lnc, uint newsize)
{
    uint   i;

#ifdef DEBUG
    printf("[lncache] set cache size to (%u)\n", newsize);
#endif

    if(newsize < lnc->size) {
        for(i=newsize; i < lnc->size; i++) {
            dstring_reset(lnc->lne[i].ln);
            dstring_reset(lnc->lne[i].ln_f);
            dstring_reset(lnc->lne[i].ln_raw);
        }
        lnc->size = newsize;
    } else if(newsize > lnc->size) {
        if(newsize > lnc->alloced) {
            lnc->lne = realloc(lnc->lne, newsize * sizeof(lnentry));

            for(i=lnc->alloced; i < newsize; i++) {
                lnc->lne[i].ln = dstring_new();
                lnc->lne[i].ln_f = dstring_new();
                lnc->lne[i].ln_raw = dstring_new();
                lnc->lne[i].la = (LINE_ATTRIBS*)calloc(1, sizeof(LINE_ATTRIBS)); //FIXME
            }

            lnc->alloced = newsize;
        }

        for(i=lnc->size; i < newsize; i++) {
            buff_getline_all(lnc->canv->buff,
                             lnc->yoffset + i + 1,
                             lnc->lne[i].ln_raw,
                             lnc->lne[i].ln,
                             lnc->lne[i].ln_f, -1, -1);

            syntax_format_line(lnc->canv->buff->lsyn, lnc->lne[i].ln_f, lnc->lne[i].la, lnc->yoffset + i, -1, -1);
        }

        lnc->size = newsize;
    }
}

void lncache_set_yoffset(lncache *lnc, ulong ny)
{
    ulong oy = lnc->yoffset;

#ifdef DEBUG
    printf("[lncache] set y offset to (%lu)\n", ny);
#endif

    lnc->yoffset = ny;

    if(oy > ny) {
        lncache_lines_move_down(lnc, 0, oy - ny);
    } else if(oy < ny) {
        lncache_lines_move_up(lnc, 0, ny - oy);
    }
}

void lncache_lines_changed(lncache *lnc, ulong lnum, ulong n)
{
    uint   size = lnc->size;
    ulong  yoff = lnc->yoffset;
    ulong  ymin;
    ulong  ymax;
    ulong  i;
    int    hbn_1;
    int    hbn_2;

#ifdef DEBUG
    printf("[lncache] lines changed (%lu, %lu)\n", lnum, n);
#endif

    ymin = max(yoff, lnum);
    ymax = min(yoff + size, lnum + n);

    for(i=ymin; i < ymax; i++) {
        hbn_1 = (lnc->canv->b_lnum == i) ? lnc->canv->b_bnum : -1;
        hbn_2 = (lnc->canv->m_lnum == i) ? lnc->canv->m_bnum : -1;

        buff_getline_all(lnc->canv->buff, i+1,
                         lnc->lne[i-yoff].ln_raw,
                         lnc->lne[i-yoff].ln,
                         lnc->lne[i-yoff].ln_f,
                         hbn_1, hbn_2);


        syntax_format_line(lnc->canv->buff->lsyn, lnc->lne[i-yoff].ln_f, lnc->lne[i-yoff].la, i, hbn_1, hbn_2);
    }
}

void lncache_reformat_lines(lncache *lnc, ulong lnum, ulong n)
{
    uint   size = lnc->size;
    ulong  yoff = lnc->yoffset;
    ulong  ymin;
    ulong  ymax;
    ulong  i;
    int    hbn_1;
    int    hbn_2;

    ymin = max(yoff, lnum);
    ymax = min(yoff + size, lnum + n);

    for(i=ymin; i < ymax; i++) {
        hbn_1 = (lnc->canv->b_lnum == i) ? lnc->canv->b_bnum : -1;
        hbn_2 = (lnc->canv->m_lnum == i) ? lnc->canv->m_bnum : -1;

        // (didn't need this bit originally) //////

//        buff_getline_all(lnc->canv->buff, i+1,
//                         lnc->lne[i-yoff].ln_raw,
//                         lnc->lne[i-yoff].ln,
//                         lnc->lne[i-yoff].ln_f,
//                         hbn_1, hbn_2);

        dstring_reset(lnc->lne[i-yoff].ln_f);
        dstring_insert(lnc->lne[i-yoff].ln_f, lnc->lne[i-yoff].ln);

        syntax_format_line(lnc->canv->buff->lsyn, lnc->lne[i-yoff].ln_f, lnc->lne[i-yoff].la, i, hbn_1, hbn_2);
    }
}

void lncache_lines_inserted(lncache *lnc, ulong lnum, ulong n)
{
    uint   y;
    uint   size = lnc->size;
    ulong  yoff = lnc->yoffset;

#ifdef DEBUG
    printf("[lncache] lines inserted (%lu, %lu)\n", lnum, n);
#endif

    if(lnum < yoff + size) {
        y = (lnum < yoff) ? 0 : lnum - yoff;
        lncache_lines_move_down(lnc, y, n);
    }
}

void lncache_lines_deleted(lncache *lnc, ulong lnum, ulong n)
{
    uint   y;
    uint   size = lnc->size;
    ulong  yoff = lnc->yoffset;

#ifdef DEBUG
    printf("[lncache] lines deleted (%lu, %lu)\n", lnum, n);
#endif

    if(lnum < yoff + size) {
        y = (lnum < yoff) ? 0 : lnum - yoff;
        lncache_lines_move_up(lnc, y, n);
    }
}

/*
 * Move lines in the cache down (scroll up) and fill in the gaps
 * by calling the canvas line get routine.
 */

void lncache_lines_move_down(lncache *lnc, uint y, uint n)
{
    uint    i;   
    lnentry tmp;

#ifdef DEBUG
    printf("[lncache] lines move down (%u, %u)\n", y, n);
#endif

    if(n > lnc->size) {
        lncache_force_update(lnc);
    } else {
        for(i=lnc->size-1; ; i--) {
            if(i >= (y+n)) {
                tmp = lnc->lne[i];
                lnc->lne[i] = lnc->lne[i-n];
                lnc->lne[i-n] = tmp;
            } else {
                buff_getline_all(lnc->canv->buff,
                                 lnc->yoffset + i + 1,
                                 lnc->lne[i].ln_raw,
                                 lnc->lne[i].ln,
                                 lnc->lne[i].ln_f, -1, -1);

                syntax_format_line(lnc->canv->buff->lsyn, lnc->lne[i].ln_f, lnc->lne[i].la, lnc->yoffset + i, -1, -1);
            }

            if(i == y) break;
        }
    }
}

/*
 * Move lines in the cache up (scroll down) and fill in the gaps
 * by calling the canvas line get routine.
 */

void lncache_lines_move_up(lncache *lnc, uint y, uint n)
{
    uint    i;
    lnentry tmp;

#ifdef DEBUG
    printf("[lncache] lines move up (%u, %u)\n", y, n);
#endif

    if(n > lnc->size) {
        lncache_force_update(lnc);
    } else {
        for(i=y; i < lnc->size; i++) {
            if(i < lnc->size - n) {
                tmp = lnc->lne[i];
                lnc->lne[i] = lnc->lne[i+n];
                lnc->lne[i+n] = tmp;
            } else {
                buff_getline_all(lnc->canv->buff,
                                 lnc->yoffset + i + 1,
                                 lnc->lne[i].ln_raw,
                                 lnc->lne[i].ln,
                                 lnc->lne[i].ln_f, -1, -1);

                syntax_format_line(lnc->canv->buff->lsyn, lnc->lne[i].ln_f, lnc->lne[i].la, lnc->yoffset + i, -1, -1);
            }
        }
    }
}

uint lncache_get_line_formatted(lncache *lnc, ulong lnum, dstring *ln)
{
    if(lncache_line_in_cache(lnc, lnum)) {
        dstring_reset(ln);
        dstring_insert(ln, lnc->lne[lnum - lnc->yoffset].ln_f);
        return(1);
    }

    return(0);
}

uint lncache_get_line_raw(lncache *lnc, ulong lnum, dstring *ln)
{
    if(lncache_line_in_cache(lnc, lnum)) {
        dstring_reset(ln);
        dstring_insert(ln, lnc->lne[lnum - lnc->yoffset].ln_raw);
        return(1);
    }

    return(0);
}

uint lncache_get_line(lncache *lnc, ulong lnum, dstring *ln)
{
    if(lncache_line_in_cache(lnc, lnum)) {
        dstring_reset(ln);
        dstring_insert(ln, lnc->lne[lnum - lnc->yoffset].ln);
        return(1);
    }

    return(0);
}

uint lncache_line_in_cache(lncache *lnc, ulong lnum)
{
#ifdef DEBUG
    printf("[lncache] line in cache (%lu)\n", lnum);
#endif

    if(lnum >= lnc->yoffset && lnum < lnc->yoffset + lnc->size) {
        return(1);
    }

    return(0);
}

