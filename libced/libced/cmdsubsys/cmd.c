/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2001
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Handles the processing of all sessions based on some event occuring.
 *    An event is an input from the user.  This input is checked for a session
 *    mapping, which if found is executed.
 *
 *  Last Updated
 *    8th August 2001
 *
 */

#include <stdlib.h>
#include <string.h>
#include "cmdsubsys.h"

cmd *cmd_new()
{
    cmd *cs = (cmd*)calloc(1, sizeof(cmd));
    cs->instr = CMD_NONE;

    return(cs);
}

void cmd_free(cmd *cs)
{
    uint i;

    if(cs) {
        for(i=0; i < 4; i++) {
            if(cs->params[i].flag) free(cs->params[i].flag);
            if(cs->params[i].st)   free(cs->params[i].st);
            if(cs->params[i].cmds) cmdlist_free(cs->params[i].cmds);

            ptr_array_free_data(&cs->params[i].list);
        }

        free(cs);
    }
}

void cmd_copy(cmd *dest, cmd *src)
{
    int i;

    memcpy(dest, src, sizeof(cmd));

    for(i=0; i<4; i++) {
        if( dest->params[i].st) {
            dest->params[i].st = strdup(src->params[i].st);
        }

        if( dest->params[i].flag) {
            dest->params[i].flag = strdup(src->params[i].flag);
        }

        if( dest->params[i].cmds) {
            dest->params[i].cmds = cmdlist_duplicate(src->params[i].cmds);
        }

        if( dest->params[i].list.items > 0) {
            ptr_array_copy(&dest->params[i].list, &src->params[i].list);
        }
    }
}

cmd *cmd_duplicate(cmd *cs)
{
    cmd *new_cs;
    new_cs = cmd_new();
    cmd_copy(new_cs, cs);

    return(new_cs);
}

