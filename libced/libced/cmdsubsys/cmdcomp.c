/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Parses ced's command set.
 *
 *  Last Updated
 *    8th July 2001
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include <libced/tools/callbacks.h>
#include <libced/tools/stream.h>
#include <libced/tools/dstring.h>

#include <libced/utils/utils.h>

#include "cmdsubsys.h"

//#define DEBUG

ulong      CURR_LNUM;
ulong      CURR_LEVEL;
cmdsubsys *CMD_SUBSYS;

#define PARENT(x)  x->struct_ptr

typedef struct _parse_data {
    stream    *st;
    cmdlist   *cmds;
    cmd       *cs;
    uchar     c;
} PDATA;

static void cmd_parse_flag        (PDATA *pd, uint param_num);
static void cmd_parse_identifier  (PDATA *pd, uint param_num);
static void cmd_parse_list        (PDATA *pd, uint param_num);
static void cmd_parse_remain      (PDATA *pd, uint param_num);
static void cmd_parse_string      (PDATA *pd, uint param_num);
static void cmd_parse_number      (PDATA *pd, uint param_num);
static void cmd_parse_literal     (PDATA *pd, uint param_num);
static void cmd_parse_regexp      (PDATA *pd, uint parts);
static void cmd_parse_kd          (PDATA *pd);
static void cmd_parse_range       (PDATA *pd);
static int  cmd_parse_statement   (PDATA *pd);
static int  cmd_parse_stateseq    (PDATA *pd);
static int  cmd_parse_compound    (PDATA *pd);

static void cmd_parse_level_up    (PDATA *this_level, PDATA *next_level, uint param_num);
static void cmd_parse_level_down  (PDATA *this_level, PDATA *next_level, uint param_num);

static inline uchar cmd_curr_char (PDATA *pd);
static inline uchar cmd_next_char (PDATA *pd);

static void cmd_parse_error(PDATA *pd, char *errmsg)
{
    char msgbuf[1024];

    sprintf(msgbuf, "%lu: %s", CURR_LNUM, errmsg);

    CMD_SUBSYS->func_message(PARENT(CMD_SUBSYS), msgbuf, 0);
}

cmdlist *cmd_parse_stream(cmdsubsys *csys, stream *st)
{
    PDATA pd;

    CURR_LNUM  = 1;
    CURR_LEVEL = 0;
    CMD_SUBSYS = csys;

    pd.cs = NULL;
    pd.cmds = cmdlist_new();

    if(st) {
        pd.st = st;

        cmd_next_char(&pd);
        cmd_parse_stateseq(&pd);

        while(cmd_curr_char(&pd) != '\0') {
              cmd_parse_error(&pd, "expected end of input.");
              cmd_parse_stateseq(&pd);
        }
    }

    return(pd.cmds);
}

cmdlist *cmd_parse_str(cmdsubsys *csys, char *s)
{
    cmdlist *clist;

    stream *st = stream_new();
    stream_set_string(st, s);

    clist = cmd_parse_stream(csys, st);

    stream_free(st);

    return(clist);
}

cmdlist *cmd_parse_file(cmdsubsys *csys, char *ffname)
{
    cmdlist *clist;

    stream *st = stream_new();
    stream_set_file(st, ffname);

    clist = cmd_parse_stream(csys, st);

    stream_free(st);

    return(clist);
}
                         
static void cmd_parse_white(PDATA *pd)
{
    uchar c;

    while(isspace(cmd_curr_char(pd))) {
        cmd_next_char(pd);
    }

    if(cmd_curr_char(pd) == '#') {
        while((c = cmd_next_char(pd))) {
            if(c == 10 || c == 13) {
                cmd_parse_white(pd);
                break;
            }
        }
    }
}

static int cmd_parse_compound(PDATA *pd)
{
    if( cmd_curr_char(pd) == '{') {
        cmd_next_char(pd);
        cmd_parse_stateseq(pd);

        if( cmd_curr_char(pd) == '}') {
            cmd_next_char(pd);
        } else {
            cmd_parse_error(pd, "expected close brace");
        }

        return(1);
    }

    return(0);
}

static int cmd_parse_stateseq(PDATA *pd)
{
    cmd_parse_white(pd);

    if( cmd_parse_statement(pd)) {
        cmd_parse_white(pd);

        if( cmd_curr_char(pd) == ';') {
            cmd_next_char(pd);
            cmd_parse_stateseq(pd);

//            cmd_parse_white(pd);
//
//            if( cmd_curr_char(pd) != '}') {
//                cmd_parse_stateseq(pd);
//            }
        }

        cmd_parse_white(pd);

        return(1);
    }

    return(0);
}

static int cmd_parse_statement(PDATA *pd)
{
    PDATA  pdtmp;
    dstring *lbuf;
    uint   code;
    char   *buf;
    char   *msgbuf;
    char   c;

#ifdef DEBUG
    printf("[cmd] cmd_parse_statement\n");
#endif

    msgbuf = (char*)alloca(128);

    cmd_parse_white(pd);

    /*
     * If we have an open curley-brace at this point, then we
     * have a compound statement, so call the appropriate function
     * and return.
     */

    if(cmd_parse_compound(pd))
    {
        return(1);
    }

    /*
     * All commands in the command set are alpha numeric.  First
     * we extract the command, then find out what the command code
     * is, assign it to the command and parse any other parameters
     * or flags accordingly.
     */

    cmd_parse_white(pd);

    c = cmd_curr_char(pd);

    pd->cs = cmd_new();
    pd->cs->lnum = CURR_LNUM;

    if(isalpha(c)) {
        lbuf = dstring_new();

        do {
            dstring_insert_char(lbuf, c);
        } while((c = cmd_next_char(pd)) && isalpha(c));

        buf = dstring_get_string(lbuf);

        dstring_free(lbuf);

        /*
         * Find out the code from the command list.  The code is
         * assigned to the cmd structure.
         */

        if((code = (uint)sh_get(CMD_SUBSYS->commands, buf))) {
            pd->cs->instr = code;

            switch(code) {
                /*
                 * Substitution commands all take a 2 part regular
                 * expression as there only parameter.
                 */

                case SUBST:    /* Substitute within selection */
                case SUBONCE:  /* Substitute once */
                case SUBGLOB:  /* Substitute globally */
                    cmd_parse_regexp(pd, 2);
                    break;

                /* Miscellaneous */

                case KLOAD  : cmd_parse_remain(pd, 0); break;
                case KMERGE : cmd_parse_remain(pd, 0); break;
                case PROMPT : cmd_parse_remain(pd, 0); break;
                case MSG    : cmd_parse_remain(pd, 0); break;
                case ECHO   : cmd_parse_flag(pd, 0); break;
                case CASE   : cmd_parse_flag(pd, 0); break;
                case KD     : cmd_parse_kd(pd); break;
                case ES     : cmd_parse_string(pd, 0); break;
                case WC     : cmd_parse_flag(pd, 0); break;
                case PW     : cmd_parse_flag(pd, 0); break;
                case TWB    : cmd_parse_flag(pd, 0); break;
                case CD     : cmd_parse_remain(pd, 0); break;
                case CED    : cmd_parse_list(pd, 0); break;
                case FT     : cmd_parse_identifier(pd, 0); break;
                case AL     : cmd_parse_flag(pd, 0); break;
                case AR     : cmd_parse_flag(pd, 0); break;
                case EE     : cmd_parse_flag(pd, 0); break;

                /*
                 * Path name command takes two flags and then
                 * takes whatever is left as the final argument.
                 */

                case PN:
                    cmd_parse_flag(pd, 0);
                    cmd_parse_flag(pd, 1);
                    cmd_parse_remain(pd, 0);
                    break;

                /*
                 * Cut, copy and paste commands all take a flag
                 * and an identifier as a parameter.
                 */

                case XC:   /* Copy to a buffer */
                case XD:   /* Cut to a buffer */
                case XP:   /* Paste from a buffer */
                    cmd_parse_flag(pd, 0);
                    cmd_parse_identifier(pd, 0);
                    break;

                /*
                 * Appending buffers take two identifiers,
                 * the destination followed by the source.
                 */

                case XA:
                    cmd_parse_identifier(pd, 0);
                    cmd_parse_identifier(pd, 1);
                    break; 

                /*
                 * Commands that all take a single number as
                 * an argument.
                 */

                case PP:   /* Pad scroll by page */
                case PV:   /* Pad scroll virtical */
                case PH:   /* Pad scroll horizontal */
                case WP:   /* Viewport scroll by page */
                case WV:   /* Viewport scroll Virtical */
                case WH:   /* Viewport scroll Horizontal */
                case CP:   /* Cursor move by page*/
                case CV:   /* Cursor move Virtical */
                case CH:   /* Cursor move Horizontal */
                case PU:   /* Page up */
                case PD:   /* Page down */
                case TS:   /* Set tab stops */
                    cmd_parse_number(pd, 0);
                    break;

                /*
                 * Label commands.  Both take a single identifier
                 * as a parameter.
                 */

                case GL:   /* Goto label */
                case DL:   /* Define label */
                    cmd_parse_identifier(pd, 0);
                    break;

                default:   /* Do nothing */
            }
        } else {
            sprintf(msgbuf, "Unrecognised command: %s", buf);
            cmd_parse_error(pd, msgbuf);
        }

        free(buf);
    }

    /*
     * Special characters not found in the command set.  These are
     * things like coordinates, etc.
     */

    else {
        switch(c = cmd_curr_char(pd)) {
            case '!' :
                pd->cs->instr = BANG;
                cmd_next_char(pd);
                cmd_parse_flag(pd, 0);
                cmd_parse_literal(pd, 0);
                break;

            case '/' :  /* fall through */
            case '?' :
                pd->cs->instr = (c == '/') ? FWDSRCH : REVSRCH;

                cmd_parse_regexp(pd, 1);

                if( cmd_curr_char(pd) == '-') {
                    cmd_parse_flag(pd, 0);
                }

                // FIXME - parse white here ?

                c = cmd_curr_char(pd);
            
                if(c && c != ';' && c != '}') {
                    cmd_parse_level_up(pd, &pdtmp, 3);
                    cmd_parse_statement(&pdtmp);
                    cmd_parse_level_down(pd, &pdtmp, 3);
                }
            
                cmd_parse_white(pd);

                break;

            case '[' :
                cmd_next_char(pd);
                cmd_parse_range(pd);

                if( cmd_curr_char(pd) == ']') {
                    cmd_next_char(pd);
                    pd->cs->instr = POSCIW;
                }
                break;

            case '<' :
                cmd_next_char(pd);
                cmd_parse_range(pd);

                if( cmd_curr_char(pd) == '>') {
                    cmd_next_char(pd);
                    pd->cs->instr = POSTLW;
                }
                break;

            case '(' :
                cmd_next_char(pd);
                cmd_parse_range(pd);

                if( cmd_curr_char(pd) == ')') {
                    cmd_next_char(pd);
                    pd->cs->instr = POSCAC;
                }
                break;

            case '$' :
                pd->cs->instr = POSBOF;
                cmd_next_char(pd);
                break;

            default :
                if(isdigit(cmd_curr_char(pd))) {
                    pd->cs->instr = POSLN;
                    cmd_parse_number(pd, 0);
                } else {
                    sprintf(msgbuf, "unexpected symbol: '%c'", cmd_curr_char(pd));
                    cmd_parse_error(pd, msgbuf);
                    cmd_next_char(pd);
                }
        }
    }

    if(pd->cs->instr != CMD_NONE) {
        cmdlist_add(pd->cmds, pd->cs);
        cmd_parse_white(pd);

        return(1);
    } else {
        cmd_free(pd->cs);
        cmd_parse_white(pd);

        return(0);
    }
}

static void cmd_parse_number(PDATA *pd, uint param_num)
{
    dstring *lbuf;
    char    *buf;
    char    c;
    uint    dp_flag = 0;

    cmd_parse_white(pd);

    c = cmd_curr_char(pd);

    pd->cs->params[param_num].num = 0;

    if(isdigit(c) || c == '.' || c == '-' || c == '+') {
        lbuf = dstring_new();
        dstring_insert_char(lbuf, c);

        while((c = cmd_next_char(pd))) {
            if(!isdigit(c) && c != '.') break;

            if(c == '.') {
                if(dp_flag) {
                    break;
                }

                dp_flag = 1;
            }

            dstring_insert_char(lbuf, c);
        }

        buf = dstring_get_string(lbuf);

        dstring_free(lbuf);

        pd->cs->params[param_num].num = atof(buf);
        free(buf);
    }
}

static void cmd_parse_range(PDATA *pd)
{
    cmd_parse_number(pd, 0);
    cmd_parse_white(pd);

    if( cmd_curr_char(pd) == ',') {
        cmd_parse_white(pd);
        cmd_parse_number(pd, 1);
    }
}

static void cmd_parse_regexp(PDATA *pd, uint parts)
{
    dstring *lbuf;
    char   *buf;
    char   delim;
    char   c;
    uint   eflag = 0;
    uint   part = 0;

    cmd_parse_white(pd);

    c = cmd_curr_char(pd);

    if(c == '?' || c == '/') {
        lbuf = dstring_new();
        delim = c;

        for(part=0; part < parts; part++) {
            eflag = 0;

            while((c = cmd_next_char(pd))) {
                if(!eflag && c == delim) {
                    break;
                } else {
                    eflag = 0;
                }

                if(c == '\\') {
                    eflag = 1;
                }

                dstring_insert_char(lbuf, c);
            }

            if(dstring_get_length(lbuf)) {
                buf = dstring_get_string(lbuf);

                pd->cs->params[part].st = buf;
                pd->cs->params[part].st = str_decode_escapes(buf);
            }

            dstring_reset(lbuf);
        }

        dstring_free(lbuf);

        cmd_next_char(pd);
    }

    cmd_parse_white(pd);
}

static void cmd_parse_flag(PDATA *pd, uint param_num)
{
    dstring *lbuf;
    char *buf;
    char c;

    cmd_parse_white(pd);

    if(cmd_curr_char(pd) == '-') {
        lbuf = dstring_new();

        while((c = cmd_next_char(pd))) {
            if(!isalnum(c)) {
                break;
            }

            dstring_insert_char(lbuf, c);
        }

        if(dstring_get_length(lbuf)) {
            buf = dstring_get_string(lbuf);
            pd->cs->params[param_num].flag = buf;
        }

        dstring_free(lbuf);
    }
}

static void cmd_parse_list(PDATA *pd, uint param_num)
{
    dstring *lbuf;
    char *buf;
    char delim;
    char c;
    int  eflag;

    cmd_parse_white(pd);

    c = cmd_curr_char(pd);

    lbuf = dstring_new();

    while(c != '\0' && c != ';') {
        eflag = 0;

        if(c == '"' || c == '\'') {
            delim = c;

            while((c = cmd_next_char(pd))) {
                if(!eflag) {
                    if(c == '\0')  break;
                    if(c == delim) break;
                } else {
                    eflag = 0;
                }

                if(c == '\\') {
                    eflag = 1;
                }

                dstring_insert_char(lbuf, c);
            }

            cmd_next_char(pd);
        } else {
            do {
                if(!eflag) {
                    if(c == '\0') break;
                    if(c == '\'') break;
                    if(c == '"') break;
                    if(c == ' ') break;
                    if(c == ';') break;
                } else {
                    eflag = 0;
                }

                if(c == '\\') {
                    eflag = 1;
                }

                dstring_insert_char(lbuf, c);
            } while((c = cmd_next_char(pd)));
        }

        if(dstring_get_length(lbuf) > 0) {
            buf = dstring_get_string(lbuf);
            ptr_array_add(&pd->cs->params[param_num].list, (void*)buf);
            str_unescape(str_decode_escapes(buf));
        }

        cmd_parse_white(pd);

        c = cmd_curr_char(pd);

        dstring_reset(lbuf);
    }

    dstring_free(lbuf);
}

static void cmd_parse_remain(PDATA *pd, uint param_num)
{
    int     eflag;
    dstring *lbuf;
    uchar   *buf;
    uchar   c;

    cmd_parse_white(pd);

    c = cmd_curr_char(pd);

    if(c == '"' || c == '\'') {
        cmd_parse_string(pd, param_num);
        return;
    }

    lbuf = dstring_new();
    eflag = 0;

    do {
        if(!eflag) {
            if(c == '\0') break;
            if(c == ';' ) break;
            if(c == ' ' ) break;
        } else {
            eflag = 0;
        }

        if(c == '\\') {
            eflag = 1;
        }

        dstring_insert_char(lbuf, c);
    } while((c = cmd_next_char(pd)));

    if(dstring_get_length(lbuf) > 0) {
        buf = dstring_get_string(lbuf);
        pd->cs->params[param_num].st = buf;
        str_unescape(str_decode_escapes(buf));
    }

    dstring_free(lbuf);
}

static void cmd_parse_literal(PDATA *pd, uint param_num)
{
    dstring *lbuf;
    char *buf;
    char delim;
    char c;

    delim = ';';

    cmd_parse_white(pd);
    c = cmd_curr_char(pd);

    lbuf = dstring_new();

    if(c) {
        do {
            if(c == '"' || c == '\'') {
                delim = c;

                do {
                    dstring_insert_char(lbuf, c);
                } while((c = cmd_next_char(pd)) && c != delim);
            }

            dstring_insert_char(lbuf, c);
        } while((c = cmd_next_char(pd)) && c != ';' && c != '}');
    }

    if(dstring_get_length(lbuf) > 0) {
        buf = dstring_get_string(lbuf);
        pd->cs->params[param_num].st = buf;
    }

    dstring_free(lbuf);
}

static void cmd_parse_string(PDATA *pd, uint param_num)
{
    dstring *lbuf;
    char *buf;
    char delim;
    char c;
    int  eflag;

    cmd_parse_white(pd);

    c = cmd_curr_char(pd);

    if(c == '"' || c == '\'') {
        lbuf = dstring_new();
        delim = c;
        eflag = 0;

        while((c = cmd_next_char(pd))) {
            if(!eflag) {
                if(c == '\0')  break;
                if(c == delim) break;
            } else {
                eflag = 0;
            }

            if(c == '\\') {
                eflag = 1;
            }

            dstring_insert_char(lbuf, c);
        }

        if(dstring_get_length(lbuf) > 0) {
            buf = dstring_get_string(lbuf);
            pd->cs->params[param_num].st = str_unescape(str_decode_escapes(buf));
        }

        cmd_next_char(pd);

        dstring_free(lbuf);
    }
}

static void cmd_parse_identifier(PDATA *pd, uint param_num)
{
    dstring *lbuf;
    uchar *buf;
    uchar c;

    cmd_parse_white(pd);

    c = cmd_curr_char(pd);

    if(isalnum(c) || c == '_' || c == '.') {
       lbuf = dstring_new();

       do {
            if(!(isalnum(c) || c == '_')) {
                break;
            }

            dstring_insert_char(lbuf, c);
        } while((c = cmd_next_char(pd)));

        if(dstring_get_length(lbuf)) {
            buf = dstring_get_string(lbuf);
            pd->cs->params[param_num].st = buf;
        }

        dstring_free(lbuf);
    } 
}

static void cmd_parse_kd(PDATA *pd)
{
    dstring *lbuf;
    PDATA   pdtmp;
    uint    code;
    char    *buf;
    char    c;

#ifdef DEBUG
    fprintf(stderr, "[cmd] cmd_parse_kd\n");
#endif

    cmd_parse_white(pd);

    c = cmd_curr_char(pd);

    if(isalnum(c)) {
        lbuf = dstring_new();

        do {
            if(!isalnum(c) && c != '_') {
                break;
            }

            dstring_insert_char(lbuf, c);
        } while((c = cmd_next_char(pd)));

        buf = dstring_get_string(lbuf);

        dstring_free(lbuf);

        if((code = (uint)sh_get(CMD_SUBSYS->keys, buf))) {
            pd->cs->params[0].id = ID_KEY;
        } else {
            if(!strcmp(buf, "m1")) code = 1;
            else if(!strcmp(buf, "m2")) code = 2;
            else if(!strcmp(buf, "m3")) code = 3;

            if(code) {
                pd->cs->params[0].id = ID_MOUSE;
            }
        }

        free(buf);

        /*
         * At this point the input code could be invalid, but we continue
         * regardless so the parser can eat the statement following the
         * key definition, free it, set the command as CMD_NONE so that the
         * parser ignores it, and return.
         */

        pd->cs->params[1].state = 0;
        pd->cs->params[2].code = code;

        cmd_parse_white(pd);

        if( cmd_curr_char(pd) == '^') {
            cmd_next_char(pd);
            pd->cs->params[1].state |= MOD_UP;
        }

        cmd_parse_white(pd);

        if(cmd_curr_char(pd) == '+') {
            while((c = cmd_next_char(pd))) {
                if(isspace(c)) break;
                if(tolower(c) == 's') pd->cs->params[1].state |= MOD_SHIFT;
                if(tolower(c) == 'c') pd->cs->params[1].state |= MOD_CONTROL;
                if(tolower(c) == 'a') pd->cs->params[1].state |= MOD_ALT;
            }
        }

        /*
         * Go up a parse level and parse the statement to be attached to
         * the key definition.
         */

        cmd_parse_white(pd);
        cmd_parse_level_up(pd, &pdtmp, 3);
        cmd_parse_statement(&pdtmp);
        cmd_parse_level_down(pd, &pdtmp, 3);

        /* Handle invalid key definition */

        if(!code) {
            pd->cs->instr = CMD_NONE;
            cmd_parse_error(pd, "kd: unrecognised key or mouse button");
        }
    }

    cmd_parse_white(pd);
}

static inline uchar cmd_curr_char(PDATA *pd)
{
    return(pd->c);
}

static inline uchar cmd_next_char(PDATA *pd)
{
    pd->c = stream_get_char(pd->st);

    if(pd->c == 10) {
        CURR_LNUM++;
    }

    return(pd->c);
}

static void cmd_parse_level_up(PDATA *this_level, PDATA *next_level, uint param_num)
{
    next_level->cmds = cmdlist_new();
    next_level->st   = this_level->st;
    next_level->c    = this_level->c;
    next_level->cs   = NULL;

    CURR_LEVEL++;
}

static void cmd_parse_level_down(PDATA *this_level, PDATA *next_level, uint param_num)
{
    this_level->cs->params[param_num].cmds = next_level->cmds;
    this_level->st = next_level->st;
    this_level->c  = next_level->c;

    CURR_LEVEL--;
}

