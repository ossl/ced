/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2001
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Handles the processing of all commands based on some event occuring.
 *    An event is an input from the user.  This input is checked for a command
 *    mapping, which if found is executed.
 *
 *  Last Updated
 *    15th July 2001
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <libced/common.h>
#include <libced/tools/structs.h>

#include "cmdsubsys.h"

cmdlist *cmdlist_new()
{
    cmdlist *clist;

    clist = (cmdlist*)calloc(1, sizeof(cmdlist));

    ptr_array_init(&clist->cmds);

    return(clist);
}

cmdlist *cmdlist_duplicate(cmdlist *clist)
{
    cmdlist *new_list;
    cmd     *cs;
    uint    i;

    new_list = cmdlist_new();

    for(i=0; i < cmdlist_size(clist); i++) {
        if((cs = cmdlist_get_cmd(clist, i)) != NULL) {
            cs = cmd_duplicate(cs);

            cmdlist_add(new_list, cs);
        }
    }

    return(new_list);
}

cmd *cmdlist_get_cmd(cmdlist *clist, uint elem)
{
    int  s;

    s = cmdlist_size(clist);

    if(elem < s) {
        return((cmd*)clist->cmds.data[elem]);
    } else {
        fprintf(stderr, "[cmdlist] cmdlist_get_cmd: out of range [%u/%u]\n\r", elem, s);
    }

    return(NULL);
}

void cmdlist_add(cmdlist *clist, cmd *cs)
{
    ptr_array_add(&clist->cmds, (void*)cs);
}

uint cmdlist_size(cmdlist *clist)
{
    return(clist->cmds.items);
}

void cmdlist_free(cmdlist *clist)
{
    cmd  *cs;
    uint i;

    for(i=0; i < cmdlist_size(clist); i++) {
        if((cs = cmdlist_get_cmd(clist, i)) != NULL) {
            cmd_free(cs);
        }
    }

    ptr_array_free_data(&clist->cmds);

    free(clist);
}

