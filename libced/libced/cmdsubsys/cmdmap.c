/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Handles binding between key values and commands.
 *
 *  Last Updated
 *    16th December 2000
 *
 */
 
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <libced/utils/utils.h>
#include <libced/hash/inthash.h>

#include "cmdsubsys.h"

#define PARENT(x) x->struct_ptr

void cmd_map_init(cmdsubsys *csys)
{
    int i;

    for(i=0;i<16;i++) csys->key_map[i] = ih_new(64);
    for(i=0;i<16;i++) csys->mbs_map[i][0] = NULL;
    for(i=0;i<16;i++) csys->mbs_map[i][1] = NULL;
    for(i=0;i<16;i++) csys->mbs_map[i][2] = NULL;
}

void cmd_map_flush(cmdsubsys *csys)
{
    int i;

    for(i=0;i<16;i++) {
        ih_free(csys->key_map[i]);
    }

    cmd_map_init(csys);
}

void cmd_map_load(cmdsubsys *csys, char *fname)
{
    char buf[1024];
    char ffname[1024];

    get_full_path(ffname, fname, 1024);

    if(file_exists(ffname) && file_can_read(ffname)) {
        cmd_map_flush(csys);
        cmd_map_merge(csys, ffname);
    } else {
        sprintf(buf, "Couldn't access file: %s\n", ffname);
        csys->func_message(PARENT(csys), buf, 1);
    }
}

void cmd_map_merge(cmdsubsys *csys, char *fname)
{
    cmdlist *cmds;
    char    ffname[1024];
    char    buf[1024];
    int     i;

    get_full_path(ffname, fname, 1024);

    if(file_exists(ffname) && file_can_read(ffname)) {
        cmds = cmd_parse_file(csys, ffname);

        for(i = 0; i < cmdlist_size(cmds); i++) {
            cmd_map_bind(csys, cmdlist_get_cmd(cmds, i));
        }

        cmdlist_free(cmds);
    } else {
        sprintf(buf, "Couldn't access file: %s\n", ffname);
        csys->func_message(PARENT(csys), buf, 1);
    }
}

void cmd_map_bind(cmdsubsys *csys, cmd *cs)
{
    char buf[256];

    if(cs->instr != KD) {
        sprintf(buf, "Bind:%lu: command is not a key definition.\n", cs->lnum);
        csys->func_message(PARENT(csys), buf, 0);
    } else {
        if(cs->params[0].id == ID_KEY) {
            cmd_map_setkey(csys, cs->params[1].state,
                                 cs->params[2].code,
                                 cs->params[3].cmds);
        }

        else if(cs->params[0].id == ID_MOUSE) {
            cmd_map_setmb(csys, cs->params[1].state,
                                cs->params[2].code,
                                cs->params[3].cmds);
        }
    }
}

void cmd_map_setkey(cmdsubsys *csys, int state, int keycode, cmdlist *cmds)
{
    cmdlist *old_clist;
    cmdlist *new_clist;

    if(cmdlist_size(cmds) > 0) {
        if((old_clist = cmd_map_getkey(csys, state, keycode))) {
            cmdlist_free(old_clist);
        }

        new_clist = cmdlist_duplicate(cmds);

        ih_set(csys->key_map[state], keycode, (void*)new_clist);
    }
}

void cmd_map_setmb(cmdsubsys *csys, int state, int button, cmdlist *cmds)
{
    cmdlist *old_clist;
    cmdlist *new_clist;

    if(button <= 3 && cmdlist_size(cmds) > 0) {
        if((old_clist = cmd_map_getmb(csys, state, button))) {
            cmdlist_free(old_clist);
        }

        new_clist = cmdlist_duplicate(cmds);

        csys->mbs_map[state][button-1] = new_clist;
    }
}

cmdlist *cmd_map_getkey(cmdsubsys *csys, int state, int keycode)
{
    return((csys) ? (cmdlist*)ih_get(csys->key_map[state], keycode)
                  : (cmdlist*)NULL);
}

cmdlist *cmd_map_getmb(cmdsubsys *csys, int state, int button)
{
    return((csys && button <= 3)
        ? (csys->mbs_map[state][button-1])
        : (cmdlist*)NULL);
}

