/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *
 *  Todo
 *
 *  Changes
 *
 *  03/01/02
 *    (MAS) Added "halt on error" option
 *
 *  Last Updated
 *    3rd January 2002
 *
 */
 
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "cmdsubsys.h"

#define PARENT(x) x->struct_ptr

extern void cmd_map_init           (cmdsubsys *csys);
static void cmd_sys_default_msg_cb (cmdsubsys *csys, char *errmsg, int critical);

cmdsubsys *cmd_sys_new(strhash *commands, strhash *keys)
{
    cmdsubsys *csys;

    csys = (cmdsubsys*)calloc(1,sizeof(cmdsubsys));
    csys->cbacks = callbacks_new("cmdsubsys");
    csys->keys = keys;
    csys->commands = commands;
    csys->err_halt = 1;

    PARENT(csys) = csys;

    callbacks_add(csys->cbacks, "message", CALLBACK_REF(csys->func_message));
    callbacks_set(csys->cbacks, "message", cmd_sys_default_msg_cb);

    cmd_map_init(csys);

    return(csys);
}

void cmd_sys_free(cmdsubsys *csys)
{
    cmd_map_flush(csys);

    free(csys);
}

/****************************************************************************/

void cmd_sys_set_halt_on_error(cmdsubsys *csys, int b)
{
    csys->err_halt = b;
}

int cmd_sys_get_halt_on_error(cmdsubsys *csys)
{
    return(csys->err_halt);
}

/****************************************************************************/

void cmd_sys_set_parent(cmdsubsys *csys, void *struct_ptr)
{
    csys->struct_ptr = struct_ptr;
}

void cmd_sys_reset_cb(cmdsubsys *csys, char *name)
{
    callbacks_reset(csys->cbacks, name);

    if(!strcmp(name, "message")) {
        callbacks_set(csys->cbacks, "message", cmd_sys_default_msg_cb);
    }
}

void cmd_sys_set_callback(cmdsubsys *csys, char *name, void (*func)())
{
    callbacks_set(csys->cbacks, name, func);
}

static void cmd_sys_default_msg_cb(cmdsubsys *csys, char *errmsg, int critical)
{
    fprintf(stderr, "%s\n", errmsg);
}

