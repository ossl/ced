#ifndef _CMDSUBSYS_H
#define _CMDSUBSYS_H

#include <libced/common.h>
#include <libced/canvas/canvas.h>
#include <libced/hash/strhash.h>
#include <libced/hash/inthash.h>
#include <libced/tools/structs.h>
#include <libced/tools/callbacks.h>
#include <libced/tools/stream.h>
#include <libced/tools/dstring.h>

typedef enum   _cmdcode      cmdcode;
typedef enum   _keycode      keycode;
typedef enum   _cmdset       cmdset;
typedef struct _param        param;
typedef struct _coord        coord;
typedef struct _cmd_subsys   cmdsubsys;
typedef struct _cmd          cmd;
typedef struct _cmdlist      cmdlist;
typedef struct _ptr_array    vlist;

cmdsubsys *cmd_sys_new          (strhash *commands, strhash *keys);
void       cmd_sys_free         (cmdsubsys *csys);
void       cmd_sys_reset_cb     (cmdsubsys *csys, char *name);
void       cmd_sys_set_callback (cmdsubsys *csys, char *name, void (*func)());
void       cmd_sys_set_parent   (cmdsubsys *csys, void *struct_ptr);
void       cmd_sys_halt_on_error(cmdsubsys *csys, int b);

/* create command and key lists */

strhash *cmd_make_comlist       (void);
strhash *cmd_make_keylist       (void);

/* command object */

cmd     *cmd_new                (void);
cmd     *cmd_duplicate          (cmd *cs);
void     cmd_free               (cmd *cs);

/* command list object */

cmdlist *cmdlist_new            (void);
cmdlist *cmdlist_duplicate      (cmdlist *clist);
cmd     *cmdlist_get_cmd        (cmdlist *clist, uint elem);
void     cmdlist_add            (cmdlist *clist, cmd *cs);
uint     cmdlist_size           (cmdlist *clist);
void     cmdlist_free           (cmdlist *clist);

/* command mapping */

cmdlist *cmd_map_getkey         (cmdsubsys *csys, int state, int keycode);
cmdlist *cmd_map_getmb          (cmdsubsys *csys, int state, int button);
void     cmd_map_setkey         (cmdsubsys *csys, int state, int keycode, cmdlist *cmds);
void     cmd_map_setmb          (cmdsubsys *csys, int state, int button, cmdlist *cmds);
void     cmd_map_load           (cmdsubsys *csys, char *fname);
void     cmd_map_merge          (cmdsubsys *csys, char *fname);
void     cmd_map_bind           (cmdsubsys *csys, cmd *cs);
void     cmd_map_flush          (cmdsubsys *csys);

/* command parsing */

cmdlist *cmd_parse_stream       (cmdsubsys *csys, stream *st);
cmdlist *cmd_parse_str          (cmdsubsys *csys, char *s);
cmdlist *cmd_parse_file         (cmdsubsys *csys, char *ffname);

/* types and enums */

enum _input_id
{
    ID_NONE,
    ID_KEY,
    ID_MOUSE
};

enum _modifier_states
{
    MOD_SHIFT    = 1 << 0,
    MOD_CONTROL  = 1 << 1,
    MOD_ALT      = 1 << 2,
    MOD_UP       = 1 << 3
};

enum _cmdset
{
    CMD_NONE, /* dummy command                     */

    /* single step cursor movement */

    AD,       /* Arrow down                        */
    AL,       /* Arrow left                        */
    AR,       /* Arrow right                       */
    AU,       /* Arrow up                          */

    /* navigation within the window */

    TB,       /* To bottom window border           */
    TL,       /* To left window border             */
    TR,       /* To right window border            */
    TT,       /* To top window border              */

    /* cursor navigation within the file */

    CP,       /* Cursor page                       */
    CV,       /* Cursor move virtically            */
    CH,       /* Cursor move horizontally          */
    CT,       /* Cursor top                        */
    CB,       /* Cursor bottom                     */

    /* pad navigation within the file */

    PP,       /* Pad page                          */
    PV,       /* Pad move virtically               */
    PH,       /* Pad move horizontally             */
    PB,       /* Pad page to bottom                */
    PT,       /* Pad page to top                   */

    /* window nagivation */

    WP,       /* View page                         */
    WV,       /* View move virtically              */
    WH,       /* View move horizontally            */
    WT,       /* View top                          */
    WB,       /* View bottom                       */

    /* edit commands */

    ES,       /* edit - insert string              */
    EE,       /* edit - erase character            */
    ED,       /* edit - delete character to left   */
    EN,       /* edit - insert newline             */

    /* buffer commands */

    XP,       /* buffer paste                      */
    XC,       /* buffer copy                       */
    XD,       /* buffer cut                        */
    XA,       /* buffer append                     */

    /* miscellaneous */

    FT,       /* set file type                     */
    DL,       /* define label (n/i)                */
    GL,       /* goto label (n/i)                  */
    CD,       /* change directory                  */
    CED,      /* create edit                       */
    PN,       /* set path name                     */
    TS,       /* set tab stops                     */
    BANG,     /* execute region                    */
    SUBST,    /* substitute region                 */
    SUBONCE,  /* substitute once                   */
    SUBGLOB,  /* substitute globally               */
    KD,       /* key define                        */
    DR,       /* define region                     */
    PW,       /* pad write                         */
    WC,       /* window close                      */
    ABRT,     /* abort current operation           */
    CASE,     /* case adjust                       */
    ECHO,     /* echo region                       */
    PROMPT,   /* set prompt                        */
    LINENO,   /* toggle line numbers               */
    MOUSE,    /* toggle mouse binding              */
    SYNTAX,   /* toggle syntax highlighting        */
    UNDO,     /* perform undo                      */
    REDO,     /* perform redo                      */
    RO,       /* toggle read only flag             */
    CC,       /* create carbon copy window         */
    PU,       /* page up                           */
    PD,       /* page down                         */
    TWB,      /* to window border                  */
    TCW,      /* to command window                 */
    TMW,      /* to message window                 */
    TEW,      /* to edit window                    */
    PWD,      /* print working directory           */
    UNTAB,    /* untab text region                 */
    TABS,     /* toggle displaying tabs            */
    MSG,      /* display a message                 */

    /* searching */

    FWDSRCH,  /* forward search                    */
    REVSRCH,  /* reverse search                    */

    /* positioning */

    POSCIW,   /* position cursor in window         */
    POSTLW,   /* position window at coordinate     */
    POSCAC,   /* position cursor at coordinate     */
    POSBOF,   /* goto bottom of file               */
    POSLN,    /* goto line number                  */

    /* key bindings */

    KLOAD,    /* load command map from file        */
    KMERGE,   /* merge command map from file       */
    KFLUSH,   /* flush command map                 */

    /* cursor tabbing */

    THR,      /* tab horizontal right              */
    THL,      /* tab horizontal left               */

    STRIP
};

struct _cmd_subsys
{
    strhash   *commands;        /* available commands */
    strhash   *keys;            /* available keys */
    inthash   *key_map[16];     /* key definition map */
    cmdlist   *mbs_map[16][3];  /* mouse button definition map */
    callbacks *cbacks;
    void      *struct_ptr;
    int       err_halt;

    CALLBACK_DEF (func_message) (void *, char *, int);
};

struct _cmdlist
{
    vlist cmds;
};

struct _coord
{
    uint  x;
    ulong y;
};

struct _param
{
    coord   co;      /* coordinates */
    int     id;      /* input device, 0=key 1=mouse */
    int     code;    /* keycode/command code/mouse button */
    int     shift;   /* shift flag */
    int     state;   /* state, ie shift, alt, ctrl, (up) */
    double  num;     /* number */
    char    *st;     /* string */
    char    *flag;   /* flag */
    cmdlist *cmds;   /* list of commands */
    vlist   list;    /* list of anything */
};

struct _cmd
{
    cmdset instr;     /* actual instruction */
    param  params[4]; /* instruction parameter(s) */
    cmd    *next;     /* next instruction in definition */
    ulong  lnum;      /* line number parsed on (for error reporting) */
};

#endif /* _CMDSUBSYS_H */

