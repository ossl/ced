/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Creates a hash of ced commands.
 *
 *  Last Updated
 *    23rd December 2000
 *
 */

#include <libced/hash/strhash.h>
#include "cmdsubsys.h"

strhash *cmd_make_comlist()
{
  strhash *comlist = sh_new(64, 1);

  /* single step cursor navigation */

  sh_set(comlist, "ad",     (void*)AD);      /* Arrow down                 */
  sh_set(comlist, "al",     (void*)AL);      /* Arrow left                 */
  sh_set(comlist, "ar",     (void*)AR);      /* Arrow right                */
  sh_set(comlist, "au",     (void*)AU);      /* Arrow up                   */

  /* navigation within the window */

  sh_set(comlist, "tb",     (void*)TB);      /* To bottom of window        */
  sh_set(comlist, "tl",     (void*)TL);      /* To beginning of line       */
  sh_set(comlist, "tr",     (void*)TR);      /* To end of line             */
  sh_set(comlist, "tt",     (void*)TT);      /* To top of window/file      */

  /* cursor navigation within the file */

  sh_set(comlist, "cp",     (void*)CP);      /* Cursor page                */
  sh_set(comlist, "cv",     (void*)CV);      /* Cursor move virtically     */
  sh_set(comlist, "ch",     (void*)CH);      /* Cursor move horizontally   */
  sh_set(comlist, "ct",     (void*)CT);      /* Cursor to top of file      */
  sh_set(comlist, "cb",     (void*)CB);      /* Cursor to bottom of file   */

  /* pad navigation within the file */

  sh_set(comlist, "pp",     (void*)PP);      /* Pad page                   */
  sh_set(comlist, "pv",     (void*)PV);      /* Pad move virtically        */
  sh_set(comlist, "ph",     (void*)PH);      /* Pad move horizontally      */
  sh_set(comlist, "pt",     (void*)PT);      /* Pad page to top            */
  sh_set(comlist, "pb",     (void*)PB);      /* Pad page to bottom         */

  /* window navigation */

  sh_set(comlist, "wp",     (void*)WP);      /* Window page                */
  sh_set(comlist, "wv",     (void*)WV);      /* Window move virtically     */
  sh_set(comlist, "wh",     (void*)WH);      /* Window move horizontally   */
  sh_set(comlist, "wt",     (void*)WT);      /* Window to top of file      */
  sh_set(comlist, "wb",     (void*)WB);      /* Window to bottom of file   */

  /* edit commands */

  sh_set(comlist, "es",     (void*)ES);      /* edit string                */
  sh_set(comlist, "ee",     (void*)EE);      /* edit erase                 */
  sh_set(comlist, "ed",     (void*)ED);      /* edit delete                */
  sh_set(comlist, "en",     (void*)EN);      /* edit newline               */

  /* buffer commands */

  sh_set(comlist, "xp",     (void*)XP);      /* buffer paste               */
  sh_set(comlist, "xc",     (void*)XC);      /* buffer copy                */
  sh_set(comlist, "xd",     (void*)XD);      /* buffer cut                 */
  sh_set(comlist, "xa",     (void*)XA);      /* buffer append              */

  /* miscellaneous */

  sh_set(comlist, "ft",     (void*)FT);      /* set file type (n/i)        */
  sh_set(comlist, "dl",     (void*)DL);      /* define label (n/i)         */
  sh_set(comlist, "gl",     (void*)GL);      /* goto label (n/i)           */
  sh_set(comlist, "cd",     (void*)CD);      /* change directory           */
  sh_set(comlist, "ced",    (void*)CED);     /* create edit                */
  sh_set(comlist, "msg",    (void*)MSG);     /* display a message          */
  sh_set(comlist, "pn",     (void*)PN);      /* set path name              */
  sh_set(comlist, "ts",     (void*)TS);      /* set tab stops              */
  sh_set(comlist, "!",      (void*)BANG);    /* execute region             */
  sh_set(comlist, "s",      (void*)SUBST);   /* substitute region          */
  sh_set(comlist, "so",     (void*)SUBONCE); /* substitute once            */
  sh_set(comlist, "sg",     (void*)SUBGLOB); /* substitute globally        */
  sh_set(comlist, "kd",     (void*)KD);      /* key define                 */
  sh_set(comlist, "dr",     (void*)DR);      /* define region              */
  sh_set(comlist, "pw",     (void*)PW);      /* pad write                  */
  sh_set(comlist, "wc",     (void*)WC);      /* window close               */
  sh_set(comlist, "abrt",   (void*)ABRT);    /* abort current operation    */
  sh_set(comlist, "case",   (void*)CASE);    /* case adjust                */
  sh_set(comlist, "echo",   (void*)ECHO);    /* echo region                */
  sh_set(comlist, "prompt", (void*)PROMPT);  /* set prompt                 */
  sh_set(comlist, "lineno", (void*)LINENO);  /* toggle line numbers        */
  sh_set(comlist, "mouse",  (void*)MOUSE);   /* toggle mouse binding       */
  sh_set(comlist, "syntax", (void*)SYNTAX);  /* toggle syntax highlighting */
  sh_set(comlist, "undo",   (void*)UNDO);    /* perform undo               */
  sh_set(comlist, "redo",   (void*)REDO);    /* perform redo               */
  sh_set(comlist, "ro",     (void*)RO);      /* toggle read only flag      */
  sh_set(comlist, "twb",    (void*)TWB);     /* to window border           */
  sh_set(comlist, "cc",     (void*)CC);      /* create carbon copy window  */
  sh_set(comlist, "tcw",    (void*)TCW);     /* to command interface       */
  sh_set(comlist, "tmw",    (void*)TMW);     /* to message window          */
  sh_set(comlist, "tew",    (void*)TEW);     /* to viewport                */
  sh_set(comlist, "pwd",    (void*)PWD);     /* print working directory    */
  sh_set(comlist, "pu",     (void*)PU);      /* page up                    */
  sh_set(comlist, "pd",     (void*)PD);      /* page down                  */
  sh_set(comlist, "untab",  (void*)UNTAB);   /* page down                  */
  sh_set(comlist, "tabs",   (void*)TABS);    /* toggle show tabs           */
 
  sh_set(comlist, "kload",  (void*)KLOAD);   /* load key defs              */
  sh_set(comlist, "kmerge", (void*)KMERGE);  /* merge key defs             */
  sh_set(comlist, "kflush", (void*)KFLUSH);  /* flush key defs (!)         */

  sh_set(comlist, "thr",    (void*)THR);     /* tab horizontal             */
  sh_set(comlist, "thl",    (void*)THL);     /* tab horizontal left        */

  sh_set(comlist, "strip",  (void*)STRIP);   /* tab horizontal left        */

  return(comlist);
}

