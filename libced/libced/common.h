#ifndef _COMMON_H
#define _COMMON_H

#ifndef NULL
#define NULL '\0'
#endif /* NULL */

typedef enum _OUTPUT_MODE
{
    OM_SILENT,
    OM_VERBOSE
} OUTPUT_MODE;

typedef enum _FILE_TYPE
{
    FT_NULL,
    FT_UNIX,
    FT_DOS,
    FT_MAC,
    FT_BINARY
} FILE_TYPE;

typedef enum _BOOL
{
    TRUE  = 1,
    FALSE = 0
} BOOL;

#define CR               '\r'
#define LF               '\n'
#define max(x,y)         ((x) < (y) ? (y) : (x))
#define min(x,y)         ((x) > (y) ? (y) : (x))
#define diff(x,y)        max((x),(y)) - min((x),(y))
#define smallest(x,y,z)  min(min((x),(y)),(z))
#define largest(x,y,z)   max(max((x),(y)),(z))
#define SCHAR            sizeof(char)

#define ULONG    unsigned long
#define UINT     unsigned int
#define USHORT   unsigned short
#define UCHAR    unsigned char
#define COORD    long

#define ulong    unsigned long
#define uint     unsigned int
#define ushort   unsigned short
#define uchar    unsigned char

#endif /* _COMMON_H */

