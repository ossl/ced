/*
 *  CED - The Editor
 *  
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    An integer hash library.
 *
 *  Last Updated
 *    8th February 2000
 *
 *  To Do
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include "inthash.h"

int ih_set(inthash *ih, long num, void *item)
{
    long act_num;       /* actual number in array */
    elem *e = NULL;     /* new or found element */
    elem **elements;    /* array of element lists found */
    int  i = 0;         /* important to initialise to 0 */

    act_num  = num % ih->maxsize;
    elements = ih->elements[act_num];

    /*
     * First goto end of list of elements at that point in the hash.
     * Hopefully if its not a clash, it'll be the first element.
     */

    if(elements) {
        for(i=0; elements[i] != NULL; i++) {
            if(elements[i]->num == num) {
                e = elements[i];
                break;
            }
        }
        if(!e) {
            elements = (elem**)realloc(elements, (i+2)*sizeof(elem*));
            elements[i] = (elem*)calloc(1, sizeof(elem));
            elements[i+1] = NULL;
            e = elements[i];
            ih->elements[act_num] = elements;
            ih->entries++;
        }
    }
    else {
        elements = (elem**)calloc(2, sizeof(elem*));
        elements[0] = (elem*)calloc(1, sizeof(elem));
  
        e = elements[0];
        ih->elements[act_num] = elements;
        ih->entries++;
    }
 
    e->num  = num;
    e->item = item;

    return(1);
}

void *ih_get(inthash *ih, long num)
{
    elem **elements;  /* array of element lists found */           
    long act_num;     /* actual number in array */
    int  i;
  
    act_num  = num % ih->maxsize;
    elements = ih->elements[act_num];

    if(elements) {
        for(i=0; elements[i] != NULL; i++) {
            if(elements[i]->num == num) {
                return(elements[i]->item);
            }
        }
    }

    return(NULL);
}

int ih_del(inthash *ih, long num)
{
    elem **elements;  /* array of element lists found */
    long act_num;     /* actual number in array */
    int  i;

    act_num  = num % ih->maxsize;
    elements = ih->elements[act_num];

    if(elements) {
        for(i=0; elements[i] != NULL; i++) {
            if(elements[i]->num == num) {
                free(elements[i]);

                for( ; elements[i] != NULL; i++) {
                    elements[i] = elements[i+1];
                }

                ih->entries--;
                ih->elements[act_num] = (elem**)realloc(elements, i*sizeof(elem*));

                return(1);
            }
        }
    }

    return(0);
}

inthash *ih_new(long maxsize)
{
    inthash *ih;

    ih = (inthash*)calloc(1, sizeof(inthash));
    ih->maxsize  = maxsize;
    ih->entries  = 0;
    ih->elements = (elem***)calloc(maxsize, sizeof(elem**));

    return(ih);
}

void ih_free(inthash *ih)
{
    elem **elems;
    uint   i;

    if(!ih) {
        fprintf(stderr, "[inthash] ih_free: attempt to free null pointer\n");
        return;
    }

    for(i=0; i < ih->maxsize; i++) {
        elems = ih->elements[i];

        if(elems) {
            for( ; *elems ; elems++) {
                free(*elems);
            }
            free(ih->elements[i]);
        }
    }

    free(ih->elements);
    free(ih);
}

