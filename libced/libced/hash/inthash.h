#ifndef _INTHASH_H
#define _INTHASH_H

typedef struct _inthash  inthash;
typedef struct _elem     elem;

struct _elem
{ 
  long  num;
  void  *item;
};

struct _inthash
{
  long  maxsize;
  long  entries;
  elem  ***elements;
};

int       ih_set(inthash *ih, long num, void *elem);
int       ih_del(inthash *ih, long num);
void     *ih_get(inthash *ih, long num);
inthash  *ih_new(long maxsize);
void      ih_free(inthash *ih);

#endif /* _INTHASH_H */

