/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    A string hash library.
 *
 *  Last Updated
 *    16th September 2000
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <libced/common.h>
#include <libced/tools/structs.h>

#include "strhash.h"

static long sh_hash(strhash *sh, char *name);

int sh_set(strhash *sh, char *name, void *item)
{
    strelem  **elements;  /* array of element lists found */
    strelem  *e = NULL;   /* new or found element */
    long     act_num;     /* actual number in array */
    int      i = 0;       /* important to initialise to 0 */

    act_num  = sh_hash(sh, name);
    elements = sh->elements[act_num];

    /*
     * First goto end of list of elements at that point in the hash.
     * Hopefully if its not a clash, it'll be the first element.
     */

    if(elements) {
        for(i=0; elements[i] != NULL; i++) {
            if(!sh->str_cmp(elements[i]->name, name)) {
                e = elements[i];
                break;
            }
        }
        if(!e) {
            elements = (strelem**)realloc(elements, (i+2)*sizeof(strelem*));
            elements[i] = calloc(1, sizeof(strelem));
            elements[i+1] = NULL;
            e = elements[i];
            e->name = strdup(name);
            sh->elements[act_num] = elements;
        }
    }
    else {
        elements = (strelem**)calloc(2, sizeof(strelem*));
        elements[0] = calloc(1, sizeof(strelem));
        e = elements[0];
        e->name = strdup(name);
        sh->elements[act_num] = elements;
    }

    e->item = item;

    return(1);
}

void *sh_get(strhash *sh, char *name)
{
    strelem  **elements;  /* array of element lists found */           
    long     act_num;     /* actual number in array */
    int      i;
  
    act_num  = sh_hash(sh, name);

    if((elements = sh->elements[act_num]) != NULL) {
        for(i=0; elements[i] != NULL; i++) {
            if(!sh->str_cmp(elements[i]->name, name)) {
                return(elements[i]->item);
            }
        }
    }

    return(NULL);
}

int sh_del(strhash *sh, char *name)
{
    strelem **elements;  /* array of element lists found */
    long    act_num;     /* actual number in array */
    int     i;

    act_num  = sh_hash(sh, name);
    elements = sh->elements[act_num];

    if(elements) {
        for(i=0; elements[i] != NULL; i++) {
            if(!sh->str_cmp(elements[i]->name, name)) {
                for(;elements[i] != NULL; i++) {
                    elements[i] = elements[i+1];
                }

                sh->elements[act_num] = realloc(elements, i*sizeof(strelem*));

                return(1);
            }
        }
    }

    return(0);
}

void sh_free(strhash *sh)
{
    strelem **elems;
    uint    i;

    if(!sh) {
        fprintf(stderr, "[strhash] sh_free: attempt to free null pointer\n");
        return;
    }

    for(i=0; i < sh->maxsize; i++) {
        if((elems = sh->elements[i]) != NULL) {
            for( ; *elems ; elems++) {
                free((*elems)->name);
                free(*elems);
            }

            free(sh->elements[i]);
        }
    }

    free(sh->elements);
    free(sh);
}

PARRAY *sh_get_keys(strhash *sh)
{
    int i;
    strelem **elems;
    PARRAY *pa;

    pa = (PARRAY*)calloc(1, sizeof(PARRAY));

    for(i=0; i < sh->maxsize; i++) {
        if((elems = sh->elements[i]) != NULL) {
            for( ; *elems ; elems++) {
                ptr_array_add(pa, (void*)(*elems)->name);
            }
        }
    }

    return(pa);
}

PARRAY *sh_get_values(strhash *sh)
{
    int i;
    strelem **elems;
    PARRAY *pa;

    pa = (PARRAY*)calloc(1, sizeof(PARRAY));

    for(i=0; i < sh->maxsize; i++) {
        if((elems = sh->elements[i]) != NULL) {
            for( ; *elems ; elems++) {
                ptr_array_add(pa, (void*)(*elems)->item);
            }
        }
    }

    return(pa);
}

strhash *sh_new(long maxsize, int match_case)
{
    strhash *sh;

    sh = calloc(1, sizeof(strhash));
    sh->str_cmp = (match_case) ? &strcmp : &strcasecmp;
    sh->match_case = match_case;
    sh->maxsize = maxsize;
    sh->entries = 0;
    sh->elements = (strelem***)calloc(maxsize, sizeof(strelem**));

    return(sh);
}

/*
 * Noddy hash function..
 * Okay okay.. it'll be improved !
 */

static long sh_hash(strhash *sh, char *name)
{
    int first;
    int last;
    int len;
    int val;

    len = strlen(name);
    val = 0;

    if(len>0) {
        if(!sh->match_case) {
            first = toupper(name[0]);
            last = toupper(name[len-1]);
        } else {
            first = name[0];
            last = name[len-1];
        }

        val = ((first*last)+len) % sh->maxsize;
    }

//  printf("name: %s, hash: %d\n", name, val);

    return(val);
}


