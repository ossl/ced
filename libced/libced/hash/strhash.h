#ifndef _STRHASH_H
#define _STRHASH_H

#include <libced/common.h>
#include <libced/tools/structs.h>

typedef struct _strhash  strhash;
typedef struct _strelem  strelem;

struct _strelem
{ 
  char  *name;
  void  *item;
};

struct _strhash
{
  int     (*str_cmp)();
  int     match_case;
  long    maxsize;
  long    entries;
  strelem ***elements;
};

int       sh_set(strhash *sh, char *name, void *strelem);
int       sh_del(strhash *sh, char *name);
void     *sh_get(strhash *sh, char *name);
strhash  *sh_new(long maxsize, int match_case);
void      sh_free(strhash *sh);
PARRAY   *sh_get_keys(strhash *sh);
PARRAY   *sh_get_values(strhash *sh);

#endif /* _STRHASH_H */

