/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2001
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Handles the processing of all sessions based on some event occuring.
 *    An event is an input from the user.  This input is checked for a session
 *    mapping, which if found is executed.
 *
 *  Last Updated
 *    17th August 2001
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>
#include "cmdsubsys.h"
#include "session.h"
#include "canvas.h"
#include "buffer.h"
#include "line.h"
#include "utils.h"
#include "error.h"
#include "sys.h"

#define DEFAULT_BUFFER  "PRIMARY"

typedef enum _substitute_type
{
    ST_GLOBAL,
    ST_ONCE,
    ST_LINE
} SUBS_TYPE;

typedef enum _move_context
{
    MC_WINDOW,
    MC_CURSOR,
    MC_PAD
} MOVE_CONTEXT;

typedef enum _session_flags
{
    CF_DEFAULT  = 1 << 0,
    CF_INVALID  = 1 << 1,
    CF_WRAP     = 1 << 2,
    CF_NOWRAP   = 1 << 3,
    CF_CUT      = 1 << 4,
    CF_COPY     = 1 << 5,
    CF_PASTE    = 1 << 6,
    CF_FORCE    = 1 << 7,
    CF_TOP      = 1 << 8,
    CF_BOTTOM   = 1 << 9,
    CF_LEFT     = 1 << 10,
    CF_RIGHT    = 1 << 11,
    CF_RECT     = 1 << 12,
    CF_SAVE     = 1 << 13,
    CF_TO_UPPER = 1 << 14,
    CF_TO_LOWER = 1 << 15
} SESSION_FLAGS;

/* functions to resolve flags from strings */

static SESSION_FLAGS session_rf_save(char *flag);
static SESSION_FLAGS session_rf_wrap(char *flag);
static SESSION_FLAGS session_rf_copy(char *flag);
static SESSION_FLAGS session_rf_close(char *flag);
static SESSION_FLAGS session_rf_rename(char *flag);
static SESSION_FLAGS session_rf_window_border(char *flag);
static SESSION_FLAGS session_rf_region_type(char *flag);
static SESSION_FLAGS session_rf_case(char *flag);

/* sessions */

static int session_define_key(session *se, cmd *cs);
static int session_set_tab_stops(session *se, int size);
static int session_switch_context(session *se, SESSION_CONTEXT ct);
static int session_create_carbon_copy(session *se);
static int session_goto_window_coord(session *se, int col, long row);
static int session_goto_coord(session *se, int col, long row);
static int session_goto_line(session *se, long row);
static int session_set_top_left_coord(session *se, int col, long row);
static int session_to_window_border(session *se, char *flag);
static int session_substitute(session *se, char *find, char *replace, SUBS_TYPE st);
static int session_search(session *se, char *flag, char *srch, cmdlist *cmds, int reverse);
static int session_delete_char(session *se);
static int session_erase_char(session *se, char *flag);
static int session_insert_string(session *se, char *str);
static int session_arrow_up(session *se);
static int session_arrow_down(session *se);
static int session_arrow_left(session *se, char *flag);
static int session_arrow_right(session *se, char *flag);
static int session_paste_from_buffer(session *se, char *flag, char *bname);
static int session_copy_to_buffer(session *se, char *flag, char *bname);
static int session_cut_to_buffer(session *se, char *flag, char *bname);
static int session_append_buffers(session *se, char *dest, char *src);
static int session_move_to_top(session *se, MOVE_CONTEXT mc);
static int session_move_to_bottom(session *se, MOVE_CONTEXT mc);
static int session_move_vertical(session *se, MOVE_CONTEXT mc, long rows);
static int session_move_page(session *se, MOVE_CONTEXT mc, double proportion);
static int session_move_horizontal(session *se, MOVE_CONTEXT mc, int cols);
static int session_bang(session *se, char *flag, char *cmd);
static int session_case(session *se, char *flag);
static int session_to_top(session *se);
static int session_to_bottom(session *se);
static int session_to_left(session *se);
static int session_to_right(session *se);
static int session_abort(session *se);
static int session_close_window(session *se, char *flag);
static int session_change_directory(session *se, char *pname);
static int session_pwd(session *se);
static int session_pad_write(session *se, char *flag);
static int session_define_region(session *se);
static int session_redo(session *se);
static int session_undo(session *se);
static int session_echo_region(session *se, char *rtype);
static int session_flip_tabs(session *se);
static int session_flip_lnums(session *se);
static int session_flip_highlighting(session *se);
static int session_flip_mouse_binding(session *se);
static int session_flip_read_only(session *se);
static int session_set_prompt(session *se, char *prompt);
static int session_define_label(session *se, char *label);
static int session_goto_label(session *se, char *label);
static int session_tab_left(session *se);
static int session_tab_right(session *se);
static int session_flush_keys(session *se);
static int session_strip(session *se);
static int session_merge_keys(session *se, char *fname);
static int session_load_keys(session *se, char *fname);
static int session_set_file_type(session *se, char *ftype);
static int session_insert_newline(session *se);
static int session_open_files(session *se, vlist list);
static int session_untab(session *se);
static int session_set_path_name(session *se, char *flag1, char *flag2, char *pname);
 
/* some auxiliary functions */

static void session_post_y_move(session *se);

session *session_new(canvas *txt, canvas *cmd, canvas *msg)
{
    session    *se;
    const char *ffname;

    se = (session*)calloc(1, sizeof(session));
    se->last_instr = CMD_NONE;
    se->canv_text = txt;
    se->canv_cmd = cmd;
    se->canv_msg = msg;
    se->last_y[SESSION_EDITWIN] = 0;
    se->last_y[SESSION_MSGWIN] = 0;
    se->last_y[SESSION_CMDWIN] = 0;
    se->cbacks = callbacks_new("session");

    if(txt) {
        if((ffname = buff_get_pathname(canv_get_buffer(txt))) != NULL) {
            get_path(se->cwd, ffname, 1024);
        }
    }

    if(strlen(se->cwd) == 0) {
        getcwd(se->cwd, 1024);
    }

    if(CED_CONFIG.prompt) {
        strcpy(se->prompt, CED_CONFIG.prompt);
    } else {
        strcpy(se->prompt, "Command:");
    }

    callbacks_add(se->cbacks, "open_file"          , CALLBACK_REF(se->func_open_file));
    callbacks_add(se->cbacks, "prompt_changed"     , CALLBACK_REF(se->func_prompt_changed));
    callbacks_add(se->cbacks, "close_window"       , CALLBACK_REF(se->func_close_window));
    callbacks_add(se->cbacks, "create_carbon_copy" , CALLBACK_REF(se->func_create_carbon_copy));
    callbacks_add(se->cbacks, "context_changed"    , CALLBACK_REF(se->func_context_changed));

    return(se);
}

void session_set_canvas(session *se, SESSION_CONTEXT context, canvas *canv)
{
    switch(context)
    {
        case SESSION_EDITWIN : se->canv_text = canv; break;
        case SESSION_CMDWIN  : se->canv_cmd = canv; break;
        case SESSION_MSGWIN  : se->canv_msg = canv; break;

        default:
            fprintf(stderr, "session_set_canvas(): Invalid context");
    }
}

void session_free(session *se)
{
    cmd_sys_reset_cb(sys_get_cmd_subsystem(), "message");
    cmd_sys_set_parent(sys_get_cmd_subsystem(), NULL);

    callbacks_free(se->cbacks);

    free(se);
}

/*
 * Executes a command list based on an input event.  The input event is looked
 * up from the various map hash tables (i.e. the key map, and the  mouse maps).
 * The command list is retrieved from the map and then executed.
 */

int session_process_event(session *se, canvas *canv, events *e)
{
    cmd     *cs;
    cmdlist *cmds;
    int     st;
    int     cont;

    cmds = NULL;
    cont = 1;
    st = e->state;

    // Look up the even in the key map and the mouse map.  If the event is the
    // result of an upstroke of perhaps a keyboard key or a mouse butten, then
    // this can return a different command list.

    if(e->upstroke) st |= MOD_UP;
    if(e->button)   cmds = cmd_map_getmb(sys_get_cmd_subsystem(), st, e->button);
    if(e->key)      cmds = cmd_map_getkey(sys_get_cmd_subsystem(), st, e->key);

    if( canv != se->canv_msg && (cmds || e->printable)) {
        canv->func_message(FRONTEND, "", 0);
    }

    // If the cursor is in a different y position, then we always end the
    // current undo event.  This ensures that any one undo event only affects
    // changes made on a single line.

    if( se->last_y[se->active_canv] != canv_get_ypos(canv)) {
        se->last_y[se->active_canv]  = canv_get_ypos(canv);
        undo_end_event(canv->buff->undo_list);
    }

    // If the input event was not found in any of the map hash tables, then
    // check if the input event has a printable character.  If it does, then
    // insert the character into the canvas.

    if(cmds == NULL) {
        if(e->printable) {
            if( se->last_instr != CMD_NONE) {
                se->last_instr  = CMD_NONE;
                undo_end_event(canv->buff->undo_list);
            }

            canv_insert_char(canv, e->printable);

        }
    }

    // Otherwise, we first need to set the active canvas to be the canvas
    // passed so that the command processor knows what canvas to apply the
    // commands to.  The command list is then passed to the command processor
    // to be executed.

    else {
        cs = cmdlist_get_cmd(cmds, 0);

        if(canv == se->canv_text) se->active_canv = SESSION_EDITWIN;
        else if(canv == se->canv_cmd)  se->active_canv = SESSION_CMDWIN;
        else if(canv == se->canv_msg)  se->active_canv = SESSION_MSGWIN;

        cont = session_exec(se, cmds);
    }

    return(cont);
}

canvas *session_get_active_canv(session *se)
{
    switch( se->active_canv ) {
        case SESSION_EDITWIN : return(se->canv_text); 
        case SESSION_CMDWIN  : return(se->canv_cmd); 
        case SESSION_MSGWIN  : return(se->canv_msg); 
    }  

    return(NULL);
}

char *session_get_string(session *se, char *key)
{
    return(NULL);
}

int session_exec_ln(session *se, line *ln)
{
    char *buf;
    int cont;

    buf  = line_get_string(ln);
    cont = session_exec_string(se, buf);

    free(buf);

	return(cont);	
}

int session_exec_string(session *se, char *str)
{
    cmdlist *cmds;
    int     cont = 1;

    se->active_canv = SESSION_EDITWIN;

    if(str && strlen(str)) {
        cmd_sys_set_parent(sys_get_cmd_subsystem(), (void*)se->canv_text);
        cmd_sys_set_callback(sys_get_cmd_subsystem(), "message", canv_od_message);

        if((cmds = cmd_parse_str(sys_get_cmd_subsystem(), str)) != NULL) {
            cont = session_exec(se, cmds);
            cmdlist_free(cmds);
        }    
    }

    return(cont);
}

int session_exec(session *se, cmdlist *cmds)
{
    canvas *canv;
    cmd    *cs;
    int    i;
    int    s;

    /*
     * If there is more than one session to execute, then we
     * end the current undo event.
     */

    canv = session_get_active_canv(se);

    if(cmdlist_size(cmds) == 0) {
        return(0);
    }

    cs = cmdlist_get_cmd(cmds, 0);

    if(cmdlist_size(cmds) > 1) {
        undo_end_event(canv->buff->undo_list);
    } else {
        if((cs->instr != ES  &&
            cs->instr != EE  &&
            cs->instr != ED) ||
            cs->instr != se->last_instr)
        {
            undo_end_event(canv->buff->undo_list);
        }
    }

    for(i=0; i < cmdlist_size(cmds); i++) {
        cs = cmdlist_get_cmd(cmds, i);
        s = 1;

        switch(cs->instr) {

            /* context switching */

            case TCW      : s = session_switch_context     (se, SESSION_CMDWIN); break;
            case TMW      : s = session_switch_context     (se, SESSION_MSGWIN); break;
            case TEW      : s = session_switch_context     (se, SESSION_EDITWIN); break;

            /* window coordinates */

            case POSTLW   : s = session_set_top_left_coord (se, (int)cs->params[0].num, (long)cs->params[1].num); break;
            case POSCIW   : s = session_goto_window_coord  (se, (int)cs->params[0].num, (long)cs->params[1].num); break;
            case POSCAC   : s = session_goto_coord         (se, (int)cs->params[0].num, (long)cs->params[1].num); break;
            case POSLN    : s = session_goto_line          (se, (int)cs->params[0].num); break;
            case POSBOF   : s = session_goto_line          (se, canv->buff->nlines); break;

            /* searching and substitution */

            case SUBST    : s = session_substitute         (se, cs->params[0].st, cs->params[1].st, ST_LINE); break;
            case SUBONCE  : s = session_substitute         (se, cs->params[0].st, cs->params[1].st, ST_ONCE); break;
            case SUBGLOB  : s = session_substitute         (se, cs->params[0].st, cs->params[1].st, ST_GLOBAL); break;
            case FWDSRCH  : s = session_search             (se, cs->params[0].flag, cs->params[0].st, cs->params[3].cmds, 0); break;
            case REVSRCH  : s = session_search             (se, cs->params[0].flag, cs->params[0].st, cs->params[3].cmds, 1); break;

            /* basic editing sessions */

            case ES       : s = session_insert_string      (se, cs->params[0].st); break;
            case EE       : s = session_erase_char         (se, cs->params[0].flag); break;
            case ED       : s = session_delete_char        (se); break;
            case EN       : s = session_insert_newline     (se); break;

            /* arrow sessions */

            case AU       : s = session_arrow_up           (se); break;
            case AD       : s = session_arrow_down         (se); break;
            case AL       : s = session_arrow_left         (se, cs->params[0].flag); break;
            case AR       : s = session_arrow_right        (se, cs->params[0].flag); break;

            /* buffer sessions */

            case XP       : s = session_paste_from_buffer  (se, cs->params[0].flag, cs->params[0].st); break;
            case XC       : s = session_copy_to_buffer     (se, cs->params[0].flag, cs->params[0].st); break;
            case XD       : s = session_cut_to_buffer      (se, cs->params[0].flag, cs->params[0].st); break;
            case XA       : s = session_append_buffers     (se, cs->params[0].st, cs->params[1].st); break;

            /* window move sessions */

            case WP       : s = session_move_page          (se, MC_WINDOW, cs->params[0].num); break;
            case WV       : s = session_move_vertical      (se, MC_WINDOW, (long)cs->params[0].num); break;
            case WH       : s = session_move_horizontal    (se, MC_WINDOW, (int)cs->params[0].num); break;
            case WT       : s = session_move_to_top        (se, MC_WINDOW); break;
            case WB       : s = session_move_to_bottom     (se, MC_WINDOW); break;

            /* pad move sessions */

            case PP       : s = session_move_page          (se, MC_PAD, cs->params[0].num); break;
            case PV       : s = session_move_vertical      (se, MC_PAD, (long)cs->params[0].num); break;
            case PH       : s = session_move_horizontal    (se, MC_PAD, (int)cs->params[0].num); break;
            case PT       : s = session_move_to_top        (se, MC_PAD); break;
            case PB       : s = session_move_to_bottom     (se, MC_PAD); break;

            /* cursor move sessions */

            case CP       : s = session_move_page          (se, MC_CURSOR, cs->params[0].num); break;
            case CV       : s = session_move_vertical      (se, MC_CURSOR, (long)cs->params[0].num); break;
            case CH       : s = session_move_horizontal    (se, MC_CURSOR, (int)cs->params[0].num); break;
            case CT       : s = session_move_to_top        (se, MC_CURSOR); break;
            case CB       : s = session_move_to_bottom     (se, MC_CURSOR); break;

            /* in-window quick navigation */

            case TT       : s = session_to_top             (se); break;
            case TB       : s = session_to_bottom          (se); break;
            case TL       : s = session_to_left            (se); break;
            case TR       : s = session_to_right           (se); break;

            /* key definition related */

            case KFLUSH   : s = session_flush_keys         (se); break;
            case KD       : s = session_define_key         (se, cs); break;
            case KLOAD    : s = session_load_keys          (se, cs->params[0].st); break;
            case KMERGE   : s = session_merge_keys         (se, cs->params[0].st); break;

            /* undo and redo */

            case UNDO     : s = session_undo               (se); break;
            case REDO     : s = session_redo               (se); break;

            /* labels */

            case DL       : s = session_define_label       (se, cs->params[0].st); break;
            case GL       : s = session_goto_label         (se, cs->params[0].st); break;

            /* miscellaneous */

            case BANG     : s = session_bang               (se, cs->params[0].flag, cs->params[0].st); break;
            case TWB      : s = session_to_window_border   (se, cs->params[0].flag); break;
            case ECHO     : s = session_echo_region        (se, cs->params[0].flag); break;
            case CASE     : s = session_case               (se, cs->params[0].flag); break;
            case WC       : s = session_close_window       (se, cs->params[0].flag); break;
            case PW       : s = session_pad_write          (se, cs->params[0].flag); break;
            case CD       : s = session_change_directory   (se, cs->params[0].st); break;
            case CED      : s = session_open_files         (se, cs->params[0].list); break;
            case MSG      : s = session_message            (se, cs->params[0].st); break;
            case PROMPT   : s = session_set_prompt         (se, cs->params[0].st); break;
            case FT       : s = session_set_file_type      (se, cs->params[0].st); break;
            case TS       : s = session_set_tab_stops      (se, (int)cs->params[0].num); break;
            case PWD      : s = session_pwd                (se); break;
            case ABRT     : s = session_abort              (se); break;
            case DR       : s = session_define_region      (se); break;
            case TABS     : s = session_flip_tabs          (se); break;
            case LINENO   : s = session_flip_lnums         (se); break;
            case SYNTAX   : s = session_flip_highlighting  (se); break;
            case MOUSE    : s = session_flip_mouse_binding (se); break;
            case RO       : s = session_flip_read_only     (se); break;
            case THR      : s = session_tab_right          (se); break;
            case THL      : s = session_tab_left           (se); break;
            case CC       : s = session_create_carbon_copy (se); break;
            case UNTAB    : s = session_untab              (se); break;
            case STRIP    : s = session_strip              (se); break;
            case PN       : s = session_set_path_name      (se, cs->params[0].flag, cs->params[1].flag, cs->params[0].st); break;

            /* some checks */

            case CMD_NONE:
                canv->func_message(FRONTEND, "Unrecognised session.", 1); break;

            default:
                canv->func_message(FRONTEND, "Not implemented, watch this space !", 1);
        }

        /* if status is 0 then we should exit the loop */

        if(s == 0) {
            return(0);
        }

        /* Remember the session just executed */

        se->last_instr = cs->instr;
    }

    return(1);
}

void session_set_callback(session *se, char *name, void (*func)())
{
    callbacks_set(se->cbacks, name, func);
}

static int session_define_key(session *se, cmd *cs)
{
    if(cs) {
        cmd_map_bind(sys_get_cmd_subsystem(), cs);
    }

    return(1);
}

static int session_untab(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_untab(canv);

    return(1);
}

static int session_set_tab_stops(session *se, int size)
{
    if(se->canv_text) canv_set_tab_stops(se->canv_text, size);
    if(se->canv_msg)  canv_set_tab_stops(se->canv_msg, size);
    if(se->canv_cmd)  canv_set_tab_stops(se->canv_cmd, size);

    return(1);
}

static int session_switch_context(session *se, SESSION_CONTEXT ct)
{
    se->active_canv = ct;

    if(ct != SESSION_EDITWIN) {
        session_to_right(se);
    }

    se->func_context_changed(se->struct_ptr);

    return(1);
}

static int session_create_carbon_copy(session *se)
{
    se->func_create_carbon_copy(se->struct_ptr);

    return(1);
}

static int session_goto_window_coord(session *se, int col, long row)
{
    canvas *canv;
    canv = session_get_active_canv(se);

    if(col && row && col <= canv->od_cols && row <= canv->od_rows) {
        col += canv->od_xoffset - 1;
        row += canv->od_yoffset - 1;
        canv_set_col_row(canv, col, row);
    } else {
        canv->func_message(FRONTEND, "Value out of range", 0);
    }

    return(1);
}

static int session_goto_coord(session *se, int col, long row)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_set_col(canv, abs(max(col, 1) - 1), 0);
    canv_set_ypos(canv, abs(max(row, 1) - 1));

    return(1);
}

static int session_goto_line(session *se, long row)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_goto_line(canv, abs(max(row, 1)));

    return(1);
}

static int session_set_top_left_coord(session *se, int col, long row)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_pad_set_position(canv, max((uint)col, 1) - 1, max((ulong)row, 1) - 1);

    return(1);
}

static int session_to_window_border(session *se, char *flag)
{
    WINDOW_BORDER wb;
    canvas *canv;
    canv = session_get_active_canv(se);

    switch(session_rf_window_border(flag)) {
        case CF_TOP    : wb = CANV_WB_TOP; break;
        case CF_BOTTOM : wb = CANV_WB_BOTTOM; break;
        case CF_LEFT   : wb = CANV_WB_LEFT; break;
        case CF_RIGHT  : wb = CANV_WB_RIGHT; break;

        default :
            canv->func_message(FRONTEND, "twb: invalid flag.", 1);
            return(0);
    }

    canv_cursor_to_window_border(canv, wb);

    return(1);
}

static int session_substitute(session *se, char *find, char *replace, SUBS_TYPE st)
{
    canvas *canv;
    canv = session_get_active_canv(se);

    if(canv_find_init(canv, find, replace)) {
        switch(st) {
            case ST_ONCE   : canv_substitute_once(canv); break;
            case ST_GLOBAL : canv_substitute_global(canv); break;
            case ST_LINE   : canv_substitute_line(canv); break;
        }
    }

    return(1);
}

static int session_search(session *se, char *flag, char *srch, cmdlist *cmds, int reverse)
{
    canvas *canv;
    uint   wrap;

    canv = session_get_active_canv(se);

    if(srch) {
        if(!canv_find_init(canv, srch, NULL)) {
            return(0);
        }
    }

    switch(session_rf_wrap(flag)) {
        case CF_NOWRAP  : wrap = 0; break;
        case CF_WRAP    : wrap = 1; break;
        default         : wrap = 1; break;
    }

    if(canv_find_next(canv, reverse, wrap) && cmds) {
        if(cmdlist_size(cmds) > 0) {
            session_exec(se, cmds);
        }
    }

    return(1);
}

static int session_delete_char(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_delete_char(canv);

    return(1);
}

static int session_erase_char(session *se, char *flag)
{
    SESSION_FLAGS cf;
    canvas *canv;
    canv = session_get_active_canv(se);
    cf = session_rf_wrap(flag);

    if((cf & CF_WRAP) && se->active_canv != SESSION_EDITWIN) {
        cf &= ~CF_WRAP;
        cf |=  CF_DEFAULT;
    }

    switch(cf) {
        case CF_WRAP    : canv_erase_char(canv, 1); break;
        case CF_DEFAULT : canv_erase_char(canv, 0); break;

        default :
            canv->func_message(FRONTEND, "ee: invalid flag.", 1);
            return(0);
    }

    return(1);
}

static int session_insert_string(session *se, char *str)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_insert_string(canv, str);

    return(1);
}

static int session_arrow_up(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_arrow_up(canv);
    session_post_y_move(se);

    return(1);
}

static int session_arrow_down(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_arrow_down(canv);
    session_post_y_move(se);

    return(1);
}

static int session_arrow_left(session *se, char *flag)
{
    canvas *canv;
    canv = session_get_active_canv(se);

    switch(session_rf_wrap(flag)) {
        case CF_WRAP    : canv_arrow_left(canv, 1); break;
        case CF_DEFAULT : canv_arrow_left(canv, 0); break;

        default:
            canv->func_message(FRONTEND, "al: invalid flag.", 1);
            return(0);
    }

    return(1);
}

static int session_arrow_right(session *se, char *flag)
{
    canvas *canv;
    canv = session_get_active_canv(se);

    switch(session_rf_wrap(flag)) {
        case CF_WRAP    : canv_arrow_right(canv, 1); break;
        case CF_DEFAULT : canv_arrow_right(canv, 0); break;

        default:
            canv->func_message(FRONTEND, "ar: invalid flag.", 1);
            return(0);
    }

    return(1);
}

static int session_append_buffers(session *se, char *dest, char *src)
{
    canvas *canv;

    canv = session_get_active_canv(se);
    canv_append_buffers(canv, dest, src);

    return(1);
}

static int session_move_to_top(session *se, MOVE_CONTEXT mc)
{
    canvas *canv;
    canv = session_get_active_canv(se);

    switch(mc) {
        case MC_CURSOR : canv_cursor_to_top(canv); break;
        case MC_WINDOW : canv_window_to_top(canv); break;
        case MC_PAD    : canv_pad_to_top(canv); break;
    }

    session_post_y_move(se);

    return(1);
}

static int session_move_to_bottom(session *se, MOVE_CONTEXT mc)
{
    canvas *canv;
    canv = session_get_active_canv(se);

    switch(mc) {
        case MC_CURSOR : canv_cursor_to_bottom(canv); break;
        case MC_WINDOW : canv_window_to_bottom(canv); break;
        case MC_PAD    : canv_pad_to_bottom(canv); break;
    }

    session_post_y_move(se);

    return(1);
}

static int session_move_vertical(session *se, MOVE_CONTEXT mc, long rows)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    rows = (rows != 0) ? rows : 1;

    switch(mc) {
        case MC_CURSOR : canv_cursor_move_vertical(canv, rows); break;
        case MC_WINDOW : canv_window_move_vertical(canv, rows); break;
        case MC_PAD    : canv_pad_move_vertical(canv, rows); break;
    }

    session_post_y_move(se);

    return(1);
}

static int session_move_page(session *se, MOVE_CONTEXT mc, double proportion)
{
    canvas *canv;
    canv = session_get_active_canv(se);

    switch(mc) {
        case MC_CURSOR : canv_cursor_page(canv, proportion); break;
        case MC_WINDOW : canv_window_move_page(canv, proportion); break;
        case MC_PAD    : canv_pad_move_page(canv, proportion); break;
    }

    session_post_y_move(se);

    return(1);
}

static int session_move_horizontal(session *se, MOVE_CONTEXT mc, int cols)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    cols = (cols != 0) ? cols : 1;

    switch(mc) {
        case MC_CURSOR : canv_cursor_move_horizontal(canv, cols); break;
        case MC_WINDOW : canv_window_move_horizontal(canv, cols); break;
        case MC_PAD    : canv_pad_move_horizontal(canv, cols); break;
    }

    return(1);
}

static int session_bang(session *se, char *flag, char *cmd)
{
    canvas *canv;
    canv = session_get_active_canv(se);

    chdir(se->cwd);

    switch(session_rf_copy(flag)) {
        case CF_COPY    : canv_exec_region(canv, cmd, 1); break;
        case CF_DEFAULT : canv_exec_region(canv, cmd, 0); break;

        default:
            canv->func_message(FRONTEND, "bang: invalid flag.", 1);
            return(0);
    }

    return(1);
}

static int session_case(session *se, char *flag)
{
    canvas *canv;
    canv = session_get_active_canv(se);

    switch(session_rf_case(flag)) {
        case CF_TO_UPPER : canv_case(canv, CANV_CASE_UPPER); break;
        case CF_TO_LOWER : canv_case(canv, CANV_CASE_LOWER); break;
        case CF_DEFAULT  : canv_case(canv, CANV_CASE_FLIP); break;

        default:
            canv->func_message(FRONTEND, "case: invalid flag.", 1);
            return(0);
    }        

    return(1);
}

static int session_to_top(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_cursor_to_top(canv);

    return(1);
}

static int session_to_bottom(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_cursor_to_bottom(canv);

    return(1);
}

static int session_to_left(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_cursor_to_left(canv);

    return(1);
}

static int session_to_right(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_cursor_to_right(canv);

    return(1);
}

static int session_abort(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_undefine_region(canv);

    return(1);
}

static int session_close_window(session *se, char *flag)
{
    canvas *canv;

    if((canv = se->canv_text) != NULL) {
        switch(session_rf_close(flag)) {
            case CF_SAVE: /* fall through */
                canv_save(canv); 

            case CF_FORCE:
                se->func_close_window(se->struct_ptr);

                /* We don't want to carry on because the window is closing */

                return(0);

            case CF_DEFAULT:
                if(buff_number_of_links(canv_get_buffer(canv)) == 1 && canv->buff->dirty) {
                    canv->func_message(FRONTEND, "wc: file modified, use wc -f to override.", 1);
                } else {
                    se->func_close_window(se->struct_ptr);
                }

                return(0);

            default:
                canv->func_message(FRONTEND, "wc: invalid flag.", 1);
                return(0);
        }
    }

    return(1);
}

static int session_change_directory(session *se, char *pname)
{
    canvas *canv;
    char   *buf;

    canv = se->canv_text;
    buf  = se->cwd;

    chdir(buf);

    if(pname) {
		get_full_path(buf, pname, 1024);
    } else {
        get_home_dir(buf, 1024);
    }

    if(chdir(buf) == -1) {
        canv->func_message(FRONTEND, error_get_str(errno), 1);
        return(0);
    }

    canv->func_message(FRONTEND, getcwd(buf, 1024), 0);

    return(1);
}

static int session_pwd(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv->func_message(FRONTEND, se->cwd, 0);

    return(1);
}

static int session_pad_write(session *se, char *flag)
{
    canvas *canv;

    if((canv = se->canv_text) != NULL) {
        switch(session_rf_save(flag)) {
            case CF_FORCE   : break;
            case CF_DEFAULT :
                if(canv_disk_file_timestamp_changed(canv)) {
                    canv->func_message(FRONTEND, "pw: file on disk has changed, use -f to force", 1);
                    return(0);
                }

                break;

            default:
                canv->func_message(FRONTEND, "pw: invalid flag.", 1);
                return(0);
        }

        canv_save(canv);
    }

    return(1);
}

static int session_strip(session *se)
{
    canvas *canv;

    if((canv = se->canv_text) != NULL) {
        canv_strip(canv);
    }

    return(1);
}

static int session_define_region(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_define_region(canv);

    return(1);
}

static int session_undo(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_undo(canv);

    return(1);
}

static int session_redo(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_redo(canv);

    return(1);
}

static int session_echo_region(session *se, char *flag)
{
    canvas *canv;
    canv = session_get_active_canv(se);

    switch(session_rf_region_type(flag)) {
        case CF_RECT    : canv_echo_region(canv, RT_RECTANGULAR); break;
        case CF_DEFAULT : canv_echo_region(canv, RT_NORMAL); break;

        default :
            canv->func_message(FRONTEND, "echo: invalid flag.", 1);
            return(0);
    }

    return(1);
}

static int session_flip_tabs(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_toggle_tabs(canv);

    return(1);
}

static int session_flip_lnums(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_toggle_lnums(canv);

    return(1);
}

static int session_flip_highlighting(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_toggle_highlighting(canv);

    return(1);
}

static int session_flip_mouse_binding(session *se)
{
    canv_toggle_mouse_binding(se->canv_text);
    canv_toggle_mouse_binding(se->canv_cmd);
    canv_toggle_mouse_binding(se->canv_msg);

    return(1);
}

static int session_set_prompt(session *se, char *prompt)
{
    if(prompt) {
        strcpy(se->prompt, prompt);
    } else {
        se->prompt[0] = '\0';
    }

    se->func_prompt_changed(se->struct_ptr);

    return(1);
}

static int session_define_label(session *se, char *label)
{
    canvas *canv;
    canv = session_get_active_canv(se);

    if(label) {
        canv_define_label(canv, label);
    }

    return(1);
}

static int session_goto_label(session *se, char *label)
{
    canvas *canv;
    canv = session_get_active_canv(se);

    if(label) {
        canv_goto_label(canv, label);
    }

    return(1);
}

static int session_flip_read_only(session *se)
{
    if(se->canv_text) {
        canv_toggle_readonly(se->canv_text);
    }

    return(1);
}

static int session_tab_left(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_tab_left(canv);

    return(1);
}

static int session_tab_right(session *se)
{
    canvas *canv;
    canv = session_get_active_canv(se);
    canv_tab_right(canv);

    return(1);
}

static int session_flush_keys(session *se)
{
    cmd_map_flush(sys_get_cmd_subsystem());

    return(1);
}

static int session_merge_keys(session *se, char *fname)
{
    char ffname[1024];

    if(fname) {
        get_full_path(ffname, fname, 1024);
        cmd_map_merge(sys_get_cmd_subsystem(), ffname);
    }

    return(1);
}

static int session_load_keys(session *se, char *fname)
{
    char ffname[1024];

    if(fname) {
        get_full_path(ffname, fname, 1024);
        cmd_map_load(sys_get_cmd_subsystem(), ffname);
    }

    return(1);
}

/*
 * Insert a newline.  If we are in the session context, we want to
 * execute the line that we are currently on.  That line should then
 * be moved to the bottom of the canvas, and then a newline inserted.
 */

static int session_insert_newline(session *se)
{
    int    cont;
    canvas *canv;
    line   *lbuf;

    canv = session_get_active_canv(se);
    cont = 1;

    switch(se->active_canv) {
        case SESSION_CMDWIN:
            lbuf = line_new();

            canv_line_get_raw(canv, canv_get_ypos(canv), lbuf);
			canv_delete_line(canv);
            canv_cursor_to_bottom(canv);

            if(line_length(lbuf) > 0) {
                canv_replace_line(canv, lbuf);
                canv_cursor_to_right(canv);
                canv_insert_newline(canv);

                while(canv_get_nlines(canv) > 50) {
                    canv_delete_at_line(canv, 0);
                }

                canv_cursor_to_bottom(canv);
            }

            if((cont = session_exec_ln(se, lbuf))) {
                se->func_context_changed(se->struct_ptr);
            }

            line_free(lbuf);

            break;

        case SESSION_MSGWIN:
            canv_cursor_to_bottom(canv);
            canv_cursor_to_left(canv);
            se->active_canv = SESSION_EDITWIN;
            se->func_context_changed(se->struct_ptr);

            break;

		default :
            canv_insert_newline(canv);
    }

    return(cont);
}

/*
 * Display a message in the message canvas.
 */

int session_message(session *se, char *msg)
{
    canvas *canv;
    canv = se->canv_msg;

    if(canv) {
        if(!msg || strlen(msg) == 0) {
            canv_cursor_to_bottom(canv);
        } else {
            canv_set_readonly(canv, 0);

            while(canv_get_nlines(canv) >= 50) {
                canv_delete_at_line(canv, 0);
            }

            canv_cursor_to_bottom(canv);
            canv_cursor_to_left(canv);
            canv_insert_string(canv, msg);
            canv_insert_newline(canv);
            canv_arrow_up(canv);
            canv_set_readonly(canv, 1);
        }
    }

    return(1);
}

static int session_set_file_type(session *se, char *ftype)
{
    canvas *canv;
    canv = session_get_active_canv(se);

    if(ftype != NULL) {
        if(!strcasecmp(ftype, "unix")) canv_set_filetype(canv, FT_UNIX);
        else if(!strcasecmp(ftype, "dos")) canv_set_filetype(canv, FT_DOS);
        else if(!strcasecmp(ftype, "mac")) canv_set_filetype(canv, FT_MAC);
        else canv->func_message(FRONTEND, "ft: unknown file type.", 0);
    } else {
        canv->func_message(FRONTEND, "ft: please specify a file type.", 0);
    }

    return(1);
}

static int session_copy_to_buffer(session *se, char *flag, char *bname)
{
    canvas *canv;
    canv = session_get_active_canv(se);

    switch(session_rf_region_type(flag)) {
        case CF_RECT    : canv_copy_region(canv, RT_RECTANGULAR, bname); break;
        case CF_DEFAULT : canv_copy_region(canv, RT_NORMAL, bname); break;

        default :
            canv->func_message(FRONTEND, "xc: invalid flag.", 1);
            return(0);
    }

    return(1);
}

static int session_cut_to_buffer(session *se, char *flag, char *bname)
{
    canvas *canv;
    canv = session_get_active_canv(se);

    switch(session_rf_region_type(flag)) {
        case CF_RECT    : canv_cut_region(canv, RT_RECTANGULAR, bname); break;
        case CF_DEFAULT : canv_cut_region(canv, RT_NORMAL, bname); break;

        default :
            canv->func_message(FRONTEND, "xd: invalid flag.", 1);
            return(0);
    }

    return(1);
}

static int session_paste_from_buffer(session *se, char *flag, char *bname)
{
    regtype rt;
    canvas  *canv;
    clip    *cl;
    char    fname[1024];
    int     i;

    canv = session_get_active_canv(se);

    switch(session_rf_region_type(flag)) {
        case CF_RECT    : rt = RT_RECTANGULAR; break;
        case CF_DEFAULT : rt = RT_NORMAL; break;

        default:
            canv->func_message(FRONTEND, "xp: invalid flag.", 1);
            return(0);
    }

    switch(se->active_canv) {
        case SESSION_EDITWIN:
            canv_paste_region(canv, rt, bname);
            break;

        case SESSION_CMDWIN:
			if(flag && flag[0] == 'r') {
                canv_paste_region(canv, rt, bname);
            } else {
                if(!(bname && strlen(bname) > 0)) {
                    bname = DEFAULT_BUFFER;
                }

                if(!strcmp(bname, DEFAULT_BUFFER)) {
                    canv->func_get_selection(FRONTEND);
                }

                cl = clip_new();
                get_paste_buffer_ffname(fname, bname, 1024);

                if(clip_load(cl, fname)) {
                    for(i=0; i < cl->used; i++) {
                        canv_insert_ln(canv, cl->lines[i]);

                        if(i+1 < cl->used) {
                            canv_insert_newline(canv);
                            session_exec_ln(se, cl->lines[i]);
                        }
                    }
                }
            }
            break;

        default :
    }

    return(1);
}

static int session_set_path_name(session *se, char *flag1, char *flag2, char *pname)
{
    char     ffname[1024];
    canvas   *canv;
    buffer   *buff;
    SESSION_FLAGS cf;

    canv = se->canv_text;
    buff = canv_get_buffer(canv);

    cf  = session_rf_rename(flag1);
    cf |= session_rf_rename(flag2);

    if(pname) {
        get_full_path(ffname, pname, 1024);

        if(!strcmp(ffname, buff->ffname)) {
            return(1);
        }

        if(sys_get_buffer(ffname) != NULL) {
            canv->func_message(FRONTEND, "pn: that file is currently open !", 1);
            return(0);
        }

        if(file_exists(ffname) && !(cf & CF_FORCE)) {
            canv->func_message(FRONTEND, "pn: file exists, -f overrides", 1);
            return(0);
        }

        if(file_exists(buff->ffname) && !(cf & CF_COPY)) {
            if(file_move(buff->ffname, ffname) != -1) {
                if(!buff_set_name(buff, ffname)) {
                    return(0);
                }
            } else {
                canv->func_message(FRONTEND, "pn: rename failed", 1);
                return(0);
            }
        } else {
            if(!buff_set_name(buff, ffname)) {
                return(0);
            }
        }
    }

    return(1);
}

static int session_open_files(session *se, vlist list)
{
    buffer *buff;
    canvas *canv;
    canvas *clink;
    char   fnbuf[128];
    int    retval;
    int    i;

    canv = se->canv_text;

    chdir(se->cwd);

    if(list.items == 0) {
        se->func_open_file(se->struct_ptr, NULL, &retval);
    }

    for(i=0; i < list.items; i++) {
        strcpy(fnbuf, list.data[i]);

        if(strlen(fnbuf)) {
            if((buff = sys_get_buffer(fnbuf))) {
                canv->func_message(FRONTEND, "ced: file already open", 1);

                if((clink = buff_get_link(buff, 0))) {
                    clink->func_bring_to_front(clink->struct_ptr);
                    clink->func_message(clink->struct_ptr, "I'm already open !", 0);
                }
            } else {
                se->func_open_file(se->struct_ptr, fnbuf, &retval);

                if(retval == -1) {
                    canv->func_message(FRONTEND, error_get_str(errno), 0);
                    return(0);
                }
            }
        }
    }

    return(1);
}

/*
 * Routines to resolve string flags to real flags that
 * can be passed to the canvas.
 */

static SESSION_FLAGS session_rf_region_type(char *flag)
{
    if(flag) {
        switch(flag[0])  {
            case 'r' : return(CF_RECT);
            default  : return(CF_INVALID);
        }
    }

    return(CF_DEFAULT);
}

static SESSION_FLAGS session_rf_save(char *flag)
{
    if(flag) {
        switch(flag[0]) {
            case 'f' : return(CF_FORCE);
            default  : return(CF_INVALID);
        }
    }

    return(CF_DEFAULT);
}

static SESSION_FLAGS session_rf_close(char *flag)
{
    if(flag) {
        switch(flag[0]) {
            case 's' : return(CF_SAVE);
            case 'f' : return(CF_FORCE);
            default  : return(CF_INVALID);
        }
    }

    return(CF_DEFAULT);
}

static SESSION_FLAGS session_rf_wrap(char *flag)
{
    if(flag) {
        switch(flag[0]) {
            case 'w' : return(CF_WRAP);
            case 'n' : return(CF_NOWRAP);
            default  : return(CF_INVALID);
        }
    }

    return(CF_DEFAULT);
}

static SESSION_FLAGS session_rf_copy(char *flag)
{
    if(flag) {
        switch(flag[0]) {
            case 'c' : return(CF_COPY);
            default  : return(CF_INVALID);
        }
    }

    return(CF_DEFAULT);
}

static SESSION_FLAGS session_rf_rename(char *flag)
{
    if(flag) {
        switch(flag[0]) {
            case 'f' : return(CF_FORCE);
            case 'c' : return(CF_COPY);
            default  : return(CF_INVALID);
        }
    }

    return(CF_DEFAULT);
}

static SESSION_FLAGS session_rf_window_border(char *flag)
{
    if(flag) {
        switch(flag[0]) {
            case 't' : return(CF_TOP);
            case 'b' : return(CF_BOTTOM);
            case 'l' : return(CF_LEFT);
            case 'r' : return(CF_RIGHT);
            default  : return(CF_INVALID);
        }
    }

    return(CF_DEFAULT);
}

static SESSION_FLAGS session_rf_case(char *flag)
{
    if(flag) {
        switch(flag[0]) {
            case 'u' : return(CF_TO_UPPER);
            case 'l' : return(CF_TO_LOWER);
            default  : return(CF_INVALID);
        }
    }

    return(CF_DEFAULT);
}

/*
 * This function should be called everytime a command is executed that
 * would alter the y position of the canvas.  If the command was executed
 * in the command or message window context, we need to move the cursor
 * fully to the right.
 */

static void session_post_y_move(session *se)
{
	if(se->active_canv != SESSION_EDITWIN) {
        canv_cursor_to_right(session_get_active_canv(se));
    }
}

