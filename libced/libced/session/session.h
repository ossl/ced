#ifndef _SESSION_H
#define _SESSION_H

#include "common.h"
#include "canvas.h"
#include "strhash.h"
#include "inthash.h"
#include "structs.h"
#include "callbacks.h"
#include "stream.h"
#include "line.h"
#include "cmdsubsys.h"

typedef enum   _session_context  SESSION_CONTEXT;
typedef struct _session          session;

session *session_new           (canvas *txt, canvas *cmd, canvas *msg);
void     session_free          (session *se);
void     session_set_callback  (session *se, char *name, void (*func)());
int      session_process_event (session *se, canvas *canv, events *e);
int      session_exec          (session *se, cmdlist *cmds);
int      session_exec_string   (session *se, char *str);
int      session_exec_ln       (session *se, line *ln);

/* NEW STUFF */

//void     session_add_canvas    (session *se, canvas *canv);

/***********************************************************/

enum _session_context
{
    SESSION_EDITWIN,
    SESSION_MSGWIN,
    SESSION_CMDWIN
};

struct _session
{
    cmdset      last_instr;    /* the last instruction processed */
    int         last_y[3];     /* last y position of an event */
    canvas      *canv_text;    /* the main text canvas */
    canvas      *canv_cmd;     /* command canvas */
    canvas      *canv_msg;     /* message canvas */
    char        prompt[128];   /* command prompt */
    uchar       cwd[1024];     /* current working directory */

    SESSION_CONTEXT active_canv;   /* the current active canvas */

    callbacks   *cbacks;       /* callbacks */

    /* callback stuff */

    void *struct_ptr;

    CALLBACK_DEF (func_open_file)          (void *, char *, int *);
    CALLBACK_DEF (func_prompt_changed)     (void *);
    CALLBACK_DEF (func_close_window)       (void *);
    CALLBACK_DEF (func_create_carbon_copy) (void *);
    CALLBACK_DEF (func_context_changed)    (void *);
};

/* msg command direct access */

void session_set_canvas(session *se, SESSION_CONTEXT context, canvas *canv);
int  session_message(session *se, char *msg);

#endif /* _SESSION_H */

