/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Internal ced library system stuff.
 *
 *  Last Updated
 *    10th August 2000
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <libced/utils/utils.h>
#include <libced/buffer/buffer.h>
#include <libced/tools/structs.h>
#include <libced/cmdsubsys/cmdsubsys.h>

#include "sys.h"

typedef struct _ptr_array BUFFER_LIST;

BUFFER_LIST  SYS_BUFFERS;
cmdsubsys   *CMD_SUBSYS;

static void sys_realize_config (void);
static void sys_interrupt      (int sig);
static void sys_emergency_stop (int sig);
static void sys_handle_stdin   (void);
static void sys_interrupt      (int sig);
static void sys_init_signals   (void);
static int  sys_load_config    (void);
static int  sys_load_syntax    (void);
static int  sys_load_keydefs   (void);

IERROR sys_init(int argc, char **argv, strhash *keylist)
{
    static int initialised = 0;                                                           
    char       wkbuf[1024];
    int        fd;

    if(initialised == 0) {
        ptr_array_init(&SYS_BUFFERS);
        srand((int)time(NULL));

        /*
         * Check if the .ced directory exists in the users
         * home directory, and if not create it.
         */

        get_ced_dir(wkbuf, 1024);

        if((fd = open(wkbuf, O_RDONLY, 0)) < 0) {
            if(mkdir(wkbuf, 00700) != 0) {
                return(IERR_CREATING_WKDIR);
            }
        } else {
            close(fd);
        }

        /* Same with the paste directory */

        strcat(wkbuf, "paste/");

        if((fd = open(wkbuf, O_RDONLY, 0)) < 0) {
            if(mkdir(wkbuf, 00700) != 0) {
                return(IERR_CREATING_WKDIR);
            }
        } else {
            close(fd);
        }

        CMD_SUBSYS = cmd_sys_new(cmd_make_comlist(), keylist);

        sys_init_signals();
        sys_handle_stdin();
        sys_load_config();
        sys_load_syntax();

        if(!sys_load_keydefs()) {
            fprintf(stderr, "[ced] can't continue without key definitions\r\n");
            exit(0);
        }

        /* Execute the startup file */

        if(SYS_CONFIG) {
            sys_realize_config();
        }

        /*
         * System is now initialised, so set the static flag to 1 so this
         * routine can't be run again.
         */

        initialised = 1;
    }

    return(IERR_NONE);
}

cmdsubsys *sys_get_cmd_subsystem()
{
    return(CMD_SUBSYS);
}

buffer *sys_get_buffer(char *fname)
{
    ulong  n;
    buffer *buff;
    char   ffname[1024];

    get_full_path(ffname, fname, 1024);

    for(n=0; n < SYS_BUFFERS.items; n++) {
        buff = (buffer*)SYS_BUFFERS.data[n];

        if(!strcmp(ffname, buff->ffname)) {
            return(buff);
            break;
        }
    }
    
    return(NULL);
}

void sys_add_buffer(buffer *buff)
{
    if(buff) {
        ptr_array_add(&SYS_BUFFERS, (void*)buff);
    }
}

void sys_del_buffer(buffer *buff)
{
    ulong n;

    if(buff) {
        if( ptr_array_lookup(&SYS_BUFFERS, (void*)buff, &n)) {
            ptr_array_del(&SYS_BUFFERS, n);
        }
    }
}

void sys_quit()
{
    while(SYS_BUFFERS.items) {
        buff_destroy(SYS_BUFFERS.data[0]);
        sys_del_buffer(SYS_BUFFERS.data[0]);
    }
}

void sys_sync_swaps(int verbose)
{
    int i;
    buffer *buff;

    for(i=0; i < SYS_BUFFERS.items; i++)
    {
        if((buff = (buffer*)SYS_BUFFERS.data[i]) != NULL)
        {
            if(buff_has_swap(buff))
            {
                if(verbose)
                {
                    fprintf(stderr, "ced: synchronising swap file [%s]\r\n", buff_get_pathname(buff));
                    fflush(stderr);
                }

                buff_sync_swap(buff);
            }
        }
    }
}

/* private functions */

static void sys_realize_config()
{
    char **cedconf = NULL;
    char **lnums   = NULL;
    char **synh    = NULL;
    char **mbind   = NULL;
    char **fmode   = NULL;
    char **tabs    = NULL;
    char **prompt  = NULL;
    char **keyfile = NULL;
    char **synfile = NULL;

    CED_CONFIG.tabstops = 4;
    CED_CONFIG.lnums = 0;
    CED_CONFIG.highlight = 1;
    CED_CONFIG.mousebind = 1;
    CED_CONFIG.filemode = FT_UNIX;
    CED_CONFIG.prompt = "Command:";
    CED_CONFIG.keyfile = NULL;
    CED_CONFIG.synfile = NULL;

    if((cedconf = conf_params(SYS_CONFIG, NULL, "Config"))) {
        tabs    = conf_params(SYS_CONFIG, cedconf[0], "tabstops");
        lnums   = conf_params(SYS_CONFIG, cedconf[0], "linenumbers");
        synh    = conf_params(SYS_CONFIG, cedconf[0], "highlighting");
        mbind   = conf_params(SYS_CONFIG, cedconf[0], "mousebinding");
        fmode   = conf_params(SYS_CONFIG, cedconf[0], "filemode");
        prompt  = conf_params(SYS_CONFIG, cedconf[0], "prompt");
//        keyfile = conf_params(SYS_CONFIG, cedconf[0], "keydeffile");
//        synfile = conf_params(SYS_CONFIG, cedconf[0], "syntaxfile");
    }

    if( tabs    ) CED_CONFIG.tabstops  = atoi(tabs[0]);
    if( lnums   ) CED_CONFIG.lnums     = (!strcasecmp(lnums[0], "off")) ? 0 : 1;
    if( synh    ) CED_CONFIG.highlight = (!strcasecmp(synh[0], "off")) ? 0 : 1;
    if( mbind   ) CED_CONFIG.mousebind = (!strcasecmp(mbind[0], "off")) ? 0 : 1;
    if( prompt  ) CED_CONFIG.prompt    = strdup(prompt[0]);
    if( synfile ) CED_CONFIG.synfile   = strdup(synfile[0]);
    if( keyfile ) CED_CONFIG.keyfile   = strdup(keyfile[0]);

    if(fmode) {
        if(!strcasecmp(fmode[0], "mac")) CED_CONFIG.filemode = FT_MAC;
        else if(!strcasecmp(fmode[0], "dos")) CED_CONFIG.filemode = FT_DOS;
        else CED_CONFIG.filemode = FT_UNIX;
    }
}

static void sys_handle_stdin()
{
    char ffname[512];

    get_paste_buffer_ffname(ffname, "PIPE", 512);
    save_stdin(ffname);
}

static void sys_interrupt(int sig)
{
    fprintf(stderr, "ced: caught interruption signal\n\r");

    sys_sync_swaps(1);
    signal(sig, SIG_DFL);
    kill(getpid(), sig);
}

static void sys_emergency_stop(int sig)
{         
    static int dejavu = 0;

    if(dejavu++) {
        fprintf(stderr, "failed\n\r");
    } else {
        fprintf(stderr, "\n\r");
        fprintf(stderr, "ced: deadly signal caught, doing emergency stop.\n\r");
        sys_sync_swaps(1);
    }

    fprintf(stderr, "ced: please keep the core file and email me at:\n\r");
    fprintf(stderr, "ced: nutty@users.sourceforge.net\n\r");

    signal(sig, SIG_DFL);
    kill(getpid(), sig);
}

static void sys_init_signals()
{
    /* Internal sync timer */

    signal(SIGALRM, sys_sync_swaps);

    /* Interruption */

    signal(SIGPIPE, sys_interrupt);
    signal(SIGHUP,  sys_interrupt);
    signal(SIGTERM, sys_interrupt);
    signal(SIGQUIT, sys_interrupt);
    signal(SIGSTOP, sys_interrupt);
    signal(SIGTSTP, sys_interrupt);
    signal(SIGINT,  sys_interrupt);

    /* Deadly signals */

    signal(SIGILL,  sys_emergency_stop);
    signal(SIGABRT, sys_emergency_stop);
    signal(SIGFPE,  sys_emergency_stop);
    signal(SIGSEGV, sys_emergency_stop);
    signal(SIGBUS,  sys_emergency_stop);
    signal(SIGXCPU, sys_emergency_stop);
    signal(SIGXFSZ, sys_emergency_stop);
}

static int sys_load_config()
{
    char ff[1024];

    if(get_conf_file(ff, "CED_CONFIG", NULL, "ced.conf", 1024) != NULL) {
        SYS_CONFIG = conf_new(ff);
        conf_parse(SYS_CONFIG);
        return(1);
    } else {
        SYS_CONFIG = NULL;
        fprintf(stderr, "[ced] warning: couldn't find ced.conf\r\n");
    }

    return(0);
}

static int sys_load_syntax()
{
    char ff[1024];

    if(get_conf_file(ff, "CED_SYNTAX", CED_CONFIG.synfile, "syntax.conf", 1024) != NULL) {
        SYS_CONFIG_LANG = conf_new(ff);
        conf_parse(SYS_CONFIG_LANG);
        return(1);
    } else {
        SYS_CONFIG_LANG = NULL;
        fprintf(stderr, "[ced] warning: couldn't find syntax highlighting file\r\n");
    }

    return(0);
}

static int sys_load_keydefs()
{
    char ff[1024];

    if(get_conf_file(ff, "CED_KEYDEFS", CED_CONFIG.keyfile, "key.def", 1024) != NULL) {
        cmd_map_merge(sys_get_cmd_subsystem(), ff);
        return(1);
    } else {
        fprintf(stderr, "[ced] warning: couldn't find key definition file\r\n");
    }

    return(0);
}


