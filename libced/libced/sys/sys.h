#ifndef _SYS_H
#define _SYS_H

#include <libced/cmdsubsys/cmdsubsys.h>
#include <libced/hash/strhash.h>
#include <libced/buffer/buffer.h>
#include <libced/tools/conf.h>

typedef struct _ced_settings CED_SETTINGS;
typedef enum   _init_errors  IERROR;

enum _init_errors
{
    IERR_NONE,
    IERR_CREATING_WKDIR,
    IERR_NO_KEYDEF,
    IERR_NO_PASTE_DIR,
    IERR_CANT_OPEN_LOG
};

struct _ced_settings
{
    int  tabstops;
    int  lnums;
    int  highlight;
    int  mousebind;
    int  filemode;
    char *prompt;
    char *keyfile;
    char *synfile;
    char *confile;
};

CED_SETTINGS CED_CONFIG;
config *SYS_CONFIG;
config *SYS_CONFIG_LANG;
FILE   *SYS_LOG;

IERROR  sys_init(int argc, char **argv, strhash *keylist);
buffer *sys_get_buffer(char *fname);
cmdsubsys *sys_get_cmd_subsystem(void);
void    sys_add_buffer(buffer *buff);
void    sys_del_buffer(buffer *buff);
void    sys_sync_swaps(int verbose);
void    sys_quit(void);

#endif /* _SYS_H */

