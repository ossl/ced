#ifndef _SYSDATA_H
#define _SYSDATA_H

#define SYSDATA_SYSDIR  "/usr/local/etc/ced/"
#define SYSDATA_BINDIR  "/usr/local/bin/"
#define SYSDATA_VERSION "1.3.0"
#define SYSDATA_V_MAJOR 1
#define SYSDATA_V_MINOR 3
#define SYSDATA_V_MICRO 0


#endif /* _SYSDATA_H */

