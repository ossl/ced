/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2001
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    The buffer contains functions for accessing and manipulating text
 *    contained within it.
 *
 *  Last Updated
 *    19th April 2001
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "avltree.h"

//#define DEBUG
#define MAX_HEIGHT 64

/* Internal tree functions */

AVLTree *avltree_rebalance(AVLTree *stack[], int height, int bf, int deleting);

/* Rotation functions */

inline AVLTree *avltree_rotate_left  (AVLTree *root, AVLTree *parent);
inline AVLTree *avltree_rotate_right (AVLTree *root, AVLTree *parent);
inline AVLTree *avltree_ll (AVLTree *pivot);
inline AVLTree *avltree_lr (AVLTree *pivot);
inline AVLTree *avltree_rr (AVLTree *pivot);
inline AVLTree *avltree_rl (AVLTree *pivot);

/* Some utility functions */

static inline AVLTree *avltree_copy_item_ptr(AVLTree *t, void **item);

AVLTree *avltree_replace(AVLTree *t, long v, void *item, void **old_item)
{
    while(t) {
        if(v == t->value) {
            avltree_copy_item_ptr(t, old_item);
            t->item = item;
            return(t);
        }
        else if(v > t->value) t = t->right;
        else if(v < t->value) t = t->left;
    }

    avltree_copy_item_ptr(NULL, old_item);

    return(NULL);
}

AVLTree *avltree_insert(AVLTree *t, long v, void *item)
{
    AVLTree *root;
    AVLTree *node;
    AVLTree *stack[MAX_HEIGHT];
    int     height;

#ifdef DEBUG
    printf("[avltree] insert: %ld\n", v);
#endif

    node = (AVLTree*)calloc(1, sizeof(AVLTree));
    node->value = v;
    node->item  = item;

    if((root = t) != NULL) {
        stack[0] = NULL;

        for( height=1 ; t != NULL ; height++) {
            stack[height] = t;

            if(v == t->value) return(root);
            else if(v > t->value) t = t->right;
            else if(v < t->value) t = t->left;
        }

        height--;

        if(v > stack[height]->value) {
            stack[height]->right = node;
            t = avltree_rebalance(stack, height, 1, 0);
        } else {
            stack[height]->left = node;
            t = avltree_rebalance(stack, height, -1, 0);
        }

        return(t);
    }

    return(node);
}

AVLTree *avltree_remove(AVLTree *t, long v, void **item)
{
    AVLTree *swap;
    AVLTree *root;
    AVLTree *stack[MAX_HEIGHT];
    int     height;

#ifdef DEBUG
    printf("[avltree] delete: %ld\n", v);
#endif

    swap = NULL;
    root = t;
    stack[0] = NULL;

    for( height=1 ; t != NULL ; height++ ) {
        stack[height] = t;

        if(v == t->value) {
            swap = t;
            break;
        }
        else if(v > t->value) t = t->right;
        else if(v < t->value) t = t->left;
    }

    avltree_copy_item_ptr(swap, item);

    if(!swap) {
        return(root);
    }

    if(t->left) {
        stack[++height] = t = t->left;
    }

    while(t->right) {
        stack[++height] = t = t->right;
    }

    if(height == 1) {
        free(stack[height]);
        return(NULL);
    }

    swap->value = t->value;
    swap->item  = t->item;

    if(t == stack[height-1]->left) {
        stack[height-1]->left = stack[height]->left;
        free(stack[height]);
        t = avltree_rebalance(stack, height - 1, -1, 1);
    } else {
        stack[height-1]->right = stack[height]->left;
        free(stack[height]);
        t = avltree_rebalance(stack, height - 1, 1, 1);
    }

    return(t);
}

AVLTree *avltree_rebalance(AVLTree *stack[], int height, int bf, int deleting)
{
    for( ; ; height--) {
        if(deleting) {
            /*
             * If deleting from the tree, we want to negate the balance factor,
             * If the result of our negation causes the tree to go more than 1
             * out of balance, then we do a rotation.
             */

            switch(stack[height]->balance -= bf) {
                case  2 : stack[height] = avltree_rotate_left(stack[height] , stack[height-1]); break;
                case -2 : stack[height] = avltree_rotate_right(stack[height], stack[height-1]);
            }

            /*
             * If the balance of the current node is not 0, then the height of the
             * tree has no changed, so we finish.
             */

            if(stack[height]->balance != 0) {
                return(stack[1]);
            }
        } else {
            /*
             * If adding to the tree, we need to add the balance factor.  If the
             * result causes our tree to go more than 1 out of balance, we do a
             * rotation.  If the result is 0, we can finish because the height of
             * the tree has not changed.
             */

            switch(stack[height]->balance += bf) {
                case  2 : stack[height] = avltree_rotate_left(stack[height] , stack[height-1]); return(stack[1]);
                case -2 : stack[height] = avltree_rotate_right(stack[height], stack[height-1]); return(stack[1]);
                case  0 : if(!deleting) return(stack[1]);
            }
        }

        /*
         * We need to adjust the balance factor here depending on whether the
         * parent of the current node is its left or right child.
         */

        if(height == 1) return(stack[1]);
        else if(stack[height] == stack[height-1]->left)  bf = -1;
        else if(stack[height] == stack[height-1]->right) bf = +1;
    }

    return(NULL);
}

inline AVLTree *avltree_rotate_left(AVLTree *pivot, AVLTree *parent)
{
    AVLTree *t;

#ifdef DEBUG
    printf("[avltree] rotate left\n");
#endif

    if(pivot && pivot->right) {
        switch(pivot->right->balance) {
            case -1 : t = avltree_rl(pivot); break;
            default : t = avltree_ll(pivot);
        }

        if(parent) {
            (pivot == parent->left)
               ? (parent->left  = t)
               : (parent->right = t);
        }

        return(t);
    }

    return(pivot);
}

inline AVLTree *avltree_rotate_right(AVLTree *pivot, AVLTree *parent)
{
    AVLTree *t;

#ifdef DEBUG
    printf("[avltree] rotate right\n");
#endif

    if(pivot && pivot->left) {
        switch(pivot->left->balance) {
            case  1 : t = avltree_lr(pivot); break;
            default : t = avltree_rr(pivot);
        }

        if(parent) {
            (pivot == parent->left)
               ? (parent->left  = t)
               : (parent->right = t);
        }

        return(t);
    }

    return(pivot);
}

inline AVLTree *avltree_ll(AVLTree *pivot)
{
    AVLTree *right;

#ifdef DEBUG
    printf("[avltree] rotate ll, node: %ld\n", pivot->value);
#endif

    right = pivot->right;
    pivot->right = right->left;
    right->left = pivot;

    pivot->balance -= (1 + right->balance);
    right->balance--;

    return(right);
}

inline AVLTree *avltree_rr(AVLTree *pivot)
{
    AVLTree *left;

#ifdef DEBUG
    printf("[avltree] rotate rr\n");
#endif

    left = pivot->left;
    pivot->left = left->right;
    left->right = pivot;

    pivot->balance += (1 - left->balance);
    left->balance++;

    return(left);
}

inline AVLTree *avltree_rl(AVLTree *pivot)
{
    AVLTree *p;
    AVLTree *q;

#ifdef DEBUG
    printf("[avltree] rotate rl\n");
#endif

    p = pivot->right;
    q = p->left;

    pivot->right = q->left;

    p->left = q->right;
    q->right = p;
    q->left = pivot;
    pivot = q;

    switch(pivot->balance) {
        case  0 : pivot->left->balance = 0;
                  pivot->right->balance = 0;
                  break;

        case  1 : pivot->balance = 0;
                  pivot->left->balance = -1;
                  pivot->right->balance = 0;
                  break;

        case -1 : pivot->balance = 0;
                  pivot->left->balance = 0;
                  pivot->right->balance = 1;
                  break;
    }

    return(pivot);
}

/* insert into right node of left subtree */

inline AVLTree *avltree_lr(AVLTree *pivot)
{
    AVLTree *p;
    AVLTree *q;

#ifdef DEBUG
    printf("[avltree] rotate lr\n");
#endif

    p = pivot->left;
    q = p->right;

    pivot->left = q->right;

    p->right = q->left;
    q->left = p;
    q->right = pivot;
    pivot = q;

    switch(pivot->balance) {
        case  0 :  pivot->left->balance = 0;
                   pivot->right->balance = 0;
                   break;

        case  1 :  pivot->balance = 0;
                   pivot->left->balance = -1;
                   pivot->right->balance = 0;
                   break;

        case -1 :  pivot->balance = 0;
                   pivot->left->balance = 0;
                   pivot->right->balance = 1;
                   break;
    }

    return(pivot);
}

/*
 * Recurse through the given tree and free each node.
 */

void avltree_free(AVLTree *t)
{
    if(t) {
        avltree_free(t->left);
        avltree_free(t->right);

        free(t);
    }
}

void avltree_map_preorder(AVLTree *t, void (*func)(void*))
{
    if(t && func) {
        func(t->item);

        avltree_map_preorder(t->left, func);
        avltree_map_preorder(t->right, func);
    }
}

void avltree_map_inorder(AVLTree *t, void (*func)(void*))
{
    if(t && func) {
        avltree_map_inorder(t->left, func);

        func(t->item);

        avltree_map_inorder(t->right, func);
    }
}

void avltree_map_postorder(AVLTree *t, void (*func)(void*))
{
    if(t && func) {
        avltree_map_postorder(t->left, func);
        avltree_map_postorder(t->right, func);

        func(t->item);
    }
}

void avltree_map_range(AVLTree *t, long start, long end, void (*func)(void*))
{
    if(t) {
        if(t->value >= start && t->value <= end) {
            func(t->item);
        }

        avltree_map_range(t->left, start, end, func);
        avltree_map_range(t->right, start, end, func);
    }
}

AVLTree *avltree_get_eq(AVLTree *t, long v, void **item)
{
    for( ; t != NULL ;  ) {
        if(v == t->value) return(avltree_copy_item_ptr(t, item));
        else if(v > t->value) t = t->right;
        else if(v < t->value) t = t->left;
    }

    return(avltree_copy_item_ptr(NULL, item));
}

AVLTree *avltree_get_gt_or_eq(AVLTree *t, long v, void **item)
{
    AVLTree *prev = NULL;

    while(t) {
        if(t->value == v) return(avltree_copy_item_ptr(t, item));
        else if(t->value < v) t = t->right;
        else if(t->value > v) {
            prev = t;
            t = t->left;
        }
    }

    return(avltree_copy_item_ptr(prev, item));
}

AVLTree *avltree_get_lt_or_eq(AVLTree *t, long v, void **item)
{
    AVLTree *prev = NULL;

    while(t) {
        if(t->value == v) return(avltree_copy_item_ptr(t, item));
        else if(t->value > v) t = t->left;
        else if(t->value < v) {
            prev = t;
            t = t->right;
        }
    }

    return(avltree_copy_item_ptr(prev, item));
}

AVLTree *avltree_get_lt(AVLTree *t, long v, void **item)
{
    return(avltree_get_lt_or_eq(t, v - 1, item));
}

AVLTree *avltree_get_gt(AVLTree *t, long v, void **item)
{
    return(avltree_get_gt_or_eq(t, v + 1, item));
}

void avltree_print(AVLTree *t)
{
    if(t == NULL) {
        printf("null");
        return;
    }

    printf("%ld", t->value);

    switch(t->balance) {
        case  0: printf("-EQ"); break;
        case -1: printf("-LH"); break;
        case  1: printf("-RH"); break;
        default: printf("-I"); break;
    }

    if(!t->left && !t->right) {
        return;
    }

    printf(" ( ");
    avltree_print(t->left);
    printf(" , ");
    avltree_print(t->right);
    printf(" ) ");
}

static inline AVLTree *avltree_copy_item_ptr(AVLTree *t, void **item)
{
    if(item) {
        if(t) *item = t->item;
        else  *item = NULL;
    }

    return(t);
}

