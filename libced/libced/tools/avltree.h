#ifndef _AVLTREE_H
#define _AVLTREE_H

typedef struct _avl_tree AVLTree;

struct _avl_tree
{
    int      balance;
    long     value;
    void     *item;
    AVLTree  *left;
    AVLTree  *right;
};

AVLTree *avltree_replace(AVLTree *t, long v, void *item, void **old_item);
AVLTree *avltree_insert(AVLTree *t, long v, void *item);
AVLTree *avltree_remove(AVLTree *t, long v, void **item);
AVLTree *avltree_get_lt(AVLTree *t, long v, void **item);
AVLTree *avltree_get_gt(AVLTree *t, long v, void **item);
AVLTree *avltree_get_eq(AVLTree *t, long v, void **item);
AVLTree *avltree_get_gt_or_eq(AVLTree *t, long v, void **item);
AVLTree *avltree_get_lt_or_eq(AVLTree *t, long v, void **item);

void avltree_map_inorder   (AVLTree *t, void (*func)(void *));
void avltree_map_preorder  (AVLTree *t, void (*func)(void *));
void avltree_map_postorder (AVLTree *t, void (*func)(void *));
void avltree_map_range     (AVLTree *t, long start, long end, void (*func)(void*));

void avltree_free(AVLTree *t);

#endif /* _AVLTREE_H */

