/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2001
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Replaces old callback system.
 *
 *  Last Updated
 *    9th March 2000
 *
 */

#include <stdlib.h>
#include <stdio.h>

#include <libced/hash/strhash.h>

#include "callbacks.h"

static void callbacks_dummy(){}

callbacks *callbacks_new(char *class_name)
{
    callbacks *cbacks;

    cbacks = (callbacks*)calloc(1, sizeof(callbacks));
    cbacks->cname = class_name;
    cbacks->cbs = sh_new(64, 0);

    return(cbacks);
}

void callbacks_free(callbacks *cbacks)
{
    sh_free(cbacks->cbs);

    free(cbacks);
}

void callbacks_add(callbacks *cbacks, char *name, void *f)
{
    sh_set(cbacks->cbs, name, (void*)f);
    callbacks_set(cbacks, name, callbacks_dummy);
}

void callbacks_remove(callbacks *cbacks, char *name)
{
    sh_del(cbacks->cbs, name);
}

void callbacks_reset(callbacks *cbacks, char *name)
{
    callbacks_set(cbacks, name, callbacks_dummy);
}

void callbacks_set(callbacks *cbacks, char *name, void (*func)())
{
    void **f;

    f = (void**)sh_get(cbacks->cbs, name);

    if(f) {
        *f = func;
    } else {
        printf("[callbacks] callbacks_set[%s]: no such callback '%s'\n", cbacks->cname, name);
    }
}


