#ifndef _CALLBACKS_H
#define _CALLBACKS_H

#define CALLBACK_DEF(x) void (*x)
#define CALLBACK_REF(x) (void*)&x

#include <libced/hash/strhash.h>

typedef struct _callbacks callbacks;

struct _callbacks
{
    char    *cname;
    strhash *cbs;
};

callbacks  *callbacks_new    (char *class_name);
void        callbacks_free   (callbacks *cbacks);
void        callbacks_add    (callbacks *cbacks, char *name, void *f);
void        callbacks_set    (callbacks *cbacks, char *name, void (*func)());
void        callbacks_reset  (callbacks *cbacks, char *name);
void        callbacks_remove (callbacks *cbacks, char *name);

#endif /* _CALLBACKS_H */

