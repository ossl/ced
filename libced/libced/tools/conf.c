/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Configuration system.
 *
 *  Last Updated
 *    22nd August 2000
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <libced/utils/utils.h>
#include <libced/hash/strhash.h>
#include <libced/tools/structs.h>

#include "conf.h"

typedef struct _ptr_array PARAMS;

static int  conf_readline(int fd, char *buf, int buflen);
static char *conf_parse_wordlist(char *s, PARAMS *prms);
static char *conf_parse_word(char *s, char **buf, char *delim);

config *conf_new(char *fname)
{
    config *conf = calloc(1, sizeof(config));
    conf->fname = strdup(fname);
    return(conf);
}

/*
 * Parse the configuration, using the file descripter
 * fd as the input.  Prolly changed this bit soon.
 */

void conf_parse(config *conf)
{
    char     buf[1024];
    char     *s;
    char     *word;
    strhash  *labels;
    strhash  *headers;
    PARAMS   prms;
    int      fd;

    headers = sh_new(16, 0);
    labels  = sh_new(16, 0);

    conf->labels = labels;
    conf->sections = headers;

    if((fd = open(conf->fname, O_RDONLY)) < 0) {
        printf("[conf] can't open file: %s\n", conf->fname);
        return;
    }

    while(conf_readline(fd, buf, 1024) != -1) {
        s = buf;
        s = str_skip_white(s);

        if(isalpha(*s)) {
            s = conf_parse_word(s, &word, " \t=");
            s = str_skip_white(s);

            if(*s == '=') {
                s = str_skip_white(s + 1);

                ptr_array_init(&prms);

                do {
                    s = conf_parse_wordlist(s, &prms);

                    if(*s == '\\' && conf_readline(fd, buf, 1024) != -1) {
                        s = buf;
                    }
                } while(*s);

                sh_set(labels, word, (void*)prms.data);
            }

            free(word);
        } else if(*s == '[') {
            s++;
            s = conf_parse_word(s, &word, "]");
            labels = sh_new(16, 0);
            sh_set(headers, word, (void*)labels);
            free(word);
        }
    }

    close(fd);
} 

void conf_free_labels(strhash *section)
{
    int i;
    int j;
    PARRAY *sect_labels;
    char **l_vals;

    sect_labels = sh_get_values(section);

    for(i=0; i < sect_labels->items; i++) {
        if((l_vals = (char**)sect_labels->data[i]) != NULL) {
            for(j=0 ; l_vals[j] != NULL; j++) {
                free((char*)l_vals[j]);
            }

            free(l_vals);
        }
    }

    ptr_array_free_data(sect_labels);
    free(sect_labels);
    sh_free(section);
}

void conf_free(config *conf)
{
    PARRAY *h_vals;
    int i;

    /* Clear labels under the root section */

    conf_free_labels(conf->labels);

    /* Go through each section and clear its labels */

    h_vals = sh_get_values(conf->sections);

    for(i=0; i < h_vals->items; i++) {
        conf_free_labels((strhash*)h_vals->data[i]);
    }

    ptr_array_free_data(h_vals);

    free(h_vals);

    sh_free(conf->sections);

    free(conf->fname);
    free(conf);
}

/*
 * Parses the string s, using a backslash as a escape
 * character (if included in the delims list).  If an
 * unescaped escape character is the last character in
 * s, s is returned at that point.
 */

char *conf_parse_word(char *s, char **buf, char *delims)
{
    uint len = 0;
    char *start = s;

    while(*s) {
        if(strchr(delims, *s)) break;

        if(s[0] == '\\') {
            if(s[1] == '\0') abort();
            s++;
        }

        s++;
    }

    len = s - start;

    *buf = (char*)calloc(1, len+1);
    memcpy(*buf, start, len);
    str_unescape(*buf);

    return(s);
}

char *conf_parse_wordlist(char *s, PARAMS *prms)
{
    uint     i;
    char     *word;

    s = str_skip_white(s);

    for(i=0 ; *s ; i++) {
        s = conf_parse_word(s, &word, " \t");
        s = str_skip_white(s);

        ptr_array_add(prms, (void*)word);

        if(*s == '\\' && s[1] == '\0') {
            break;
        }
    }

    return(s);
}

/*
 * Get the set of parameters from the section name and
 * label name given.  Returns a null terminated array
 * of characters.
 */

char **conf_params(config *conf, char *sect_name, char *lab_name)
{
    strhash     *labels;
    char        **params;
    static char *empty[1];

    empty[0] = NULL;
    params   = NULL;

    if(!sect_name) {
        params = (char**)sh_get(conf->labels, lab_name);
    } else {
        labels = (strhash*)sh_get(conf->sections, sect_name);

        if(labels) {
            params = (char**)sh_get(labels, lab_name);
        }
    }

    return(params);
}

/*
 * Read the next line from the configuration
 * file into buf.  Returns number of characters
 * read.  Returns -1 at EOF.
 */

int conf_readline(int fd, char *buf, int buflen)
{
    int  i = 0;
    int  r = 0;
    char ch = EOF;
    
    while((r = read(fd, &ch, 1))) {
        if(i == buflen) break;
     
        if(ch != 13) {
            if(ch == 10) break;
            buf[i] = ch;
            i++;
        }
    }

    buf[i] = '\0';
    if(r==0) return(-1);
    return(i);
}

