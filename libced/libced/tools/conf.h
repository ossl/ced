#ifndef _CONF_H
#define _CONF_H

#include <libced/hash/strhash.h>

typedef struct config config;
typedef struct section section;
typedef struct label label;

struct config
{
  char    *fname;
  strhash *labels;
  strhash *sections;
};

struct section
{
  char     *name;
  label   **labels;
};

struct label
{
  char     *name;
  char    **params;
};

config *conf_new(char *fname);
char   **conf_params(config *conf, char *sect_name, char *lab_name);
void   conf_parse(config *conf);

#endif /* _CONF_H */

