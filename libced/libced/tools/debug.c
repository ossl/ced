/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *
 *  Created
 *    26th March 2003 - MAS
 *  
 *  Last Updated
 *    26th March 2003 - MAS
 *
 */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#include "debug.h"

#define DEBUG_PREFIX "libced <"
#define DEBUG_BUFFER_SIZE 4096
#define DEBUG_PIPE stderr

void DEBUG_DO_ASSERT(int condition)
{
    if(!condition)
    {
        fprintf(stderr, "ASSERTION FAILED, Aborting.");

        abort();
    }
}

void DEBUG_PRINT(DEBUG_MSG_TYPE mtype, const char* fmt, ...)
{
    char buf[DEBUG_BUFFER_SIZE];
    char *m = "";

    va_list ap;
    va_start(ap, fmt);

    switch(mtype)
    {
        case D_RAW    : m = ""; break;
        case D_MSG    : m = "message> "; break;
        case D_ASSERT : m = "assert> "; break;
        case D_WARN   : m = "warning> "; break;
        case D_ERROR  : m = "error> "; break;
        case D_FATAL  : m = "fatal> "; break;
    }

    vsnprintf(buf, DEBUG_BUFFER_SIZE, fmt, ap);

    if(mtype != D_RAW)
    {
        fprintf(DEBUG_PIPE, DEBUG_PREFIX);
        fprintf(DEBUG_PIPE, m);
    }

    fprintf(DEBUG_PIPE, buf);

    if(mtype != D_RAW)
    {
        fprintf(DEBUG_PIPE, "\n");
    }

    fflush(DEBUG_PIPE);

    va_end(ap);

    if(mtype == D_FATAL)
    {
        fprintf(DEBUG_PIPE, DEBUG_PREFIX);
        fprintf(DEBUG_PIPE, m);
        fprintf(DEBUG_PIPE, "Fatal error, Aborting.\n");

        abort();
    }
}

