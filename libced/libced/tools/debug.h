#ifndef _DEBUG_H
#define _DEBUG_H

typedef enum _DEBUG_MSG_TYPE
{
    D_RAW,
    D_MSG,
    D_ASSERT,
    D_WARN,
    D_ERROR,
    D_FATAL,
} DEBUG_MSG_TYPE;

#ifdef DEBUG
    #define TRACE DEBUG_PRINT
#else
    #define TRACE(...)
#endif

#ifdef DEBUG_ASSERT
    #define ASSERT DEBUG_DO_ASSERT
#else
    #define ASSERT(...)
#endif


void DEBUG_PRINT(DEBUG_MSG_TYPE mtype, const char* fmt, ...);
void DEBUG_DO_ASSERT(int condition);

#endif

