/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    A dstring is a string that contains two internal position pointers,
 *    and functions that perform operations at the current internal cursor
 *    position, or between the two pointers.  This makes text manipulation
 *    very easy and safe.
 *
 *  Last Updated
 *    10/Aug/2001:
 *        MAS - Updates and fixes
 *
 *    30/Mar/2003:
 *        MAS - Renamed original dstring_goto_col function to dstring_goto_x
 *        MAS - Rewrote dstring_expand_tabs
 *        MAS - Added dstring_goto_col
 *        MAS - Added dstring_get_x_from_col
 *        MAS - Added dstring_get_col_from_x
 *        MAS - Added dstring_define_region_at_col
 *        MAS - Added dstring_length_cols
 *
 *    04/Apr/2003:
 *        MAS - Fixed dstring_expand_tabs
 *
 *    25/May/2003:
 *        MAS - Renamed library from "line" to "dstring"
 *        MAS - Renamed functions and methods appropriately
 *        MAS - Renamed dstring_goto_col to dstring_cursor_goto_col
 *        MAS - Renamed dstring_goto_x to dstring_cursor_goto_x
 *        MAS - Renamed dstring_arrow_left to dstring_cursor_left
 *        MAS - Renamed dstring_arrow_right to dstring_cursor_right
 *        MAS - Renamed dstring_to_left to dstring_cursor_to_left
 *        MAS - Renamed dstring_to_right to dstring_cursor_to_right
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dstring.h"

/**********************************************************************************************
 * Create a new dstring object and initialise it.
 **********************************************************************************************/

inline dstring *dstring_new()
{
    dstring *str = malloc(sizeof(dstring));
    dstring_init(str);

    return(str);
}

/**********************************************************************************************
 * Initialise the dstring object and reset it.
 **********************************************************************************************/

inline void dstring_init(dstring *str)
{
    str->data = calloc(DSTRING_SEGSIZE, sizeof(char));
    str->res  = DSTRING_SEGSIZE;
    str->used = 0; 

    dstring_reset(str);
}

/**********************************************************************************************
 * Reset the dstring object.  The cursor and region positions are reset to 0, and the data
 * is blanked.  The reserved amount is not changed.
 **********************************************************************************************/

inline void dstring_reset(dstring *str)
{
    str->data[0] = '\0';
    str->used    = 0; 
    str->rpos    = 0;
    str->xpos    = 0;
}

/**********************************************************************************************
 * Create a new dstring object using the passed in character array as the initial data.
 **********************************************************************************************/

inline dstring *dstring_new_with_string(char *str)
{
    dstring *dnew;

    if((dnew = dstring_new()) != NULL)
    {
        dstring_insert_string(dnew, str);
    }

    return(dnew);
}

/**********************************************************************************************
 * Internal cursor movement functions
 **********************************************************************************************/

inline void dstring_cursor_to_right(dstring *str)
{
    str->xpos = max(0, str->used);
}

inline void dstring_cursor_to_left(dstring *str)
{
    str->xpos = 0;
}

inline void dstring_cursor_left(dstring *str, long n)
{
    str->xpos -= min(str->xpos, n);
}

inline void dstring_cursor_right(dstring *str, long n)
{
    str->xpos += n;
}

inline void dstring_cursor_goto_x(dstring *str, long n)
{
    str->xpos = n;
}

inline void dstring_cursor_goto_col(dstring *str, long col, uint tabstops, DSTRING_TAB_ALIGN ta)
{
    str->xpos = dstring_map_to_x(str, col, tabstops, ta);
}

/**********************************************************************************************
 * Strip out non-printable characters
 **********************************************************************************************/

inline long dstring_strip(dstring *ln)
{
    long i;
    long j;
    char *tmp;

    if(ln->used > 0)
    {
        tmp = malloc((size_t)ln->used);

        for(i=0, j=0; i < ln->used; i++)
        {
            if((ln->data[i] & 96) || ln->data[i] == 9)
            {
                tmp[j++] = ln->data[i];
            }
        }

        for(i = j - 1 ; i < j; i++)
        {
            ln->data[i] = tmp[i];
        }

        ln->used = j;
        ln->data[j] = '\0';

        free(tmp);
    }

    return(1);
}

/**********************************************************************************************
 * Free the dstringing
 **********************************************************************************************/

inline void dstring_free(dstring *ln)
{
    free(ln->data);
    free(ln);
}

/**********************************************************************************************
 * Delete the character at the current cursor position
 **********************************************************************************************/

inline void dstring_delete_char(dstring *ln)
{
    long xpos = ln->xpos;

    if(xpos < ln->used) {
        memmove(&ln->data[xpos], &ln->data[xpos+1], (ln->used-xpos));
        ln->used--;
    }
}

/**********************************************************************************************
 * Move the internal cursor back one position, and then delete
 **********************************************************************************************/

inline void dstring_erase_char(dstring *ln)
{
    dstring_cursor_left(ln, 1);
    dstring_delete_char(ln);
}

/**********************************************************************************************
 * Get the character at the current internal cursor position
 **********************************************************************************************/

inline int dstring_get_char(dstring *ln)
{
    int ch = EOF;

    if(ln->xpos < ln->used) {
        ch = ln->data[ln->xpos];
        ln->xpos++;
    }

    return(ch);
}

inline void dstring_insert_string_at(dstring *ln, long xpos, char *s)
{
    dstring_insert_raw_at(ln, xpos, s, strlen(s));
}

inline void dstring_insert_string(dstring *ln, char *s)
{
    dstring_insert_raw(ln, s, strlen(s));
}

inline void dstring_insert_char(dstring *ln, char c)
{
    dstring_insert_raw(ln, &c, 1);
}

inline void dstring_insert_char_at(dstring *ln, long xpos, char c)
{
    dstring_insert_raw_at(ln, xpos, &c, 1);
}

inline void dstring_insert_raw(dstring *ln, char *s, long len)
{
    dstring_insert_raw_at(ln, ln->xpos, s, len);
    ln->xpos += len;
}

inline void dstring_insert_raw_at(dstring *ln, long xpos, char *s, long len)
{
    long newlen;

    if(!s)
    {
        return;
    }

    newlen = max(xpos, ln->used) + len;

    if(newlen >= ln->res)
    {
        ln->data = realloc(ln->data, newlen + DSTRING_SEGSIZE);
        ln->res  = newlen + DSTRING_SEGSIZE;
    }

    if(xpos > ln->used) {
        memset(&ln->data[ln->used], ' ', xpos - ln->used);
        ln->used = ln->xpos;
    }

    memmove(&ln->data[xpos + len], &ln->data[xpos], ln->used - xpos);
    memcpy(&ln->data[xpos], s, len);

    ln->data[newlen] = '\0';
    ln->used += len;
}

inline void dstring_insert_padding(dstring *ln, long n)
{
    if(n)
    {
        dstring_insert_padding_at(ln, ln->xpos, n);
        ln->xpos += n;
    }
}

inline void dstring_insert_padding_at(dstring *ln, long xpos, long n)
{
    char *pad;

    if(n) {
        pad = alloca(n + 1);
        memset(pad, ' ', n);
        pad[n] = '\0';

        dstring_insert_raw_at(ln, xpos, pad, n);
    }
}

inline void dstring_pad(dstring *ln, char pad)
{
    dstring_pad_to(ln, ln->xpos, pad);
}

inline void  dstring_pad_to(dstring *ln, long xpos, char pad)
{
    if(xpos > ln->used)
    {
        if(xpos >= ln->res)
        {
            ln->data = realloc(ln->data, xpos + DSTRING_SEGSIZE);
            ln->res = xpos + DSTRING_SEGSIZE;
        }

        memset(&ln->data[ln->used], pad, xpos - ln->used);

        ln->used = xpos;
    }
}

dstring *dstring_dup(dstring *ln)
{
    dstring *lnew = NULL;

    if(ln) {
        lnew = dstring_new();
        dstring_insert(lnew, ln);
    }

    return(lnew);
}

dstring *dstring_region_copy(dstring *ln, dstring *buf)
{
    long st   = min(min(ln->xpos, ln->rpos), ln->used);
    long end  = min(max(ln->xpos, ln->rpos), ln->used);
    long plen = end-st;

    if(buf) {
        dstring_reset(buf);

        if(plen) {
            dstring_insert_raw(buf, &ln->data[st], plen);
        }
    }

    return(buf);
}

dstring *dstring_region_cut(dstring *ln, dstring *buf)
{
    dstring_region_copy(ln, buf);
    dstring_region_delete(ln);

    return(buf);
}

void dstring_region_delete(dstring *ln)
{
    long st   = min(min(ln->xpos, ln->rpos), ln->used);
    long end  = min(max(ln->xpos, ln->rpos), ln->used);

    if(st <= ln->used) {
        memmove(&ln->data[st], &ln->data[end], (ln->used-end));
        ln->xpos = st;
        ln->used -= (end-st);
    }
}

void dstring_region_invert_case(dstring *ln)
{
    long  st   = min(min(ln->xpos, ln->rpos), ln->used);
    long  end  = min(max(ln->xpos, ln->rpos), ln->used);
    char  *data;

    if(st <= ln->used) {
        data = ln->data;

        for(; st <= end; st++) {
            if(data[st] >= 'A' && data[st] <= 'Z') {
                data[st] += 32;
            } else if(data[st] >= 'a' && data[st] <= 'z') {
                data[st] -= 32;
            }
        }
    }
}

void dstring_region_to_upper(dstring *ln)
{
    long  st   = min(min(ln->xpos, ln->rpos), ln->used);
    long  end  = min(max(ln->xpos, ln->rpos), ln->used);
    char  *data;

    if(st <= ln->used) {
        data = ln->data;

        for(; st <= end; st++) {
            if(data[st] >= 'a' && data[st] <= 'z') data[st] -= 32;
        }
    }
}

void dstring_region_to_lower(dstring *ln)
{
    long  st   = min(min(ln->xpos, ln->rpos), ln->used);
    long  end  = min(max(ln->xpos, ln->rpos), ln->used);
    char  *data;

    if(st <= ln->used) {
        data = ln->data;

        for(; st <= end; st++) {
            if(data[st] >= 'A' && data[st] <= 'Z') data[st] += 32;
        }
    }
}

void dstring_region_expand_tabs(dstring *ln, uint tabstops, int tabchar)
{
    char *pad = (char*)alloca(tabstops*sizeof(char));
    long  end = min(max(ln->xpos, ln->rpos), ln->used);
    long  st  = min(min(ln->xpos, ln->rpos), ln->used);
    long  i;
    long  t;
    long  p;

    memset(pad, (char)tabchar, tabstops);

    for(i=0, p=0; i < st; i++) {
        if( ln->data[i] == '\t') {
            p += (tabstops - ((i+p) % tabstops) - 1);
        }
    }

    for( ; i < end; i++) {
        if( ln->data[i] == '\t') {
            ln->data[i] = (char)tabchar;

            if((t = tabstops - ((i+p) % tabstops) - 1) > 0) {
                dstring_insert_raw_at(ln, i + 1, pad, t);

                if(ln->xpos > i) ln->xpos += t;
                if(ln->rpos > i) ln->rpos += t;

                end += t;
                i   += t;
            }
        }
    }
}

void dstring_expand_tabs(dstring *ln, uint tabstops, int tabchar)
{
    long i;
    long t = 0;
    char *ptr;
    char *pad = (char*)alloca(tabstops*sizeof(char));

    memset(pad, (char)tabchar, tabstops);

    ptr = ln->data;

    while((ptr = strchr(ptr, '\t')) != NULL)
    {
        if((i = ptr - ln->data) >= ln->used)
        {
            break;
        }                       	

        ln->data[i] = (char)tabchar;

        if(tabstops > 0 && (t = tabstops - (i % tabstops) - 1) > 0)
        {
            dstring_insert_raw_at(ln, i + 1, pad, t);
        }

        ptr = &ln->data[i + t + 1];
    }
}

inline long dstring_get_length(dstring *ln)
{
    return(ln->used);
}

inline char *dstring_get_string(dstring *ln)
{
    char *st;
    long len;

    len = dstring_get_length(ln);
    st = calloc(1, len + 1);
    memcpy(st, ln->data, len);

    return(st);
}

inline char *dstring_get_string2(dstring *ln, char *buf, ulong bsize)
{
    memset(buf, 0, bsize);
    memcpy(buf, ln->data, min(dstring_get_length(ln), bsize - 1));

    return(buf);
}

inline long dstring_find_string(dstring *ln, char *str)
{
    long xpos = ln->xpos;
    long st = min(xpos, ln->used);
    long end = ln->used;
    long flen = strlen(str);
    long i; 

    if(end < flen)
    {
        return(0);
    }

    for(i=st; i <= max(0,(end - flen)); i++)
    {
        if(!memcmp(str, &ln->data[i], flen))
        {
            ln->xpos = i;
            return(1);
        }
    }

    return(0);
}

inline long dstring_find_string_rev(dstring *ln, char *str)
{
    long xpos = ln->xpos;
    long st = min(xpos, ln->used);
    long end = ln->used;
    long flen = strlen(str);
    long i; 

    if(end < flen) return(0);

    for(i=st; i >= 0; i--) {
        if(!memcmp(str, &ln->data[i], flen)) {
            ln->xpos = i;
            return(1);
        }
    }

    return(0);
}

LCDATA *dstring_get_column_data(dstring *ln, LCDATA *lcd, long col, uint tabstops)
{
    char *ptr;
    char *buf;
    long xpos;
    long tab = 0;
    long tw;

    memset(lcd, 0, sizeof(LCDATA));

    lcd->col = col;

    ptr = buf = ln->data;

    // Scan the dstring for tab characters so we can compensate for them and find
    // the actual x position.

    while((ptr = strchr(ptr, '\t')) != NULL)
    {
        // If the tab character found is beyond the column position, then we've
        // done compensating for tab characters, so we exit the loop.

        if((xpos = ptr - buf) > col)
        {
            break;
        }

        // Calculate the width of the tab character we've found.  The width
        // will be different depending on where the tab character starts.

        tw = tabstops - (tab + xpos) % tabstops;

        // If the column position is "within" the tab character found, then
        // we set the x position to either the tab character itself, or the
        // character afterward, depending on the passed tab align parameter.

        if( col >= xpos && col < xpos + tw )
        {
            lcd->is_tab = 1;
            lcd->col_a = xpos + tab;
            lcd->col_diff = lcd->col - lcd->col_a;
            lcd->col_next = xpos + tab + tw;
            lcd->xpos = xpos;
            lcd->tab_width = tw;

            break;
        }

        // Compensate for the tab width by taking off the extra width from
        // the column. We accumulate the amount of space we've compensated
        // for in the tab variable so that the next tab width can be
        // calculated correctly.

        tw--;

        tab += tw;
        col -= tw;

        ptr++;
    }

    if( lcd->is_tab == 0 ) {
        lcd->col_a = lcd->col;
        lcd->col_next = lcd->col + 1;
        lcd->col_diff = 0;
        lcd->xpos = col;
    }

    return(lcd);
}

inline void dstring_insert_at(dstring *ln, long xpos, dstring *ins)
{
    dstring_insert_raw_at(ln, xpos, ins->data, ins->used);
}

inline void dstring_insert(dstring *ln, dstring *ins)
{
    dstring_insert_raw(ln, ins->data, ins->used);
}

////////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////

long dstring_get_length_cols(dstring *ln, uint tabstops)
{
    return(dstring_map_to_col(ln, ln->used, tabstops));
}

////////////////////////////////////////////////////////////////////////////////////////
// This function takes an x position and converts it to a column number.
////////////////////////////////////////////////////////////////////////////////////////

long dstring_map_to_col(dstring *ln, long xpos, uint tabstops)
{
    char *buf;
    char *ptr;
    uint tw;
    long tab = 0;
    long x;

    ptr = buf = ln->data;

    // Scan the dstring for tab characters so we can compensate for them and find
    // the actual column position.

    while((ptr = strchr(ptr, '\t')) != NULL)
    {
        // If the tab character found is beyond the column position, then we've
        // done compensating for tab characters, so we exit the loop.

        if((x = ptr - buf) >= xpos)
        {
            break;
        }

        // Calculate the width of the tab character we've found.  The width
        // will be different depending on where the tab character starts.

        tw = tabstops - (tab + x) % tabstops;

        // Accumulate the tab width difference.  We accumulate the amount of
        // space we've compensated for in the tab variable so that the next
        // tab width can be calculated correctly.

        tab += (tw - 1);

        ptr++;
    }

    return(xpos + tab);
}

////////////////////////////////////////////////////////////////////////////////////////
// This function takes a column number and converts it to the x position.
////////////////////////////////////////////////////////////////////////////////////////

long dstring_map_to_x(dstring *ln, long col, uint tabstops, DSTRING_TAB_ALIGN ta)
{
    char *ptr;
    char *buf;
    long xpos;
    long tab = 0;
    long tw;

    ptr = buf = ln->data;

    // Scan the dstring for tab characters so we can compensate for them and find
    // the actual x position.

    while((ptr = strchr(ptr, '\t')) != NULL)
    {
        // If the tab character found is beyond the column position, then we've
        // done compensating for tab characters, so we exit the loop.

        if((xpos = ptr - buf) >= col)
        {
            break;
        }

        // Calculate the width of the tab character we've found.  The width
        // will be different depending on where the tab character starts.

        tw = tabstops - (tab + xpos) % tabstops;

        // If the column position is "within" the tab character found, then
        // we set the x position to either the tab character itself, or the
        // character afterward, depending on the passed tab align parameter.

        if( col >= xpos && col <= xpos + tw ) {
            col = (ta == DSTRING_TAB_START) ? xpos : xpos + 1;

            break;
        }

        // Compensate for the tab width by taking off the extra width from
        // the column. We accumulate the amount of space we've compensated
        // for in the tab variable so that the next tab width can be
        // calculated correctly.

        tw--;

        tab += tw;
        col -= tw;

        ptr++;
    }

    // The column position is now the xposition, so we update the objects
    // x position accordingly.

    return(col);
}

inline void dstring_define_region_at(dstring *ln, long xpos)
{
    ln->rpos = xpos;
}

inline void dstring_define_region_at_col(dstring *ln, long col, uint tabstops, DSTRING_TAB_ALIGN ta)
{
    ln->rpos = dstring_map_to_x(ln, col, tabstops, ta);
}

inline void dstring_define_region(dstring *ln)
{
    ln->rpos = ln->xpos;
}

void dstring_define_all(dstring *ln)
{
    dstring_cursor_to_left(ln);
    dstring_define_region(ln);
    dstring_cursor_to_right(ln);
}

void dstring_substitute(dstring *ln, dstring *from, dstring *to)
{
    long xpos = ln->xpos;
    long rpos = ln->rpos;
    long st = min(min(xpos, rpos),ln->used);
    long end = min(max(xpos, rpos),ln->used);
    long flen = from->used;
    long tlen = to->used;
    long movelen;
    long i;

    if(flen == 0)
    {
        return;
    }

    movelen = (tlen < flen) ? tlen : flen;

    for(i=st; i < max(0,(end - flen)); i++)
    {
        if(!memcmp(from->data, &ln->data[i], flen))
        {
            if(tlen != flen)
            {
                memmove(&ln->data[i + tlen], &ln->data[i + flen], ln->used - i - movelen);
            }

            ln->used = ln->used - flen + tlen;
            end = end - flen + tlen;
            memcpy(&ln->data[i], to->data, tlen);
            i += tlen;
        } 
    }
}

long dstring_fputs(dstring *ln, FILE *stream, FILE_TYPE ft, int write_nl)
{
    long len;
    char *nl = "";

    len = fwrite(ln->data, 1, ln->used, stream);

    if(write_nl)
    {
        switch(ft)
        {
            case FT_UNIX : nl = "\n"; break;
            case FT_MAC  : nl = "\r"; break;
            case FT_DOS  : nl = "\r\n"; break;

            default: break;
        }

        len += fwrite(nl, 1, strlen(nl), stream);
    }

    return((len == ln->used + strlen(nl)) ? len : -1);
}

dstring *dstring_fgets(dstring *ln, FILE *stream, FILE_TYPE ft)
{
    char buf[DSTRING_BUF_SIZE];
    int i = 0;
    int ch;

    dstring_reset(ln);

    while((ch = fgetc(stream)) != EOF)
    {
        if((ch == CR && ft == FT_MAC) ||
           (ch == CR && ft == FT_DOS) ||
           (ch == LF && ft != FT_BINARY))
        {
            if(ft == FT_DOS && ch == CR && fgetc(stream) != LF)
            {
                fseek(stream, -1, SEEK_CUR);
            }
            else
            {
                break;
            }
        }
	
        if( i == DSTRING_BUF_SIZE ) {
            i = 0;

            dstring_insert_raw(ln, buf, DSTRING_BUF_SIZE);
        }

        buf[i++] = ch;
    }

    if(i > 0)
    {
        dstring_insert_raw(ln, buf, i);
    }

    return(ln);
}
