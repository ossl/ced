#ifndef _DSTRING_H
#define _DSTRING_H

#include <stdio.h>
#include <libced/common.h>

#define DSTRING_SEGSIZE 32
#define DSTRING_BUF_SIZE 4096

typedef enum _DSTRING_TAB_ALIGN
{
    DSTRING_TAB_START,
    DSTRING_TAB_END
}   DSTRING_TAB_ALIGN;

typedef struct _dstring dstring;
typedef struct _dstring_column_data LCDATA;

struct _dstring
{
    long   rpos;
    long   xpos;
    long   res;
    long   used;
    char   *data;
};

struct _dstring_column_data
{
    long col;       // requested column
    long col_a;     // aligned column
    long col_diff;  // difference between requested and aligned
    long col_next;  // next column
    long xpos;      // character position
    int  tab_width; // tab width
    int  is_tab;    // is start of or inside a tab
};

// Class settings


/*****************************************************************************/

inline dstring *dstring_new            (void);
inline dstring *dstring_new_with_string(char *str);

/*****************************************************************************/

inline void  dstring_free              (dstring *ln);
inline void  dstring_init              (dstring *ln);
inline void  dstring_reset             (dstring *ln);

/*****************************************************************************/

inline void  dstring_insert            (dstring *ln, dstring *s);
inline void  dstring_insert_char       (dstring *ln, char c);
inline void  dstring_insert_raw        (dstring *ln, char *s, long len);
inline void  dstring_insert_string     (dstring *ln, char *s);
inline void  dstring_insert_padding    (dstring *ln, long n);

inline void  dstring_insert_at         (dstring *ln, long xpos, dstring *ins);
inline void  dstring_insert_char_at    (dstring *ln, long xpos, char c);
inline void  dstring_insert_raw_at     (dstring *ln, long xpos, char *s, long n);
inline void  dstring_insert_string_at  (dstring *ln, long xpos, char *s);
inline void  dstring_insert_padding_at (dstring *ln, long xpos, long n);

inline void  dstring_insert_at_col     (dstring *ln, long col, dstring *lins, uint tabstops);

/*****************************************************************************/

inline void  dstring_pad               (dstring *ln, char pad);
inline void  dstring_pad_to            (dstring *ln, long xpos, char pad);

/*****************************************************************************/

inline long  dstring_find_string       (dstring *ln, char *str);

/*****************************************************************************/

inline void  dstring_cursor_to_left    (dstring *ln);
inline void  dstring_cursor_to_right   (dstring *ln);
inline void  dstring_cursor_left       (dstring *ln, long n);
inline void  dstring_cursor_right      (dstring *ln, long n);
inline void  dstring_cursor_goto_x     (dstring *ln, long n);
inline void  dstring_cursor_goto_col   (dstring *ln, long col, uint tabstops, DSTRING_TAB_ALIGN ta);

/*****************************************************************************/

inline long  dstring_get_cursor_pos    (dstring *ln);
inline long  dstring_get_region_pos    (dstring *ln);
inline long  dstring_get_length        (dstring *ln);
inline long  dstring_get_length_cols   (dstring *ln, uint tabstops);
inline int   dstring_get_char          (dstring *ln);
inline char *dstring_get_string        (dstring *ln);
inline char *dstring_get_string2       (dstring *ln, char *buf, ulong bsize);

/*****************************************************************************/

inline void  dstring_erase             (dstring *ln);
inline void  dstring_delete            (dstring *ln);

/*****************************************************************************/

inline void  dstring_define_region     (dstring *ln);
inline void  dstring_define_region_at  (dstring *ln, long xpos);
inline void  dstring_define_region_at_col(dstring *ln, long col, uint tabstops, DSTRING_TAB_ALIGN ta);

/*****************************************************************************/

inline void  dstring_region_delete      (dstring *ln);
inline void  dstring_region_expand_tabs (dstring *ln, uint tabstops, int tabchar);
inline void  dstring_region_invert_case (dstring *ln);
inline void  dstring_region_to_upper    (dstring *ln);
inline void  dstring_region_to_lower    (dstring *ln);
inline dstring *dstring_region_copy     (dstring *ln, dstring *buf);
inline dstring *dstring_region_cut      (dstring *ln, dstring *buf);

/*****************************************************************************/

dstring *dstring_dup                    (dstring *ln);
void dstring_define_all                 (dstring *ln);
void dstring_expand_tabs                (dstring *ln, uint tabstops, int tabchar);
void dstring_transliterate              (dstring *ln, dstring *from, dstring *to);
void dstring_substitute                 (dstring *ln, dstring *from, dstring *to);

/*****************************************************************************/

long dstring_map_to_x   (dstring *ln, long col , uint tabstops, DSTRING_TAB_ALIGN ta);
long dstring_map_to_col (dstring *ln, long xpos, uint tabstops);

/*****************************************************************************/

dstring *dstring_fgets(dstring *ln, FILE *stream, FILE_TYPE ft);
long     dstring_fputs(dstring *ln, FILE *stream, FILE_TYPE ft, int write_nl);

/*****************************************************************************/

long dstring_strip(dstring *ln);

/*****************************************************************************/

LCDATA *dstring_get_column_data(dstring *ln, LCDATA *lcd, long col, uint tabstops);

/*****************************************************************************/

#endif /* _DSTRING_H */

