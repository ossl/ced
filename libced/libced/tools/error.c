/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999, 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *
 *  Last Updated
 *    16th December 2000
 *
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "config.h"

/*
 * Common file open problems that we can put into a
 * string for creating meaningful error messages :)
 */

char *error_get_str(int err)
{
#if HAVE_STRERROR
    return(strerror(err));
#else
    switch(err) {
      case 0:       return("Success."); break;
      case EACCES : return("Permission denied."); break;
      case EAGAIN : return("File locked."); break;
      case EDQUOT : return("Disk quota exceeded."); break;
      case EINTR  : return("Open interrupted."); break;
      case EISDIR : return("Specified file is a directory."); break;
      case ELOOP  : return("Too many symbolic links."); break;
      case ENOENT : return("No such file or directory."); break;
      default     : return("Unknown I/O error."); break;
    }
#endif
}


