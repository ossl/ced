/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999, 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A NARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *
 *  Last Updated
 *    20th April 2001
 *
 */

#include <stdlib.h>
#include <string.h>

#include <libced/utils/utils.h>

#include "structs.h"

#define PTRSIZE sizeof(long)
#define NA_MIN  8
#define NA_MULT 2

void num_array_init(NARRAY *na)
{
    memset(na, 0, sizeof(NARRAY));
}

void num_array_free_data(NARRAY *na)
{
    if(na && na->size) {
        free(na->data);
        na->data = NULL;
        na->size = 0;
        na->items = 0;
    }
}

void num_array_add(NARRAY *na, long num)
{
    if(!na->size) {
        na->size = NA_MIN;
        na->data = (long*)calloc(na->size+1, PTRSIZE);
    } else if(na->size == na->items) {
        na->size *= 2;
        na->data = (long*)realloc(na->data, PTRSIZE * (na->size+1));
    }

    na->data[na->items] = num;
    na->items++;
}

void num_array_ins(NARRAY *na, ulong n, long num)
{
    ulong mx;

    mx = largest(NA_MIN, na->items, n);

    if(!na->size) {
        na->size = mx * 2;
        na->data = (long*)calloc(na->size + 1, PTRSIZE);
    } else if(mx >= na->size) {
        na->size = mx * 2;
        na->data = (long*)realloc(na->data, PTRSIZE * (na->size+1));
    }

    if(n < na->items) {
        memmove(&na->data[n+1], &na->data[n], PTRSIZE * (na->items-n));
    } else if(n > na->items) {
        memset(&na->data[na->items], 0, PTRSIZE * (n-na->items));
        na->items = n;
    }

    na->data[n] = num;
    na->items++;
}

void num_array_del(NARRAY *na, ulong n)
{
    if(n < na->items) {
        memmove(&na->data[n], &na->data[n+1], PTRSIZE * (na->items-n));
        na->items--;
    }
}

uint num_array_lookup(NARRAY *na, long num, ulong *n)
{
    ulong i;

    for(i=0; i < na->items; i++) {
        if(na->data[i] == num) {
            if(n) *n = i;
            return(1);
        }
    }

    return(0);
}

