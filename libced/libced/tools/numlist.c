/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    A binary tree that contains numbers.  It allows looking up numbers or
 *    adding a number to the numbers in the tree that are greater than, less
 *    than or equal to a given number.  This library is used for syntax
 *    highlighting.
 *
 *  Last Updated
 *    26th August 2000
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include <libced/common.h>

#include "numlist.h"

//#define DEBUG

nlist_in *numlist_in_find_equal(nlist_in *ni, ulong num);
nlist_in *numlist_in_find_nearest_less(nlist_in *ni, ulong num);
nlist_in *numlist_in_find_nearest_leq(nlist_in *ni, ulong num);
nlist_in *numlist_in_find_nearest_meq(nlist_in *ni, ulong num);
nlist_in *numlist_in_find(nlist_in *ni, ulong num);
nlist_in *numlist_in_balance( nlist_in *tree );
void      numlist_in_print(nlist_in *ni);
void      numlist_in_add_to_more(nlist_in *ni, ulong num, long amount);
void      numlist_in_add_to_meq(nlist_in *ni, ulong num, long amount);
void      numlist_balance(nlist *nl);
void      numlist_print(nlist *nl);
void      numlist_in_free(nlist_in *ni);
int       numlist_in_maxdepth( const nlist_in *tree );

nlist *numlist_new()
{
    nlist *nl = calloc(1, sizeof(nlist));
    nl->head = NULL;
    nl->nent = 0;
    return(nl);
}

void numlist_free(nlist *nl)
{
    numlist_in_free(nl->head);

    free(nl);
}

void numlist_in_free(nlist_in *ni)
{
    if(ni == NULL) return;

    numlist_in_free(ni->less);
    numlist_in_free(ni->more);

    free(ni);
}

nlist_in *numlist_in_find_equal(nlist_in *ni, ulong num)
{
    while(ni) {
        if(ni->num < num) {
            ni = ni->more;
        } else if(ni->num > num) {
            ni = ni->less;
        } else if(ni->num == num) {
            return(ni);
        }
    }

    return(NULL);
}

nlist_in *numlist_in_find_nearest_less(nlist_in *ni, ulong num)
{
    nlist_in *nearest = NULL;

    while(ni) {
        if(ni->num < num) {
            nearest = ni;
            ni = ni->more;
        } else if(ni->num >= num) {
            ni = ni->less;
        }
    }

    return(nearest);
}

nlist_in *numlist_in_find_nearest_leq(nlist_in *ni, ulong num)
{
    nlist_in *nearest = NULL;

    while(ni) {
        if(ni->num <= num) {
            nearest = ni;
            ni = ni->more;
        } else if(ni->num > num) {
            ni = ni->less;
        }
    }

    return(nearest);
}

nlist_in *numlist_in_find_nearest_meq(nlist_in *ni, ulong num)
{
    nlist_in *nearest = NULL;

    while(ni) {
        if(ni->num >= num) {
            nearest = ni;
            ni = ni->less;
        } else if(ni->num < num) {
            ni = ni->more;
        }
    }

    return(nearest);
}

void numlist_add(nlist *nl, ulong num, uint is_last)
{
    nlist_in *new_ni;
    nlist_in *nearest_ni;
    nlist_in *ni;
    static uint adds = 0;


#ifdef DEBUG
    printf("[numlist] add number %lu\n", num);
#endif

    new_ni = calloc(1, sizeof(nlist_in));
    new_ni->num = num;
    new_ni->last = is_last;

    if(nl->head == NULL) {
        nl->head = new_ni;
    } else {
        ni = nl->head;

        while(ni) {
            nearest_ni = ni;

            if(num < ni->num) {
                ni = ni->less;
            } else if(num > ni->num) {
                ni = ni->more;
            } else {
                ni->last = is_last;
                free(new_ni);
                return;
            }
        }

        new_ni->parent = nearest_ni;

        if(num  < nearest_ni->num) nearest_ni->less = new_ni;
        else if(num  > nearest_ni->num) nearest_ni->more = new_ni;

        if(num != nearest_ni->num) {
            if(adds == 10) {
                numlist_balance(nl);
                adds = 0;
            } else {
                adds++;
            }
        }

        else printf("[numlist] oops, shouldn't happen\n");
    }

#ifdef DEBUG
    numlist_print(nl);
#endif
}

void numlist_del(nlist *nl, ulong num)
{
    nlist_in *prev = NULL;
    nlist_in *left;
    nlist_in *right;
    nlist_in *tail;
    nlist_in *repl;
    nlist_in *ni = nl->head;
 
#ifdef DEBUG
    printf("[numlist] delete number %lu\n", num);
#endif

    while(ni) {
        if(ni->num < num) {
            prev = ni;
            ni = ni->more;
        } else if(ni->num > num) {
            prev = ni;
            ni = ni->less;
        } else if(ni->num == num) {
            left  = ni->less;
            right = ni->more;

            if(left == NULL) {
                repl = right;
            } else if(right == NULL) {
                repl = left;
            } else {
                tail = right;
                if(tail) while(tail->less) tail = tail->less;
                tail->less = left;
                repl = right;
            }


            if(repl) repl->parent = prev;

            if(prev) {
                if(prev->less == ni) prev->less = repl;
                if(prev->more == ni) prev->more = repl;
            } else {
                if(repl) repl->parent = NULL;
                nl->head = repl;
            }

#ifdef DEBUG 
            numlist_print(nl);
#endif
            free(ni);

            return;
        }
    }

#ifdef DEBUG 
    printf("[numlist] line couldnt be found for deletion\n");
#endif
}

long numlist_nearest_less(nlist *nl, ulong num)
{
    nlist_in *ni;

    ni = numlist_in_find_nearest_less(nl->head, num);

    if(ni) {
//        return(ni->num);
        return(ni->last);
    }

    return(-1);
}

long numlist_nearest_leq(nlist *nl, ulong num)
{
    nlist_in *ni;

    ni = numlist_in_find_nearest_leq(nl->head, num);

    if(ni) {
        return(ni->num);
    }

    return(-1);
}

long numlist_nearest_meq(nlist *nl, ulong num)
{
    nlist_in *ni;

    ni = numlist_in_find_nearest_meq(nl->head, num);

    if(ni) {
        return(ni->num);
    }

    return(-1);
}

uint numlist_exists(nlist *nl, ulong num)
{
    if(numlist_in_find_equal(nl->head, num)) {
        return(1);
    }

    return(0);
}

void numlist_add_to_leq(nlist *nl, ulong num, long amount)
{
    nlist_in *ni;
    ni = nl->head;

    while(ni) {
        if(ni->num <= num) {
            ni->num += amount;
            ni = ni->more;
        } else if(ni->num > num) {
            ni = ni->less;
        }
    }
}

void numlist_in_add_to_meq(nlist_in *ni, ulong num, long amount)
{
    if(ni) {
        if(ni->num >= num) {
            ni->num += amount;
        }

        numlist_in_add_to_meq(ni->less, num, amount);
        numlist_in_add_to_meq(ni->more, num, amount);
    }
}

void numlist_in_add_to_more(nlist_in *ni, ulong num, long amount)
{
    if(ni) {
        if(ni->num > num) {
            ni->num += amount;
            numlist_in_add_to_more(ni->less, num, amount);
        }

        numlist_in_add_to_more(ni->more, num, amount);
    }
}

void numlist_add_to_meq(nlist *nl, ulong num, long amount)
{
    nlist_in *ni;
    ni = nl->head;

    numlist_in_add_to_meq(ni, num, amount);

#ifdef DEBUG 
    numlist_print(nl);
#endif
}

void numlist_add_to_more(nlist *nl, ulong num, long amount)
{
    nlist_in *ni;
    ni = nl->head;

    numlist_in_add_to_more(ni, num, amount);
}

void numlist_in_print(nlist_in *ni)
{
    if(ni == NULL) {
        printf("N");
        return;
    }

    printf("%lu", ni->num + 1);

    if(!ni->less && !ni->more) return;
    printf("(");

    numlist_in_print(ni->less);
    printf(",");
    numlist_in_print(ni->more);
    printf(")");
}

void numlist_print(nlist *nl)
{
    printf("[numlist] ");
    numlist_in_print(nl->head);
    printf("\n");
}

void numlist_balance(nlist *nl)
{
    nl->head = numlist_in_balance(nl->head);
}


/* The following tree functions are by Dave Fennell */
/* email: unluckypixie@freeserve.co.uk */
/* Recursively gets the maximum tree depth */

int numlist_in_maxdepth( const nlist_in *tree )
{
    int lessDepth;
    int moreDepth;

    /* Check for valid tree */
    if( tree==NULL ) return( 0 );

    /* Calculate the tree depths */
    if( (lessDepth=numlist_in_maxdepth(tree->less)) >= (moreDepth=numlist_in_maxdepth(tree->more)) )
        return( 1 + lessDepth );

    return( 1 + moreDepth );
}
/* Balances the tree */

nlist_in *numlist_in_balance( nlist_in *tree )
{
    int twist;

    nlist_in *ptr, *parent;
    nlist_in *subtree, **treeaddr;
 
    /* If the tree is NULL do nothing */
    if(tree==NULL) return( tree );
 
    /* Check the balance of the tree */
    twist = numlist_in_maxdepth( tree->more ) - numlist_in_maxdepth( tree->less );

    /* If needed rotate less or more */
    if( abs(twist) > 1){
        do{
            if( twist>0 ){ /* Rotate the tree less */
                twist-=2;

                treeaddr    = &(tree->more);
                ptr         = tree->more;
                subtree     = tree->more;

                /* If we have a problem then re-call to recalculate the depths */
                if( ptr == NULL ){
                    twist = 1;
                    continue;
                    }      
 
                /* Find where to attach to */
                while( ptr->less!=NULL )
                    ptr=ptr->less;
      
                /* Attach tree to it */
                ptr->less = tree;
                }

            else{ /* Rotate the tree more */
                twist+=2;

                treeaddr     = &(tree->less);
                ptr         = tree->less;
                subtree     = tree->less;

                /* If we have a problem then re-call to recalculate the depths */
                if( ptr == NULL ){
                    twist = 1;
                    continue;
                    }      

                /* Find where to attach to */
                while( ptr->more!=NULL )
                    ptr=ptr->more;
      
                /* Attach tree to it */
                ptr->more = tree;      
                }

            /* Sort out the parents pointers */
            if( (parent=tree->parent) != NULL ){
                if(parent->less == tree) parent->less=subtree;
                if(parent->more == tree) parent->more=subtree;
                }    
   
            /* Sort out the childs parent */
            (subtree)->parent = parent;

            /* Clear the link to the subtree which is no longer there! */
            *treeaddr = NULL;

            /* Set the parent to be correct */
            tree->parent = ptr;
     
            /* For the next loop, use the new root */
            tree = subtree;
 
            } while( abs(twist) > 1 );

        /* It has changed so balance it again */
        return( numlist_in_balance( tree ) );
        }

    numlist_in_balance( tree->less );
    numlist_in_balance( tree->more );

    return( tree );
}

nlist_in *numlist_find(nlist *nl, ulong num)
{
    return(numlist_in_find(nl->head, num));
}

nlist_in *numlist_in_find(nlist_in *ni, ulong num)
{
    if(ni == NULL) return( NULL );
    if( ni->num > num ) return( numlist_in_find( ni->less, num ) );
    if( ni->num < num ) return( numlist_in_find( ni->more, num ) );

    return( ni );
}

