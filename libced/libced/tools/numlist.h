#ifndef _NUMLIST_H
#define _NUMLIST_H

#include <libced/common.h>

typedef struct _numlist_in nlist_in;
typedef struct _numlist    nlist;

struct _numlist_in
{
    ulong num;          /* the line number */
    uint  last;         /* is the last block marker on the line */
    nlist_in *parent;   /* the parent node */
    nlist_in *less;     /* all numbers less than the current */
    nlist_in *more;     /* all numbers more than the current */
};

struct _numlist
{
    ulong     nent;     /* number of entries in the tree */
    nlist_in *head;     /* the root of the tree */
};

nlist *numlist_new(void);
void   numlist_free(nlist *nl);
void   numlist_add(nlist *nl, ulong num, uint is_last);
void   numlist_del(nlist *nl, ulong num);
long   numlist_nearest_less(nlist *nl, ulong num);
long   numlist_nearest_leq(nlist *nl, ulong num);
long   numlist_nearest_meq(nlist *nl, ulong num);
ulong  numlist_nearest_morethan(nlist *nl, ulong num);
ulong  numlist_nearest_lessthan(nlist *nl, ulong num);
void   numlist_add_to_leq(nlist *nl, ulong num, long amount);
void   numlist_add_to_meq(nlist *nl, ulong num, long amount);
void   numlist_add_to_more(nlist *nl, ulong num, long amount);
uint   numlist_exists(nlist *nl, ulong num);
nlist_in *numlist_find(nlist *nl, ulong num);

#endif /* _NUMLIST_H */
