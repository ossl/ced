/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Parses ced's command set and compiles it into a data structure.
 *
 *  Last Updated
 *    05th October 2000
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <libced/utils/utils.h>

#include "parse.h"

#define ESCAPE '\\'
#define SQUOTE '\''
#define DQUOTE '"'

/*
 * Some new standard parsing routines.  Might help
 * out in some areas..
 */

char *parse_string(char *dest, char *src, int len)
{
    char match;
    char *st;

    src = str_skip_white(src);
    memset(dest, 0, len);

    if(*src == SQUOTE || *src == DQUOTE) {
        match = *src;
        src++;

        for(st = src; *src; src++) {
            if(*src == ESCAPE) {
                src++;
            } else if(*src == match) {
                break;
            }

            if(*src == '\0') break;
        }

        len = min(len, src-st);
        strncpy(dest, st, len);
        dest[len] = '\0';

        if(*src) src++;
    }

    src = str_skip_white(src);

    return(src);
}

char *parse_identifier(char *dest, char *src, int len)
{
    char *st;

    src = str_skip_white(src);
    memset(dest, 0, len);

    if(*src == SQUOTE || *src == DQUOTE) {
        src = parse_string(dest, src, len);
    } else {
        st = src;

        while(*src && !isspace(*src)) {
            src++;
        }

        len = min(len, src-st);
        strncpy(dest, st, len);
        dest[len] = '\0';
    }

    src = str_skip_white(src);

    return(src);
}

