#ifndef _PARSE_H
#define _PARSE_H

char *parse_string(char *dest, char *src, int len);
char *parse_identifier(char *dest, char *src, int len);

#endif /* _PARSE_H */
