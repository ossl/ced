/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999, 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *
 *  Last Updated
 *    16th March 2000
 *
 */

#include <stdlib.h>
#include <string.h>

#include "structs.h"

#include <libced/utils/utils.h>

#define PTRSIZE sizeof(void*)
#define PA_MIN  8
#define PA_MULT 2

void ptr_array_init(PARRAY *pa)
{
    memset(pa, 0, sizeof(PARRAY));
}

void ptr_array_free_data(PARRAY *pa)
{
    if(pa && pa->size) {
        free(pa->data);
        pa->size = 0;
    }
}

void ptr_array_add(PARRAY *pa, void *item)
{
    if(!pa->size) {
        pa->size = PA_MIN;
        pa->data = (void**)calloc(pa->size+1, PTRSIZE);
    } else if(pa->size == pa->items) {
        pa->size *= 2;
        pa->data = (void**)realloc(pa->data, PTRSIZE * (pa->size+1));
    }

    pa->data[pa->items] = item;
    pa->items++;
    pa->data[pa->items] = NULL;
}

void ptr_array_ins(PARRAY *pa, ulong n, void *item)
{
    ulong mx;

    mx = largest(PA_MIN, pa->items, n);

    if(!pa->size) {
        pa->size = mx * 2;
        pa->data = (void**)calloc(pa->size + 1, PTRSIZE);
    } else if(mx >= pa->size) {
        pa->size = mx * 2;
        pa->data = (void**)realloc(pa->data, PTRSIZE * (pa->size+1));
    }

    if(n < pa->items) {
        memmove(&pa->data[n+1], &pa->data[n], PTRSIZE * (pa->items-n));
    } else if(n > pa->items) {
        memset(&pa->data[pa->items], 0, PTRSIZE * (n-pa->items));
        pa->items = n;
    }

    pa->data[n] = item;
    pa->items++;
}

void ptr_array_del(PARRAY *pa, ulong n)
{
    if(n < pa->items) {
        memmove(&pa->data[n], &pa->data[n+1], PTRSIZE * (pa->items-n));
        pa->items--;
        pa->data[pa->items] = NULL;
    }
}

uint ptr_array_lookup(PARRAY *pa, void *ptr, ulong *n)
{
    ulong i;

    for(i=0; i < pa->items; i++) {
        if(pa->data[i] == ptr) {
            if(n) *n = i;
            return(1);
        }
    }

    return(0);
}

void ptr_array_copy(PARRAY *dest, PARRAY *src)
{
    ulong size;
    void  **data;

    size = max(PA_MIN, src->items * 2);
    data = dest->data;

    dest->data  = (void**)calloc(size + 1, PTRSIZE);
    dest->items = src->items;
    dest->size  = size;

    memcpy(dest->data, src->data, PTRSIZE * src->items);
}

