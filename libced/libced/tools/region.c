/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    This module provides a way of finding out what segment of a line
 *    is highlighting.
 *
 *  Last Updated
 *    12th February 2000
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <libced/tools/region.h>

region *region_new(regtype rt)
{
    region *reg = (region*)calloc(1, sizeof(region));
    reg->rt = rt;

    return(reg);
}          

void region_get_line_geometry(region *reg, ulong lnum, uint *x, uint *w, uint len)
{
    uint   sx, nx, sx_r, nx_r;
    ulong  sy, ny, sy_r, ny_r;

    region_get_top_coords(reg, &sx, &sy);
    region_get_bottom_coords(reg, &nx, &ny);
    region_get_coords(reg, &sx_r, &sy_r, &nx_r, &ny_r);

    /* Increment length so that it includes the newline char */

    (*x) = 0;
    (*w) = 0;

    /*
     * First check line is even in the region boundaries.  This is the
     * same for normal and rectangular selection so may aswell do it now.
     */

    if((lnum >= sy) && (lnum <= ny)) {

        /* if its a rectangle  */

        if(reg->rt == RT_RECTANGULAR) {
            (*x) = sx;
            (*w) = nx - sx;
        }

        /* if its a normal region */

        if(reg->rt == RT_NORMAL) {
            if(lnum + 1 < reg->nlines) len++;

            if(lnum == sy_r && lnum  < ny_r) (*x) = sx_r;
            if(lnum == sy_r && lnum == ny_r) (*x) = sx;
            if(lnum  > sy_r && lnum  < ny_r) (*x) = 0;
            if(lnum  > sy_r && lnum == ny_r) (*x) = 0;
            // ----------------------------------------------------------------
            if(lnum == sy_r && lnum  > ny_r) (*x) = 0;
            if(lnum  < sy_r && lnum  > ny_r) (*x) = 0;
            if(lnum  < sy_r && lnum == ny_r) (*x) = nx_r;

            /* now deal with the width */

            if(lnum == sy_r && lnum  < ny_r) (*w) = len - min((*x), len);
            if(lnum == sy_r && lnum == ny_r) (*w) = min(nx,   len) - smallest((*x), nx,   len);
            if(lnum  > sy_r && lnum  < ny_r) (*w) = len - min((*x), len);
            if(lnum  > sy_r && lnum == ny_r) (*w) = min(nx_r, len) - smallest((*x), nx_r, len);
            // ----------------------------------------------------------------
            if(lnum == sy_r && lnum  > ny_r) (*w) = min(sx_r, len) - smallest((*x), sx_r, len);
            if(lnum  < sy_r && lnum  > ny_r) (*w) = len - min((*x), len);
            if(lnum  < sy_r && lnum == ny_r) (*w) = len - min((*x), len);
        }
    }
}

void region_get_top_coords(region *reg, uint *x, ulong *y)
{
    uint  px = 0;
    ulong py = 0;

    if(reg->rt == RT_RECTANGULAR) {
        px = min(reg->x1, reg->x2);
        py = min(reg->y1, reg->y2);
    } else {
        if(reg->y1 == reg->y2) {
            px = min(reg->x1, reg->x2);
            py = reg->y1;
        } else if(reg->y1 < reg->y2) {
            px = reg->x1;
            py = reg->y1;
        } else if(reg->y1 > reg->y2) {
            px = reg->x2;
            py = reg->y2;
        }

        py = min(reg->nlines, py);
    }

    if(x) (*x) = px;
    if(y) (*y) = py;
}

void region_get_bottom_coords(region *reg, uint *x, ulong *y)
{
    uint  px = 0;
    ulong py = 0;

    if(reg->rt == RT_RECTANGULAR) {
        px = max(reg->x1, reg->x2);
        py = max(reg->y1, reg->y2);
    } else {
        if(reg->y1 == reg->y2) {
            px = max(reg->x1, reg->x2);
            py = reg->y1;
        } else if(reg->y1 > reg->y2) {
            px = reg->x1;
            py = reg->y1;
        } else if(reg->y1 < reg->y2) {
            px = reg->x2;
            py = reg->y2;
        }

        py = min(reg->nlines, py);
    }

    if(x) (*x) = px;
    if(y) (*y) = py;
}

void region_get_coords(region *reg, uint *x1, ulong *y1, uint *x2, ulong *y2)
{
    if(x1) (*x1) = reg->x1;
    if(y1) (*y1) = reg->y1;
    if(x2) (*x2) = reg->x2;
    if(y2) (*y2) = reg->y2;
}

void region_set_coords(region *reg, uint x1, ulong y1, uint x2, ulong y2)
{
    reg->x1 = x1;
    reg->y1 = y1;
    reg->x2 = x2;
    reg->y2 = y2;
}

void region_set_start_coords(region *reg, uint x1, ulong y1)
{
    reg->x1 = x1;
    reg->y1 = y1;
}

void region_set_end_coords(region *reg, uint x2, ulong y2)
{
    reg->x2 = x2;
    reg->y2 = y2;
}

void region_set_type(region *reg, regtype rt)
{
    reg->rt = rt;
}

void region_set_nlines(region *reg, ulong nlines)
{
    reg->nlines = nlines;
}

regtype region_get_type(region *reg)
{
    return(reg->rt);
}

void region_free(region *reg)
{
    if(reg) {
        free(reg);
    }
}

