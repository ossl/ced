#ifndef _REGION_H
#define _REGION_H

#include <libced/common.h>

typedef enum   _regtype  regtype;
typedef struct _region   region;

enum _regtype
{
    RT_NORMAL,
    RT_RECTANGULAR
};

struct _region
{
    regtype rt;
    ulong   x1;
    ulong   y1;
    ulong   x2;
    ulong   y2;
    ulong   nlines;
};

region *region_new(regtype rt);
void    region_free(region *reg);
void    region_get_line_geometry(region *reg, ulong lnum, uint *x, uint *w, uint len);
void    region_get_coords(region *reg, uint *x1, ulong *y1, uint *x2, ulong *y2);
void    region_set_start_coords(region *reg, uint x1, ulong y1);
void    region_set_end_coords(region *reg, uint x2, ulong y2);
void    region_set_coords(region *reg, uint x1, ulong y1, uint x2, ulong y2);
void    region_set_type(region *reg, regtype rt);
void    region_set_nlines(region *reg, ulong nlines);
void    region_get_top_coords(region *reg, uint *x, ulong *y);
void    region_get_bottom_coords(region *reg, uint *x, ulong *y);
regtype region_get_type(region *reg);

#endif /* _REGION_H */

