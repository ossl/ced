/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2001
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Provides a search and replace mechanism.  It allows a line to be
 *    searched backward or forward, and can calculate the replacement
 *    string by resolving back-references in the search part.
 *
 *  Last Updated
 *    21st July 2001
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include <libced/tools/search.h>
#include <libced/tools/dstring.h>
#include <libced/tools/regex.h>

#include <libced/utils/utils.h>

search *search_new()
{
    search *srch;
    srch = (search*)calloc(1, sizeof(search));

    srch->re = (regex_t*)calloc(1, sizeof(regex_t));
    srch->rm = (regmatch_t*)calloc(SEARCH_MAX_REFS, sizeof(regmatch_t));

    return(srch);
}

int search_init(search *srch)
{
    uchar      *find;
    uchar      *error;
    int        err;
    int        len;
    int        flags;
    int        esc;
    uint       i;

    if((find = srch->srch) == NULL) {
        srch->error = "No search pattern.";
        return(0);
    }

    flags = REG_EXTENDED;
    len = strlen(find);
    esc = 0;

    srch->fnd_eol = 0;
    srch->fnd_bol = 0;

    if(len && find[len-1] == '$') {
        for(i=0; i < len-1; i++) {
            esc = (!esc && find[i] == '\\') ? 1 : 0;
        }

        if(esc == 0) {
            srch->fnd_eol = 1;
        }
    }
    else if(find[0] == '^') {
        srch->fnd_bol = 1;
    }


    srch->init_ok = 0;
    err = regcomp(srch->re, find, flags);

    switch(err) {
        case REG_BADRPT  : error = "Invalid use of repetition operators."; break;
        case REG_BADBR   : error = "Invalid use of back reference operator."; break;
        case REG_EBRACE  : error = "Un-matched brace interval operators."; break;
        case REG_EBRACK  : error = "Un-matched bracket list operators."; break;
        case REG_ERANGE  : error = "Invalid use of the range operator."; break;
        case REG_ECTYPE  : error = "Unknown character class name."; break;
        case REG_ECOLLATE: error = "Invalid collating element."; break;
        case REG_EPAREN  : error = "Un-matched parenthesis group operators."; break;
        case REG_ESUBREG : error = "Invalid back reference to a subexpression."; break;
        case REG_EEND    : error = "Non specific error (oops)"; break;
        case REG_EESCAPE : error = "Trailing backslash."; break;
        case REG_BADPAT  : error = "Invalid use of pattern operators."; break;
        case REG_ESIZE   : error = "Compiled regular expression too large."; break;
        case REG_ESPACE  : error = "Out of memory during regexp compilation."; break;

        default: 
            if(find[0] == '*' || find[0] == '+') {
                error = "Repetition operator follows nothing.";
                err = REG_BADRPT;
            } else {
                srch->init_ok = 1;
                error = NULL;
            }
    }

    srch->error = error;

    return((err)?0:1);
}

sresult search_find(search *srch, uint rev)
{
    int        rflag;
    int        result;
    int        len, pos;
    uchar      *buf;
    sresult    sr;
    regex_t    *re;
    regmatch_t *rm;

    if(srch->init_ok && srch->ln) {
        re  = srch->re;
        rm  = srch->rm;
        len = dstring_get_length(srch->ln);

        result = REG_NOMATCH;
        rflag  = 0;
        pos    = 0;

        buf = dstring_get_string(srch->ln);
        memset(rm, 0, sizeof(regmatch_t) * SEARCH_MAX_REFS);

        /* forward search */

        if(!rev) {
            if(srch->st_pos <= len) {
                pos = srch->st_pos;

                if(srch->end_pos < len) {
                    buf[srch->end_pos] = '\0';
                    rflag |= REG_NOTEOL;
                }
    
                if(srch->st_pos > 0) {
                    rflag |= REG_NOTBOL;
                }
    
                if(buf[srch->st_pos] != '\0' || srch->fnd_bol || srch->fnd_eol) {
                    result = regexec(re, &buf[srch->st_pos], SEARCH_MAX_REFS, rm, rflag);
                }
            }
        }

        /* reverse search */

        else {
            pos = min(srch->st_pos, len);

            if(srch->st_pos < len) {
                buf[srch->st_pos + 1] = '\0';
                rflag = REG_NOTEOL;
            }

            if(pos > 0) {
                if(buf[pos] == '\0' && !srch->fnd_eol) {
                    pos--;
                }

                rflag |= REG_NOTBOL;
            }

            if(buf[pos] != '\0' || srch->fnd_bol || srch->fnd_eol) {
                while(pos >= srch->end_pos &&
                     (result = regexec(re, &buf[pos], SEARCH_MAX_REFS, rm, rflag))) {
                    pos--;

                    if(pos == 0) {
                        rflag &= ~REG_NOTBOL;
                    }
                }
            }
        }

        if(result == 0) {
            srch->fnd_off   = pos;
            srch->fnd_start = rm[0].rm_so + pos;
            srch->fnd_size  = rm[0].rm_eo - rm[0].rm_so;

            if(rm[1].rm_so || rm[1].rm_eo) {
                srch->extra = 1;
            } else {
                srch->extra = 0;
            }

            sr = SEARCH_FOUND;
        } else {
            sr = SEARCH_NOT_FOUND;
        }

        free(buf);
    }
    else {
        sr = SEARCH_NOT_READY;
    }

    return(sr);
}

sresult search_replace_all(search *srch)
{
    static int init = 0;
    static dstring *lrep;
    int status;
    int len;

    if(!init) {
        lrep = dstring_new();
        init = 1;
    }

    while((status = search_find(srch, 0)) == SEARCH_FOUND) {
        search_get_replace(srch, lrep);

        len = dstring_get_length(lrep);

        dstring_cursor_goto_x(srch->ln, srch->fnd_start);
        dstring_define_region(srch->ln);
        dstring_cursor_right(srch->ln, srch->fnd_size);
        dstring_region_delete(srch->ln);
        dstring_cursor_goto_x(srch->ln, srch->fnd_start);
        dstring_insert(srch->ln, lrep);

        srch->st_pos  = srch->fnd_start + len;
        srch->end_pos = dstring_get_length(srch->ln);
    }

    return(0);
}

sresult search_replace_once(search *srch, uint rev)
{
    return(0);
}

int search_get_replace(search *srch, dstring *lrep)
{
    uint        start;
    uint        size;
    uint        ref;
    int         ch;
    dstring     *ln;
    regmatch_t  *rm;
    static dstring *lcop = NULL;

    if(srch->init_ok && srch->repl) {
        if(srch->extra == 0) {
          dstring_insert_string(lrep, srch->repl);
          return(1);
        }

        if(!lcop) lcop = dstring_new();
        rm = srch->rm;
        ln = srch->ln;

        dstring_reset(lrep);
        dstring_insert_string(lrep, srch->repl);
        dstring_cursor_to_left(lrep);

        while((ch = dstring_get_char(lrep)) != EOF) {
            if(ch == '\\') {
                dstring_cursor_left(lrep, 1);
                dstring_define_region(lrep);
                dstring_cursor_right(lrep, 1);
                ch = dstring_get_char(lrep);

                if(isdigit(ch)) {
                    ref = ch - '0';

                    if(rm[ref].rm_so || rm[ref].rm_eo) {
                        start = rm[ref].rm_so + srch->fnd_off;
                        size  = rm[ref].rm_eo - rm[ref].rm_so;

                        dstring_region_delete(lrep);
                        dstring_cursor_goto_x(ln, start);
                        dstring_define_region(ln);
                        dstring_cursor_right(ln, size);
                        dstring_region_copy(ln, lcop);
                        dstring_insert(lrep, lcop);
                    }
                } else {
                    dstring_cursor_left(lrep, 1);
                    dstring_region_delete(lrep);
                    dstring_cursor_right(lrep, 1);
                }
            }
        }

        return(1);
    }

    return(0);
}

void search_reset(search *srch)
{
    if(srch->re) {
        regfree(srch->re);
    }

    search_set_find(srch, NULL);
    search_set_replace(srch, NULL);

    srch->init_ok = 0;
}

void search_set_find(search *srch, uchar *find)
{
    if(srch->srch) {
        free(srch->srch);
        srch->srch = NULL;
    }

    if(find) srch->srch = strdup(find);

    srch->init_ok = 0;
}

void search_set_replace(search *srch, uchar *repl)
{
    if(srch->repl) {
        free(srch->repl);
        srch->repl = NULL;
    }

    if(repl) srch->repl = strdup(repl);
}

void search_set_line(search *srch, dstring *ln)
{
    srch->ln = ln;
}

void search_set_start_pos (search *srch, uint pos)
{
    srch->st_pos = pos;
}

void search_set_end_pos(search *srch, uint pos)
{
    srch->end_pos = pos;
}

uint search_get_pos(search *srch)
{
    return(srch->st_pos);
}

void search_free(search *srch)
{
    if(srch->srch) free(srch->srch);
    if(srch->repl) free(srch->repl);

    regfree(srch->re);

    free(srch->re);
    free(srch->rm);
    free(srch);
}

