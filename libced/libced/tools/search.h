#ifndef _SEARCH_H
#define _SEARCH_H

#include <libced/common.h>
#include <libced/tools/dstring.h>
#include <libced/tools/regex.h>

#define SEARCH_MAX_REFS 16

typedef enum   _search_result sresult;
typedef struct _search        search;

enum _search_result
{
  SEARCH_FOUND,
  SEARCH_NOT_FOUND,
  SEARCH_NOT_READY
};

struct _search
{
  int        init_ok;
  int        fnd_start;
  int        fnd_size;
  int        fnd_off;
  int        fnd_bol;
  int        fnd_eol;
  int        extra;
  int        st_pos;
  int        end_pos;
  uchar      *srch;
  uchar      *repl;
  uchar      *error;
  dstring    *ln;
  regex_t    *re;
  regmatch_t *rm;
};

search *search_new           (void);
int     search_init          (search *srch);
void    search_reset         (search *srch);
void    search_set_find      (search *srch, uchar *find);
void    search_set_replace   (search *srch, uchar *repl);
void    search_set_line      (search *srch, dstring *ln);
void    search_set_start_pos (search *srch, uint pos);
void    search_set_end_pos   (search *srch, uint pos);
uint    search_get_pos       (search *srch);
sresult search_find          (search *srch, uint rev);
int     search_get_replace   (search *srch, dstring *lrep);
void    search_free          (search *srch);

#endif /* _SEARCH_H */

