/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2001
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Provides a way of transparently reading from a file or a string.
 *
 *  Last Updated
 *    8th July 2001
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

#include <libced/common.h>
#include <libced/tools/stream.h>

stream *stream_new()
{
    stream *st;

    st = (stream*)calloc(1, sizeof(stream));
    st->stype = ST_NONE;
    st->fptr  = NULL;
    st->str   = NULL;
    st->pos   = 0;

    return(st);
}

void stream_free(stream *st)
{
    switch(st->stype) {
        case ST_FILE   : fclose(st->fptr); break;
        case ST_STRING : free(st->str); break;
        default: break; /* do nothing */
    }

    free(st);
}

void stream_set_file(stream *st, char *fname)
{
    if(st->stype != ST_NONE) {
        fprintf(stderr, "[stream] stream_set_file: stream already active\n\r");
    } else {
        st->stype = ST_FILE;
        st->fptr  = fopen(fname, "r");
    }
}

void stream_set_string(stream *st, char *str)
{
    if(st->stype != ST_NONE) {
        fprintf(stderr, "[stream] stream_set_string: stream already active\n\r");
    } else {
        st->stype = ST_STRING;
        st->str   = strdup(str);
    }
}

void stream_rewind(stream *st)
{
    switch(st->stype) {
        case ST_NONE   : return;
        case ST_FILE   : rewind(st->fptr); break;
        case ST_STRING : break; /* do nothing */
    }

    st->pos = 0;
}

uchar stream_get_char(stream *st)
{
    int c;

    switch(st->stype) {
        case ST_FILE:

            if((c = fgetc(st->fptr)) != EOF) {
                st->pos++;
            } else {
                c = '\0';
            }

            break;

        case ST_STRING: 

            if((c = st->str[st->pos]) != '\0') {
                st->pos++;
            }

            break;

        default:
            return('\0');
    }

    return((uchar)c);
}

