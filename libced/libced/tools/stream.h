#ifndef _STREAM_H
#define _STREAM_H

#include <libced/common.h>

typedef enum   _stream_type STYPE;
typedef struct _stream      stream;

enum _stream_type
{
    ST_NONE,
    ST_FILE,
    ST_STRING
};

struct _stream
{
    STYPE  stype;  /* type of stream */
    FILE   *fptr;  /* file pointer for file streams */
    uchar  *str;   /* start of string pointer for character streams */
    ulong  pos;    /* position in the stream */
};
stream  *stream_new(void);
void     stream_free(stream *st);
void     stream_set_file(stream *st, char *fname);
void     stream_set_string(stream *st, char *str);
void     stream_rewind(stream *st);
uchar    stream_get_char(stream *st);

#endif /* _STREAM_H */
