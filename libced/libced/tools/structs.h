#ifndef _STRUCTS_H
#define _STRUCTS_H

#include <libced/common.h>

typedef struct _ptr_array PARRAY;
typedef struct _num_array NARRAY;
typedef struct _llist     LLIST;

struct _ptr_array {
    void  **data;    /* pointer to array */
    ulong items;     /* number of items in array */
    ulong size;      /* size allocated for array */
};

struct _num_array {
    long  *data;     /* numeric array */
    ulong items;     /* number of items in array */
    ulong size;      /* size allocated for array */
};

struct _llist {
    void          *data;  /* pointer item data */
    struct _llist *next;  /* pointer to next node */
};

void ptr_array_init(struct _ptr_array *pa);
void ptr_array_add(struct _ptr_array *pa, void *item);
void ptr_array_del(struct _ptr_array *pa, ulong n);
void ptr_array_ins(struct _ptr_array *pa, ulong n, void *item);
void ptr_array_free_data(struct _ptr_array *pa);
uint ptr_array_lookup(struct _ptr_array *pa, void *ptr, ulong *n);
void ptr_array_copy(struct _ptr_array *dest, struct _ptr_array *src);

void num_array_init(struct _num_array *na);
void num_array_add(struct _num_array *na, long num);
void num_array_del(struct _num_array *na, ulong n);
void num_array_ins(struct _num_array *na, ulong n, long num);
void num_array_free_data(struct _num_array *na);
uint num_array_lookup(struct _num_array *na, long num, ulong *n);

#endif /* STRUCTS_H */
