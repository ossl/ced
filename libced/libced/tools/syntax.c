/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999, 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    A library for syntax highlighting.
 *
 *  Last Updated
 *    13th August 2001
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <libced/tools/dstring.h>
#include <libced/tools/conf.h>
#include <libced/tools/numlist.h>
#include <libced/tools/avltree.h>
#include <libced/tools/structs.h>

#include <libced/utils/utils.h>

#include <libced/hash/strhash.h>

#include "syntax.h"

//#define INDENT_ON(x)    x->indent_on[0]
//#define INDENT_OFF(x)   x->indent_off[0]
#define COMMENT_ON(x)   x->comment_on[0]
#define COMMENT_OFF(x)  x->comment_off[0]
#define COMMENT_LINE(x) x->comment_line[0]
#define START_LINE(x)   x->line_start[0]

char COLSET1[2] = { 27, 1 };
char COLSET2[2] = { 27, 2 };
char COLSET3[2] = { 27, 3 };
char COMMSET[2] = { 27, 4 };
char STRSET[2]  = { 27, 5 };
char ENDSET[2]  = { 27, 8 };

char INDENT_ON_MARKER[2]  = { 27, 9  };
char INDENT_OFF_MARKER[2] = { 27, 10 };

//#define DEBUG

typedef struct _line_data     LINE_DATA;
typedef enum   _char_lookup   CharTable;

enum _char_lookup {
    CQL_NONE       = 1 << 0,
    CQL_KEY_LEFT   = 1 << 1,
    CQL_DELIM_ROKW = 1 << 2,
    CQL_DELIM_KEY  = 1 << 3,
    CQL_DELIM      = 1 << 4,
    CQL_STRING     = 1 << 5,
    CQL_ESCAPE     = 1 << 6,
    CQL_INDENT     = 1 << 7,
    CQL_PAIR       = 1 << 8
};

struct _line_data
{
    LINE_ATTRIBS *la;  /* line attributes to return */
    dstring *ln;       /* actual line being formatted */
    char *line;        /* pointer to start of string */
    char *cpos;        /* pointer to position in line */
//    char *in_open;     /* pointer to next open indent */
//    char *in_close;    /* pointer to next close indent */
    char *cm_open;     /* pointer to next open comment */
    char *cm_line;     /* pointer to next line comment */
    char *st_line;     /* pointer to next line start */
    uint comp;         /* line formatting compensation */
    int  hbn_1;        /* highlight block number */
    int  hbn_2;        /* highlight block number */
    int  nblocks;      /* number of blocks found */
};

inline void  syntax_parse_string         (syntax *syn, LINE_DATA *lpd);
inline void  syntax_parse_keyword        (syntax *syn, LINE_DATA *lpd);
inline void  syntax_parse_comment        (syntax *syn, LINE_DATA *lpd);
inline void  syntax_parse_space_comment  (syntax *syn, LINE_DATA *lpd);
static void  syntax_process_keyset       (syntax *syn, char **set);
static void  syntax_process_ql           (syntax *syn, char **chs, int n);
static char *syntax_get_language_name    (config *syntax_conf, char *fname);

static void syntax_create_pair_hash      (syntax *syn, char **set);
static void syntax_create_keyword_hash   (syntax *syn, char **set, char *colset);

static uint syntax_set_language_internal (syntax *syn, char *name);

/* some temporary avltree extensions, don't like this being here! */

void avltree_add_to_leq  (AVLTree *t, long v, long amount);
void avltree_add_to_meq  (AVLTree *t, long v, long amount);
void avltree_add_to_lt   (AVLTree *t, long v, long amount);
void avltree_add_to_mt   (AVLTree *t, long v, long amount);

BLOCK_STATE syntax_previous_comment (syntax *syn, long lnum, long *lfound);
BLOCK_STATE syntax_next_comment     (syntax *syn, long lnum, long *lfound);

syntax *syntax_new(config *syntax_conf)
{
    syntax *syn;

    syn = (syntax*)calloc(1, sizeof(syntax));
    syn->conf = syntax_conf;

    return(syn);
}

uint syntax_set_from_filename(syntax *syn, char *fname)
{
    char *lname;

    if((lname = syntax_get_language_name(syn->conf, fname)) != NULL) {
        return(syntax_set_language(syn, lname));
    }

    return(0);
}

uint syntax_set_language(syntax *syn, char *name)
{
    if((conf_params(syn->conf, name, "syntax")) != NULL) {
        syntax_reset(syn);
        syntax_set_language_internal(syn, name);

        return(1);
    }

    return(0);
}

void syntax_free(syntax *syn)
{
    syntax_reset(syn);

    free(syn);
}

void syntax_reset(syntax *syn)
{
    uint   i;
    char   **set;
    config *conf;

    if(syn->curr_syn) {
        if((set = syn->plist) != NULL) {
            for(i=0; set[i] != NULL; i++) {
                free(sh_get(syn->pairs, set[i]));
            }
        }

        sh_free(syn->keywords);
        sh_free(syn->pairs);
        free(syn->curr_syn);
    }

    avltree_free(syn->cm_blocks);
    conf = syn->conf;
    memset(syn, 0, sizeof(syntax));
    syn->conf = conf;
}

static uint syntax_set_language_internal(syntax *syn, char *name)
{
    char  **files;
    char  **sect_keywords;
    char  **sect_syntax;
    char  **keyw_set1;
    char  **keyw_set2;
    char  **keyw_set3;
    char  **mc;

    syn->curr_syn = strdup(name);
    syn->pairs    = sh_new(256, syn->match_case);
    syn->keywords = sh_new(256, syn->match_case);

    if((files = conf_params(syn->conf, syn->curr_syn, "files")) == NULL) {
        return(0);
    }

    if((sect_syntax = conf_params(syn->conf, syn->curr_syn, "syntax")) != NULL) {
//        syn->indent_on  = conf_params(syn->conf, sect_syntax[0], "indent_on");
//        syn->indent_off = conf_params(syn->conf, sect_syntax[0], "indent_off");
        syn->func_regex = conf_params(syn->conf, sect_syntax[0], "function");

        syntax_process_ql(syn, conf_params(syn->conf, sect_syntax[0], "escape"), CQL_ESCAPE);
        syntax_process_ql(syn, conf_params(syn->conf, sect_syntax[0], "string"), CQL_STRING);
        syntax_process_ql(syn, conf_params(syn->conf, sect_syntax[0], "delimeters"), CQL_DELIM);

        if((syn->comment_line = conf_params(syn->conf, sect_syntax[0], "line")) != NULL) {
            if(syn->comment_line[0]) {
                syn->have_line_comment = 1;
            }
        }

        if((syn->line_start = conf_params(syn->conf, sect_syntax[0], "line_start")) != NULL) {
            if(syn->line_start[0]) {
                syn->have_line_start = 1;
            }
        }

        syn->comment_on  = conf_params(syn->conf, sect_syntax[0], "block_on");
        syn->comment_off = conf_params(syn->conf, sect_syntax[0], "block_off");

        if(syn->comment_on && syn->comment_off) {
            syn->have_block_comment = 1;
        }

//        if(syn->indent_on && syn->indent_off) {
//            syn->have_block_indent = 1;
//        }

        if((mc = conf_params(syn->conf, sect_syntax[0], "case")) != NULL) {
            if(tolower(mc[0][0]) == 'y') syn->match_case = 1;
        }

        if((syn->plist = conf_params(syn->conf, sect_syntax[0], "pairs")) != NULL) {
            syntax_process_keyset(syn, syn->plist);
            syntax_create_pair_hash(syn, syn->plist);
        }
    }

    if((sect_keywords = conf_params(syn->conf, syn->curr_syn, "keywords")) != NULL) {
        keyw_set1 = conf_params(syn->conf, sect_keywords[0], "set_1");
        keyw_set2 = conf_params(syn->conf, sect_keywords[0], "set_2");
        keyw_set3 = conf_params(syn->conf, sect_keywords[0], "set_3");

        syntax_process_keyset(syn, keyw_set1);
        syntax_process_keyset(syn, keyw_set2);
        syntax_process_keyset(syn, keyw_set3);

        syntax_create_keyword_hash(syn, keyw_set1, COLSET1);
        syntax_create_keyword_hash(syn, keyw_set2, COLSET2);
        syntax_create_keyword_hash(syn, keyw_set3, COLSET3);
    }

    /*
     * Force some default delimeters.. seems sensible.  Email me if you
     * think otherwise.  Never found a sensible language where this
     * would be false but I could be wrong.
     */

    syn->char_ql[0]  = CQL_DELIM;  /* End of line is a delimeter   */
    syn->char_ql[9]  = CQL_DELIM;  /* Tab is always a delimieter   */
    syn->char_ql[32] = CQL_DELIM;  /* Space is always a delimeter  */

    syn->initialised = 1;

    return(1);
}

char *syntax_get_language_name(config *syntax_conf, char *fname)
{
    char *ext;
    char **langs;
    char **files;
    uint i, f;

    if(!(fname && syntax_conf))
    {
        return(NULL);
    }

    if((ext = strrchr(fname, '.'))) {
        ext++;

        if((langs = conf_params(syntax_conf, NULL, "languages"))) {
            for(i=0 ; langs[i] != NULL; i++ ) {
                if((files = conf_params(syntax_conf, langs[i], "files"))) {
                    for(f=0; files[f] != NULL; f++) {
                        if(!strcasecmp(files[f], ext)) {
                            return(langs[i]);
                        }
                    }
                }
            }
        }
    }

    return(NULL);
}

static void syntax_process_ql(syntax *syn, char **chs, int n)
{
    int i;

    if(chs) {
        for(i=0; chs[i] != NULL; i++) {
            syn->char_ql[(uint)chs[i][0]] |= n;
        }
    }
}

static void syntax_create_keyword_hash(syntax *syn, char **set, char *colset)
{
    uint i;

    if(set && colset) {
        for(i=0; set[i] != NULL; i++) {
            sh_set(syn->keywords, set[i], (void*)colset);
        }
    }
}

static void syntax_create_pair_hash(syntax *syn, char **pairs)
{
    uint i;
    uint *num;

    if(pairs) {
        for(i=0; pairs[i] != NULL; i++) {
            num = (uint*)calloc(1, sizeof(uint));
            *num = i;
            sh_set(syn->pairs, pairs[i], (void*)num);
        }
    }
}

static void syntax_process_keyset(syntax *syn, char **set)
{
    uint i;
    uint cf;
    uint cl;
    uint mc;
    uint len;
    char *ql;

    if(!syn || !set) {
        return;
    }

    ql = syn->char_ql;
    mc = syn->match_case;

    for(i=0; set[i] != NULL; i++) {
        len = strlen(set[i]);
        cf = (uint)set[i][0];
        cl = (uint)set[i][len-1];

        // If the length of the keyword is more than 1

        if(len > 1) {
            if(mc) {
                ql[cf] |= CQL_KEY_LEFT;

                if( ql[cl]  & CQL_DELIM) {
                    ql[cl] |= CQL_DELIM_ROKW;
                }
            } else {
                ql[tolower(cf)] |= CQL_KEY_LEFT;
                ql[toupper(cf)] |= CQL_KEY_LEFT;

                if( ql[cl] & CQL_DELIM) {
                    ql[tolower(cl)] |= CQL_DELIM_ROKW;
                    ql[toupper(cl)] |= CQL_DELIM_ROKW;
                }
            }
        }

        // If the length is only 1..

        else {
            if(mc) {
                if( ql[cf]  & CQL_DELIM) {
                    ql[cf] |= CQL_DELIM_KEY;
                }
            } else {
                if( ql[cf] & CQL_DELIM) {
                    ql[tolower(cf)] |= CQL_DELIM_KEY;
                    ql[toupper(cf)] |= CQL_DELIM_KEY;
                }
            }
        }
    }
}

void syntax_line_delete_compensate(syntax *syn, ulong atline, long n)
{
    LINE_ATTRIBS *cm_la;

    syn->nlines -= n;
    syn->cm_blocks = avltree_remove(syn->cm_blocks, atline, (void*)&cm_la);

    avltree_add_to_meq(syn->cm_blocks, atline + 1, -n);

    if(cm_la) free(cm_la);
}

void syntax_line_insert_compensate(syntax *syn, ulong atline, long n)
{
    syn->nlines += n;

    avltree_add_to_meq(syn->cm_blocks, atline, n);
}

/* returns number of lines affected by preprocessed line */

ulong syntax_preprocess_line(syntax *syn, dstring *ln, ulong lnum)
{
    LINE_ATTRIBS *la_cm;    /* line data */
    BLOCK_STATE  last;      /* found open or close comment last */
    char        *chstart;   /* pointer to start of the line */
    char        *chline;    /* pointer to current position in line */
    char        *cm_open;   /* next open comment block */
    char        *cm_close;  /* next close comment block */
    long        changed;    /* how many lines affected */
    char        std;        /* delimeter used for opening a string */
    long        lnum_end;

    syn->nlines = max(syn->nlines, lnum);

    if(!syn->have_block_comment) {
        return(1);
    }

    last = BLOCK_NONE;
    changed = 1;

    chline    = dstring_get_string(ln);
    chstart   = chline;
    cm_open   = strstr(chline, COMMENT_ON(syn));
    cm_close  = strstr(chline, COMMENT_OFF(syn));

    avltree_get_eq(syn->cm_blocks, lnum, (void*)&la_cm);

    if(cm_open || cm_close) {
        if(syntax_previous_comment(syn, lnum, NULL) == BLOCK_OPEN) {
            last = BLOCK_OPEN;
        }

        for( ; ; chline++) {
            /*
             * If we find an open comment, skip past it and set the last found
             * variable to 1.  If there is no comment close, we break out of the
             * loop, else we set cm_open to point to the next comment open on the
             * line, or null if there isn't one.
             */

            if(chline == cm_open) {
                last = BLOCK_OPEN;

                if(cm_close) {
                    chline += strlen(COMMENT_ON(syn));
                    cm_open = strstr(chline, COMMENT_ON(syn));
                } else {
                    break;
                }
            }

            /*
             * The same principle applies here when we find a close comment as
             * when we find an open comment as explained above.
             */

            if(chline == cm_close) {
                last = BLOCK_CLOSE;

                if(cm_open) {
                    chline += strlen(COMMENT_OFF(syn)) - 1;
                    cm_close = strstr(chline, COMMENT_OFF(syn));
                }
            }

            /*
             * If we come across a string, then skip to the end of it, making
             * sure we take into account escape characters.
             */

            if(syn->char_ql[(uint)*chline] & CQL_STRING && last != BLOCK_OPEN) {
                std = *chline;

                for(chline++ ; ; chline++) {
                    if(syn->char_ql[(uint)*chline] & CQL_ESCAPE) {
                        chline++;
                    } else {
                        if(*chline == std) {
                            break;
                        }
                    }

                    if(*chline == '\0') break;
                }

                /*
                 * Make sure cm_open and cm_close are pointing to the first
                 * occurance of any comment open and close blocks after the
                 * end of the string.
                 */

                if(chline > cm_open)  cm_open  = strstr(chline, COMMENT_ON(syn));
                if(chline > cm_close) cm_close = strstr(chline, COMMENT_OFF(syn));
            }

            if(*chline == '\0') break;
        }
    }

    /*
     * Work out the number of lines that the syntax highlighting
     * would of changed on because of any changes discovered
     * processing this line.
     */

    if((la_cm && la_cm->comment != last) || last != BLOCK_NONE) {
        switch(syntax_next_comment(syn, lnum, &lnum_end)) {
            case BLOCK_CLOSE : changed = lnum_end - lnum + 1; break;
            default          : changed = syn->nlines - lnum + 1;
        }
    }

    /*
     * If there is a block comment marker on this line, then store
     * it, else if was originally a block, remove it.
     */

    if(last != BLOCK_NONE) {
        if(!la_cm) {
            la_cm = (LINE_ATTRIBS*)calloc(1, sizeof(LINE_ATTRIBS));
            syn->cm_blocks = avltree_insert(syn->cm_blocks, lnum, (void*)la_cm);
        }

        la_cm->comment = last;
    } else if(la_cm) {
        syn->cm_blocks = avltree_remove(syn->cm_blocks, lnum, NULL);
        free(la_cm);
    }

    free(chstart);

    return(changed);
}

void syntax_format_line(syntax *syn, dstring *ln, LINE_ATTRIBS *la, ulong lnum, int hbn_1, int hbn_2)
{
    LINE_DATA lpd;
    uint l_cql;
    uint c_cql;

    if(!syn->initialised) {
        return;
    }

    memset(&lpd, 0, sizeof(LINE_DATA));

    lpd.la    = la;
    lpd.ln    = ln;
    lpd.line  = dstring_get_string(ln);
    lpd.cpos  = lpd.line;
    lpd.hbn_1 = hbn_1;
    lpd.hbn_2 = hbn_2;

    if(syn->have_block_comment) {
        lpd.cm_open = strstr(lpd.line, COMMENT_ON(syn));
    }

    if(syntax_previous_comment(syn, lnum, NULL) == BLOCK_OPEN) {
        syntax_parse_comment(syn, &lpd);
    }

    if(syn->have_line_comment) {
        lpd.cm_line = strstr(lpd.cpos, COMMENT_LINE(syn));
    }

    if(syn->have_line_start) {
        lpd.st_line = strstr(lpd.cpos, START_LINE(syn));

        if(lpd.cpos != lpd.st_line) {
            syntax_parse_space_comment(syn, &lpd);
        }
    }

    if(la) {
        num_array_free_data(&la->bpairs);
        num_array_free_data(&la->blocks);
        num_array_free_data(&la->bpos);
    }

    l_cql = 0 | CQL_DELIM;

    while(*lpd.cpos) {
        c_cql = syn->char_ql[(uint)*lpd.cpos];

        if( lpd.cpos == lpd.cm_open ||
            lpd.cpos == lpd.cm_line) {

            syntax_parse_comment(syn, &lpd);

        } else if(c_cql & CQL_STRING) {

            syntax_parse_string(syn, &lpd);

        } else if((c_cql & CQL_DELIM    || l_cql & CQL_DELIM ) &&
                  (c_cql & CQL_KEY_LEFT || c_cql & CQL_DELIM_KEY)) {

            syntax_parse_keyword(syn, &lpd);

        } else if(*lpd.cpos) {
            lpd.cpos++;
        }

        l_cql = syn->char_ql[(uint)*(lpd.cpos-1)];
    }

    free(lpd.line);
}

inline void syntax_parse_keyword(syntax *syn, LINE_DATA *lpd)
{
    char *st;        /* parse starting point */
    char *set;       /* emcoding set to insert */
    uint *pnum;      /* pair number pointer */
    char cql;        /* character quick lookup flags */
    char word[1024]; /* keyword */

    pnum = NULL;
    set  = NULL;
    st   = lpd->cpos;

    lpd->cpos++;

    for( ; *lpd->cpos != '\0' ; lpd->cpos++ ) {
        if(syn->char_ql[(uint)*lpd->cpos] & CQL_DELIM) {
            break;
        }
    }

    memcpy(word, st, lpd->cpos - st + 1);

    cql = syn->char_ql[(uint)*lpd->cpos];

    /*
     * If the delimeter is marked as also being the last character of
     * a keyword, then we first check for the word found with the delimeter
     * being its last character.  If it is found, we need to advance the
     * current position to skip past the delimeter.
     */

    if(cql & CQL_DELIM_ROKW) {
        word[lpd->cpos - st + 1] = '\0';

        if((set  = sh_get(syn->keywords, word)) ||
           (pnum = sh_get(syn->pairs, word))) {
            lpd->cpos++;
        }
    } else {
        word[lpd->cpos - st] = '\0';
        set  = sh_get(syn->keywords, word);
        pnum = sh_get(syn->pairs, word);
    }

    /*
     * If the previous two tries failed, then we check to see if the
     * first character alone is a keyword.
     */

    if(!(set || pnum) && syn->char_ql[(uint)*st] & CQL_DELIM_KEY) {
        word[1] = '\0';
        set  = sh_get(syn->keywords, word);
        pnum = sh_get(syn->pairs, word);
        lpd->cpos = st + 1;
    }

    /* If the keyword we've found is part of a pair, then we add data
     * about it to the line attributes structure.
     */

    if(pnum) {
        if(lpd->nblocks == lpd->hbn_1 || lpd->nblocks == lpd->hbn_2) {
            set = INDENT_ON_MARKER;
        }

        if(lpd->la) {
            num_array_add(&lpd->la->bpairs, abs(*pnum / 2));
            num_array_add(&lpd->la->blocks, (*pnum % 2) ? -1 : 1);
            num_array_add(&lpd->la->bpos, st - lpd->line + strlen(word));
        }

        lpd->nblocks++;
    }

    if(set) {
        dstring_cursor_goto_x(lpd->ln, st - lpd->line + lpd->comp);
        dstring_insert_raw(lpd->ln, set, 2);
        dstring_cursor_right(lpd->ln, lpd->cpos - st);
        dstring_insert_raw(lpd->ln, ENDSET, 2);
        lpd->comp += 4;
    }
}

inline void syntax_parse_string(syntax *syn, LINE_DATA *lpd)
{
    char sqln;

    sqln = *lpd->cpos;
    dstring_cursor_goto_x(lpd->ln, lpd->cpos - lpd->line + lpd->comp);
    dstring_insert_raw(lpd->ln, STRSET, 2);
    lpd->comp += 2;
    lpd->cpos++;

    for( ; ; lpd->cpos++) {
        if(syn->char_ql[(uint)*lpd->cpos] & CQL_ESCAPE) {
            lpd->cpos++;
        } else if(*lpd->cpos == sqln) {
            dstring_cursor_goto_x(lpd->ln, lpd->cpos - lpd->line + 1 + lpd->comp);
            dstring_insert_raw(lpd->ln, ENDSET, 2);
            lpd->comp += 2;
            lpd->cpos++;
            sqln = 0;
            break;
        }

        if(*lpd->cpos == '\0') break;
    }

    if(syn->have_block_comment) {
        lpd->cm_open = strstr(lpd->cpos, COMMENT_ON(syn));
    }
    if(syn->have_line_comment) {
        lpd->cm_line = strstr(lpd->cpos, COMMENT_LINE(syn));
    }
}

inline void syntax_parse_space_comment(syntax *syn, LINE_DATA *lpd)
{
    char *end_ptr;

    dstring_cursor_goto_x(lpd->ln, lpd->cpos - lpd->line + lpd->comp);
    dstring_insert_raw(lpd->ln, COMMSET, 2);
    lpd->comp += 2;

    end_ptr = NULL;

    if(lpd->st_line && lpd->cm_line) {
        end_ptr = (lpd->st_line < lpd->cm_line) ? lpd->st_line : lpd->cm_line;
    } else {
        if(lpd->st_line) {
            end_ptr = lpd->st_line;
        } else if(lpd->cm_line) {
            end_ptr = lpd->cm_line;
        }
    }

    if(end_ptr) {
        lpd->cpos = end_ptr;
        dstring_cursor_goto_x(lpd->ln, lpd->cpos - lpd->line + lpd->comp);
        dstring_insert_raw(lpd->ln, ENDSET, 2);
        lpd->comp += 2;
    } else {
        lpd->cpos += strlen(lpd->cpos);
    }
}

inline void syntax_parse_comment(syntax *syn, LINE_DATA *lpd)
{
    char *cm_close;  /* position of comment close */

    if(lpd->cm_line && lpd->cpos == lpd->cm_line) {
        dstring_cursor_goto_x(lpd->ln, lpd->cpos - lpd->line + lpd->comp);
        dstring_insert_raw(lpd->ln, COMMSET, 2);
        lpd->cpos += strlen(lpd->cpos);
    } else {
        dstring_cursor_goto_x(lpd->ln, lpd->cpos - lpd->line + lpd->comp);
        dstring_insert_raw(lpd->ln, COMMSET, 2);
        lpd->comp += 2;

        if((cm_close = strstr(lpd->cpos, COMMENT_OFF(syn)))) {
            lpd->cpos = cm_close + strlen(COMMENT_OFF(syn));
            dstring_cursor_goto_x(lpd->ln, lpd->cpos - lpd->line + lpd->comp);
            dstring_insert_raw(lpd->ln, ENDSET, 2);
            lpd->comp += 2;
            lpd->cm_open = strstr(lpd->cpos, COMMENT_ON(syn));
        } else {
            lpd->cpos += strlen(lpd->cpos);
        }
    }
}

/*
 * Some avltree functions.  Umm, ok they don't really belong here !
 */

void avltree_add_to_leq(AVLTree *t, long v, long amount)
{
    if(t) {
        if(t->value <= v) {
            t->value += amount;
        }

        avltree_add_to_leq(t->left, v, amount);
        avltree_add_to_leq(t->right, v, amount);
    }
}

void avltree_add_to_meq(AVLTree *t, long v, long amount)
{
    if(t) {
        if(t->value >= v) {
            t->value += amount;
        }

        avltree_add_to_meq(t->left, v, amount);
        avltree_add_to_meq(t->right, v, amount);
    }
}

void avltree_add_to_lt(AVLTree *t, long v, long amount)
{
    avltree_add_to_leq(t, v - 1, amount);
}

void avltree_add_to_mt(AVLTree *t, long v, long amount)
{
    avltree_add_to_meq(t, v + 1, amount);
}

BLOCK_STATE syntax_previous_comment(syntax *syn, long lnum, long *lfound)
{
    AVLTree     *t;
    LINE_ATTRIBS *la;

    if((t = avltree_get_lt(syn->cm_blocks, lnum, (void*)&la))) {
        if(lfound) {
            *lfound = t->value;
        }

        return(la->comment);
    }

    return(BLOCK_NONE);
}

BLOCK_STATE syntax_next_comment(syntax *syn, long lnum, long *lfound)
{
    AVLTree     *t;
    LINE_ATTRIBS *la;

    if((t = avltree_get_gt(syn->cm_blocks, lnum, (void*)&la))) {
        if(lfound) {
            *lfound = t->value;
        }

        return(la->comment);
    }

    return(BLOCK_NONE);
}

