#ifndef _SYNTAX_H
#define _SYNTAX_H

#include <libced/common.h>

#include <libced/tools/conf.h>
#include <libced/tools/dstring.h>
#include <libced/tools/numlist.h>
#include <libced/tools/avltree.h>
#include <libced/tools/structs.h>

#include <libced/hash/strhash.h>

typedef enum   _block_state   BLOCK_STATE;
typedef struct _pair          PAIR;
typedef struct _pair_entry    PAIR_ENTRY;
typedef struct _line_attribs  LINE_ATTRIBS;
typedef struct _syntax        syntax;

enum _block_state {
    BLOCK_NONE,
    BLOCK_OPEN,
    BLOCK_CLOSE
};

struct _syntax
{
    ulong     nlines;
    config    *conf;
    char      *conf_fname;
    char      *curr_syn;
    char      **files;
    AVLTree   *cm_blocks;

    /* syntax highlighting options */

    strhash   *pairs;
    strhash   *keywords;
    char      **plist;
    char      **line_start;
    char      **comment_line;
    char      **comment_on;
    char      **comment_off;
    char      **indent_on;
    char      **indent_off;
    char      **syntax_on;
    char      **syntax_off;
    char      **func_regex;
    char      **delims;
    char      **string;
    char      **escape;
    char      **rgb[5];
    char      char_ql[256];
    uint      have_block_indent;
    uint      have_block_comment;
    uint      have_line_comment;
    uint      have_line_start;
    uint      match_case;
    uint      initialised;
};

struct _line_attribs
{
    int is_function;      /* is the line a function */
    NARRAY      blocks;   /* block start/end column array */
    NARRAY      bpos;     /* block start/end positions */
    NARRAY      bpairs;   /* pair entries */
    BLOCK_STATE comment;  /* type of last comment marker on line */
};

struct _pair_entry
{
    uint pair_num;        /* pair number */
    uint data;            /* open (1) or close (-1) */
    uint col;             /* column its on */
};

struct _pair
{
    char *start;          /* start of pair */
    char *end;            /* end of pair */
};

syntax *syntax_new                     (config *syntax_conf);
uint    syntax_set_language            (syntax *l, char *name);
uint    syntax_set_from_filename       (syntax *l, char *fname);
ulong   syntax_preprocess_line         (syntax *l, dstring *ln, ulong lnum);
void    syntax_format_line             (syntax *l, dstring *ln, LINE_ATTRIBS *la, ulong lnum, int hbn_1, int hbn_2);
void    syntax_line_delete_compensate  (syntax *l, ulong atline, long n);
void    syntax_line_insert_compensate  (syntax *l, ulong atline, long n);
void    syntax_reset                   (syntax *l);
void    syntax_free                    (syntax *l);

#endif /* _SYNTAX_H */
