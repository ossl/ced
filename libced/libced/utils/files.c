/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2003
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Provides unix function wrappers.
 *
 *  Last Updated
 *    02/Jun/2003
 *
 */

#include <sys/types.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pwd.h>

#include "utils.h"

/*****************************************************************************************
 * Get the current working directory
 *****************************************************************************************/

char *get_cwd(char *str, uint len)
{
    getcwd(str, len);  

    if(str[strlen(str)-1] != '/') {
        strcat(str, "/");
    }

    return(str);
}

/*****************************************************************************************
 * Resolve the path name of the passed in filename
 *****************************************************************************************/

char *get_path(char *str, const char *ffname, uint len)
{
    char cwdir[1024];  /* current work dir */
    char *pname;       /* path name */
    char *ptr;         /* pointer to dir seperator */

    pname = NULL;

    get_cwd(cwdir, 1024);

    if(is_dir(ffname)) {
        pname = strdup(ffname);
    } else {
        if((ptr = strrchr(ffname, '/')) != NULL)  {
            pname = str_ndup(ffname, (size_t)(ptr - ffname + 1));
        } else {
            pname = strdup(cwdir);
        }
    }

    chdir(pname);
    get_cwd(str, len);
    chdir(cwdir);
    free(pname);

    return(str);
}

/*****************************************************************************************
 * Get the "filename part" of the passed in full filename
 *****************************************************************************************/

char *get_file(char *str, const char *ffname, uint len)
{
    char *ptr;         /* pointer to dir seperator */

    if((ptr = strrchr(ffname, '/')) != NULL)  {
        strncpy(str, ++ptr, len);
    } else {
        strncpy(str, ffname, len);
    }

    return(str);
}

/*****************************************************************************************
 * Resolve the full path name of the passed in filename
 *****************************************************************************************/

char *get_full_path(char *str, const char *fname, uint len)
{
    struct passwd *pw; /* passwd user data structure */
    char cwdir[1024];  /* current work dir */
    char pname[1024];  /* path name */
    char sfname[1024]; /* single file name */
    char *ptr;         /* pointer to dir seperator */
    char *remain;      /* remainder of path */
    char *user;        /* user name to resolve */

    if(str) {
        *str = '\0';
    }

    if(!fname || strlen(fname) == 0) {
        return(NULL);
    }

    memset(pname , 0, 1024);
    memset(sfname, 0, 1024);

    get_cwd(cwdir, 1024);

    if(*fname == '~') {
        fname++;

        if(strlen(fname) && *fname != '/') {
            if((remain = strchr(fname, '/'))) {
                user = str_ndup(fname, (size_t)(remain - fname));
                pw = getpwnam(user);
                fname = remain;
            } else {
                pw = getpwnam(fname);
                fname = "";
            }
        } else {
            pw = getpwuid(getuid());
        }

        if(pw) {
            strcpy(pname, pw->pw_dir);
            strcat(pname, fname);
        }
    } else if(*fname != '/') {
        strcpy(pname, cwdir);
        strcat(pname, fname);
    } else {
        strcpy(pname, fname);
    }

    if(strlen(pname)) {
        if(!file_exists(pname) || is_file(pname)) {
            ptr = strrchr(pname, '/');
            ptr[0] = '\0';
            ptr++;

            strcpy(sfname, ptr);
        }

        if(file_exists(pname) && is_dir(pname)) {
            if(str) {
                chdir(pname);
                get_cwd(str, len - strlen(sfname));
                strcat(str, sfname);
                chdir(cwdir);
            }

            return(str);
        }
    }

    return(NULL);
}

/*****************************************************************************************
 * Copy file dest to the file src
 *****************************************************************************************/

int file_copy(const char *src, const char *dest)
{
    size_t rd;
    int    fd_src;
    int    fd_dest;
    int    err = -1;
    char   buf[1024];

    if((fd_src = open(src, O_RDONLY)) != -1) {
        if((fd_dest = open(dest, O_CREAT | O_TRUNC | O_WRONLY, 0666)) != -1) {
            err = 0;

            while((rd = read(fd_src, buf, 1024))) {
                write(fd_dest, buf, rd);
            }

            close(fd_dest);
        }

        close(fd_src);
    }

    return(err);
}

/*****************************************************************************************
 * Move the file dest to the file src
 *****************************************************************************************/

int file_move(const char *src, const char *dest)
{
    int status;

    if((status = rename(src, dest)) != -1)
    {
        return(0);
    }

    if(errno == EXDEV)
    {
        if((status = file_copy(src, dest)) == -1)
        {
            unlink(src);
        }
    }

    return(status);
}

/*****************************************************************************************
 * Determine in the file is soft-locked
 *****************************************************************************************/

int file_is_locked(const char *fname)
{
    struct flock lock;
    int    fd;

    if((fd = open(fname, O_RDONLY)))
    {
        memset(&lock, 0, sizeof(lock));
        fcntl(fd, F_GETLK, &lock);

        if(lock.l_type == F_WRLCK || lock.l_type == F_RDLCK)
        {
            return(1);
        }
    }

    return(0);
}

/*****************************************************************************************
 * Get the last modified time the file
 *****************************************************************************************/

long file_get_last_modified(const char *fname)
{
    struct stat st;

    if(stat(fname, &st) != -1)
    {
        return((long)st.st_mtime);
    }

    return(0);
}

/*****************************************************************************************
 * Get the file type, either ascii (unix, max or dos), or binary.
 *****************************************************************************************/

FILE_TYPE file_get_type(const char *fname, FILE_TYPE def)
{
    FILE_TYPE ftype = def;
    FILE_TYPE ftpos;
    FILE *fptr;
    int ch;
    int ftgot = 0;
    int ftdone = 0;

    if((fptr = fopen(fname, "rb")) == NULL)
    {
        ftype = FT_NULL;
    }
    else
    {
        while((ch = fgetc(fptr)) != EOF)
        {
            switch(ch)
            {
                case '\0':
                    ftype = FT_BINARY;
                    ftdone = 1;

                    break;

                case '\n':
                    if( ftgot == 0 ) {
                        ftgot++;
                        ftype = FT_UNIX;
                    }
                    else
                    {
                        if( ftype != FT_UNIX ) {
                            ftype  = FT_BINARY;
                            ftdone = 1;
                        }
                    }
    
                    break;
    
                case '\r':
                    ftpos = (fgetc(fptr) == '\n') ? FT_DOS : FT_MAC;

                    if( ftgot == 0 ) {
                        ftgot++;
                        ftype = ftpos;
                    }
                    else
                    {
                        if( ftype != ftpos) {
                            ftype  = FT_BINARY;
                            ftdone = 1;
                        }
                    }
    
                    break;
            }

            if(ftdone || ftgot == 1)
            {
                break;
            }
        }

        fclose(fptr);
    }

    return(ftype);
}

/*****************************************************************************************
 * Determine if the two passed in filenames are actually the same file by comparing the
 * inodes of the files.
 *****************************************************************************************/

int file_is_same(const char *f1, const char *f2)
{
    struct stat st1;
    struct stat st2;

    stat(f1, &st1);
    stat(f2, &st2);

    return((st1.st_ino == st2.st_ino)?1:0);
}

/*****************************************************************************************
 * Determine if the file is a directory.
 *****************************************************************************************/

int is_dir(const char *fname)
{
    struct stat st;

    stat(fname, &st);

    if(S_ISDIR(st.st_mode))
    {
        return(1);
    }

    return(0);
}

/*****************************************************************************************
 * Determine if the file is a regular file and not a directory.
 *****************************************************************************************/

int is_file(const char *fname)
{
    struct stat st;

    stat(fname, &st);

    if(S_ISREG(st.st_mode))
    {
        return(1);
    }

    return(0);
}

/*****************************************************************************************
 * Determine if the file is read only.
 *****************************************************************************************/

int is_readonly(const char *fname)
{
    int fd;

    if(file_exists(fname)) {
        fd = open(fname, O_RDWR);

        if(fd < 0) {
            return(1);
        } else {
            close(fd);
            return(0);
        }
    }

    return(0);  
}

/*****************************************************************************************
 * Determine if the file is readable.
 *****************************************************************************************/

int is_readable(const char *fname)
{
    struct stat st;

    if(stat(fname, &st) == -1) {
        return(0);
    }

    return(1);
}

/*****************************************************************************************
 * Determine if we can write the file.
 *****************************************************************************************/

int file_can_write(const char *pname)
{
    return(!access(pname, W_OK));
}

/*****************************************************************************************
 * Determine if we can read the file.
 *****************************************************************************************/

int file_can_read(const char *pname)
{
    return(!access(pname, R_OK));
}

/*****************************************************************************************
 * Determine if we can read and write the file.
 *****************************************************************************************/

int file_can_rw(const char *pname)
{
    return(!access(pname, R_OK) && !access(pname, W_OK));
}

/*****************************************************************************************
 * Determine if the file exists.
 *****************************************************************************************/

int file_exists(const char *fname)
{
    return(!access(fname, F_OK));
}

/*****************************************************************************************
 * Save the contents of the standard input buffer to the specified file name.
 *****************************************************************************************/

void save_stdin(const char *dest)
{
    struct timeval tv;
    char   buf[1024];
    size_t r;
    fd_set rfds;
    FILE   *fptr;

    FD_ZERO(&rfds);
    FD_SET(0, &rfds);

    tv.tv_sec  = 0;
    tv.tv_usec = 100000;

    if((fptr = fopen(dest, "w")) != NULL) {
        if(select(1, &rfds, NULL, NULL, &tv)) {
            while((r = read(STDIN_FILENO, buf, 1024)) > 0) {
                fwrite(buf, 1, r, fptr);
            }

            freopen("/dev/tty", "r", stdin);
        }

        fclose(fptr);
    }
}

/*****************************************************************************************/
