/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2003
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Provides unix function wrappers.
 *
 *  Last Updated
 *    02/Jun/2003
 *
 */

#include "utils.h"

void long_to_char(long n, uchar *str)
{
    short i;

    for(i=0; i <= 2; i++) {
        str[i] = (n & 0xff);
        n = (unsigned)n >> 8;
    }

    str[3] = (n & 0xff);
}

long char_to_long(uchar *str)
{
    short i;
    long  l = 0;

    for(i=3; i >= 1; i--) {
        l += str[i];
        l <<= 8;
    }

    l += str[0];

    return(l);
}

