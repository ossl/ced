/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2003
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Provides unix function wrappers.
 *
 *  Last Updated
 *    02/Jun/2003
 *
 */

#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include "utils.h"

/****************************************************************************
 * Find the next non white space character in the given string.
 ****************************************************************************/

char *str_skip_white(char *s)
{
    while(isspace(*s)) {
        s++;
    }

    return((char*)s);
}

/****************************************************************************
 * Remove all escape characters from a string.
 ****************************************************************************/

char *str_unescape(char *s)
{
    ulong extra;
    ulong len;
    ulong i;

    if(s) {
        len = strlen(s);
        extra = 0;

        for(i=0; i < len; i++) {
            if(s[i] == '\\') {
                extra++;
                i++;
            }

            s[i - extra] = s[i];
        }

        s[i - extra] = '\0';
    }

    return(s);
}

/****************************************************************************
 * Decode escape codes into there real values.
 ****************************************************************************/

char *str_decode_escapes(char *s)
{
    ulong extra;
    ulong len;
    ulong i;

    if(s) {
        len = strlen(s);
        extra = 0;

        for(i=0; i < len; i++) {
            if(s[i] == '\\') {
                i++;

                switch(s[i]) {
                    case 't' : s[i - (++extra)] = 9;  break;
                    case 'n' : s[i - (++extra)] = 10; break;
                    case 'r' : s[i - (++extra)] = 13; break;
                    default  : s[i - extra] = s[i];
                }
            } else {
                s[i - extra] = s[i];
            }
        }

        s[i - extra] = '\0';
    }

    return(s);
}

/****************************************************************************
 * Perform a reverse search for a string.  See strstr.
 ****************************************************************************/

char *str_rstr(const char *haystack, const char *needle)
{
    char *s;
    char *last = NULL;

    if((s = strstr(haystack, needle))) {
        last = s;

        while((s = strstr(last+1, needle))) {
            last = s;
        }
    }

    return(last);
}

/****************************************************************************
 * Creates a random string of the length specified.
 ****************************************************************************/

char *str_random(ulong len)
{
    char *rs = (char*)malloc(len + 1);
    uint i;

    for(i=0; i < len; i++) {
        rs[i] = (rand() % 26) + 65;

        if(rand() < (RAND_MAX / 2)) {
            rs[i] = tolower(rs[i]);
        }
    }

    rs[len] = '\0';

    return(rs);
}

/****************************************************************************
 * Duplicates the first len characters of a string and returns it.
 ****************************************************************************/

char *str_ndup(const char *str, uint len)
{
    char *strnew = NULL;

    if(str && len) {
        strnew = calloc(len+1, sizeof(char));
        strncpy(strnew, str, min(strlen(str), len));
    }

    return(strnew);
}
