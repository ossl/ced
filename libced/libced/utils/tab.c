/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2003
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) a ny later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Provides unix function wrappers.
 *
 *  Last Updated
 *    02/Jun/2003
 *
 */

#include <stdlib.h>
#include <string.h>

#include <libced/tools/dstring.h>

#include "utils.h"

///// ALL DEPRECATED ? /////////////////////////////////////////////////////

uint map_column_to_x(dstring *ln, uint x, uint tabstops)
{
    char *buf;
    char *ptr;
    uint len;
    uint tab = 0;
    uint i;
    uint ins;

    len = dstring_get_length(ln);
    buf = dstring_get_string(ln);
    ptr = buf;

    while((ptr = strchr(ptr, '\t')) != NULL)
    {
        i = ptr - buf;

        if(i >= x)
        {
            break;
        }

        ins = (tabstops - ((tab+i) % tabstops))-1;
        tab += ins;
        x -= ins;

        if(i >= x)
        {
            x+=(tabstops - ((x+tab) % tabstops))-1;
            break;
        }

        ptr++;
    }

    free(buf);

    return(x);
}

uint map_x_to_column(dstring *ln, uint n, uint tabstops)
{
    char *buf;  /* copy of line */
    char *ptr;
    uint len;   /* real length of line */
    uint tab;   /* accumulation of tab spacing */
    uint ins;
    uint i;

    tab = 0;
    buf = dstring_get_string(ln);
    ptr = buf;
    len = dstring_get_length(ln);

    while((ptr = strchr(ptr, '\t')) != NULL)
    {
        i = ptr - buf;

        if(i >= n)
        {
            break;
        }

        ins = (tabstops - ((tab+i) % tabstops))-1;
        tab +=ins;
        ptr++;
    }

    free(buf);

    return(n+tab);
}

uint get_line_length_cols(dstring *ln, uint tabstops)
{
    return(map_x_to_column(ln, dstring_get_length(ln), tabstops));
}

uint get_actual_column_position(dstring *ln, uint n, uint tabstops, uint flag)
{
    char *buf;
    char *ptr;
    uint i, p;
    uint tab = 0;   
    uint ins = 0;

    buf = dstring_get_string(ln);
    ptr = buf;
    p = n;

    while((ptr = strchr(ptr, '\t')) != NULL)
    {
        i = ptr - buf;

        if(i >= p)
        {
            break;
        }

        ins = (tabstops - ((tab + i) % tabstops)) - 1;
        tab += ins;
        p -= min(p, ins);

        if(i >= p)
        {
            if( flag) n = i + tab + 1;
            if(!flag) n = i + tab - ins;

            break;
        }

        ptr++;
    }

    free(buf);

    return(n);
}
