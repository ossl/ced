/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2003
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Provides unix function wrappers.
 *
 *  Last Updated
 *    02/Jun/2003
 *
 */

#include <unistd.h>
#include <stdio.h>
#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "utils.h"

/****************************************************************************
 * Get the username of the currently logged on user.
 ****************************************************************************/

char *get_user_name(char *str, uint len)
{
    uid_t uid;
    struct passwd *p;

    uid = getuid();
    p = getpwuid(uid);
    strncpy(str, p->pw_name, len);

    return(str);
}

/****************************************************************************
 * Get the systems host name.
 ****************************************************************************/

char *get_host_name(char *str, uint len)
{
    gethostname(str, len);

    return(str);
}

/****************************************************************************
 * Get the home directory of the currently logged on user.
 ****************************************************************************/

char *get_home_dir(char *str, uint len)
{
    static char *home = NULL;
    char        *ptr;

    if( home == NULL ) {
        home = (char*)calloc(1024, sizeof(char));

        ptr = getenv("HOME");

        get_path(home, ptr, 1024);
    }

    strncpy(str, home, len);

    return(str);
}

/****************************************************************************
 * Fork a new process.
 ****************************************************************************/

void detach()
{
    switch(fork())
    {
        case -1: perror("Warning: couldn't fork process to detach");
                 break;

        case  0: break;

        default: exit(0);
    }
}

