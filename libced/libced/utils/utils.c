/*
 *  CED - The Editor                                
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Provides miscelleneous functions.
 *
 *  Last Updated
 *    16/Dec/2000
 *
 *    06/Apr/2003
 *      MAS - Added file_get_type
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <pwd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <ctype.h>
#include <time.h>
#include <stdarg.h>

#include <libced/common.h>
#include <libced/tools/dstring.h>
#include <libced/sysdata.h>

#include "utils.h"
#include "config.h"

/*****************************************************************************************
 * General Stuff
 *****************************************************************************************/

char *get_swap_name(char *str, const char *ffname, int ch, uint len)
{
    char tmp_p[1024];
    char tmp_f[256];

    get_path(tmp_p, ffname, 1024);
    get_file(tmp_f, ffname, 256);

    sprintf(str, "%s.~%s.sw%c", tmp_p, tmp_f, (char)ch);

    return(str);
}

char *get_ced_dir(char *str, uint len)
{
    get_home_dir(str, len);
    strcat(str, ".ced/");

    return(str);
}

char *get_keydef_ffname(char *str, uint len)
{
    get_ced_dir(str, len);
    strcat(str, "key.def");

    return(str);
}

char *get_sys_dir(char *str, uint len)
{
    if(SYSDATA_SYSDIR) {
        strncpy(str, SYSDATA_SYSDIR, 1024);
    }

    return(str);
}

char *get_paste_buffer_ffname(char *str, const char *name, uint len)
{
    get_ced_dir(str, len);
    strcat(str, "paste/");
    strcat(str, name);

    return(str);
}

char *get_conf_file(char *ffname, const char *env, const char *conf, const char *fname, uint len)
{
    char *fenv;
    char *fconf;

    fconf = NULL;  // eh ?

    if(env && ((fenv = getenv(env)) != NULL)) {
        get_full_path(ffname, fenv, len);

        if(file_exists(ffname)) {
            return(ffname);
        }  else {
            fprintf(stderr, "[ced] warning: environment variable %s ", env);
            fprintf(stderr, "contains none-existant filename\r\n");
        }
    }

    if(conf && fconf == NULL) {
        get_full_path(ffname, conf, len);

        if(file_exists(ffname)) {
            return(ffname);
        }  else {
            fprintf(stderr, "[ced] warning: file '%s' ", conf);
            fprintf(stderr, "specified in ced.conf does not exist\r\n");
        }
    }

    if(fname && fconf == NULL) {
        get_ced_dir(ffname, len);
        strcat(ffname, fname);

        if(file_exists(ffname)) {
            return(ffname);
        }
    }

    if(fname && fconf == NULL) {
        get_sys_dir(ffname, len);
        strcat(ffname, fname);

        if(file_exists(ffname)) {
            return(ffname);
        }
    }

    return(NULL);
}

/*****************************************************************************************
 * Library Info
 *****************************************************************************************/

const char *get_libced_version()
{
    return(SYSDATA_VERSION);
}

const int get_libced_version_minor()
{
    return(SYSDATA_V_MINOR);
}

const int get_libced_version_major()
{
    return(SYSDATA_V_MAJOR);
}

const int get_libced_version_micro()
{
    return(SYSDATA_V_MICRO);
}

