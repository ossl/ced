#ifndef _UTILS_H
#define _UTILS_H

#include <libced/common.h>

/*****************************************************************
 * Debugging
 *****************************************************************/

void T(const char *fmt, ...);

/*****************************************************************
 * String
 *****************************************************************/

char *str_skip_white(char *s);
char *str_rstr(const char *haystack, const char *needle);
char *str_ndup(const char *str, uint len);
char *str_unescape(char *s);
char *str_decode_escapes(char *s);
char *str_random(ulong len);

void detach(void);
void long_to_char(long n, uchar *str);
void save_stdin(const char *dest);

char *get_ced_dir(char *str, uint len);
char *get_sys_dir(char *str, uint len);
char *get_keydef_ffname(char *str, uint len);
char *get_paste_buffer_ffname(char *str, const char *name, uint len);
char *get_user_name(char *str, uint len);
char *get_host_name(char *str, uint len);
char *get_home_dir(char *str, uint len);
char *get_swap_name(char *str, const char *ffname, int ch, uint len);
char *get_cwd(char *str, uint len);
char *get_path(char *str, const char *ffname, uint len);
char *get_file(char *str, const char *ffname, uint len);
char *get_full_path(char *str, const char *fname, uint len);

int  file_copy(const char *src, const char *dest);
int  file_move(const char *src, const char *dest);
int  file_can_write(const char *pname);
int  file_can_read(const char *pname);
int  file_can_rw(const char *pname);
int  file_exists(const char *fname);
int  file_is_locked(const char *fname);
int  file_is_same(const char *f1, const char *f2);
long file_get_last_modified(const char *fname);
FILE_TYPE file_get_type(const char *fname, FILE_TYPE def);

int  is_readable(const char *fname);
int  is_readonly(const char *fname);
int  is_dir(const char *fname);
int  is_file(const char *fname);

long char_to_long(uchar *str);

/*
uint map_column_to_x(dstring *ln, uint x, uint tabstops);
uint map_x_to_column(dstring *ln, uint n, uint tabstops);
uint get_line_length_cols(dstring *ln, uint tabstops);
uint get_actual_column_position(dstring *ln, uint n, uint tabstops, uint flag);
*/

char *get_conf_file(char *ffname, const char *env, const char *conf, const char *fname, uint len);

const char *get_libced_version(void);
const int   get_libced_version_minor(void);
const int   get_libced_version_major(void);
const int   get_libced_version_micro(void);

#endif /* _UTILS_H */

