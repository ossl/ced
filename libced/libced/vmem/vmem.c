/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999, 2000, 2001, 2002, 2003
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Implements a basic swap file system.  Consists of blocks of
 *    data which can be either in memory or in the swap file, or both.
 *
 *  Last Updated
 *    08 Oct 2002 - MAS
 *    30 Mar 2003 - MAS
 *    
 * To Do
 *    - Write some debugging and analysis code
 *    - Optimise disk block size usage
 *    - Optimise cacheing
 *    - Check for memory leaks
 *    - Tidy up code
 */

//#define DEBUG

#include <stdlib.h>
#include <sys/mount.h>
#include <sys/types.h>
#include <ustat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#include <libced/common.h>
#include <libced/hash/inthash.h>
#include <libced/utils/utils.h>
#include <libced/tools/error.h>
#include <libced/tools/debug.h>

#include "vmem.h"

typedef enum _alloc_options
{
    ALLOC_NORMAL = 0,
    ALLOC_FORCE  = 1
}   ALLOC_OPT;

void vm_debug_status(vmem*);
void vm_show_cached(vmem*);
void vm_show_used(vmem*);
void vm_show_free(vmem*);

static block *vm_alloc_block(vmem *vm, int pages, ALLOC_OPT opt);
static block *vm_release(vmem *vm, int pages);

///////////////////////////////////////////////////////////////////////////////////
// Add block to the front of the used list.
///////////////////////////////////////////////////////////////////////////////////

void vm_add_used(vmem *vm, block *blk)
{
    blk->next = NULL;
    blk->prev = vm->used_last;

    if( vm->used_last ) {
        vm->used_last->next = blk;
        vm->used_last = blk;
    } else {
        vm->used_list = blk;
        vm->used_last = blk;
    }
}

///////////////////////////////////////////////////////////////////////////////////
// Add block to the cache hash.
///////////////////////////////////////////////////////////////////////////////////

void vm_add_cache(vmem *vm, block *blk)
{
    ih_set(vm->cache, blk->num, (void*)blk);
    
    vm->pages_cached += blk->pages;
}

///////////////////////////////////////////////////////////////////////////////////
// Remove a block from the used list and return a pointer to it.
///////////////////////////////////////////////////////////////////////////////////

block *vm_rem_used(vmem *vm, block *blk)
{
    block *list = NULL;

    if( vm->used_last == blk) {
        vm->used_last = blk->prev;
        vm->used_last->next = NULL;

        return(blk);
    }
    else
    {
        for(list = vm->used_list; list != NULL; list = list->next)
        {
            if(list == blk)
            {
                if( list->prev ) {
                    list->prev->next = list->next;
                }

                if( list->next ) {
                    list->next->prev = list->prev;
                }

                list->next = NULL;
                list->prev = NULL;

                return(blk);
            }
        }
    }

    return(NULL);
}

block *vm_rem_cache(vmem *vm, block *blk)
{
    if(vm && blk)
	{
        if((blk = ih_get(vm->cache, blk->num)))
		{
            ih_del(vm->cache, blk->num);
            vm->pages_cached -= blk->pages;
			
            return(blk);
        }
    }

    return(NULL);
}

///////////////////////////////////////////////////////////////////////////////////
// Search used list for a block number.
///////////////////////////////////////////////////////////////////////////////////

block *vm_srch_used(vmem *vm, long num)
{
    block *list = vm->used_list;

    while( list && list->num != num ) {
           list = list->next;
    }

    return(list);
}

///////////////////////////////////////////////////////////////////////////////////
// Add a block to the front of the free list
///////////////////////////////////////////////////////////////////////////////////

void vm_add_free(vmem *vm, block *blk)
{
    if(vm && blk)
	{
        blk->next = vm->free_list;
        vm->free_list = blk;
        free(blk->data);
    }
}

///////////////////////////////////////////////////////////////////////////////////
// Remove a block from the free list
///////////////////////////////////////////////////////////////////////////////////

block *vm_rem_free(vmem *vm, int pages)
{
    block *last;
    block *list;
    block *blk;

    TRACE(D_MSG, "vmem::vm_rem_free: get free block of %d pages", pages);

    if((list = vm->free_list) != NULL)
    {
        last = NULL;

        do
		{
            if(list->pages >= pages)
            {
                TRACE(D_MSG, "vmem::vm_rem_free: found a block >= size required");

                if( list->pages > pages ) {
                    list->pages -= pages;

                    TRACE(D_MSG, "vmem::vm_rem_free: creating new block from free block");

                    blk = vm_alloc_block(vm, pages, ALLOC_FORCE);
                    blk->num = list->num + list->pages;

                    return(blk);
                }
                else
                {
                    if(last == NULL)
                    {
                        vm->free_list = list->next;
                    }
                    else
                    {
                        last->next = list->next;
                    }

                    TRACE(D_MSG, "vmem::vm_rem_free: removing and returning free block from list");

                    list->next = NULL;
                    list->data = (char*)calloc((ulong)vm->page_size * pages, sizeof(char));

                    return(list);
                }
            }

            last = list;

        } while((list = list->next) != NULL);
    }

    return(NULL);
}

///////////////////////////////////////////////////////////////////////////////////
// vm_get_block
///////////////////////////////////////////////////////////////////////////////////

block *vm_get_block(vmem *vm, long bnum, long npages)
{
    block *blk  = NULL;

    TRACE(D_MSG, "vmem::vm_get_block: block number:%lu, pages:%lu", bnum, npages);

    if((blk = ih_get(vm->cache, bnum))) {
        blk->locked = 1;

        vm_rem_used(vm, blk);
        vm_add_used(vm, blk);
        vm->hit++;
    }
    else
    {
        if((blk = vm_alloc_block(vm, npages, ALLOC_NORMAL)) ||
           (blk = vm_release(vm, npages)) ||
           (blk = vm_alloc_block(vm, npages, ALLOC_FORCE)))
        {
            blk->num = bnum;
            blk->locked = 1;
            blk->dirty = 0;

            vm_read(vm, blk);
            vm_add_used(vm, blk);
            vm_add_cache(vm, blk);
            vm->miss++;
        }
        else
        {
            TRACE(D_ERROR, "vmem::vm_get_block: no memory or disk space available");
        }
    }

    if(blk->pages != npages)
    {
        TRACE(D_FATAL, "vmem::vm_get_block: page count mismatch [req: %d, got %d]", npages, blk->pages);
    }

    //vm_debug_status(vm);

    return(blk);
}

///////////////////////////////////////////////////////////////////////////////////
// Putting a block confirms any changes made to it by marking it
// dirty.  The block is also unlocked so it can be written to
// disk and forgotten.
///////////////////////////////////////////////////////////////////////////////////

void vm_put_block(vmem *vm, block *blk)
{
    if(vm->fname != NULL)
    {
        TRACE(D_MSG, "vmem::vm_put_block: putting block (%lu)", blk->num);

        blk->dirty = 1;
        blk->locked = 0;
    }
}

///////////////////////////////////////////////////////////////////////////////////
// Unlock a block but do not alter its dirty status.  This effectively
// loses any changes made to a block.
///////////////////////////////////////////////////////////////////////////////////

void vm_unlock_block(vmem *vm, block *blk)
{
    if(vm->fname != NULL)
    {
        TRACE(D_MSG, "vmem::vm_unlock_block: unlocking block (%lu)", blk->num);

        blk->locked = 0;
    }
}

///////////////////////////////////////////////////////////////////////////////////
// Releasing all the blocks simply means that all locked blocks are
// written back to the swap file and unlocked.  Should be used with
// caution.
///////////////////////////////////////////////////////////////////////////////////

void vm_unlock_all(vmem *vm)
{
    block *list;

    if(vm->fname != NULL) {
        for(list = vm->used_list; list != NULL; list = list->next) {
            if(list->locked) {
                vm_put_block(vm, list);
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////
// Releasing all the blocks simply means that all locked blocks are written back
// to the swap file and unlocked.  Should be used with caution.
///////////////////////////////////////////////////////////////////////////////////

void vm_release_all(vmem *vm)
{
    block *list;

    if(vm->fname != NULL) {
        for(list = vm->used_list; list != NULL; list = list->next) {
            if(list->locked) {
                vm_put_block(vm, list);
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////
// Release the first available unlocked block and return it.
///////////////////////////////////////////////////////////////////////////////////

static block *vm_release(vmem *vm, int pages)
{
    block *blk;
    block *list;
    int   released;
    
    TRACE(D_MSG, "vmem::vm_release: release [pages: %d]", pages);
    
    released = 0;

    for(list = vm->used_list; list != NULL; )
    {
        blk = list;

        list = list->next;
    
        if(!blk->locked)
        {
            if(blk->dirty && vm->fname != NULL)
			{
                if(!vm_write(vm, blk))
                {
                    TRACE(D_WARN, "vmem::vm_release: skipping unwriteable block");
                }
            }
	    
            released += blk->pages;

            vm_rem_used(vm, blk);
            vm_rem_cache(vm, blk);

            TRACE(D_MSG, "vmem::vm_release_block: releasing block: %lu [pages: %d]", blk->num, blk->pages);

            if(released < pages)
            {
                free(blk->data);
                free(blk);
            }
            else
            {
                if( blk->pages != pages ) {
                    blk->pages = pages; 
                    blk->data = (char*)realloc(blk->data, sizeof(char) * (vm->page_size * pages));

		            memset(blk->data, 0, (ulong)vm->page_size * pages);
                }
	    
	    	    blk->num = 0;
		
                return(blk);
	        }
        }
    }

    return(NULL);
}

///////////////////////////////////////////////////////////////////////////////////
// Change the state of a passed in block.  Needed ??
///////////////////////////////////////////////////////////////////////////////////

void vm_set_dirty(vmem *vm, block *blk)
{
    blk->dirty = 1;
}

void vm_set_locked(vmem *vm, block *blk)
{
    blk->locked = 1;
}

///////////////////////////////////////////////////////////////////////////////////
// Write all dirty blocks back to the swap file and reset the dirty flag.
///////////////////////////////////////////////////////////////////////////////////

void vm_sync_all(vmem *vm)
{
    block *list;

    if(vm->fname == NULL)
    {
        return;
    }
  
    for(list = vm->used_list; list != NULL; list = list->next) {
        if(list->dirty) {
            if(vm_write(vm, list)) {
                list->dirty = 0;
            } else {
                TRACE(D_WARN, "vmem::vm_sync_all: warning: couldn't sync block %lu", list->num);
            }
        }
    }

    vm_write_header(vm);
    vm_debug_status(vm);
}

///////////////////////////////////////////////////////////////////////////////////
// Display debugging information about the current state of the swap file.
///////////////////////////////////////////////////////////////////////////////////

void vm_debug_status(vmem* vm)
{
#ifdef DEBUG
    vm_show_cached(vm);
    vm_show_used(vm);
    vm_show_free(vm);
#endif

    TRACE(D_MSG, "vmem::vm_debug_status: [hits: %lu]", vm->hit);
    TRACE(D_MSG, "vmem::vm_debug_status: [misses: %lu]", vm->miss);
}

///////////////////////////////////////////////////////////////////////////////////
// Free a block thats currently in the cache.  This block number can now be
// re-used for new data.
///////////////////////////////////////////////////////////////////////////////////

void vm_free_block(vmem *vm, block *blk)
{
    vm_rem_used(vm, blk);
    vm_rem_cache(vm, blk);
    vm_add_free(vm, blk);
}

///////////////////////////////////////////////////////////////////////////////////
// Create a new vmem structure with default values and return a pointer to it.
///////////////////////////////////////////////////////////////////////////////////

vmem *vm_new(char *fname)
{
    vmem *vm;

    if((vm = (vmem*)calloc(1, sizeof(vmem))) == NULL) {
        TRACE(D_ERROR, "vmem::vm_new: couldn't allocate memory");
        return(NULL);
    }

    vm->fdesc = -1;
    vm->page_size = DEFAULT_PAGE_SIZE;
    vm->free_list = NULL;
    vm->used_list = NULL;
    vm->used_last = NULL;
    vm->cache = ih_new(2048); // for now..
    vm->blocks_used = 0;
    vm->pages_used = 0;
    vm->pages_cached = 0;
    vm->max_pages = MAX_CACHE_SIZE / vm->page_size;
    vm->dirty = 1;

    strcpy(vm->fname, fname);
    strncpy(vm->vmh.ident, VMEM_IDENT_STRING, 4);
    strncpy(vm->vmh.version, VMEM_VERSION_STRING, 8);
	
    long_to_char(vm->page_size, vm->vmh.page_size);

    return(vm);
}

///////////////////////////////////////////////////////////////////////////////////
// Destroy the vmem object.  The swap file is NOT synchronised first so any
// changes made in blocks that have no been written back to the swap file will
// be lost.
///////////////////////////////////////////////////////////////////////////////////

void vm_free(vmem *vm)
{
    block *tmp;

    ih_free(vm->cache);

    while((tmp = vm->used_list) != NULL)
	{
        vm->used_list = vm->used_list->next;
		
        free(tmp->data);
        free(tmp);
    }

    while((tmp = vm->free_list) != NULL)
	{
        vm->free_list = vm->free_list->next;
		
        free(tmp);
    }

    free(vm);
}

///////////////////////////////////////////////////////////////////////////////////
// Create a new block of n pages and assign it a block number new unique block
// number.  The block is locked and returned.
///////////////////////////////////////////////////////////////////////////////////

block *vm_new_block(vmem *vm, long pages)
{
    long   total_size;
    long   page_size;
	long   p;
    block  *blk = NULL;
    block  *tmp;
//    int m;

    TRACE(D_MSG, "vmem::vm_new_block: requested block of %lu pages", pages);

    page_size = vm->page_size;
    total_size = page_size * pages;

    // The algorithm for getting a new block tries in the following order: -
    //     1) Free blocks
    //     2) Allocate block
    //     3) Release block
    //     4) Force allocate block
    //
    // We go for the free blocks first to keep the swap file as small as
    // possible.  If that fails we make sure the cache is utilised by
    // allocating blocks.  If the cache is full, then we try and release
    // a block, hopefully the user will of remembered to unlock or put
    // blocks that they aren't currently using.
    //
    // If all else fails, we allocate a block and exceed the cache limit.
    // This is better than a crash !
    //

    if((blk = vm_rem_free(vm, pages)))
    {
        TRACE(D_MSG, "vmem::vm_new_block: [opt1] found a free block", pages);

        // We need to release here because we are reusing a free block and
        // putting it in the used list.  If we don't do any releasing then
        // the used list will get too big.

        p = vm->pages_cached + pages;

		if( p >  vm->max_pages ) {
		    p -= vm->max_pages;
			
            if((tmp = vm_release(vm, p)) != NULL)
			{
			    free(tmp->data);
				free(tmp);
            }
	    }
	
        memset(blk->data, total_size, 0);
    }
/*
    else
    {
        m = 0;

        if(((blk = vm_alloc_block(vm, pages, ALLOC_NORMAL)) && m = 1)
        || ((blk = vm_release(vm, pages))                   && m = 2)
        || ((blk = vm_alloc_block(vm, pages, ALLOC_FORCE))  && m = 3))
        {
            blk->num = vm->pages_used;
            vm->pages_used += pages;
        }

        switch(m)
        {
            case 1: TRACE(D_MSG, "vmem::vm_new_block: [opt2] allocated a block", pages); break;
            case 2: TRACE(D_MSG, "vmem::vm_new_block: [opt2] released a block", pages);
            case 3: TRACE(D_MSG, "vmem::vm_new_block: [opt2] force allocated a block", pages);
        }
    }
*/
    else if((blk = vm_alloc_block(vm, pages, ALLOC_NORMAL)))
    {
        TRACE(D_MSG, "vmem::vm_new_block: [opt2] allocated a block", pages);
	
        blk->num = vm->pages_used;
        vm->pages_used += pages;
    }
    else if((blk = vm_release(vm, pages)))
    {
        TRACE(D_MSG, "vmem::vm_new_block: [opt3] released a block", pages);
	
        blk->num = vm->pages_used;
        vm->pages_used += pages;
    }
    else if((blk = vm_alloc_block(vm, pages, ALLOC_FORCE)))
    {
        TRACE(D_MSG, "vmem::vm_new_block: [opt4] force allocated a block", pages);
	
        blk->num = vm->pages_used;
        vm->pages_used += pages;
    }

    if( blk ) {
        blk->locked = 1;
        blk->dirty = 1;

        vm_add_used(vm, blk);
        vm_add_cache(vm, blk);
    } else {
        TRACE(D_WARN, "vmem::vm_new_block: no memory or disk space available");
    }

    vm_debug_status(vm);

    return(blk);
}

int vm_set_header_user_data(vmem *vm, void *ptr, int size)
{
    size = min(size, 1004);

    memcpy(vm->vmh.user_data, ptr, (uint)size);

    return(1);
}

int vm_write_header(vmem *vm)
{
    vm_update_header(vm);

    if(lseek(vm->fdesc, 0, SEEK_SET) == 0) {
        write(vm->fdesc, &vm->vmh, VMEM_HEADER_SIZE);
        return(1);
    }

    return(0);
}

int vm_read_header(vmem *vm)
{
    if(lseek(vm->fdesc, 0, SEEK_SET) == 0) {
        if(read(vm->fdesc, &vm->vmh, VMEM_HEADER_SIZE) == VMEM_HEADER_SIZE) {
            if(!strncmp(vm->vmh.ident, VMEM_IDENT_STRING, 4)) {
                if(!strncmp(vm->vmh.version, VMEM_VERSION_STRING, 8)) {
                    return(1);
                }
            }
        }
    }

    TRACE(D_WARN, "vmem::vm_read_header: couldn't read header");

    return(0);
}

int vm_update_header(vmem *vm)
{
    long_to_char(vm->pages_used, vm->vmh.pages_used);

    return(1);
}

///////////////////////////////////////////////////////////////////////////////////
// Read a block from the disk.
///////////////////////////////////////////////////////////////////////////////////

int vm_read(vmem *vm, block *blk)
{
    long   r;
    int    fdesc;
    int    pagesize;
    int    blksize;
    long   offset;

    if(vm->fname == NULL)
    {
        return(0);
    }

    TRACE(D_MSG, "vmem::vm_read: read block (%ld)", blk->num);

    pagesize = vm->page_size;
    blksize = blk->pages * pagesize;
    offset = (blk->num+1) * pagesize;
    fdesc = vm->fdesc;

    if(lseek(fdesc, offset, SEEK_SET) == offset) {
        if((r = read(fdesc, blk->data, (uint)blksize)) != blksize) {
            TRACE(D_ERROR, "vmem::vm_read: corrupt block (bnum: %ld, bsize: %d, read: %ld)", blk->num, blksize, r);
        } else {
            return(1);
        }
    } else {
        TRACE(D_ERROR, "vmem::vm_read: error seeking (bnum: %ld)", blk->num);
    }

    return(0);
}

///////////////////////////////////////////////////////////////////////////////////
// Write a block to the disk.
///////////////////////////////////////////////////////////////////////////////////

int vm_write(vmem *vm, block *blk)
{
    int    fdesc;
    int    pagesize;
    int    blksize;
    long   offset;
    char   *errstr;

    if(vm->fname == NULL)
    {
        return(0);
    }

    TRACE(D_MSG, "vmem::vm_write: write block (%ld)", blk->num);

    pagesize = vm->page_size;
    blksize = blk->pages * pagesize;
    offset = (blk->num+1) * pagesize;
    fdesc = vm->fdesc;

    if(lseek(fdesc, offset, SEEK_SET) == offset)
    {
        if(write(fdesc, blk->data, (uint)blksize) != blksize)
        {
            errstr = error_get_str(errno);

            TRACE(D_ERROR, "vmem::vm_write: error writing: %s", errstr);
        }
        else
        {
            return(1);
        }
    }
    else
    {
        TRACE(D_ERROR, "vmem::vm_write: error seeking to offset %lu\n", offset);
    }

    return(0);
}

///////////////////////////////////////////////////////////////////////////////////
// Rename the swap file.
//
// Do we need to close the file and re-open it when we rename it ?  Lets do so
// for safety for now.
///////////////////////////////////////////////////////////////////////////////////

int vm_rename(vmem *vm, char *fnew)
{
    int st;

    if(file_exists(fnew))
    {
        errno = EEXIST;
        return(-1);
    }

    vm_close(vm);

    if((st = file_move(vm->fname, fnew)) != -1)
    {
        strcpy(vm->fname, fnew);
    }

    vm_open(vm);

    return(st);
}

///////////////////////////////////////////////////////////////////////////////////
// Open the swap file.
///////////////////////////////////////////////////////////////////////////////////

int vm_open(vmem *vm)
{
    uint   lh = 0;
    int    fdesc;
    struct flock lock;

    if(vm->fname == NULL) {
        return(0);
    }

    if(file_exists(vm->fname)) {
        lh = 1;
    }

    if((fdesc = open(vm->fname, O_CREAT | O_RDWR, (mode_t)0600)))
    {
        vm->fdesc = fdesc;

        memset(&lock, 0, sizeof(lock));

        lock.l_type  = F_WRLCK;
        lock.l_start = 0;
        lock.l_len   = 0;

        fcntl(fdesc, F_SETLK, &lock);

        if(lh && vm_read_header(vm))
        {
            vm->page_size = char_to_long(vm->vmh.page_size);
            vm->pages_used = char_to_long(vm->vmh.pages_used);
            vm->max_pages = vm->pages_used + (MAX_CACHE_SIZE / vm->page_size);
        }
        else
        {
            ftruncate(fdesc, 0);

            vm->page_size = DEFAULT_PAGE_SIZE;
            vm->pages_used = 0;
            vm->max_pages = MAX_CACHE_SIZE / vm->page_size;

            long_to_char(vm->page_size, vm->vmh.page_size);
        }

        vm_write_header(vm);
    }

    return(1);
}

///////////////////////////////////////////////////////////////////////////////////
// Close the swap file.
///////////////////////////////////////////////////////////////////////////////////

void vm_close(vmem *vm)
{
    if(vm->fname == NULL)
    {
        return;
    }

    close(vm->fdesc);
}

static block *vm_alloc_block(vmem *vm, int pages, ALLOC_OPT opt)
{
    block *blk = NULL;

//    if(opt == ALLOC_FORCE || (vm->cache->entries < vm->max_pages))
//    if(opt == ALLOC_FORCE || (vm->pages_used < vm->max_pages))
    if(opt == ALLOC_FORCE || (vm->pages_cached < vm->max_pages))
    {
        TRACE(D_MSG, "vmem::vm_alloc_block: allocating %d pages [%d bytes]", pages, vm->page_size * pages);

        blk = (block*)calloc(1, sizeof(block));
        blk->data = (char*)calloc(sizeof(char), (ulong)vm->page_size * pages);
        blk->pages = pages; 
    }

    return(blk);
}

long vm_get_cache_hits(vmem *vm)
{
    return(vm->hit);
}

long vm_get_cache_misses(vmem *vm)
{
    return(vm->miss);
}

inline int vm_get_page_size(vmem *vm)
{
    return(vm->page_size);
}

///////////////////////////////////////////////////////////////////////////////////
// DEBUG
///////////////////////////////////////////////////////////////////////////////////

void vm_show_cached(vmem *vm)
{
    TRACE(D_RAW, "vmem::vm_show_cached: ");
    TRACE(D_RAW, "[");

    if(vm->cache->entries > 0)
	{
        TRACE(D_RAW, "%lu", vm->cache->entries);
    }

    TRACE(D_RAW, "]\n");
}

void vm_show_used(vmem *vm)
{
    long nb = 0;
    block *list = vm->used_list;

    TRACE(D_RAW, "vmem::vm_show_used: ");
    TRACE(D_RAW, "[");

    if(list == NULL)
	{
        TRACE(D_RAW, "NONE]");
    }
	else
	{
        for(;list;list = list->next)
        {
            TRACE(D_RAW, " %lu ", list->num);
            nb++;
        }

        TRACE(D_RAW, "] <n:%lu>", nb);
    }

    TRACE(D_RAW, "\n");
}

void vm_show_free(vmem *vm)
{
    long nb = 0;
    block *list = vm->free_list;

    TRACE(D_RAW, "vmem::vm_show_free: ");
    TRACE(D_RAW, "[");

    if(list == NULL)
	{
        TRACE(D_RAW, "NONE]");
    }
	else
	{
        for(;list;list = list->next)
        {
            TRACE(D_RAW, " %lu ", list->num);

            nb++;
        }

        TRACE(D_RAW, "] <n:%lu>", nb);
    }

    TRACE(D_RAW, "\n");
}

///////////////////////////////////////////////////////////////////////////////////

