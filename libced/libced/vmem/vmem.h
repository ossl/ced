#ifndef _VMEM_H
#define _VMEM_H

#include <stdio.h>
#include "../common.h"
#include "../hash/inthash.h"

#define VMEM_IDENT_STRING   "vmem"    /* vmem ident */
#define VMEM_VERSION_STRING "0.01"    /* current vmem version */
#define VMEM_HEADER_SIZE     1024     /* size of vmem header */
#define DEFAULT_PAGE_SIZE    4096     /* size of a page */

//#define MAX_CACHE_SIZE       1048576  /* max cache size in bytes */
//#define MAX_CACHE_SIZE       524288  /* max cache size in bytes */
//#define MAX_CACHE_SIZE       262144  /* max cache size in bytes */
//#define MAX_CACHE_SIZE       131072  /* max cache size in bytes */
//#define MAX_CACHE_SIZE       65536  /* max cache size in bytes */

#define MAX_CACHE_SIZE       40960    /* use for testing vmem/buffer */

typedef struct vmem_struct  vmem;     /* Virtual memory structure */
typedef struct block_struct block;    /* List of blocks */
typedef struct vmem_header  vmhead;

struct vmem_header
{
    char ident[4];         /* vmem */
    char version[8];       /* x.xx-x */
    char page_size[4];     /* size of page, stored as chars */
    char pages_used[4];    /* pages used in swap, stored as chars */
    char user_data[1004];  /* extra user data */
};

struct vmem_struct
{
    char     fname[256];   /* Swap file name */
    int      fdesc;        /* Swap file descriptor */
    inthash  *cache;       /* Hash of cached blocks */
    block    *free_list;   /* Head of free block list */
    block    *used_list;   /* Head of used block list */
    block    *used_last;   /* Least recently used block */
    long     blocks_used;  /* Number of blocks used */
    long     pages_cached; /* Number of pages currently cached */
    long     pages_used;   /* Number of pages used */
    int      page_size;    /* Page size */
    int      dirty : 1;    /* Dirty flag */
    int      max_pages;    /* Max pages in cache */
    long     hit;          /* Number of hits */
    long     miss;         /* Number of misses */
    vmhead   vmh;          /* Swap file header */
};

/* need lookup from actual block number to cache block number ?? */

struct block_struct
{
    long    num;        /* Block number and position */
    long    pages;      /* Number of pages in block */
    char    *data;      /* Data */
    block   *next;      /* Next block */
    block   *prev;      /* Previous block */
    int     locked : 1; /* Locked flag */
    int     dirty  : 1; /* Dirty flag */
};

/* user functions */

vmem   *vm_new(char *fname);
void    vm_free(vmem *vm);
void    vm_close(vmem *);
int     vm_open(vmem *);

block  *vm_new_block(vmem *, long);
void    vm_unlock_block(vmem *, block *blk);
void    vm_free_block(vmem *, block *blk);

/* search for block in cache */

block  *vm_find_block(vmem *, long);
block  *vm_get_block(vmem *, long, long);
void    vm_put_block(vmem *vm, block *blk);
void    vm_sync_all(vmem *vm);
void    vm_release_all(vmem *vm);
void    vm_unlock_all(vmem *vm);
void    vm_set_dirty(vmem *vm, block *blk);
void    vm_set_locked(vmem *vm, block *blk);

/* used and free list manipulation */

void    vm_add_free(vmem *, block *);
void    vm_add_used(vmem *, block *);
void    vm_add_cache(vmem *vm, block *blk);
block  *vm_rem_free(vmem *, int pages);
block  *vm_rem_used(vmem *, block *blk);
block  *vm_rem_cache(vmem *vm, block *blk);
block  *vm_srch_used(vmem *, long);

int vm_write_header(vmem *vm);
int vm_read_header(vmem *vm);
int vm_update_header(vmem *vm);
int vm_rename(vmem *vm, char *fnew);

long vm_get_cache_hits(vmem *vm);
long vm_get_cache_misses(vmem *vm);

/* internal functions only */

int vm_write(vmem *, block *);
int vm_read(vmem *, block *);

/* Attribs */

inline int vm_get_page_size(vmem *vm);

int vm_set_header_user_data(vmem *vm, void *ptr, int size);

#endif /* _VMEM_H */

