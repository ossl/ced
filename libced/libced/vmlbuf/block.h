#ifndef _BLOCK_H
#define _BLOCK_H

enum messages
{
  BLOCK_OK,
  BLOCK_FULL,
  BLOCK_BAD_IDX,
};

#endif /* _BLOCK_H */

