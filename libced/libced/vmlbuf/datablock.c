/*
 *  CED - The Editor
 *
 *  Copyright (C) Mark Simonetti 2000
 *  By Mark Simonetti
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Provides functions to manipulate and query a datablock.
 *    Datablocks are a fixed size block of memory containing
 *    a header, which contains information about the block,
 *    and textual data.
 *
 *  Last Updated
 *    22nd September 2000
 *
 */

#include <string.h>

#include <libced/common.h>
#include <libced/tools/dstring.h>

#include "datablock.h"

/*
 * Creates a new datablock by casting it onto the memory space specified.
 * The memory passed is normally that which was initialised as a vmem
 * block, so it can be swapped out to disk.
 */

dbheader *dblk_new(void *mem, int pages)
{
    dbheader* dblk;

    dblk = (dbheader*)mem;
    dblk->blktype = 'd';
    dblk->pages = pages;
    dblk->used = 0;
    dblk->nlines = 0;
    dblk->size = BLOCK_PAGE_SIZE * dblk->pages;

    return(dblk);
}

/*
 * Insert a line into the datablock.  If the line specified is one more
 * than the number of lines in the block, it is simply appended, otherwise
 * data is moved to make room for the line.  If there is no room or the
 * block specified doesn't exist, an error is returned and no change is made.
 */

int dblk_insert(dbheader *dblk, int atline, dstring *ln)
{ 
    BLOCK_IDX *off;
    char   *pre;
    char   *end;
    char   *pos;
    char   *mem;
    char   *start;
    int    used = dblk->used;
    int    nlines = dblk->nlines;
    int    len = dstring_get_length(ln);
    int    mxsize;
    int    insize;
    uint   i;
    char   *eol;

    mxsize = dblk->size - sizeof(dbheader) - used;
    insize = len + BLOCK_IDX_SIZE;

    if(insize > mxsize) {
        return(BLOCK_FULL);
    }

    if(atline > nlines+1) {
        return(BLOCK_BAD_IDX);
    }

    end = (char*)dblk + dblk->size;
    mem = (char*)dblk + sizeof(dbheader);
    off = (BLOCK_IDX*)mem;
    start = end - used + (BLOCK_IDX_SIZE * nlines);
    pos = start - len;

    if(atline == nlines + 1) {
        memcpy(pos, ln->data, len);
        off[nlines] = pos - mem;
    } else {
        eol = (atline == 1) ? end : &mem[off[atline-2]];
        pre = eol - len;

        memmove(pos, start, eol - start);
        memcpy(pre, ln->data, len);
        memmove(&off[atline], &off[atline-1], BLOCK_IDX_SIZE * (nlines - (atline-1)));
        off[atline-1] = pre - mem;

        for(i=atline; i <= nlines; i++) {
            off[i] -= len;
        }
    }

    dblk->used += insize;
    dblk->nlines++;

    return(BLOCK_OK);
}

/*
 * Deletes a line from the datablock.  If there is data following the
 * line to be deleted, that data is moved back over the top of the
 * line to be deleted.  An error return code is generated if the line
 * number specified is out of range.
 */

int dblk_delete(dbheader *dblk, int lnum)
{
    BLOCK_IDX *off;
    char   *end;
    char   *pos;
    char   *mem;
    char   *start;
    int    used = dblk->used;
    int    nlines = dblk->nlines;
    int    mxsize;
    uint   i;
    uint   len;
    char   *eol;

    mxsize = dblk->size - sizeof(dbheader) - used;

    if(lnum > nlines) return(BLOCK_BAD_IDX);

    end = (char*)dblk + dblk->size;
    mem = (char*)dblk + sizeof(dbheader);
    off = (BLOCK_IDX*)mem;
    start = end - used + (BLOCK_IDX_SIZE * nlines);

    eol = (lnum == 1) ? end : &mem[off[lnum-2]];
    pos = &mem[off[lnum-1]];
    len = eol - pos;

    if(lnum < nlines) {
        memmove(start + len, start, pos - start);
        memmove(&off[lnum-1], &off[lnum], BLOCK_IDX_SIZE * (nlines - lnum));

        for(i=lnum-1; i < nlines-1; i++) {
            off[i] += len;
        }
    }

    dblk->used -= (len + BLOCK_IDX_SIZE);
    dblk->nlines--;
    return(BLOCK_OK);
}

/*
 * Get specified line and copy into the buffer.  Enough space should
 * be allocated for this before it is passed else it could cause
 * a crash.
 */

int dblk_getline(dbheader *dblk, int lnum, dstring *ln)
{
    BLOCK_IDX *off;
    char   *end;
    char   *mem;
    uint   len;
    int    nlines;

    nlines  = dblk->nlines;

    dstring_reset(ln);

    if(lnum > nlines) {
        return(BLOCK_BAD_IDX);
    }

    end = (char*)dblk + dblk->size;
    mem = (char*)dblk + sizeof(dbheader);
    off = (BLOCK_IDX*)mem;

    if(lnum == 1) {
        len = (end-mem) - off[0];
    } else {
        len = off[lnum-2] - off[lnum-1];
    }

    dstring_insert_raw(ln, &mem[off[lnum-1]], len);

    return(BLOCK_OK);
}

inline int dblk_get_line_len(dbheader *dblk, int lnum)
{
    char  *end;
    char  *mem;
    BLOCK_IDX *off;

    end = (char*)dblk + dblk->size;
    mem = (char*)dblk + sizeof(dbheader);
    off = (BLOCK_IDX*)mem;

    if(lnum == 1)
    {
        return((end-mem) - off[0]);
    }

    return(off[lnum-2] - off[lnum-1]);
}

inline int dblk_get_len_from_point(dbheader *dblk, int lnum)
{
    char  *end;
    char  *mem;
    BLOCK_IDX *off;

    end = (char*)dblk + dblk->size;
    mem = (char*)dblk + sizeof(dbheader);
    off = (BLOCK_IDX*)mem;

    return(off[lnum-1] - off[dblk->nlines - 1]);
}

inline int dblk_avail(dbheader *dblk)
{
    return(dblk->size - sizeof(dbheader) - dblk->used);
}

inline int dblk_get_free(dbheader *dbh)
{
    return(dbh->size - sizeof(dbheader) - dbh->used);
}


