#ifndef _DATABLOCK_H
#define _DATABLOCK_H

#include <libced/common.h>
#include <libced/tools/dstring.h>

#include "block.h"

#define BLOCK_PAGE_SIZE 4096
#define BLOCK_IDX uint
#define BLOCK_IDX_SIZE sizeof(BLOCK_IDX)

typedef struct datablk_data   dbdat;
typedef struct datablk_header dbheader;

struct datablk_header
{
  char     blktype;  /* type of block, data or pointer */
  UINT     used;     /* how much space used */
  UINT     pages;    /* number of pages big */
  UINT     nlines;   /* number of lines */
  UINT     size;     /* size of block */
};

struct datablk_data
{
  BLOCK_IDX *offset;
  char      *data;
};

dbheader*   dblk_new(void* mem, int pages);
int         dblk_insert(dbheader *dblk, int atline, dstring *ln);
int         dblk_delete(dbheader *dblk, int lnum);
int         dblk_putline(dbheader *dblk, int atline, dstring *ln);
int         dblk_getline(dbheader *dblk, int lnum, dstring *ln);
inline int  dblk_avail(dbheader *dblk);

int dblk_get_free(dbheader *dbh);

#endif /* _DATABLOCK_H */

