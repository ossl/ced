/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999, 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *
 *  Last Updated
 *
 *  To Do
 *
 */

/********************************************************************************/

#include <math.h>
#include <stdlib.h>

#include <libced/tools/dstring.h>
#include <libced/tools/structs.h>
#include <libced/tools/debug.h>

#include <libced/utils/utils.h>
#include <libced/vmem/vmem.h>

#include "datablock.h"
#include "ptrblock.h"
#include "vmlbuf.h"

/********************************************************************************/

#define LEFT   1
#define RIGHT  2

/********************************************************************************/

static ulong  accumulate_block(block*);
static block *new_datablk(vmlbuf *vbuf, int pages);
static block *new_ptrblk(vmlbuf *vbuf);

/********************************************************************************/

int vmlbuf_del_line_2(vmlbuf *vbuf, ULONG lnum);
int vmlbuf_ins_line_2(vmlbuf *vbuf, ULONG lnum, dstring *ln);
int vmlbuf_get_line_2(vmlbuf *vbuf, ULONG lnum, dstring *ln);
int vmlbuf_put_line_2(vmlbuf *vbuf, ULONG lnum, dstring *ln);

/********************************************************************************
 * Create a new data block.  A data block is tagged with the character
 * 'd' to indicate its type.  When reading and writing data from a
 * data block, the data area itself is cast to type dbheader.
 ********************************************************************************/

block * new_datablk(vmlbuf *vbuf, int pages)
{
    vmem     *vm;
    block    *blk;

    vm = vbuf->vm;

    if((blk = vm_new_block(vm, pages)) == NULL)
    {
        abort();
    }

    dblk_new((dbheader*)blk->data, pages);

    return(blk);
}

/*
 * Create a new pointer block.  A pointer block is tagged with the
 * character 'p' to indicate its type.  When reading and writing data
 * from a pointer block, the data area itself is cast to type pbheader.
 */

block * new_ptrblk(vmlbuf *vbuf)
{
    vmem    *vm;
    block   *blk;

    vm = vbuf->vm;
    blk = vm_new_block(vm, 1);
    if(!blk) abort();
    pblk_new((pbheader*)blk->data);
    return(blk);
}



/********************************************************************************
 * A temporary hack until the real functions are fixed to take line numbers
 * starting from 0 rather than 1.
 ********************************************************************************/

int vmlbuf_del_line(vmlbuf* vbuf, long lnum) {
    vmlbuf_del_line_2(vbuf, lnum + 1);
}

void vmlbuf_ins_line(vmlbuf* vbuf, long lnum, dstring *dstr) {
    vmlbuf_ins_line_2(vbuf, lnum + 1, dstr);
}

int vmlbuf_get_line(vmlbuf* vbuf, long lnum, dstring *dstr) {
    return(vmlbuf_get_line_2(vbuf, lnum + 1, dstr));
}

void vmlbuf_put_line(vmlbuf* vbuf, long lnum, dstring *dstr) {
    vmlbuf_put_line_2(vbuf, lnum + 1, dstr);
}

/*******************************************************************************************
 * Delete line at the specified line number.  The datablock with the
 * line is first located, with the route being stored in a stack.
 * 
 * If the datablock contains more than 1 line, then the line is simply
 * deleted.  Update is set to the datablock for updating the ptr blocks
 * during the traversal.
 * 
 * If the datablock contains only the line to be deleted, delete the line
 * and delete the block.
 * 
 * LOOP
 *   Go through the stack.
 *   
 *   If the pointerblock with the pointer to be deleted is exactly half
 *   full, check siblings for pointers that it may borrow it keep the
 *   pointer block half full.  If both siblings are only half full too,
 *   then combine them and create one node.
 * 
 *   hmm..
 * 
 * END LOOP
 *******************************************************************************************/

int vmlbuf_del_line_2(vmlbuf* vbuf, ULONG lnum)
{
    block      *update;
    block      *delete;
    block      *currblk;
    block      *leftsib_blk;
    block      *rightsib_blk;
    pbheader   *sib_left;
    pbheader   *sib_right;
    route      *rts;
    rtelem     *parent;
    rtelem     *rte;
    int        parent_entry;
    pbheader   *parent_ptrblk;
    pbheader   *pbh;
    dbheader   *dbh;
    ptrelem    pelem;
    int        offset;
    int        sl_nptrs = 0;
    int        sr_nptrs = 0;
    int        sibused = 0;
    vmem       *vm;

    TRACE(D_MSG, "vmlbuf::delete line: %lu [nlines: %lu]", lnum, vbuf->nlines);

    if(lnum == 0 || lnum > vbuf->nlines)
    {
        TRACE(D_MSG, "vmlbuf::delete line: line out of range");

        return(0);
    }

    // Before a line can be deleted, the route to the datablock containing
    // the line must be established.  A stack is filled with the route defined
    // by the pointer blocks.

    vm = vbuf->vm;
    rts = vbuf->rt;
    route_trace(rts, vbuf, lnum);
    rte = rts->last;
    currblk = rte->blk;

    // Delete line from data block..
    // First find out offset in the datablock, ie the actual line
    // number in the datablock, then delete it.

    offset = rte->entry;
    dbh = (dbheader*)currblk->data;
    dblk_delete(dbh, offset);

    // If there are still lines in the data block, then the pointer blocks
    // enroute simply need updating to reflect the change in the number of
    // lines.  If however the datablock is now empty, then the datablock
    // itself must be deleted from the tree.

    if(dbh->nlines > 0)
    {
        TRACE(D_MSG, "vmlbuf::delete: number of lines in datablock is more than 0");

        vm_set_dirty(vm, currblk);

        update = currblk;  // want to update info about currblk
        delete = NULL;     // dont want to delete anything

    }
    else
    {
        TRACE(D_MSG, "vmlbuf::delete: datablock is empty");

        if(vbuf->nlines > 1)
        {
            update = NULL;     // dont want to update anything about currblk
            delete = currblk;  // want to delete currblk
        }
        else
        {
            TRACE(D_MSG, "vmlbuf::delete: keeping empty datablock");

            update = currblk;
            delete = NULL;
        }
    }

    // Propergate back up the stack, making changes to the pointerblocks
    // enroute according to what is set (update/delete).  If the datablock
    // was not made empty, then no block or pointer deletion is necessary,
    // just updates.

    rte = rte->prev;

    while(rte)
    {
        currblk = rte->blk;
        pbh = (pbheader*)currblk->data;

        if(update)
        {
            pelem.nlines = accumulate_block(update);
            pelem.blk_num = update->num;
            pelem.blk_pages = update->pages;

            pblk_putelem(pbh, rte->entry, pelem);

            vm_set_dirty(vm, currblk);

            update = currblk;

            TRACE(D_MSG, "vmlbuf::delete: updated ptrblk (%lu) elem (%d) with blk (%lu) with (%lu) lines", currblk->num, rte->entry, pelem.blk_num, pelem.nlines);

            rte = rte->prev;
        }

        if(delete)
        {
            TRACE(D_MSG, "vmlbuf::delete: deleting blk: %lu, from ptrblk: %lu, at elem: %d", delete->num, currblk->num, rte->entry);

            pblk_delete(pbh, rte->entry);

            vm_set_dirty(vm, currblk);
            vm_free_block(vm, delete);

            // Calculate how many elements is a half full pointerblock.

            offset = (pbh->maxptrs*0.5);

            // If the pointerblock is not the root, then it should never get
            // less than half full.  If it does, a pointer must be stolen from
            // its immediate siblings to make up the loss.

            if(rte)
            {
                if(pbh->nptrs == (offset-1))
                {
                    TRACE(D_MSG, "vmlbuf::delete: pointer block is only half full");
                    TRACE(D_MSG, "vmlbuf::delete: half entries is %d, block had %d", offset, (pbh->nptrs+1));

                    // Get left and right siblings :-
                    // First get the parent pointer block and the entry from it
                    // which originally pointed to the original pointer block.

                    rte = rte->prev;

                    if(!rte)
                    {
                        break;
                    }

                    parent = rte;
                    parent_ptrblk = (pbheader*)parent->blk->data;
                    parent_entry = parent->entry;

                    TRACE(D_MSG, "vmlbuf::delete: not root block, continuing balance correction");
                    TRACE(D_MSG, "vmlbuf::delete: parent number is (%lu)", parent->blk->num);
                    TRACE(D_MSG, "vmlbuf::delete: parent contains (%d) pointers", parent_ptrblk->nptrs);
                    TRACE(D_MSG, "vmlbuf::delete: looking at entry number (%d)", parent_entry);

                    leftsib_blk = NULL;
                    rightsib_blk = NULL;
                    sib_left = NULL;
                    sib_right = NULL;
                    sl_nptrs = 0;
                    sr_nptrs = 0;

                    // If the entry is not the first, then grab the sibling left
                    // of the entry.  If the entry is not the last, also grab the
                    // right sibling of the entry.

                    if(parent_entry > 0)
                    {
                        TRACE(D_MSG, "vmlbuf::delete: fetching left sibling");

                        pblk_getelem(parent_ptrblk, parent_entry-1, &pelem);
                        leftsib_blk = vm_get_block(vm, pelem.blk_num, 1);
                        sib_left = (pbheader*)leftsib_blk->data;
                        sl_nptrs = sib_left->nptrs;
                    }

                    if(parent_entry < (parent_ptrblk->nptrs-1))
                    {
                        TRACE(D_MSG, "vmlbuf::delete: fetching right sibling");

                        pblk_getelem(parent_ptrblk, parent_entry+1, &pelem);
                        rightsib_blk = vm_get_block(vm, pelem.blk_num, 1);
                        sib_right = (pbheader*)rightsib_blk->data;
                        sr_nptrs = sib_right->nptrs;
                    }

                    // If there are NO siblings, then the block becomes the
                    // root block and the process is done.

                    if(sl_nptrs == 0 && sr_nptrs == 0)
                    {
                        TRACE(D_MSG, "vmlbuf::delete: There are no siblings, block becomes root");

//                        vm_free_block(vm, vbuf->root);

                //        vbuf->root = currblk;
                        vbuf->root_num = currblk->num;
                        vbuf->root_pages = currblk->pages;

                        vmlbuf_update_mblock(vbuf);

                        break;
                    }

                    // If neither sibling is only exactly half full, borrow from
                    // the sibling ptrblock with most entries.  This helps keep
                    // things balanced.

                    TRACE(D_MSG, "vmlbuf::delete: left sibling contains %d pointers", sl_nptrs);
                    TRACE(D_MSG, "vmlbuf::delete: right sibling contains %d pointers", sr_nptrs);

                    if((sl_nptrs != offset) && (sr_nptrs != offset))
                    {
                        TRACE(D_MSG, "vmlbuf::delete: neither sibling is half full, so steal from most full");

                        if(sl_nptrs > sr_nptrs)
                        {
                            pblk_getelem(sib_left, sl_nptrs-1, &pelem);
                            pblk_insert(pbh, 0, pelem);
                            pblk_delete(sib_left, sl_nptrs-1);

                            sibused = LEFT;
                        }
                        else
                        {
                            pblk_getelem(sib_right, 0, &pelem);
                            pblk_insert(pbh, pbh->nptrs, pelem);
                            pblk_delete(sib_right, 0);

                            sibused = RIGHT;
                        }

                        update = currblk;
                        delete = NULL;
                    }

                    // if it is only half full, then combine the pointerblocks.

                    else
                    {
                        TRACE(D_MSG, "vmlbuf::delete: combining pointer blocks");

                        // If the left sibling is only half full, append the pointers
                        // onto the end of it, else if the right sibling is only half
                        // full, prepend the pointers onto the beginning of it.

                        if(sl_nptrs == offset)
                        {
                            pblk_append(sib_left, pbh);
                            sibused = LEFT;
                        }
                        else if(sr_nptrs == offset)
                        {
                            pblk_prepend(sib_right, pbh);
                            sibused = RIGHT;
                        }

                        // Don't want to update anything next time round, the siblings
                        // are updated below.  Do want to delete the empty pointer block.

                        update = NULL;
                        delete = currblk;
                    }

                    // If there were changes made to one of the sibling blocks then
                    // reflect the change in the parent block.  There is never a need
                    // for both siblings to be updated.

                    if(sibused == LEFT)
                    {
                        pelem.nlines = accumulate_block(leftsib_blk);
                        pelem.blk_num = leftsib_blk->num;
                        pelem.blk_pages = leftsib_blk->pages;

                        pblk_putelem(parent_ptrblk, parent_entry-1, pelem);
                    }
                    else if(sibused == RIGHT)
                    {
                        pelem.nlines = accumulate_block(rightsib_blk);
                        pelem.blk_num = rightsib_blk->num;
                        pelem.blk_pages = rightsib_blk->pages;

                        pblk_putelem(parent_ptrblk, parent_entry+1, pelem);
                    }

                    if(leftsib_blk)
                    {
                        vm_put_block(vm, leftsib_blk);
                    }

                    if(rightsib_blk)
                    {
                        vm_put_block(vm, rightsib_blk);
                    }
                }

                // If the pointer block is more than half full, the pointer has
                // already been deleted, now just set update to propergate the
                // change in number of lines back up the tree.  Nothing needs to
                // be deleted.

                else
                {
                    rte = rte->prev;
                    delete = NULL;
                    update = currblk;
                }   // endif pointer block half full
            }       // endif pointer block is root block
        }           // endif delete
    }               // end while not at top of route

    // Decrement the number of lines in the buffer, done deletion.

    vbuf->nlines--;

    vmlbuf_set_dirty(vbuf, 1);

    return(1);
}

/*******************************************************************************************
 * Insert line at the specified line number.  The datablock with the
 * line previous to the line being inserted at is first located.  The
 * route to the datablock is stored in a stack.
 *
 * If the datablock has space free, then insert into that block,
 * else a new one is created and all the lines from the old block that
 * proceeds lnum are moved to the new block.
 *
 * If there is now room in the original found datablock, then add
 * the new line to it, else put it in the new datablock.
 *
 * "Update" is set to the block that has changed so that changes can
 * be reflected in the pointer blocks on the way back up the tree.
 * "Passback" is set to the new block if it was created.
 *
 * LOOP
 *   Go through the stack.
 *   Change "Update" in the previous pointer block.
 *   Insert "Passback" in the previous pointer block.
 *
 *   If the pointer block is full, it is split in half, the latter half
 *   being put into a new block.  "Passback" is then set to the new
 *   block.
 * END LOOP
 *******************************************************************************************/

int vmlbuf_ins_line_2(vmlbuf *vbuf, ULONG lnum, dstring *ln)
{
    block      *update;
    block      *passback;
    block      *currblk;
    block      *newblk;
    pbheader   *newpbh;
    dbheader   *newdbh;
    pbheader   *pbh;
    dbheader   *dbh;
    ptrelem    pelem;
    ptrelem    pelem2;
    int        i;
    int        np;
    int        p;
    int        len;
    int        bfree;
    int        offset;
    int        nlines;
    route      *rts;
    rtelem     *rte;
    dstring    *lbuf;
    vmem       *vm;
    dstring    *l_ins;
    int        blen;
    int        bsize;

    ////////////////////////////////////////////////////////////////////////////////

    int max_datablock_size = BLOCK_PAGE_SIZE - sizeof(dbheader);

    ////////////////////////////////////////////////////////////////////////////////

    TRACE(D_MSG, "vmlbuf::insert: line number: %lu", lnum);

    ////////////////////////////////////////////////////////////////////////////////
    // FIXME - Line numbers start from 1, this should be changed to 0 and this
    // hack below should be removed.
    ////////////////////////////////////////////////////////////////////////////////

    if(lnum == 0 || (lnum - 1) > vbuf->nlines)
    {
        TRACE(D_MSG, "vmlbuf::insert line: line out of range");

        return(0);
    }

    ////////////////////////////////////////////////////////////////////////////////

    len = dstring_get_length(ln);

    // Work out the amount of space the line would take up inside
    // the block.  This is the text length plus the index size.

    blen = len + BLOCK_IDX_SIZE;

	// We are using the vmem and route properties alot, so localise them.

    vm = vbuf->vm;
    rts = vbuf->rt;

    // Before a line can be inserted, the route to the datablock containing
    // the line previous must be established.  A stack is filled with the
    // route defined by the pointer blocks.

    route_trace(rts, vbuf, lnum - 1);
	
	// Get the last element and block in the root stack.
	
    rte = rts->last;
    currblk = rte->blk;

    // The following algorithm inserts the actual line into the tree.
    //
    // If there is no room in the datablock, a new one is created.  The
    // pointer to this new block is passed back to be inserted into the
    // preceding pointer block.

    dbh = (dbheader*)currblk->data;
    bfree = dblk_get_free(dbh);
    offset = rte->entry + 1;
    nlines = dbh->nlines;

    TRACE(D_MSG, "vmlbuf::insert: =========================================================================");
    TRACE(D_MSG, "vmlbuf::insert: lnum: %d, length: %d", lnum, dstring_get_length(ln));
    TRACE(D_MSG, "vmlbuf::insert: datablock num: %d, offset: %d, nlines: %d, free space: %d", currblk->num, offset, nlines, bfree);

    //////////////////////////////////////////////////////////////////////////////////////////////
    // MAS - FIXME - URGENT - Should really check out the next datablock too if there is one...
    //////////////////////////////////////////////////////////////////////////////////////////////

    if(bfree < blen)
    {
        // Define some variables for this bit.  This will be moved
        // because it doesn't look very neat !

        int mvpages = 0;
        int mvspace = 0;

        TRACE(D_MSG, "vmlbuf::insert: insufficient room in datablock [req: %d, avail: %d]", blen, bfree);

        // We need to work out several problems here.  Firstly, is it
        // possible to make the new line fit into the current datablock
        // by moving lines out of it ?

        bsize = dbh->size - sizeof(dbheader);

        // First lets find out how much space the lines after the required
        // insert point takes up.

        TRACE(D_MSG, "vmlbuf::insert: calculating move space");

        for(i=offset;i<=nlines;i++)
        {
            mvspace += (dblk_get_line_len(dbh, i) + BLOCK_IDX_SIZE);

            TRACE(D_MSG, "vmlbuf::insert: available move space [%d]: %d", i, mvspace);
        }

        // If there is enough room in the datablock after the insert point, then
        // we should create a new datablock with enough pages to hold the new
        // lines, move the lines, and then insert the new line into the original
        // block.

        if(blen <= (bfree + mvspace))
        {
            TRACE(D_MSG, "vmlbuf::insert: there is enough room in the datablock after insertion point");

            mvpages = floor((double)mvspace / (double)max_datablock_size) + 1;

            TRACE(D_MSG, "vmlbuf::insert: creating a new block of %d pages", mvpages);

            newblk = new_datablk(vbuf, mvpages);
            newdbh = (dbheader*)newblk->data;

            // Move the lines, using lbuf as a buffer.  We should write a routine
            // to do this, or better still, a direct read/write block to block
            // routine to speed things up.

            lbuf = dstring_new();

            TRACE(D_MSG, "vmlbuf::insert: moving lines into new block");

            for(i=offset;i<=nlines;i++)
            {
                dblk_getline(dbh, offset, lbuf);
                dblk_delete(dbh, offset);
                dblk_insert(newdbh, (i-offset)+1, lbuf);
            }

            dstring_free(lbuf);

            // Insert the line into the original datablock.

            dblk_insert(dbh, offset, ln);
        }

        // If the line cannot or did not fit into the original block, then we
        // need to create a new block to put it into, along with any lines that
        // need moving from the original block to keep the line flow contiguous.

        else
        {
            int status;

            TRACE(D_MSG, "vmlbuf::insert: there is NOT enough room in the datablock");

            mvpages = floor(((double)mvspace + (double)blen) / (double)max_datablock_size) + 1;

            newblk = new_datablk(vbuf, mvpages);
            newdbh = (dbheader*)newblk->data;

            TRACE(D_MSG, "vmlbuf::insert: created a new block [%d] of %d pages", newblk->num, mvpages);

            // Insert the line into the beginning of the new datablock.

            TRACE(D_MSG, "vmlbuf::insert: inserting new line into beginning of new block");

            if((status = dblk_insert(newdbh, 1, ln)) != BLOCK_OK)
            {
                TRACE(D_ERROR, "vmlbuf::insert: datablock insert failed !");

                switch(status)
                {
                    case BLOCK_FULL    : TRACE(D_FATAL, "vmlbuf::insert: BLOCK FULL"); break;
                    case BLOCK_BAD_IDX : TRACE(D_FATAL, "vmlbuf::insert: BAD INDEX"); break;
                    default            : TRACE(D_FATAL, "vmlbuf::insert: Unknown return code");
                }
            }

            if(accumulate_block(newblk) == 0)
            {
                TRACE(D_FATAL, "vmlbuf::insert: seriously weird");
            }

            // Move the lines, using lbuf as a buffer.  We should write a routine
            // to do this, or better still, a direct read/write block to block
            // routine to speed things up.

            lbuf = dstring_new();

            TRACE(D_MSG, "vmlbuf::insert: moving remaining lines into new block");

            for(i=offset;i<=nlines;i++)
            {
                dblk_getline(dbh, offset, lbuf);
                dblk_delete(dbh, offset);
                dblk_insert(newdbh, (i-offset)+2, lbuf);
            }

            TRACE(D_MSG, "vmlbuf::insert: moved %d lines", i-offset);

            dstring_free(lbuf);
        }

        if(accumulate_block(currblk) == 0)
        {
            TRACE(D_MSG, "vmlbuf::insert: original block now empty, replace it");

            update = newblk;
            passback = NULL;

            vm_free_block(vm, currblk);

            currblk = NULL;

            if(accumulate_block(update) == 0)
            {
                TRACE(D_FATAL, "vmlbuf::insert: what the fuck ??");
            }
        }
        else
        {
            update = currblk;
            passback = newblk;
        }

        TRACE(D_MSG, "vmlbuf::insert: done.");
        TRACE(D_MSG, "vmlbuf::insert: =========================================================================");
    }

    // If the datablock has room in it, then the line is inserted into the
    // correct position in that datablock and all pointer blocks en-route
    // updated to reflect the incremented number of lines in that path.

    else
    {
        dblk_insert(dbh, offset, ln);
        update = currblk;
        passback = NULL;
    }

    ///////////////////////////////////////////////////////////////////////
     

    if(currblk) { // FIXME
        vm_set_dirty(vm, currblk);
    }

    ///////////////////////////////////////////////////////////////////////

    while((rte = rte->prev))
    {
        currblk = rte->blk;
        pbh = (pbheader*)currblk->data;

        if(update)
        {
            pelem.nlines = accumulate_block(update);
            pelem.blk_num = update->num;
            pelem.blk_pages = update->pages;

            if(pelem.nlines == 0)
            {
                TRACE(D_FATAL, "vmlbuf::insert: a pointer should never point to zero lines");
            }

            pblk_putelem(pbh, rte->entry, pelem);
        }

        ////////////////////////////////////////////////////////////////////////////
        // If there is still a block to pass back :-
        ////////////////////////////////////////////////////////////////////////////
        // If the pointer block is full, split it in half.  Create a new block and
        // take the last half from the full block and put it in the first half of
        // the new block.  The new block is then passed back to the previous
        // pointer block, splitting it if neccessary to make room, and so on.
        //

        if(passback)
        {
            pelem.nlines = accumulate_block(passback);
            pelem.blk_num = passback->num;
            pelem.blk_pages = passback->pages;

            if(pbh->nptrs == pbh->maxptrs)
            {
                TRACE(D_MSG, "vmlbuf::insert: pointer block is full");
                TRACE(D_MSG, "vmlbuf::insert: pointer info: BLKNUM %lu", currblk->num);
                TRACE(D_MSG, "vmlbuf::insert: nptrs: %d maxptrs: %d", pbh->nptrs, pbh->maxptrs);

                newblk = new_ptrblk(vbuf);
                newpbh = (pbheader*)newblk->data;
                offset = pbh->maxptrs/2;
                np = 0;

                for(p=offset; p<pbh->maxptrs; p++)
                {
                    pblk_getelem(pbh, offset, &pelem2);
                    pblk_delete(pbh, offset);
                    pblk_insert(newpbh, np, pelem2);

                    np++;
                }

                /*
                 * After the pointer block is split, the pointer must either
                 * be inserted into the new block or original block depending
                 * on its value.
                 */

                if(rte->entry < offset)
                {
                    pblk_insert(pbh, rte->entry+1, pelem);
                }
                else
                {
                    pblk_insert(newpbh, (rte->entry-(offset)+1), pelem);
                }

                vm_put_block(vm, passback);

                update = currblk;
                passback = newblk;
            }

            // If there is room in the pointer block then simply insert the
            // pointer into the correct position.  Nothing else needs to be
            // passed back.

            else
            {
                pblk_insert(pbh, rte->entry+1, pelem);
                vm_put_block(vm, passback);
                passback = NULL;
            }
        }

        vm_set_dirty(vm, currblk);

        update = currblk;
    }

    // If the top of the stack is reached (the root node) and it is full,
    // then a new root node is needed.  Create a new root node, make the
    // first entry a pointer to the original tree, and the second point
    // to the tree passed back.

    if(passback != NULL)
    {
        newblk = new_ptrblk(vbuf);

        pelem.nlines = accumulate_block(update);
        pelem.blk_num = update->num;
        pelem.blk_pages = update->pages;

        pblk_insert((pbheader*)newblk->data, 0, pelem);

        pelem.nlines = accumulate_block(passback);
        pelem.blk_num = passback->num;
        pelem.blk_pages = passback->pages;

        pblk_insert((pbheader*)newblk->data, 1, pelem);
	
//        vbuf->root = newblk;
        vbuf->root_num = newblk->num;
        vbuf->root_pages = newblk->pages;

        vm_put_block(vm, passback);

        vmlbuf_update_mblock(vbuf);
    }

    vbuf->nlines++;

    vmlbuf_set_dirty(vbuf, 1);

    return(1); // for now
}

int vmlbuf_put_line_2(vmlbuf *vbuf, ULONG lnum, dstring *ln)
{
/*
    block      *currblk;
    dbheader   *dbh;
    int        offset;
    route      *rts;
    rtelem     *rte;
    line       *lbuf;
    uint       old_len;
    uint       new_len;
*/

    if(lnum == 0)
    {
        return(0);
    }

/*
    if(!vmlbuf->use_swap)
    {
        dstring_reset(vbuf->lines2.data[lnum-1]);
        dstring_insert(vbuf->lines2.data[lnum-1], ln);

        vmlbuf_set_dirty(vbuf, 1);

        return(1);
    }
*/

    TRACE(D_MSG, "vmlbuf::putline: line number: %lu", lnum);
    TRACE(D_MSG, "vmlbuf::putline: START =====================================================");

    vmlbuf_del_line(vbuf, lnum);
    vmlbuf_ins_line(vbuf, lnum, ln);

    TRACE(D_MSG, "vmlbuf::putline: END =======================================================");

/*
	///////////////////////////////////////////////////////////////////////////
	// TODO - we want this routine to directly update the line in a datablock
	//        IF it is the only line in that datablock.  Otherwise we just
	//        call the usual delete/insert routines.
	//
	// Also - Should experiment to see if we still get the same effect of the
	//        swap file being bigger if we ONLY use the insert/delete routines
	//	      when neccessary (i.e. when a line is now too big to fit in the
	//        datablock that is currently lives in).
	///////////////////////////////////////////////////////////////////////////
	

    rts = vbuf->rt;
    route_trace(rts, vbuf, lnum);
    rte = rts->last;
    currblk = rte->blk;
    dbh = (dbheader*)currblk->data;
    offset = rte->entry;

    lbuf = dstring_new();

    dblk_getline(dbh, offset, lbuf);

    old_len = dstring_get_length(lbuf);
    new_len = dstring_get_length(ln);

    dstring_free(lbuf);
    
    if((new_len <= old_len) || ((dblk_avail(dbh)-2) >= new_len - old_len)) {
        dblk_delete(dbh, offset);
        dblk_insert(dbh, offset, ln);
        vm_set_dirty(vbuf->vm, currblk);
    } else {
        vmlbuf_del_line(vbuf, lnum);
        vmlbuf_ins_line(vbuf, lnum, ln);
    }
*/
    return(1);
}

int vmlbuf_get_line_2(vmlbuf* vbuf, ULONG lnum, dstring *ln)
{
    block      *currblk;
    dbheader   *dbh;
    route      *rts;
    rtelem     *rte;

    TRACE(D_MSG, "vmlbuf::getline: getting line (%lu)", lnum);

    if(lnum == 0 || lnum > vbuf->nlines)
    {
        TRACE(D_MSG, "vmlbuf::getline: line out of range.", lnum);

        dstring_reset(ln);

        return(0);
    }

    rts = vbuf->rt;
    route_trace(rts, vbuf, lnum);
    rte = rts->last;
    currblk = rte->blk;

    dbh = (dbheader*)currblk->data;
    dblk_getline(dbh, rte->entry, ln);

    return(1);
}

/****************************************************************************
 * Accumulates the total number of lines that a pointerblock points
 * to or if its a datablock, the number of actual lines in the block.
 ****************************************************************************/

ULONG accumulate_block(block *blk)
{
    dbheader*  dbh;
    pbheader*  pbh;
    int        i;
    ULONG      ltot = 0;

    pbh = (pbheader*)blk->data;
    dbh = (dbheader*)blk->data;

    if(pbh->blktype == 'p') {
        for(i=0;i<pbh->nptrs;i++) {
            ltot += pbh->bptr[i].nlines;
        }
    } else {
        ltot = dbh->nlines;
    }

    return(ltot);
}

/****************************************************************************/
