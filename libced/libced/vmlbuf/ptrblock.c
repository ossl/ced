/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Provides functions to manipulate and query a pointerblock.
 *    Pointerblocks are a fixed size block of memory containing
 *    a header, which contains information about the block,
 *    and an array of pointers.
 *
 *  Last Updated
 *    8th February 2000
 *
 */

#include <string.h>
#include <libced/common.h>
#include "ptrblock.h"

/* temporary definition */

#define BLOCK_PAGE_SIZE 4096
//#define BLOCK_PAGE_SIZE 200

/*
 * Creates a new datablock by casting it onto the memory space specified.
 * The memory passed is normally that which was initialised as a vmem
 * block, so it can be swapped out to disk.
 */

pbheader* pblk_new(void *mem)
{
    pbheader* pblk;

    pblk = (pbheader*)mem;
    pblk->blktype = 'p';
    pblk->nptrs = 0;
    pblk->maxptrs = (BLOCK_PAGE_SIZE - sizeof(pbheader)) / sizeof(ptrelem);

    return(pblk);
}

/*
 * Insert a pointer into the pointerblock.  If the element number specified
 * is one more than the number of elements in the block, it is simply
 * appended, otherwise pointers are moved to make room for it.
 */

int pblk_insert(pbheader* pblk, int atelem, ptrelem elem)
{
    void* mem;
    int nptrs = pblk->nptrs;
    int maxptrs = pblk->maxptrs;
    int psize = sizeof(ptrelem);
    int start, end;

    if(nptrs == maxptrs) {
        return(BLOCK_BAD_IDX);
    }

    if(atelem == nptrs) {
        pblk->bptr[atelem] = elem;
        pblk->nptrs++;
        return(BLOCK_OK);
    }

    start = atelem * psize;
    end = (atelem+1) * psize;
    mem = (void*)&pblk->bptr;
    memmove(mem+end, mem+start, (nptrs-atelem)*psize);
    pblk->bptr[atelem] = elem;
    pblk->nptrs++;

    return(BLOCK_OK);
}

/*
 * Deletes a pointer from the pointerblock.  If there are pointers following
 * the pointer to be deleted, those pointers are moved back to overlap the
 * free'd space.
 */

int pblk_delete(pbheader* pblk, int elem)
{
    void* mem;
    int nptrs = pblk->nptrs;
    int psize = sizeof(ptrelem);
    int start;
    int end;

    if(elem >= nptrs) {
        return(BLOCK_BAD_IDX);
    }

    if((elem+1) == nptrs) {
        pblk->nptrs--;
        return(BLOCK_OK);
    }

    start = elem * psize;
    end = (elem+1) * psize;
    mem = (void*)&pblk->bptr;
    memmove(mem+start, mem+end, ((nptrs-elem)-1)*psize);
    pblk->nptrs--;

    return(BLOCK_OK);
}

/*
 * Put the line in the data block at the specified line, overwriting
 * whatever was there before.
 */

int pblk_putelem(pbheader* pblk, int atelem, ptrelem elem)
{
    if(atelem >= pblk->nptrs) {
        return(BLOCK_BAD_IDX);
    }

    pblk->bptr[atelem] = elem;

    return(BLOCK_OK);
}

/*
 * Get specified pointer and return it.  NULL is returned if the
 * element doesn't exist.  This is slightly inconsistent with
 * the usual returning of return codes...
 */

int pblk_getelem(pbheader* pblk, int elem, ptrelem *bptr)
{
  if(elem >= pblk->nptrs) {
      return(BLOCK_BAD_IDX);
  }

  bptr->nlines = pblk->bptr[elem].nlines;
  bptr->blk_num = pblk->bptr[elem].blk_num;
  bptr->blk_pages  = pblk->bptr[elem].blk_pages;

  return(BLOCK_OK);
}

/*
 * Takes a source and a destination pointerblock and moves the entries
 * from the source onto the end of the destination block.  The entries
 * in the source block are deleted.
 */

int pblk_append(pbheader* dest, pbheader *src)
{
    int   dest_nptrs = dest->nptrs;
    int   src_nptrs = src->nptrs;
    int   tot_nptrs = src_nptrs + dest_nptrs;
    int   psize = sizeof(ptrelem);
    void* src_mem = (void*)&src->bptr;
    void* dest_mem = (void*)&dest->bptr;
    int   amount = src_nptrs * psize;

    if(tot_nptrs > dest->maxptrs) {
        return(BLOCK_FULL);
    }

    memmove(dest_mem+(dest_nptrs*psize), src_mem, amount);
    memset(src_mem, '\0', amount);
    dest->nptrs += src->nptrs;
    src->nptrs = 0;

    return(BLOCK_OK);
}

/*
 * Takes a source and a destination pointerblock and moves the entries
 * from the source onto the start of the destination block.  The entries
 * in the source block are deleted.
 */

int pblk_prepend(pbheader *dest, pbheader *src)
{
    int   dest_nptrs = dest->nptrs;
    int   src_nptrs = src->nptrs;
    int   tot_nptrs = src_nptrs + dest_nptrs;
    int   psize = sizeof(ptrelem);
    void* src_mem = (void*)&src->bptr;
    void* dest_mem = (void*)&dest->bptr;
    int   amount = src_nptrs * psize;

    if(tot_nptrs > dest->maxptrs) {
        return(BLOCK_FULL);
    }

    memmove(dest_mem+amount, dest_mem, (dest_nptrs*psize));
    memmove(dest_mem, src_mem, amount);
    memset(src_mem, '\0', amount);
    dest->nptrs += src_nptrs;
    src->nptrs = 0;

    return(BLOCK_OK);
}

