#ifndef _PTRBLOCK_H
#define _PTRBLOCK_H

#include <libced/common.h>
#include "block.h"

typedef struct ptrblk_header pbheader;
typedef struct ptr_elem ptrelem;

struct ptr_elem
{
    USHORT  blk_pages;  /* pages in block */
    ULONG   blk_num;    /* block number */
    ULONG   nlines;     /* number of lines */
};

struct ptrblk_header
{
    char     blktype;   /* type of block, data or pointer */
    UINT     nptrs;     /* number of pointers */
    UINT     maxptrs;   /* max ptrs that can fit */
    ptrelem  bptr[1];   /* not 1 element, see appendix */
};

pbheader*   pblk_new(void* mem);
int         pblk_insert(pbheader* pblk, int atelem, ptrelem elem);
int         pblk_delete(pbheader* pblk, int elem);
int         pblk_putelem(pbheader* pblk, int atelem, ptrelem elem);
int         pblk_getelem(pbheader* pblk, int elem, ptrelem *bptr);
int         pblk_prepend(pbheader *dest, pbheader *src);
int         pblk_append(pbheader* dest, pbheader *src);


#endif /* _PTRBLOCK_H */

