/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Provides a tree routing system.  A route is a pathway that leads
 *    from the root pointer block to a datablock.  The route is cached
 *    and keeps blocks locked until a route is accessed that takes a
 *    different path.
 *
 *  Updates
 *    18th March 2000 - MAS - General fixes
 *    24th March 2003 - MAS - Modifications to handle multipage blocks
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include <libced/vmem/vmem.h>

#include "datablock.h"
#include "ptrblock.h"
#include "vmlbuf.h"
#include "route.h"

//#define DEBUG_ROUTE

inline static rtelem *rtelem_new(block *blk);
static void   rtelem_free(rtelem *re, vmlbuf *buff);

//////////////////////////////////////////////////////////////////////////////////////

ulong accum_block(block *blk)
{
    dbheader*  dbh;
    pbheader*  pbh;
    int        i;
    ULONG      ltot = 0;

    pbh = (pbheader*)blk->data;
    dbh = (dbheader*)blk->data;

    if(pbh->blktype == 'p')
    {
        for(i=0;i<pbh->nptrs;i++)
        {
            ltot += pbh->bptr[i].nlines;
        }
    }
    else
    {
        ltot = dbh->nlines;
    }

    return(ltot);
}

//////////////////////////////////////////////////////////////////////////////////////

route *route_new()
{
    route *rt;

    rt = (route*)calloc(1, sizeof(route));
    rt->head = NULL;
    rt->current = NULL;
    rt->last = NULL;

    return(rt);
}

//////////////////////////////////////////////////////////////////////////////////////

void route_free(route *rt, vmlbuf *buff)
{
    rtelem_free(rt->head, buff);
    free(rt);
}

//////////////////////////////////////////////////////////////////////////////////////

void route_trace(route *rt, vmlbuf *buff, ulong lnum)
{
    ptrelem   pelem;   /* point block element */
    pbheader  *pbh;    /* pointer block */
    rtelem    *tmp;    /* route element */
    rtelem    *re;     /* route element */
    rtelem    *head;   /* head route element */
    block     *blk;    /* block */
    vmem      *vm;     /* virtual mem */
    uint      rn;      /* root block number */
    uint      rp;      /* root block pages */
    uint      i;       /* count */
    ulong     nlines;  /* number of lines in buffer */
    ulong     ltot;    /* line accumulation */
    uint      flag;
    uint      found = 0;

    ltot = 0;
    vm = buff->vm;
    rn = buff->root_num;
    rp = buff->root_pages;
    nlines = buff->nlines;

//    printf("route::route_trace: lnum: %lu\n", lnum);

    // If there is a route already established, check the head of it
    // is the same as the root block, because a route should always
    // start with the current root block.  If it is not the same then
    // clear the route.

    if(rt->head)
    {
        blk = rt->head->blk;
        flag = 1;

        if(blk->num != rn)
        {
            rtelem_free(rt->head, buff);
            rt->head = NULL;
        }
    }

   // If there is no route, then create a new route with the first
   // element as the current root block.

    if(!rt->head)
    {
        blk = vm_get_block(vm, rn, rp);
        rt->head = rtelem_new(blk);
        flag = 0;
    }

    pbh = (pbheader*)blk->data;
    re = rt->head;
    head = rt->head;

    while(pbh->blktype == 'p')
    {
        for(i=0; i < pbh->nptrs; i++)
        {
            pblk_getelem(pbh, i, &pelem);
            ltot += pelem.nlines;

            if(lnum <= ltot)
            {
                ltot -= pelem.nlines;
                found = 1;

                if(flag)
                {
                    if(re->entry != i || pelem.blk_num != re->next->blk->num)
                    {
                        rtelem_free(re->next, buff);
                        re->next = NULL;
                        flag = 0;
                    }
                    else
                    {
#ifdef DEBUG_ROUTE
                        printf("tracert(d): route: %lu, %lu\n", pelem.blk_num, pelem.nlines);
#endif
                        re = re->next;
                        blk = re->blk;
                        pbh = (pbheader*)blk->data;
                    }
                }

                if(!flag)
                {
#ifdef DEBUG_ROUTE
                    printf("route::route_trace: route: %lu, %lu\n", pelem.blk_num, pelem.nlines);
#endif
                    blk = vm_get_block(buff->vm, pelem.blk_num, pelem.blk_pages);
                    pbh = (pbheader*)blk->data;

                    tmp = rtelem_new(blk);

                    re->next = tmp;
                    re->entry = i;
  
                    tmp->prev = re;

                    re = tmp;
                }

                break;
            }
        }

        if(!found)
        {
            printf("route::route_trace: [FATAL] no datablock\n");
            abort();
        }
    }

    re->entry = lnum - ltot;
    rt->head = head;
    rt->last = re;
    rt->current = head;

#ifdef DEBUG_ROUTE
    printf("tracert: datablock at blknum: %lu, lnum: %u\n", blk->num, re->entry);
#endif
//show_used(vm);
//show_free(vm);
}

/* rtelem stuff */

//////////////////////////////////////////////////////////////////////////////////////

inline rtelem *rtelem_new(block *blk)
{
    rtelem *re;

    re = (rtelem*)calloc(1, sizeof(rtelem));
    re->blk = blk;

    return(re);
}

//////////////////////////////////////////////////////////////////////////////////////

void rtelem_free(rtelem *re, vmlbuf *buff)
{
    rtelem *tmp;

    while(re)
    {
        vm_unlock_block(buff->vm, re->blk);
        tmp = re;
        re = tmp->next;
        free(tmp);
    }
}

//////////////////////////////////////////////////////////////////////////////////////

