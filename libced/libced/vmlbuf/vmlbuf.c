/*
 *  CED - The Editor
 *
 *  By Mark Simonetti
 *  Copyright (C) Mark Simonetti 1999, 2000
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *  Description
 *    Implements a variation of a B-TREE.  The tree consists of pointer
 *    blocks leading to data blocks at the leafs of the tree.
 *    Pointer blocks are split in half when they are full and a new node
 *    is created.  Two entries are created in the new node, the first
 *    pointing to the left side of the split, the second to the right
 *    side.  Each pointer contains the number of lines in a certain
 *    direction.
 *
 *  Last Updated
 *    17th November 2000
 *
 *  To Do
 *    o Improve space efficiency of insertion algorithm
 *    o Implement the buffer put routine, rather than use the ins/del
 *    o Break routines up a little into smaller tree handling routines.
 *      This would improve readability and which could lead to a more
 *      efficient implementation.
 *
 */

//#define DEBUG

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <math.h>

#include <libced/tools/dstring.h>
#include <libced/tools/structs.h>
#include <libced/tools/debug.h>

#include <libced/utils/utils.h>
#include <libced/vmem/vmem.h>

#include "datablock.h"
#include "ptrblock.h"
#include "vmlbuf.h"
#include "route.h"

// FIXME

extern void vmlbuf_set_dirty(vmlbuf *vbuf, int b);

void vmlbuf_set_dirty(vmlbuf *vbuf, int b)
{
    vbuf->is_dirty = b;
}

/***********************************************************************************
 * Create a new virtual line buffer using the passed in virtual memory object.
 ***********************************************************************************/

vmlbuf *vmlbuf_new(vmem *vm)
{
    vmlbuf *vbuf = (vmlbuf*)calloc(1, sizeof(vmlbuf));

    vbuf->vm = vm;
    vbuf->rt = route_new();
//    vbuf->fname = ?? vmem
    vbuf->root_num = 0;
    vbuf->root_pages = 1;
// <== ptr blocks are always 1 remember...
    vbuf->nlines = 0;

    return(vbuf);
}

/***********************************************************************************
 * Free the virtual line buffer object.
 ***********************************************************************************/

void vmlbuf_free(vmlbuf *vbuf)
{
    free(vbuf);
}

/***********************************************************************************/

void vmlbuf_update_mblock(vmlbuf *vbuf)
{
    mblock *mb;

    if(vbuf->mstblk != NULL)
    {
        mb = (mblock*)vbuf->mstblk->data;
        long_to_char(vbuf->root_num, mb->rootnum);
        long_to_char(vbuf->root_pages, mb->rootpages);
        vm_set_dirty(vbuf->vm, vbuf->mstblk);
    }
}
     
