#ifndef _VMLBUF_H
#define _VMLBUF_H

#include <libced/tools/dstring.h>
#include <libced/vmem/vmem.h>

/****************************************************************************/

typedef struct _vmlbuf vmlbuf;

/****************************************************************************/

typedef struct _route      route;
typedef struct _route_elem rtelem;
typedef struct _master_block mblock;

/****************************************************************************/

struct _master_block
{
  char  prg_id[7];    /* ced swap file id */
  char  version[10];  /* version of program */
  char  page_size[4]; /* block vm page size */
  char  rootnum[4];   /* root block number */
  char  rootpages[4]; /* NEW : root block number of pages */
  char  fname[256];   /* original file being edited */
  char  uname[128];   /* user that edited file */
  char  hname[128];   /* host-name file was edited on */
  char  inode[4];     /* inode of original file */
  char  mtime[4];     /* modification time */
  char  proc_id[4];   /* process id of ced when editing */
  long  byte_long;    /* byte order check of long */
  int   byte_int;     /* byte order check of int */
  short byte_short;   /* byte order check of short */
};

/****************************************************************************/

struct _route
{
    rtelem  *head;      /* head of list */
    rtelem  *last;      /* last in list */
    rtelem  *current;   /* current element in the queue */
};

/****************************************************************************/

struct _route_elem
{
    uint    entry;      /* entry that points to next block in route */
    block   *blk;       /* the block itself */
    rtelem  *next;      /* next route element */
    rtelem  *prev;      /* previous route element */
};

/****************************************************************************/

struct _vmlbuf
{
    vmem    *vm;            /* virtual memory area */
    route   *rt;            /* main route */
    char    fname[1024];    /* filename of swapfile */
    long    root_num;       /* block number of root block */
    long    root_pages;     /* number of pages of root block */
    long    nlines;         /* number of lines in buffer */
    int     is_dirty;       /* is the line buffer dirty */
    block   *mstblk;        /* master block */ // FIXME
};

/****************************************************************************/

vmlbuf*  vmlbuf_new();
void     vmlbuf_free(vmlbuf *vb);

/****************************************************************************/

int  vmlbuf_get_line(vmlbuf *vbuf, long lnum, dstring *dstr);
void vmlbuf_put_line(vmlbuf *vbuf, long lnum, dstring *dstr);
void vmlbuf_ins_line(vmlbuf *vbuf, long lnum, dstring *dstr);

/****************************************************************************/

void vmlbuf_update_mblock(vmlbuf *vbuf);



/****************************************************************************/

route *route_new    ( void);
void   route_free   ( route *rt, vmlbuf *vbuf );
void   route_trace  ( route *rt, vmlbuf *vbuf, ulong lnum );
void   route_update ( route *rt, vmlbuf *vbuf );

/****************************************************************************/

#endif /* _VMLBUF_H */
